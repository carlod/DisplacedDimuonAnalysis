#include "../DisplacedDimuonAnalysis/ExtrapolatorTool.h"
#include "TVector2.h"

trajectory ExtrapolatorTool::expressAtIP(xAOD::TrackParticle* mu,bool doSE){

  m_doSE = doSE;
  m_debug = false;

  //load param vector for TP
  TVector param = TVector(9);
  param[0] = mu->d0();
  param[1] = mu->z0();
  param[2] = mu->phi();
  param[3] = mu->theta();
  param[4] = mu->qOverP();;
  param[5] = mu->vx();
  param[6] = mu->vy();
  param[7] = mu->vz();
  param[8] = mu->charge();

  xAOD::ParametersCovMatrix_t cM = mu->definingParametersCovMatrix();
  mat55 perC;
  for (unsigned int i=0;i<5;i++){
    for (unsigned int j=0;j<5;j++){
      perC[i][j] = cM(i,j);
    }
  }

  //change from qOverP to just |p| for perigee parameters
  mat55 tmpJ;
  initMomentumJ(&param,&tmpJ);
  mat55 covMatPMod = tmpJ * perC * ROOT::Math::Transpose(tmpJ);
  param[4] = fabs(1/param[4]);

  //init track object and set the covariance matrix to the modified one
  track tmpMu = TPToTrack(mu);
  tmpMu.cov() = covMatPMod;

  if (m_debug){
    std::cout << "Printing modified perigee cov matrix at MS surface" << std::endl;
    covMatPMod.Print(std::cout);
    std::cout << std::endl;
  }

  //convert perigee param (d0,z0,phi,theta,q/p,vx,vy,vz) to cartesian param (px,py,pz,x,y,z) and perigee cov to cart cov
  mat65 pcJ;
  initPCJ(&param,&pcJ);
  mat66 covMatC = pcJ * covMatPMod * ROOT::Math::Transpose(pcJ);
  tmpMu.cov() = covMatC;

  if (m_debug){
    std::cout << "Printing cart cov matrix at MS surface" << std::endl;
    tmpMu.cov().Print(std::cout);
    std::cout << std::endl;
    std::cout << "at MS surface (px,py,pz,x,y,z): (" << tmpMu.p().x() << "," << tmpMu.p().y() << "," << tmpMu.p().z() << "," << tmpMu.x().x() << "," << tmpMu.x().y() << "," << tmpMu.x().z() << ")" << std::endl;
  }

  //transport param+cov from surface of MS to surface of ID
  track muAtCaloID = doCaloProp(tmpMu);
  
  if (m_debug){
    std::cout << "Printing cart cov matrix at ID surface" << std::endl;
    muAtCaloID.cov().Print(std::cout);
    std::cout << std::endl;
    std::cout << "at ID surface (px,py,pz,x,y,z): (" << muAtCaloID.p().x() << "," << muAtCaloID.p().y() << "," << muAtCaloID.p().z() << "," << muAtCaloID.x().x() << "," << muAtCaloID.x().y() << "," << muAtCaloID.x().z() << ")" << std::endl;
  }
  
//transport param+cov through ID
  track muAtIP = doIDProp(muAtCaloID);
  trajectory muTraj = trackToTP(muAtIP,mu);

  if (m_debug){
    std::cout << "Printing cart cov matrix at closest approach" << std::endl;
    muAtIP.cov().Print(std::cout);
    std::cout << std::endl;
    std::cout << "at closest approach (px,py,pz,x,y,z): (" << muAtIP.p().x() << "," << muAtIP.p().y() << "," << muAtIP.p().z() << "," << muAtIP.x().x() << "," << muAtIP.x().y() << "," << muAtIP.x().z() << ")" << std::endl;
  }

  //convert cartesian param (px,py,pz,x,y,z)+cov to perigee param (d0,z0,phi,theta,q/p,vx,vy,vz)+cov
  mat56 cpJ;
  initCPJ(muAtIP.x(),muAtIP.p(),&cpJ);
  mat55 covMatPFinal = cpJ * muAtIP.cov() * ROOT::Math::Transpose(cpJ);

  if (m_debug){
    std::cout << "Printing perigee cov matrix at closest approach" << std::endl;
    covMatPFinal.Print(std::cout);
    std::cout << std::endl;
  }

  std::vector<float> covVec;
  // elements in definingParametersCovMatrixVec should be : 0: sigma_d0^2,
  //                                                        1: sigma_d0_z0,  sigma_z0^2,
  //                                                        3: sigma_d0_phi, sigma_z0_phi, sigma_phi^2
  //                                                        6: sigma_d0_th,  sigma_z0_th,  sigma_phi_th, sigma_th^2
  //                                                       10: sigma_d0_qp,  sigma_z0_qp,  sigma_phi_qp, sigma_th_qp, sigma_qp^2
  covVec.push_back(covMatPFinal[0][0]);
  covVec.push_back(covMatPFinal[1][0]);
  covVec.push_back(covMatPFinal[1][1]);
  covVec.push_back(covMatPFinal[2][0]);
  covVec.push_back(covMatPFinal[2][1]);
  covVec.push_back(covMatPFinal[2][2]);
  covVec.push_back(covMatPFinal[3][0]);
  covVec.push_back(covMatPFinal[3][1]);
  covVec.push_back(covMatPFinal[3][2]);
  covVec.push_back(covMatPFinal[3][3]);
  covVec.push_back(covMatPFinal[4][0]);
  covVec.push_back(covMatPFinal[4][1]);
  covVec.push_back(covMatPFinal[4][2]);
  covVec.push_back(covMatPFinal[4][3]);
  covVec.push_back(covMatPFinal[4][4]);
  mu->setDefiningParametersCovMatrixVec(covVec);

  return muTraj;
}

track ExtrapolatorTool::TPToTrack(xAOD::TrackParticle* mu){

  mat66 tmpMat;

  return track(mu->pt(),mu->theta(),mu->phi(),mu->vx(),mu->vy(),mu->vz(), mu->charge(),&tmpMat,mu->d0(), mu->z0());
}

trajectory ExtrapolatorTool::trackToTP(track mu,xAOD::TrackParticle* MSTP){
  float charge = mu.q();
  float pMag = mu.p().Mag();
  MSTP->setDefiningParameters(mu.d0(),mu.z0(),mu.p().Phi(),mu.p().Theta(),charge/pMag);
  MSTP->setParametersOrigin(0.,0.,0.);
  
  return mu.traj();
}

track ExtrapolatorTool::doCaloProp(track mu){

  //do calo prop
  float rID(1000.),zID(3000.);
  int nMax(1000);
  /* 
  float t(0.);
  TVector3 xT = getXvecT(mu.x(),mu.dp(),t);
  for (int i=0;i<nMax;i++){
    xT = getXvecT(mu.x(),mu.dp(),t);
    t -= 10.;
    if (R(xT.x(),xT.y()) < rID && fabs(xT.z()) < zID) break;
  }
  */

  double theta = (m_doSE) ? -10. : 10.0/(mu.p().Pt()/(0.3 * 0.3));
  double bLocal = 0.25;
  TVector3 xT = mu.x();

  for (int i=1;i<nMax;i++){
    bLocal = 0.25;
    double Rlocal = ROC(mu.p().Pt(),bLocal);
    theta = (m_doSE) ? -10.0 : 10.0 / Rlocal;
    float xTmp = xT.x();
    float yTmp = xT.y(); 
    float zTmp = xT.z();
    float phiTmp = mu.dp().Phi();
    xT = (m_doSE) ? getXvecT(xT,mu.dp(),theta) : getXvecTheta(xT,mu.dp(),theta,Rlocal,-1.0*mu.q());

    double d = sqrt(pow(xT.x()-xTmp,2)+pow(xT.y()-yTmp,2));
    double localPhi = d / Rlocal;
    if (!m_doSE){
      if (mu.q() > 0) phiTmp = TVector2::Phi_mpi_pi(phiTmp - localPhi);
      else phiTmp = TVector2::Phi_mpi_pi(phiTmp + localPhi);
    }

    if (!m_doSE) {
       mu.dp().SetPhi(phiTmp);
       mu.p().SetPhi(phiTmp);
    }
    float pT = mu.p().Pt();
    float pZ = mu.p().z();
    xT.SetZ(zTmp  - d*(pZ/pT));

    if (xT.Perp() < rID && fabs(xT.z()) < zID) break;

  }


  double t(0.);
  //do covariance prop
  double px = mu.p().x();; 
  double py = mu.p().y(); 
  double pz = mu.p().z(); 
  double pt = mu.p().Perp(); 
  double pm = mu.p().Mag();
  double t0 = fabs(t); 
  double radLength = 54.0 * t0 / 3250.0;
  double scatterAng = (13.6/pm)*sqrt(radLength)*(1+0.038*log(radLength));
  double scatterSize = t0*tan(scatterAng);
  
  theta = atan(px*pz/py*pm);
  double theta2 = (theta < TMath::Pi()) ? theta + TMath::Pi() : theta - TMath::Pi();
  double secondDerMag = scatterSize * (py / pt) * sqrt(1 + px*px*pz*pz/(py*py*pm*pm));
  double goodTheta = (secondDerMag < 0) ? theta2 : theta;
  double deltaX = scatterSize * (cos(goodTheta) * (py/pt) + sin(goodTheta) * px * pz /( pm * pt));

  theta = atan(-1*py*pz/px*pm);
  theta2 = (theta < TMath::Pi()) ? theta + TMath::Pi() : theta - TMath::Pi();
  secondDerMag = scatterSize * (px / pt) * sqrt(1 + py*py*pz*pz/(px*px*pm*pm));
  goodTheta = (secondDerMag < 0) ? theta : theta2;
  double deltaY = scatterSize * (cos(goodTheta) * (-1*px/pt) + sin(goodTheta) * py * pz /( pm * pt));

  double deltaZ = scatterSize * pt / pm;

  //now find t1 corresponding to the altered momenta
  double t1 = sqrt(pow(deltaX+(px/pm)*t0,2)+pow(deltaY+(py/pm)*t0,2)+pow(deltaZ+(pz/pm)*t0,2));
  double deltaPx = (deltaX*pm+px*t0)/t1 - px;
  double deltaPy = (deltaY*pm+py*t0)/t1 - py;
  double deltaPz = (deltaZ*pm+pz*t0)/t1 - pz;

  if (m_debug) std::cout << "(dpx,dpy,dpz): (" << deltaPx << "," << deltaPy << "," << deltaPz << ")" << std::endl;

  mat66 caloJ;
  mat66 tmpCov = mu.cov();
  tmpCov[0][0] += deltaPx*deltaPx;
  tmpCov[1][1] += deltaPy*deltaPy;
  tmpCov[2][2] += deltaPz*deltaPz;
  track tmpTrack = track(xT,mu.p(),mu.q(),&tmpCov);
  initCaloJ(&tmpTrack,&caloJ,t);
  tmpCov = caloJ * tmpCov * ROOT::Math::Transpose(caloJ);

  return track(xT,mu.p(),mu.q(),&tmpCov);
}

track ExtrapolatorTool::doIDProp(track mu){

  float rID(1000.),zID(3000.);
  std::vector<double> x(1000);
  std::vector<double> y(1000);
  std::vector<double> r(1000);
  std::vector<double> z(1000);
  std::vector<double> phi(1000);
  int nMax(2000);
  int nCurrent(0);
  double theta = (m_doSE) ? -10. : 10.0/(mu.p().Pt()/0.6);
  float xMin(9999.);
  float yMin(9999.);
  float rMin(9999.);
  float zMin(9999.);
  double phiMin(9999.);
  int iMin(-1);

  //do iteration 0 first
  x[0] = mu.x().x(); 
  y[0] = mu.x().y();
  z[0] = mu.x().z();
  r[0] = mu.x().Perp();  
  phi[0]= mu.dp().Phi();
  double bLocal = BatRZ(r[0],z[0]); 
  TVector3 xT = mu.x();

  for (int i=1;i<nMax;i++){
    bLocal = BatRZ(r[i-1],z[i-1]);
    double Rlocal = ROC(mu.p().Pt(),bLocal);
    nCurrent++;
    theta = (m_doSE) ? -10.0 : 10.0 / Rlocal;
    xT = (m_doSE) ? getXvecT(xT,mu.dp(),theta) : getXvecTheta(xT,mu.dp(),theta,Rlocal,mu.q());
    x[i] = xT[0];
    y[i] = xT[1];
    r[i] = xT.Perp();
        
    double d = sqrt(pow(x[i]-x[i-1],2)+pow(y[i]-y[i-1],2));
    double localPhi = d / Rlocal;
    if (!m_doSE){
      if (mu.q() < 0) phi[i] = TVector2::Phi_mpi_pi(phi[i-1] - localPhi);
      else phi[i] = TVector2::Phi_mpi_pi(phi[i-1] + localPhi);
    }

    if (!m_doSE) mu.dp().SetPhi(phi[i]);
    float pT = mu.p().Pt();
    float pZ = mu.p().z();
    z[i] = z[i-1] - d*(pZ/pT);

    if (r[i] < rMin) {
      phiMin = phi[i];
      xMin = x[i];
      yMin = y[i];
      rMin = r[i];
      zMin = z[i];
      iMin = i;
    }

    if (r[i] > rID || fabs(z[i]) > zID) break;
    
  }
  
  TVector3 finalPos(xMin,yMin,zMin);
  float tmpPx = mu.p().Pt() * cos(phiMin);
  float tmpPy = mu.p().Pt() * sin(phiMin);
  float tmpPz = mu.p().z();
  TVector3 finalMom(tmpPx,tmpPy,tmpPz);
  
  //get trajectory
  trajectory muTraj;
  for (int i=0;i<nCurrent+1;i++){
    TVector3 tmpVec = TVector3(x[i],y[i],z[i]);
    muTraj.push_back(tmpVec);
  } 

  //propagate covariance matrix
  mat66 tmpCov = mu.cov();
  TVector3 xVec(0,0,0);
  TVector3 pVec(0,0,0);
  for (int i=0;i<iMin+1;i++){
    xVec = TVector3(x[i],y[i],z[i]);
    pVec = TVector3(mu.p().Pt() * cos(phi[i]),mu.p().Pt() * sin(phi[i]),mu.p().z());
    mat66 idJ;
    float thetaI = (mu.q() < 0) ? TVector2::Phi_mpi_pi(pVec.Phi() - TMath::PiOver2()) : TVector2::Phi_mpi_pi(pVec.Phi() + TMath::PiOver2());
    float stepSize = 10.0;
    float B = BatRZ(xVec.Perp(),xVec.z());
    float deltaT = 0.3 * B * stepSize / pVec.Pt();
    track tmpTrack = track(xVec,pVec,mu.q(),&tmpCov);
    initIDJ(&tmpTrack,&idJ,thetaI,deltaT,B);
    tmpCov = idJ * tmpCov * ROOT::Math::Transpose(idJ);
  }

  return track(finalPos,finalMom,mu.q(),muTraj,&tmpCov,rMin,zMin);
}

TVector3 ExtrapolatorTool::getXvecT(TVector3 x0, TVector3 pHat, float t){
  return x0 + pHat * t;
}

TVector3 ExtrapolatorTool::getXvecTheta(TVector3 x_t0,TVector3 pHat,double theta,double roc,float q){

  //set sign of 90 degree shift
  double theta0 = TMath::PiOver2();
  if (q > 0) theta0 *= -1.0;
  //set sign of theta
  if (q < 0) theta *= -1.0;
  TVector3 x_theta = x_t0 + roc * rotMatrix(theta0) * pHat - roc * rotMatrix(theta) * rotMatrix(theta0) * pHat;
  return x_theta;

/*
  TVector3 p2D = TVector3(pHat.x(),pHat.y(),0.);
  TVector3 newPos = x_t0 - 10.0 * p2D.Unit();
  return newPos;
*/
}

double ExtrapolatorTool::ROC(double pt,double B){
  return pt / (0.3 * B);
}

double ExtrapolatorTool::BatRZ(double r,double z){
  double bLocalLargeR = 1.998 + 0.149 * fabs(z/1000.) - 0.371 * pow(z/1000.,2) + 0.259 * pow(fabs(z/1000.),3) - 0.069 * pow(z/1000.,4);
  double bLocalSmallR = 2.0 - 0.0142 * pow(z/1000.,2) - 0.0434 * pow(fabs(z/1000.),3);
  double bLocal = (r/1000. > 0.75) ? bLocalLargeR : bLocalSmallR;
  return bLocal;
}

TMatrixD ExtrapolatorTool::rotMatrix(double theta){

  TMatrixD rot = TMatrixD(3,3);
  rot[0][0] = cos(theta); rot[0][1] = -sin(theta); rot[0][2] = 0;
  rot[1][0] = sin(theta); rot[1][1] = cos(theta);  rot[1][2] = 0;
  rot[2][0] = 0;          rot[2][1] = 0;           rot[2][2] = 1;
  
  return rot;

}

float ExtrapolatorTool::R(float v1,float v2){
  return sqrt(pow(v1,2) + pow(v2,2));
}

track::track(float pt,float theta,float phi,float vx, float vy, float vz,float charge,mat66 * cov,float d0,float z0) {

  m_cov = *cov;

  m_vx.SetXYZ(vx,vy,vz);

  float tmpX = vx - d0 * sin(phi);
  float tmpY = vy + d0 * cos(phi);
  float tmpZ = vz + z0;
  m_x.SetXYZ(tmpX,tmpY,tmpZ);
  
  float tmpEta = -1.0 * log (tan(theta/2.0));
  float tmpPx = pt* cos(phi);
  float tmpPy = pt* sin(phi);
  float tmpPz = pt* sinh(tmpEta);
  m_p.SetXYZ(tmpPx,tmpPy,tmpPz);

  m_dp = m_p;
  m_dp.SetMag(1.0);

  m_q = charge;
  m_d0 = d0;
  m_z0 = z0;

}

track::track(TVector3 x,TVector3 p,float charge,trajectory muTraj,mat66 *cov,float d0,float z0) {

  m_cov = *cov;

  m_x.SetXYZ(x.x(),x.y(),x.z());

  m_p.SetXYZ(p.x(),p.y(),p.z());
  
  float tmpVx = x.x() + d0*sin(p.Phi());
  float tmpVy = x.y() - d0*cos(p.Phi());
  float tmpVz = x.z() - z0;
  m_vx.SetXYZ(tmpVx,tmpVy,tmpVz);
 
  m_dp = m_p;
  m_dp.SetMag(1.0);

  m_q = charge;
  m_d0 = d0;
  m_z0 = z0;
  m_traj = muTraj;
}

track::track(TVector3 x,TVector3 p,float charge,mat66 * cov,float d0,float z0) {

  m_cov = *cov;

  m_x.SetXYZ(x.x(),x.y(),x.z());

  m_p.SetXYZ(p.x(),p.y(),p.z());

  float tmpVx = x.x() + d0*sin(p.Phi());
  float tmpVy = x.y() - d0*cos(p.Phi());
  float tmpVz = x.z() - z0;
  m_vx.SetXYZ(tmpVx,tmpVy,tmpVz);

  m_dp = m_p;
  m_dp.SetMag(1.0);

  m_q = charge;
  m_d0 = d0;
  m_z0 = z0;
}

void ExtrapolatorTool::initMomentumJ(TVector *param,mat55 * mat){

  (*mat)[0][0] = 1;
  (*mat)[0][1] = 0; 
  (*mat)[0][2] = 0; 
  (*mat)[0][3] = 0; 
  (*mat)[0][4] = 0; 

  (*mat)[1][0] = 0; 
  (*mat)[1][1] = 1; 
  (*mat)[1][2] = 0; 
  (*mat)[1][3] = 0; 
  (*mat)[1][4] = 0; 

  (*mat)[2][0] = 0;
  (*mat)[2][1] = 0; 
  (*mat)[2][2] = 1; 
  (*mat)[2][3] = 0; 
  (*mat)[2][4] = 0; 

  (*mat)[3][0] = 0;
  (*mat)[3][1] = 0;
  (*mat)[3][2] = 0; 
  (*mat)[3][3] = 1; 
  (*mat)[3][4] = 0;

  (*mat)[4][0] = 0;
  (*mat)[4][1] = 0;
  (*mat)[4][2] = 0; 
  (*mat)[4][3] = 0; 
  (*mat)[4][4] = -1.0 / pow((*param)[4],2); 

  return;
}

void ExtrapolatorTool::initPCJ(TVector * param,mat65 * mat){

  (*mat)[0][0] = 0;
  (*mat)[0][1] = 0; 
  (*mat)[0][2] = - (*param)[4] * sin((*param)[2]) * sin((*param)[3]); 
  (*mat)[0][3] = (*param)[4] * cos((*param)[2]) * cos((*param)[3]); 
  (*mat)[0][4] = cos((*param)[2]) * sin((*param)[3]); 

  (*mat)[1][0] = 0;
  (*mat)[1][1] = 0; 
  (*mat)[1][2] = (*param)[4] * cos((*param)[2]) * sin((*param)[3]); 
  (*mat)[1][3] = (*param)[4] * sin((*param)[2]) * cos((*param)[3]); 
  (*mat)[1][4] = sin((*param)[2]) * sin((*param)[3]); 

  (*mat)[2][0] = 0;
  (*mat)[2][1] = 0; 
  (*mat)[2][2] = 0; 
  (*mat)[2][3] = -(*param)[4] * sin((*param)[3]); 
  (*mat)[2][4] = cos((*param)[3]); 

  (*mat)[3][0] = -sin((*param)[2]);
  (*mat)[3][1] = 0; 
  (*mat)[3][2] = - (*param)[0] * cos((*param)[2]); 
  (*mat)[3][3] = 0; 
  (*mat)[3][4] = 0;

  (*mat)[4][0] = cos((*param)[2]);
  (*mat)[4][1] = 0; 
  (*mat)[4][2] = - (*param)[0] * sin((*param)[2]); 
  (*mat)[4][3] = 0; 
  (*mat)[4][4] = 0;  

  (*mat)[5][0] = 0;
  (*mat)[5][1] = 1; 
  (*mat)[5][2] = 0; 
  (*mat)[5][3] = 0; 
  (*mat)[5][4] = 0; 

  return;
}

void ExtrapolatorTool::initCPJ(TVector3 x,TVector3 p,mat56 * mat){

  double pT = p.Perp();
  double pM = p.Mag();
  double xT = x.Perp();

  (*mat)[0][0] = 0;
  (*mat)[0][1] = 0; 
  (*mat)[0][2] = 0; 
  (*mat)[0][3] = x.x() / xT; 
  (*mat)[0][4] = x.y() / xT; 
  (*mat)[0][5] = 0;

  (*mat)[1][0] = 0;
  (*mat)[1][1] = 0; 
  (*mat)[1][2] = 0; 
  (*mat)[1][3] = 0; 
  (*mat)[1][4] = 0; 
  (*mat)[1][5] = 1;

  (*mat)[2][0] = -p.y() / (pT*pT);
  (*mat)[2][1] = p.x() / (pT*pT); 
  (*mat)[2][2] = 0; 
  (*mat)[2][3] = 0; 
  (*mat)[2][4] = 0; 
  (*mat)[2][5] = 0;

  (*mat)[3][0] = p.x() * p.z() / ( pM * pM * pT );
  (*mat)[3][1] = p.y() * p.z() / ( pM * pM * pT ); 
  (*mat)[3][2] = -pT / (pM * pM); 
  (*mat)[3][3] = 0; 
  (*mat)[3][4] = 0;
  (*mat)[3][5] = 0;

  (*mat)[4][0] = p.x() / pM;
  (*mat)[4][1] = p.y() / pM; 
  (*mat)[4][2] = p.z() / pM; 
  (*mat)[4][3] = 0; 
  (*mat)[4][4] = 0; 
  (*mat)[4][5] = 0; 

  return;
}

void ExtrapolatorTool::initCaloJ(track * cT,mat66 * mat,double t0){

  TVector3 pUnit = cT->p().Unit();
  double pMag = cT->p().Mag();

  
  (*mat)[0][0] = 1;
  (*mat)[0][1] = 0; 
  (*mat)[0][2] = 0; 
  (*mat)[0][3] = 0; 
  (*mat)[0][4] = 0; 
  (*mat)[0][5] = 0;
  
  
  (*mat)[1][0] = 0;
  (*mat)[1][1] = 1; 
  (*mat)[1][2] = 0; 
  (*mat)[1][3] = 0; 
  (*mat)[1][4] = 0; 
  (*mat)[1][5] = 0;

  (*mat)[2][0] = 0;
  (*mat)[2][1] = 0; 
  (*mat)[2][2] = 1; 
  (*mat)[2][3] = 0; 
  (*mat)[2][4] = 0; 
  (*mat)[2][5] = 0;

  (*mat)[3][0] = t0/pMag * (1 - pow(pUnit.x(),2));
  (*mat)[3][1] = 0; 
  (*mat)[3][2] = 0; 
  (*mat)[3][3] = 1; 
  (*mat)[3][4] = 0;
  (*mat)[3][5] = 0;

  (*mat)[4][0] = 0;
  (*mat)[4][1] = t0/pMag * (1 - pow(pUnit.y(),2));
  (*mat)[4][2] = 0; 
  (*mat)[4][3] = 0; 
  (*mat)[4][4] = 1; 
  (*mat)[4][5] = 0; 

  (*mat)[5][0] = 0;
  (*mat)[5][1] = 0; 
  (*mat)[5][2] = t0/pMag * (1 - pow(pUnit.z(),2));
  (*mat)[5][3] = 0; 
  (*mat)[5][4] = 0; 
  (*mat)[5][5] = 1;

  return;
}

void ExtrapolatorTool::initIDJ(track * cT,mat66 * mat,float thetaI,float deltaT,float B){

  double pT = sqrt(pow(cT->p().x(),2) + pow(cT->p().y(),2));

  (*mat)[0][0] = 1 - cT->p().x() * sin(thetaI) * deltaT / pT;
  (*mat)[0][1] = - cT->p().y() * sin(thetaI) * deltaT / pT; 
  (*mat)[0][2] = 0; 
  (*mat)[0][3] = 0; 
  (*mat)[0][4] = 0; 
  (*mat)[0][5] = 0;

  (*mat)[1][0] = cT->p().x() * cos(thetaI) * deltaT / pT; 
  (*mat)[1][1] = 1 + cT->p().y() * cos(thetaI) * deltaT / pT; 
  (*mat)[1][2] = 0; 
  (*mat)[1][3] = 0; 
  (*mat)[1][4] = 0; 
  (*mat)[1][5] = 0;

  (*mat)[2][0] = 0;
  (*mat)[2][1] = 0; 
  (*mat)[2][2] = 1; 
  (*mat)[2][3] = 0; 
  (*mat)[2][4] = 0; 
  (*mat)[2][5] = 0;

  (*mat)[3][0] = - cT->p().x() * sin(thetaI) * deltaT / (0.3 * B * pT);
  (*mat)[3][1] = - cT->p().y() * sin(thetaI) * deltaT / (0.3 * B * pT);
  (*mat)[3][2] = 0; 
  (*mat)[3][3] = 1; 
  (*mat)[3][4] = 0;
  (*mat)[3][5] = 0;

  (*mat)[4][0] = cT->p().x() * cos(thetaI) * deltaT / (0.3 * B * pT);
  (*mat)[4][1] = cT->p().y() * cos(thetaI) * deltaT / (0.3 * B * pT);
  (*mat)[4][2] = 0; 
  (*mat)[4][3] = 0; 
  (*mat)[4][4] = 1; 
  (*mat)[4][5] = 0; 

  (*mat)[5][0] = 0;
  (*mat)[5][1] = 0; 
  (*mat)[5][2] = deltaT/(0.3 * B); 
  (*mat)[5][3] = 0; 
  (*mat)[5][4] = 0; 
  (*mat)[5][5] = 1;

  return;
}

