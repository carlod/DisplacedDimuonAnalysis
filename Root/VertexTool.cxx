#include "../DisplacedDimuonAnalysis/VertexTool.h"
#include "../DisplacedDimuonAnalysis/AnalysisTool.h"

RecVertex VertexTool::createVertex(std::shared_ptr<AO::Muon>& tmpMuLead, std::shared_ptr<AO::Muon>& tmpMuSub, int iLead, int iSub){

  const xAOD::TrackParticle * mu1 = const_cast<const xAOD::TrackParticle*>(tmpMuLead->tpMS());
  const xAOD::TrackParticle * mu2 = const_cast<const xAOD::TrackParticle*>(tmpMuSub->tpMS());

  int barcode = (tmpMuLead->parBarCode() == tmpMuSub->parBarCode() && tmpMuLead->parBarCode() != -1) ? tmpMuLead->parBarCode() : -1;

  float pt1 = mu1->pt();
  float phi1 = mu1->phi();
  float eta1 = mu1->eta();
  float d01 = mu1->d0();
  float z01 = mu1->z0();
  float vx1 = mu1->vx();
  float vy1 = mu1->vy();
  float vz1 = mu1->vz();
  TVector3 p1_pos(vx1-d01*sin(phi1),vy1+d01*cos(phi1),vz1+z01);
  TVector3 p1_dir(cos(phi1)/cosh(eta1),sin(phi1)/cosh(eta1),tanh(eta1));
  float pt2 = mu2->pt();
  float phi2 = mu2->phi();
  float eta2 = mu2->eta();
  float d02 = mu2->d0();
  float z02 = mu2->z0();
  float vx2 = mu2->vx();
  float vy2 = mu2->vy();
  float vz2 = mu2->vz();
  float q = mu1->charge() + mu2->charge();

  TVector3 p2_pos(vx2-d02*sin(phi2),vy2+d02*cos(phi2),vz2+z02);
  TVector3 p2_dir(cos(phi2)/cosh(eta2),sin(phi2)/cosh(eta2),tanh(eta2));

  float lmin = ((p1_pos-p2_pos).Dot(p1_dir-p2_dir*(p1_dir.Dot(p2_dir))))/(pow(p1_dir.Dot(p2_dir),2)-1);
  float tmin = (p1_pos-p2_pos).Dot(p2_dir) + lmin*p1_dir.Dot(p2_dir);
  TVector3 s = p1_pos + lmin*p1_dir;
  TVector3 r = p2_pos + tmin*p2_dir;
  TVector3 d = 0.5*(s + r);
  TVector3 rs = r-s;
  float distB = rs.Mag();
  float openA = acos(p1_dir.Dot(p2_dir)/(p1_dir.Mag()*p2_dir.Mag()));
  float m = AnalysisTool::invMass(mu1,mu2);

  float sgnVtx(1.);
  TVector3 p1,p2;
  p1.SetPtEtaPhi(pt1,eta1,phi1);
  p2.SetPtEtaPhi(pt2,eta2,phi2);
  TVector3 pTotal = p1 + p2;
  if (d.X()*pTotal.X()+d.Y()*pTotal.Y() < 0 ) sgnVtx = -1.;
  std::vector<std::shared_ptr<AO::Muon>> muTP;
  muTP.push_back(tmpMuLead);
  muTP.push_back(tmpMuSub);

  RecVertex newVtx = RecVertex(muTP,d,sgnVtx,openA,distB,m,q,iLead,iSub);

  return newVtx;
}
