#include "../DisplacedDimuonAnalysis/ContainerTool.h"
#include "xAODRootAccess/TEvent.h"

bool ContainerTool::isGood(xAOD::TEvent * theEvent){
 
  m_isGood = true;

  m_eventInfo = 0;
  if ( !theEvent->retrieve(m_eventInfo,"EventInfo").isSuccess() ){
    std::cout << "Failed to retrieve EventInfo container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_isMC = true;
  if (!m_eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION ) ) m_isMC = false;

  if (m_isMC){
    m_truthVertices = 0;
    if ( !theEvent->retrieve(m_truthVertices, "TruthVertices" ).isSuccess() ){
      std::cout << "Failed to retrieve TruthVertex container. Exiting." << std::endl;
      m_isGood = false;
    }
  }

  m_muonSegments = 0;
  if (!theEvent->retrieve(m_muonSegments,"MuonSegments" ).isSuccess() ){
    std::cout << "Failed to retrieve Muon Segment container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_MSTrackParticles = 0;
  if ( !theEvent->retrieve(m_MSTrackParticles,"MuonSpectrometerTrackParticles" ).isSuccess() ){
    std::cout << "Failed to retrieve MuonSpectometerTrackParticles container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_exTrackParticles = 0;
  if ( !theEvent->retrieve(m_exTrackParticles,"ExtrapolatedMuonTrackParticles" ).isSuccess() ){
    std::cout << "Failed to retrieve ExtrapolatedMuonTrackParticles container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_combTrackParticles = 0;
  if ( !theEvent->retrieve(m_combTrackParticles,"CombinedMuonTrackParticles" ).isSuccess() ){
    std::cout << "Failed to retrieve CombinedMuonTrackParticles container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_jets = 0;
  if ( !theEvent->retrieve(m_jets,"AntiKt4EMTopoJets" ).isSuccess() ){
    std::cout << "Failed to retrieve AntiKt4EMTopo container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_muons = 0;
  if ( !theEvent->retrieve(m_muons, "Muons" ).isSuccess() ){
    std::cout << "Failed to retrieve Muons container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_MET = 0;
  if ( !theEvent->retrieve(m_MET,"MET_Reference_AntiKt4EMTopo").isSuccess() ){
    std::cout << "Failed to retrieve MET container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_LVL1EnergySumRoI = 0;
  if ( !theEvent->retrieve(m_LVL1EnergySumRoI,"LVL1EnergySumRoI").isSuccess() ){
    std::cout << "Failed to retrieve LVL1EnergySumRoI container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_HLT_MET_mht = 0;
  if ( !theEvent->retrieve(m_HLT_MET_mht,"HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht").isSuccess() ){
    std::cout << "Failed to retrieve TrigMET mht container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_HLT_MET_cell = 0;
  if ( !theEvent->retrieve(m_HLT_MET_cell,"HLT_xAOD__TrigMissingETContainer_TrigEFMissingET").isSuccess() ){
    std::cout << "Failed to retrieve TrigMET cell container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_HLT_muon = 0;
  if ( !theEvent->retrieve(m_HLT_muon,"HLT_xAOD__MuonContainer_MuonEFInfo").isSuccess() ){
    std::cout << "Failed to retrieve HLT muon container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_L1_muonRoI = 0;
  if ( !theEvent->retrieve(m_L1_muonRoI,"LVL1MuonRoIs").isSuccess() ){
    std::cout << "Failed to retrieve LVL1MuonRoIs container. Exiting." << std::endl;
    m_isGood = false;
  }

  m_PrimaryVertices = 0;
  if( !theEvent->retrieve(m_PrimaryVertices,"PrimaryVertices").isSuccess() ){
    std::cout << "Failed to retrieve PV container. Exiting." << std::endl;
    m_PrimaryVertices = NULL;
  } 

  m_inDetTrackParticles = 0;
  if ( !theEvent->retrieve(m_inDetTrackParticles,"InDetTrackParticles").isSuccess() ){
    std::cout << "Failed to retrieve inDet container. Exiting." << std::endl;
    m_inDetTrackParticles = NULL;
  }

  return m_isGood;
}
