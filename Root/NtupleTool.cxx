#include "../DisplacedDimuonAnalysis/NtupleTool.h"
#include "xAODTruth/TruthVertex.h"

#include <iostream>
#include <algorithm>
#include <TTree.h>

NtupleTool::NtupleTool(TFile* outputFile) : m_evt_num(0) {
  m_tree = new TTree("tree","tree");
  m_tree->SetDirectory(outputFile);
  Clear();
  initForWrite(m_tree);
}

void NtupleTool::initForWrite(TTree* tree){
  
  //Event Branches
  tree->Branch("EventNumber", &m_evt_num);
  tree->Branch("RunNumber", &m_run_num);
  tree->Branch("BCID", &m_bcid);
  tree->Branch("LB", &m_lumiBlock);
  tree->Branch("evtWeight", &m_evtWeight);
  tree->Branch("sumWeights", &m_sumWeights);
  tree->Branch("prw",&m_prw);
  tree->Branch("EtMiss", &m_eTMiss);
  tree->Branch("EtMissPhi", &m_eTMissPhi);
  
  //Event Vertex Branches
  tree->Branch("TPV_x",&m_tpv_x);
  tree->Branch("TPV_y",&m_tpv_y);
  tree->Branch("TPV_z",&m_tpv_z);
  tree->Branch("PV_z",&m_pv_z);
  tree->Branch("PV_ntrk",&m_pv_ntrk);
  tree->Branch("PU_nvtx",&m_nPUvtx);
  tree->Branch("nMSOnly", &m_nMuons);
  tree->Branch("NDV", &m_nRecVtx, "NDV/I");
  tree->Branch("NDVT", &m_nTruthVtx, "NDVT/I");
  
  //Trigger Branches
  tree->Branch("narrowScanTrig", &m_narrowScanTrig,"narrowScanTrig/I");
  tree->Branch("METTrig", &m_METTrig,"METTrig/I");
  tree->Branch("singleMuTrig",&m_singleMuTrig,"singleMuTrig/I");
  tree->Branch("threeMuTrig",&m_threeMuTrig,"threeMuTrig/I");
  tree->Branch("L1_MU20Trig",&m_L1_MU20Trig,"L1_MU20Trig/I");
  tree->Branch("L1_3MU6Trig",&m_L1_3MU6Trig,"L1_3MU6Trig/I");

  //MET Branches
  tree->Branch("trigMHT",&m_trig_mht);
  tree->Branch("trigMHTPhi",&m_trig_mht_phi);
  tree->Branch("MHT",&m_mht);
  tree->Branch("MHTPhi",&m_mht_phi); 

  //ID Track Branches
  tree->Branch("id_pt", &m_id_pt);
  tree->Branch("id_phi", &m_id_phi);
  tree->Branch("id_eta", &m_id_eta);
  tree->Branch("id_d0", &m_id_d0);
  tree->Branch("id_z0", &m_id_z0);
  tree->Branch("id_charge", &m_id_charge);
  tree->Branch("id_chi2", &m_id_chi2);
  tree->Branch("id_dof", &m_id_dof);
  tree->Branch("id_vtxType", &m_id_vtxType);

  //Truth Track Branches
  tree->Branch("trkt_pt", &m_trkt_pt);
  tree->Branch("trkt_phi", &m_trkt_phi);
  tree->Branch("trkt_eta", &m_trkt_eta);
  tree->Branch("trkt_d0", &m_trkt_d0);
  tree->Branch("trkt_z0", &m_trkt_z0);
  tree->Branch("trkt_charge", &m_trkt_charge);
  tree->Branch("trkt_PDG", &m_trkt_PDG);
  tree->Branch("trkt_barCode", &m_trkt_barCode);
  tree->Branch("trkt_status", &m_trkt_status);
  tree->Branch("trkt_parPDG", &m_trkt_parPDG);
  tree->Branch("trkt_parBarCode", &m_trkt_parBarCode);
 
  //Truth DV Branches
  tree->Branch("DVT_LJMatch", &m_dvt_LJMatch);
  tree->Branch("DVT_mu60Match", &m_dvt_mu60Match);
  tree->Branch("DVT_x", &m_dvt_x);
  tree->Branch("DVT_y", &m_dvt_y);
  tree->Branch("DVT_z", &m_dvt_z);
  tree->Branch("DVT_r", &m_dvt_r);
  tree->Branch("DVT_l", &m_dvt_l);
  tree->Branch("DVT_m", &m_dvt_m);
  tree->Branch("DVT_openAng", &m_dvt_openAng);
  tree->Branch("DVT_parPDG", &m_dvt_parPDG);
  tree->Branch("DVT_parBarCode", &m_dvt_parBarCode);
  tree->Branch("DVT_muIndexLead", &m_dvt_muIndexLead);
  tree->Branch("DVT_muIndexSub", &m_dvt_muIndexSub);
  
  tree->Branch("DVT_pLLP",&m_dvt_pLLP);
  tree->Branch("DVT_eLLP",&m_dvt_eLLP);
  tree->Branch("DVT_lLLP",&m_dvt_lLLP);
 
  //Comb Muon Branches
  tree->Branch("comb_pt", &m_comb_pt);
  tree->Branch("comb_phi", &m_comb_phi);
  tree->Branch("comb_eta", &m_comb_eta);
  tree->Branch("comb_d0", &m_comb_d0);
  tree->Branch("comb_z0", &m_comb_z0);
  tree->Branch("comb_isoDPV", &m_comb_isoDPV);
  tree->Branch("comb_isoDAll", &m_comb_isoDAll);
  tree->Branch("comb_isoIPV", &m_comb_isoIPV);
  tree->Branch("comb_isoIAll", &m_comb_isoIAll);
  tree->Branch("comb_JMOR", &m_comb_JMOR);
  tree->Branch("comb_author", &m_comb_author);
  tree->Branch("comb_chi2", &m_comb_chi2); 
  tree->Branch("comb_PDG", &m_comb_PDG);
  tree->Branch("comb_barcode", &m_comb_barcode);

  //Track Branches
  tree->Branch("trk_pt", &m_trk_pt);
  tree->Branch("trk_phi", &m_trk_phi);
  tree->Branch("trk_eta", &m_trk_eta);
  tree->Branch("trk_d0", &m_trk_d0);
  tree->Branch("trk_z0", &m_trk_z0);
  tree->Branch("trk_m", &m_trk_m);
  tree->Branch("trk_charge", &m_trk_charge);
  tree->Branch("trk_covd0", &m_trk_covd0);
  tree->Branch("trk_covz0", &m_trk_covz0);
  tree->Branch("trk_covPhi", &m_trk_covPhi);
  tree->Branch("trk_covTheta", &m_trk_covTheta);
  tree->Branch("trk_covP", &m_trk_covP);
  tree->Branch("trk_isTM", &m_trk_isTM,"trk_isTM/I");
  tree->Branch("trk_PDG", &m_trk_PDG);
  tree->Branch("trk_barCode", &m_trk_barCode);
  tree->Branch("trk_parPDG", &m_trk_parPDG);
  tree->Branch("trk_parBarCode", &m_trk_parBarCode);
  tree->Branch("trk_isoDPV", &m_trk_isoDPV);
  tree->Branch("trk_isoDAll", &m_trk_isoDAll);
  tree->Branch("trk_isoIPV", &m_trk_isoIPV);
  tree->Branch("trk_isoIAll", &m_trk_isoIAll);
  tree->Branch("trk_jetDR", &m_trk_jetDR);
  tree->Branch("trk_nPrecLayers", &m_trk_nPrecLayers);
  tree->Branch("trk_nPhiLayers", &m_trk_nPhiLayers);
  tree->Branch("trk_nEtaLayers", &m_trk_nEtaLayers);
  tree->Branch("trk_vx", &m_trk_vx);
  tree->Branch("trk_vy", &m_trk_vy);
  tree->Branch("trk_vz", &m_trk_vz);

  //DV Branches
  tree->Branch("DV_x", &m_dv_x);
  tree->Branch("DV_y", &m_dv_y);
  tree->Branch("DV_z", &m_dv_z); 
  tree->Branch("DV_r", &m_dv_r); 
  tree->Branch("DV_l", &m_dv_l);
  tree->Branch("DV_m", &m_dv_m);
  tree->Branch("DV_charge", &m_dv_charge);
  tree->Branch("DV_openAng", &m_dv_openAng);
  tree->Branch("DV_distV", &m_dv_distV);
  tree->Branch("DV_muIndexLead", &m_dv_muIndexLead);
  tree->Branch("DV_muIndexSub", &m_dv_muIndexSub);

}

void NtupleTool::setEvtVars(int evtNum,int runNum,int BCID,int LB,float evtW,float sumW,float prw,float MET,float METPhi){
  m_evt_num = evtNum; m_run_num = runNum; m_bcid = BCID; m_lumiBlock = LB; 
  m_evtWeight = evtW; m_sumWeights = sumW; m_prw = prw; m_eTMiss = MET; 
  m_eTMissPhi = METPhi;
}

void NtupleTool::setEvtVtxVars(float PVz,int nTrkPV,int nPU,int nMuons, int nRecVtx, int nTruthVtx){
  m_pv_z = PVz; m_pv_ntrk = nTrkPV; m_nPUvtx = nPU; m_nMuons = nMuons; 
  m_nRecVtx = nRecVtx; m_nTruthVtx = nTruthVtx;
}

void NtupleTool::setTPV(TVector3 tmpTPV){m_tpv_x = tmpTPV.x();m_tpv_y = tmpTPV.y();m_tpv_z = tmpTPV.z();}

void NtupleTool::setTrigDec(trigMap tmpMap) {
  m_narrowScanTrig = tmpMap["narrowScan"];
  m_METTrig = tmpMap["MET"];
  m_singleMuTrig = tmpMap["singleMu"];
  m_threeMuTrig = tmpMap["threeMu"];
  m_L1_MU20Trig = tmpMap["L1_MU20"];
  m_L1_3MU6Trig = tmpMap["L1_3MU6"];
}

void NtupleTool::addMET(float trigMHT,float trigMHTPhi,float mht,float mhtphi) {
  m_trig_mht = trigMHT * 0.001; m_trig_mht_phi = trigMHTPhi; m_mht = mht * 0.001; m_mht_phi = mhtphi;
}

void NtupleTool::addIDTrk(const xAOD::TrackParticle* tp,int vtxType){

  m_id_pt.push_back(tp->pt()*0.001);
  m_id_phi.push_back(tp->phi());
  m_id_eta.push_back(tp->eta());
  m_id_d0.push_back(tp->d0()*0.1);
  m_id_z0.push_back(tp->z0()*0.1);
  m_id_charge.push_back(tp->charge());
  m_id_chi2.push_back(tp->chiSquared());
  m_id_dof.push_back(tp->numberDoF());
  m_id_vtxType.push_back(vtxType);

}

void NtupleTool::addTruthMuon(xAOD::TruthParticle* tp,float d0,float z0,int parPDG,int parBarCode) {

  m_trkt_pt.push_back(tp->pt()*0.001);
  m_trkt_phi.push_back(tp->phi());
  m_trkt_eta.push_back(tp->eta());
  m_trkt_d0.push_back(d0*0.1);
  m_trkt_z0.push_back(z0*0.1);
  m_trkt_charge.push_back(tp->charge());
  m_trkt_PDG.push_back(tp->pdgId());
  m_trkt_barCode.push_back(tp->barcode());
  m_trkt_status.push_back(tp->status());
  m_trkt_parPDG.push_back(parPDG);
  m_trkt_parBarCode.push_back(parBarCode);

}

void NtupleTool::addTruthVtx(TruthVertex& vtx,const xAOD::TruthParticle* longLivedParticle,int LJMatch,int mu60Match){

  m_dvt_LJMatch.push_back(LJMatch);
  m_dvt_mu60Match.push_back(mu60Match);
  m_dvt_x.push_back(0.1*vtx.x());
  m_dvt_y.push_back(0.1*vtx.y());
  m_dvt_z.push_back(0.1*vtx.z());
  m_dvt_r.push_back(0.1*vtx.r());
  m_dvt_l.push_back(0.1*vtx.l());
  m_dvt_m.push_back(0.001*vtx.invMass());
  m_dvt_openAng.push_back(vtx.openA());
  m_dvt_parPDG.push_back(vtx.parPDG());
  m_dvt_parBarCode.push_back(vtx.parBarCode());
  m_dvt_muIndexLead.push_back(vtx.iLead());
  m_dvt_muIndexSub.push_back(vtx.iSub());
    
  //set LLP vars
  double eParent = longLivedParticle->e();
  double pParent = sqrt(pow(longLivedParticle->pt(),2) + pow(longLivedParticle->pz(),2));
  double LPar(0.0);

  if (longLivedParticle->hasProdVtx() && longLivedParticle->hasDecayVtx()) {
    double vxPar = (longLivedParticle->prodVtx()->x() - longLivedParticle->decayVtx()->x());
    double vyPar = (longLivedParticle->prodVtx()->y() - longLivedParticle->decayVtx()->y());
    double vzPar = (longLivedParticle->prodVtx()->z() - longLivedParticle->decayVtx()->z());
    LPar = sqrt(pow((vxPar),2)+pow((vyPar),2)+pow((vzPar),2));
  }

  m_dvt_pLLP.push_back(pParent*0.001);
  m_dvt_eLLP.push_back(eParent*0.001);
  m_dvt_lLLP.push_back(LPar*0.1);

}

void NtupleTool::addCombMuon(float pt,float phi,float eta,float d0,float z0,float isoDPV,float isoDAll,float isoIPV,float isoIAll,float JMOR,int author, double chi2,int PDG,int barcode){
  m_comb_pt.push_back(pt * 0.001);
  m_comb_phi.push_back(phi);
  m_comb_eta.push_back(eta);
  m_comb_d0.push_back(d0 * 0.1);
  m_comb_z0.push_back(z0 * 0.1);
  m_comb_isoDPV.push_back(isoDPV);
  m_comb_isoDAll.push_back(isoDAll);
  m_comb_isoIPV.push_back(isoIPV);
  m_comb_isoIAll.push_back(isoIAll);
  m_comb_JMOR.push_back(JMOR);
  m_comb_author.push_back(author);
  m_comb_chi2.push_back(chi2);
  m_comb_PDG.push_back(PDG);
  m_comb_barcode.push_back(barcode);
}

void NtupleTool::addMuon(AO::Muon muon){

  xAOD::TrackParticle * tp = muon.tp();
  xAOD::TrackParticle * tpMS = muon.tpMS();

  m_trk_pt.push_back(tp->pt()*0.001);
  m_trk_phi.push_back(tp->phi());
  m_trk_eta.push_back(tp->eta());
  m_trk_d0.push_back(tp->d0()*0.1);
  m_trk_z0.push_back(tp->z0()*0.1);
  m_trk_m.push_back(tp->m()*0.001);
  m_trk_charge.push_back(tp->charge());
  xAOD::ParametersCovMatrix_t tmpCov = muon.covMat();
  m_trk_covd0.push_back(tmpCov(0,0)*0.1*0.1);
  m_trk_covz0.push_back(tmpCov(1,1)*0.1*0.1);
  m_trk_covPhi.push_back(tmpCov(2,2));
  m_trk_covTheta.push_back(tmpCov(3,3));
  m_trk_covP.push_back(tmpCov(4,4)*0.001*0.001);
  m_trk_isTM.push_back(muon.isTM());
  m_trk_PDG.push_back(muon.PDG());
  m_trk_barCode.push_back(muon.barCode());
  m_trk_parPDG.push_back(muon.parPDG());
  m_trk_parBarCode.push_back(muon.parBarCode());
  m_trk_isoDPV.push_back(muon.isoDPV());
  m_trk_isoDAll.push_back(muon.isoDAll());
  m_trk_isoIPV.push_back(muon.isoIPV());
  m_trk_isoIAll.push_back(muon.isoIAll());
  m_trk_jetDR.push_back(muon.jetDR());
  m_trk_nPrecLayers.push_back(muon.nPrecLayers());
  m_trk_nPhiLayers.push_back(muon.nPhiLayers());
  m_trk_nEtaLayers.push_back(muon.nEtaLayers());
  m_trk_vx.push_back(0.1 * tpMS->vx());
  m_trk_vy.push_back(0.1 * tpMS->vy());
  m_trk_vz.push_back(0.1 * tpMS->vz());

}

void NtupleTool::addVtx(RecVertex& vtx){

  m_dv_x.push_back(0.1*vtx.x());
  m_dv_y.push_back(0.1*vtx.y());
  m_dv_z.push_back(0.1*vtx.z());
  m_dv_r.push_back(0.1*vtx.sgn()*vtx.r()); 
  m_dv_l.push_back(0.1*vtx.l());
  m_dv_m.push_back(0.001*vtx.invMass());
  m_dv_charge.push_back(vtx.charge());
  m_dv_openAng.push_back(vtx.openA());
  m_dv_distV.push_back(0.1*vtx.distB());
  m_dv_muIndexLead.push_back(vtx.iLead());
  m_dv_muIndexSub.push_back(vtx.iSub());

}

void NtupleTool::Clear(){

  //Event Vars
  m_evt_num = 0;
  m_run_num = 0;
  m_bcid = 0;
  m_lumiBlock = 0;
  m_evtWeight = 0;
  m_sumWeights = 0;
  m_prw = 0;
  m_eTMiss = 0;
  m_eTMissPhi = 0;

  //Event Vertex Vars
  m_tpv_x = 0;
  m_tpv_y = 0;
  m_tpv_z = 0;
  m_pv_z = 0;
  m_pv_ntrk = 0;
  m_nPUvtx = 0;
  m_nMuons = 0;
  m_nRecVtx = 0;
  m_nTruthVtx = 0;
  
  //Trigger Vars
  m_narrowScanTrig = 0;
  m_METTrig = 0;
  m_singleMuTrig = 0;
  m_threeMuTrig = 0;
  m_L1_MU20Trig = 0;
  m_L1_3MU6Trig = 0;
 
  //MET Vars
  m_trig_mht = 0;
  m_trig_mht_phi = 0;
  m_mht = 0;
  m_mht_phi = 0;

  //ID Track Vars
  m_id_pt.clear();
  m_id_phi.clear();
  m_id_eta.clear();
  m_id_d0.clear();
  m_id_z0.clear();
  m_id_charge.clear();
  m_id_chi2.clear();
  m_id_dof.clear();
  m_id_vtxType.clear();
 
  //Truth Track Vars
  m_trkt_pt.clear();
  m_trkt_phi.clear();
  m_trkt_eta.clear();
  m_trkt_d0.clear();
  m_trkt_z0.clear();
  m_trkt_charge.clear();
  m_trkt_PDG.clear();
  m_trkt_barCode.clear();
  m_trkt_status.clear();
  m_trkt_parPDG.clear();
  m_trkt_parBarCode.clear();

  //Truth DV Vars
  m_dvt_LJMatch.clear();
  m_dvt_mu60Match.clear();
  m_dvt_x.clear();
  m_dvt_y.clear();
  m_dvt_z.clear();
  m_dvt_r.clear();
  m_dvt_l.clear();
  m_dvt_m.clear();
  m_dvt_openAng.clear();
  m_dvt_parPDG.clear();
  m_dvt_parBarCode.clear();
  m_dvt_muIndexLead.clear();
  m_dvt_muIndexSub.clear();
  
  m_dvt_pLLP.clear();
  m_dvt_eLLP.clear();
  m_dvt_lLLP.clear();

  //Comb Muon Vars
  m_comb_pt.clear();
  m_comb_phi.clear();
  m_comb_eta.clear();
  m_comb_d0.clear();
  m_comb_z0.clear();
  m_comb_isoDPV.clear();
  m_comb_isoDAll.clear();
  m_comb_isoIPV.clear();
  m_comb_isoIAll.clear();
  m_comb_JMOR.clear(); 
  m_comb_author.clear();
  m_comb_chi2.clear(); 
  m_comb_PDG.clear();
  m_comb_barcode.clear();

  //Track Vars
  m_trk_pt.clear();
  m_trk_phi.clear();
  m_trk_eta.clear();
  m_trk_d0.clear();
  m_trk_z0.clear();
  m_trk_m.clear();
  m_trk_charge.clear();
  m_trk_covd0.clear();
  m_trk_covz0.clear();
  m_trk_covPhi.clear();
  m_trk_covTheta.clear();
  m_trk_covP.clear();
  m_trk_isTM.clear();
  m_trk_PDG.clear();
  m_trk_barCode.clear();
  m_trk_parPDG.clear();
  m_trk_parBarCode.clear();
  m_trk_isoDPV.clear();
  m_trk_isoDAll.clear();
  m_trk_isoIPV.clear();
  m_trk_isoIAll.clear();
  m_trk_jetDR.clear();
  m_trk_nPrecLayers.clear();
  m_trk_nPhiLayers.clear();
  m_trk_nEtaLayers.clear();
  m_trk_vx.clear();
  m_trk_vy.clear();
  m_trk_vz.clear();

  //DV Vars
  m_dv_x.clear();
  m_dv_y.clear();
  m_dv_z.clear();
  m_dv_r.clear();
  m_dv_l.clear();
  m_dv_m.clear();
  m_dv_charge.clear();
  m_dv_openAng.clear();
  m_dv_distV.clear();
  m_dv_muIndexLead.clear();
  m_dv_muIndexSub.clear();

}
