#include "../DisplacedDimuonAnalysis/HistogramTool.h"
#include "TDirectory.h"

HistogramTool::HistogramTool(std::vector<TH1F *>& histVect) : m_currentHist(0), m_histVect(histVect) {}

void HistogramTool::fillHist(string hname, string title, int nbinsx, Float_t xlow,Float_t xhigh,Float_t x, Float_t xw){
  TH1F* hist(NULL);

  std::map< string, TH1F*>::iterator hiter_found = m_histMap.find(hname.c_str());

  if (hiter_found == m_histMap.end()){
    hist = m_histVect[m_currentHist];
    hist->SetNameTitle(hname.c_str(),title.c_str());
    hist->SetBins(nbinsx,xlow,xhigh);
    m_currentHist++;
    m_histMap[hname.c_str()] = hist;
    hiter_found = m_histMap.find(hname.c_str());
  }

  hist = (hiter_found)->second;
  hist->Fill(x,xw);
  return;

}
