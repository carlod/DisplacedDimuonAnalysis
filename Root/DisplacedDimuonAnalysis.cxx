#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DisplacedDimuonAnalysis/DisplacedDimuonAnalysis.h>
#include <TFile.h>
#include "xAODMuon/MuonContainer.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "xAODRootAccess/TStore.h"
#include "xAODMissingET/MissingETContainer.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "TrigDecisionTool/Feature.h"
#include "TrigDecisionTool/FeatureContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthVertexAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include <algorithm>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "TDirectory.h"
#include <TSystem.h>

#include "Math/SVector.h"
#include "Math/SMatrix.h"

ClassImp(DisplacedDimuonAnalysis)

DisplacedDimuonAnalysis :: DisplacedDimuonAnalysis () {
}

EL::StatusCode DisplacedDimuonAnalysis :: setupJob (EL::Job& job)
{
  xAOD::TFileAccessTracer::enableDataSubmission(false);
  job.useXAOD ();
  xAOD::Init( "DisplacedDimuonAnalysis" ).ignore();
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DisplacedDimuonAnalysis :: histInitialize ()
{return EL::StatusCode::SUCCESS;}

EL::StatusCode DisplacedDimuonAnalysis :: fileExecute () {

  xAOD::TEvent* m_event = wk()->xaodEvent();

  TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("execute()", "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);

  TString xStream=""; //identify DxAOD flavour
  bool m_isDerivation = !MetaData->GetBranch("StreamAOD");

  if(m_isDerivation) {

    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    if(!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
      Error("execute()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
    }

    const xAOD::CutBookkeeper* allEventsCBK = 0;
    int maxCycle = -1;
    for (const auto& cbk: *completeCBC) {
      std::cout << cbk->nameIdentifier() << " : " << cbk->name() << " : desc = " << cbk->description()
                << " : inputStream = " << cbk->inputStream()  << " : outputStreams = " << (cbk->outputStreams().size() ? cbk->outputStreams()[0] : "")
                << " : cycle = " << cbk->cycle() << " :  allEvents = " << cbk->nAcceptedEvents()
                << std::endl;

      if ( cbk->name() == "AllExecutedEvents" && TString(cbk->inputStream()).Contains("StreamDAOD")){
        xStream = TString(cbk->inputStream()).ReplaceAll("Stream","");
        std::cout << "xStream = " << xStream << "  (i.e. identified DxAOD flavour)" << std::endl;
      }
      if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
        allEventsCBK = cbk;
        maxCycle = cbk->cycle();
      }
    }

    double sumOfWeights        = allEventsCBK->sumOfEventWeights();
    m_filledSumWeights = false;
    m_sumWeights = sumOfWeights;

  }

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode DisplacedDimuonAnalysis :: changeInput (bool /**firstFile*/) {return EL::StatusCode::SUCCESS;}

EL::StatusCode DisplacedDimuonAnalysis :: initialize ()
{

  std::cout << "the current sample name is " << currentSampleName << std::endl;

  m_event = wk()->xaodEvent();

  m_doTrigList = false;
  m_evt_num = 0;
  m_debug = false;
  m_filledSumWeights = false;
  m_doMSonlyMuonMomentumCalibration = false;

  //Muon Calibration and Smearing Tool
  m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool( "MuonCorrectionTool" );
  if (! m_muonCalibrationAndSmearingTool->initialize().isSuccess() ){
    Error("initialize()", "Failed to properly initialize the MonCalibrationAndSmearingTool Tool. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  //Muon Selection Tool
  m_muonSelectionTool = new CP::MuonSelectionTool( "MuonSelectionTool" );
  if (! m_muonSelectionTool->initialize().isSuccess() ){
    Error("initialize()", "Failed to properly initialize the MuonSelection Tool. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  //PRW
  std::vector<std::string> configFiles;
  std::vector<std::string> tmpPRWStringVec {"MC15C.All"};
  for (unsigned int i=0;i<tmpPRWStringVec.size();i++){
    std::string tmpS = "/data2/nrbern/PRWMC15c/" + tmpPRWStringVec[i] + ".prw.root";
    configFiles.push_back(tmpS);
  }
  //std::vector<std::string> lumicalcFiles = {"/data2/nrbern/PRWMC15c/ilumicalc_histograms_None_300345-302393_OflLumi-13TeV-005.root"};
  std::vector<std::string> lumicalcFiles = {"/data2/nrbern/PRWMC15c/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-008.root"}; 

  m_prwTool = new CP::PileupReweightingTool( "prw");
  m_prwTool->setProperty("ConfigFiles",configFiles);
  m_prwTool->setProperty("LumiCalcFiles",lumicalcFiles);
  m_prwTool->setProperty("DataScaleFactor",1.0/1.09);
  m_prwTool->setProperty("DataScaleFactorUP",1.0/1.0);
  m_prwTool->setProperty("DataScaleFactorDOWN",1.0/1.18);
  m_prwTool->initialize();

  // GRL
  m_grlTool = new GoodRunsListSelectionTool( "GoodRunsListSelectionTool" );
  const char* GRLFilePath ="/data2/nrbern/GRL/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
  const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(fullGRLFilePath);
  m_grlTool->setProperty( "GoodRunsListVec", vecStringGRL);
  m_grlTool->setProperty( "PassThrough", false); 
  m_grlTool->initialize();

  // JVT
  m_pjvtag = new JetVertexTaggerTool("jvtag");
  m_hjvtagup = ToolHandle<IJetUpdateJvt>("jvtag");
  m_pjvtag->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root");
  if (!m_pjvtag->initialize().isSuccess() ){
    Error("initialize()","Failed to properly initialize the JetVertexTagger Tool. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  // Jet Cleaning
  m_cleaningTool = new JetCleaningTool("JetCleaningTool");
  m_cleaningTool->setProperty("CutLevel","LooseBad");
  m_cleaningTool->setProperty("DoUgly",false);
  if (!m_cleaningTool->initialize().isSuccess() ){
    Error("initialize()","Failed to properly initialize the Jet Cleaning Tool. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  //JER
  m_JERTool = new JERTool("JERTool");
  m_JERTool->setProperty("PlotFileName","JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root");
  m_JERTool->setProperty("CollectionName","AntiKt4EMTopoJets");  
  m_JERTool->initialize();
  //JER Smearing
  const xAOD::EventInfo* eventInfo = 0;
  if( !m_event->retrieve( eventInfo, "EventInfo").isSuccess() ){
    std::cout << "Failed to retrieve EventInfo collection." << std::endl;
  }
  bool isMC = false;
  if( eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) isMC = true;
  m_JERSmearingTool = new JERSmearingTool("JERSmearingTool");
  m_jerHandle = ToolHandle<IJERTool> ("JERSmearingTool");
  m_JERSmearingTool->setProperty("JERTool",m_jerHandle);
  m_JERSmearingTool->setProperty("ApplyNominalSmearing",false);
  if (isMC) m_JERSmearingTool->setProperty("isMC",true);
  else m_JERSmearingTool->setProperty("isMC",false);
  m_JERSmearingTool->setProperty("SystematicMode","Simple");
  m_JERSmearingTool->initialize();

  //JES
  const std::string name = "JetCalibrationTool";
  TString jetAlgo = "AntiKt4EMTopo";
  TString config = "JES_data2016_data2015_Recommendation_Dec2016.config";
  TString calibSeq = "JetArea_Residual_Origin_EtaJES_GSC_Insitu";
  bool isData = true;
  if (isMC) isData = false;
  //m_jetCalibTool = new JetCalibrationTool(name,jetAlgo,config,calibSeq,isData);
  m_jetCalibTool = new JetCalibrationTool(name);
  m_jetCalibTool->setProperty("JetCollection",jetAlgo.Data());
  m_jetCalibTool->setProperty("ConfigFile",config.Data());
  m_jetCalibTool->setProperty("CalibSequence",calibSeq.Data());
  m_jetCalibTool->setProperty("IsData",isData);
  m_jetCalibTool->initialize();

  //Trigger Tools
  m_configTool = new TrigConf::xAODConfigTool("xAODConfigTool");
  ToolHandle<TrigConf::ITrigConfigTool> configHandle(m_configTool);
  configHandle->initialize();

  m_trigDecTool = new Trig::TrigDecisionTool("TrigDecTool");
  m_trigDecTool->setProperty("ConfigTool",configHandle);
  m_trigDecTool->setProperty("TrigDecisionKey","xTrigDecision");
  m_trigDecTool->initialize();
  ToolHandle<Trig::TrigDecisionTool> m_trigDec(m_trigDecTool);

  //User Tools
  m_vertexTool = new VertexTool();
  m_containerTool = new ContainerTool();
 
  //NtupleTool
  m_outputFile = wk()->getOutputFile(outputName);
  m_ntupleTool = new NtupleTool(m_outputFile);
  
  //Print number of events
  Info("initialize()", "Number of events = %lli", m_event->getEntries() ); 

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DisplacedDimuonAnalysis :: execute ()
{

  //initialization
  xAOD::TStore store; 

  //setup event counting
  if (m_evt_num == 0 ) std::cout << " Starting first event" << std::endl;
  if (m_evt_num % 1000 == 0) std::cout << "event number " << m_evt_num << std::endl;
  m_evt_num++;

  //get event containers
  if( !m_containerTool->isGood(m_event) ) return EL::StatusCode::FAILURE;

  //Set Event Vars
  unsigned long long tmpEvtNum = m_containerTool->m_eventInfo->eventNumber();
  uint32_t tmpRunNum = m_containerTool->m_eventInfo->runNumber();
  uint32_t tmpBCID = m_containerTool->m_eventInfo->bcid();
  uint32_t tmpLB = m_containerTool->m_eventInfo->lumiBlock();

  float tmpEvtWeight(1.0);
  if (m_containerTool->m_isMC){ 
    const std::vector<float> weights = m_containerTool->m_eventInfo->mcEventWeights();
    tmpEvtWeight = (!weights.empty()) ? weights[0] : (float) 1.0;
  }

  float tmpSumWeights = 0.0;
  if (m_containerTool->m_isMC && !m_filledSumWeights){
    tmpSumWeights = m_sumWeights;
    m_filledSumWeights = true;
  }

  float tmpPRW = (m_containerTool->m_isMC) ? m_prwTool->getCombinedWeight(*m_containerTool->m_eventInfo) : (float) 1.0;
  float tmpMET = 0.0;
  float tmpMETPhi = 0.0;
  m_ntupleTool->setEvtVars(tmpEvtNum,tmpRunNum,tmpBCID,tmpLB,tmpEvtWeight,tmpSumWeights,tmpPRW,tmpMET,tmpMETPhi);

  //Event cleaning (Data Only)
  if (!m_containerTool->m_isMC) {
  
    //GRL
    if (!m_grlTool->passRunLB(*(m_containerTool->m_eventInfo))){
      std::cout << "GRL TOOL: event rejected" << std::endl;
      return EL::StatusCode::SUCCESS; 
    }
    
    //TileCal problem check
    if (m_containerTool->m_eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) {
      std::cout << "TileCal problem: event rejected" << std::endl;
      return EL::StatusCode::SUCCESS;
    }
    //Lar problem check
    if (m_containerTool->m_eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) {
      std::cout << "LAr problem: event rejected" << std::endl;
      return EL::StatusCode::SUCCESS;
    }
    //SCT corruption check
    if (m_containerTool->m_eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) {
      std::cout << "SCT corruption: event rejected" << std::endl;
      return EL::StatusCode::SUCCESS;
    }
    //Incomplete event check
    if (m_containerTool->m_eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core,18)) {
      std::cout << "Incomplete event: event rejected" << std::endl;
      return EL::StatusCode::SUCCESS;
    }
  }

  //Process Trigger
  triggerSelection(); 
  m_ntupleTool->setTrigDec(m_trigMap);

  //process ID tracks
  const xAOD::TrackParticleContainer* IDtrks = m_containerTool->inDetTrackParticles();
  for (const auto tmpID1: *IDtrks) {
    if (tmpID1->pt() *0.001 < 10) continue;
    if (!tmpID1->vertex()) {
      m_ntupleTool->addIDTrk(tmpID1,-1);
    } else {
      m_ntupleTool->addIDTrk(tmpID1,(int) tmpID1->vertex()->vertexType());
    }
  }

  //Process Truth 
  int numTruthVtx = (m_containerTool->m_isMC) ? processTruth() : 0;

  //Process MSTP
  std::vector<std::shared_ptr<AO::Muon>> MSTPMuons = processMSTrackParticles();
  
  //Process Vertices
  int numRecVtx = (!MSTPMuons.empty()) ? processVertices(MSTPMuons) : false;
  
  //Set Vertex Vars
  TVector3 tmpTVtx = {0,0,0};
  float tmpPVz(0.0);
  int tmpNTrkPV(0);
  int tmpNPUVtx(0);
  const xAOD::VertexContainer *PVContainer = m_containerTool->PV();
  const xAOD::Vertex* pv(NULL);
  if(PVContainer) {
    if(PVContainer->size() > 0) {
      for(const xAOD::Vertex* vx: *PVContainer) {
        if (vx->vertexType() == xAOD::VxType::PileUp) tmpNPUVtx++;
        else if(vx->vertexType() == xAOD::VxType::PriVtx) pv = vx;
      }
    }
  }
  if(pv) {
    tmpPVz = pv->z();
    tmpNTrkPV = pv->nTrackParticles();
  }
  m_ntupleTool->setEvtVtxVars(tmpPVz,tmpNTrkPV,tmpNPUVtx,MSTPMuons.size(),numRecVtx,numTruthVtx);

  //Fill Ntuple Tool
  m_ntupleTool->Fill();
  m_ntupleTool->Clear();

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode DisplacedDimuonAnalysis :: postExecute ()
{
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode DisplacedDimuonAnalysis :: finalize ()
{

  if (m_doTrigList){
    std::cout << "Final trigger tally" << std::endl;
    for (auto &i : m_triggerCounts) {
      std::cout << "  " << i.first << ": " << i.second << " times" << std::endl;
    }
  }

  if(m_muonCalibrationAndSmearingTool){
    delete m_muonCalibrationAndSmearingTool;
    m_muonCalibrationAndSmearingTool = 0;
  }

  if(m_muonSelectionTool){
    delete m_muonSelectionTool;
    m_muonSelectionTool = 0;
  }

  if(m_prwTool){
    delete m_prwTool;
    m_prwTool = 0;
  }

  if (m_grlTool) {
    delete m_grlTool;
    m_grlTool = 0;
  }

  if (m_pjvtag) {
    delete m_pjvtag;
    m_pjvtag = 0;
  }

  if (m_cleaningTool) {
    delete m_cleaningTool;
    m_cleaningTool = 0;
  }

  if (m_JERTool) {
    delete m_JERTool;
    m_JERTool = 0;
  }

  if (m_JERSmearingTool) {
    delete m_JERSmearingTool;
    m_JERSmearingTool = 0;
  }
 
  if (m_jetCalibTool) {
    delete m_jetCalibTool;
    m_jetCalibTool = 0;
  }

  if(m_configTool){
    delete m_configTool;
    m_configTool = 0;
  }

  if(m_trigDecTool){
    delete m_trigDecTool;
    m_trigDecTool = 0;
  }
 
  if(m_vertexTool){
    delete m_vertexTool;
    m_vertexTool = 0;
  }

  if(m_containerTool){
    delete m_containerTool;
    m_containerTool = 0;
  }

  if(m_ntupleTool){
    delete m_ntupleTool;
    m_ntupleTool = 0;
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DisplacedDimuonAnalysis :: histFinalize ()
{return EL::StatusCode::SUCCESS;}

void DisplacedDimuonAnalysis::triggerSelection() {

  //Init TrigMap
  m_trigMap = {{"narrowScan",0},{"MET",0},{"singleMu",0},{"threeMu",0},{"L1_MU20",0},{"L1_3MU6",0}};

  //emulate HLT_mu20_msonly_mu15noL1_msonly_nscan05_noComb trigger
  bool PassLJTrig(false);
  if (m_containerTool->m_isMC) {
    PassLJTrig = passLJTrig("HLT_mu20_msonly_mu6noL1_msonly_nscan05_noComb");
  } else {
    PassLJTrig = passLJTrig("HLT_mu20_msonly_mu10noL1_msonly_nscan05_noComb");
    if (!PassLJTrig) {
      PassLJTrig = passLJTrig("HLT_mu20_msonly_mu15noL1_msonly_nscan05_noComb");
    }
  }
  m_trigMap["narrowScan"] = (int) PassLJTrig;

  //emulate HLT_xe110_mht_L1XE50 trigger
  const xAOD::EnergySumRoI* l1MetObject = m_containerTool->LVL1EnergySumRoI();
  float l1_mex = l1MetObject->exMiss();
  float l1_mey = l1MetObject->eyMiss();

  float l1_met(0.);
  bool l1_overflow(false);
  calcL1MET(l1_mex,l1_mey, l1_met, l1_overflow);

  float met_threshold = 50. * 1000.;
  bool passL1_XE50 = l1_overflow || l1_met > met_threshold;

  float mht_Met(0.0);
  float mht_MetPhi(0.0);
  const xAOD::TrigMissingETContainer* mhtCont = m_containerTool->HLT_MET_mht();
  if (!mhtCont->empty()) {
    float mht_ex = mhtCont->front()->ex();
    float mht_ey = mhtCont->front()->ey();
    mht_Met = sqrt(mht_ex*mht_ex + mht_ey*mht_ey);
    mht_MetPhi = atan2(mht_ey,mht_ex);
  }

  bool passMETTrig = m_trigDecTool->isPassed("HLT_xe80_mht_L1XE50");
  passMETTrig = (!passMETTrig) ? m_trigDecTool->isPassed("HLT_xe90_mht_L1XE50") : true; 
  passMETTrig = (!passMETTrig) ? m_trigDecTool->isPassed("HLT_xe100_mht_L1XE50") : true;
  passMETTrig = (!passMETTrig) ? m_trigDecTool->isPassed("HLT_xe110_mht_L1XE50") : true;

  m_trigMap["MET"] = (int) (passMETTrig && passL1_XE50);

  //construct offline mht
  float mht_x(0.0),mht_y(0.0),mht(0.0);
  float mht_phi(0.0);
  const xAOD::JetContainer* jetCont = m_containerTool->jets();
  if (!jetCont->empty()) {
    for (unsigned int i=0;i<jetCont->size();i++){
      const xAOD::Jet* jet = (*jetCont)[i];
      if (jet->pt() < 20. * 1000.) continue;
      mht_x += jet->px();
      mht_y += jet->py();
    }
    mht = sqrt(pow(mht_x,2)+pow(mht_y,2));
    mht_phi = TVector2::Phi_mpi_pi(atan2(mht_y,mht_x) + TMath::Pi());
  }

  //fill ntuple MET vars
  m_ntupleTool->addMET(mht_Met,mht_MetPhi,mht,mht_phi);

  //Other Triggers
  m_trigMap["singleMu"] = (int) m_trigDecTool->isPassed("HLT_mu60_0eta105_msonly");
  m_trigMap["threeMu"] = (int) m_trigDecTool->isPassed("HLT_3mu6_msonly");
  m_trigMap["L1_MU20"] = (int) m_trigDecTool->isPassed("L1_MU20");
  m_trigMap["L1_3MU6"] = (int) m_trigDecTool->isPassed("L1_3MU6");

  //do muon trigger matching
  const xAOD::MuonContainer* muCont = m_containerTool->muons();
  for (const auto mu : *muCont) {
    if (m_trigMap["singleMu"] == 1) {
    }
  }

   /*

  const std::string chain("L1_2MU10");
  Trig::FeatureContainer f = m_trigDecTool->features(chain);

  //const std::string chain("HLT_mu60_0eta105_msonly");
  //const std::string chain("HLT_mu20_msonly_mu6noL1_msonly_nscan05_noComb");
  //Trig::FeatureContainer f = m_trigDecTool->features(chain);

  std::vector< Trig::Feature<xAOD::MuonContainer> > muColls = f.containerFeature<xAOD::MuonContainer>();
  if(muColls.size()>0) {
    const Trig::Feature<xAOD::MuonContainer>& mcf = muColls[0];
    const xAOD::MuonContainer* mc = mcf.cptr();
    xAOD::MuonContainer::const_iterator mIt = mc->begin();
    for (;mIt != mc->end(); ++mIt ) {
      const xAOD::Muon* muon = *mIt;
      std::cout << "(pt,phi,eta): (" << muon->pt()*0.001 << "," << muon->phi() << "," << muon->eta() << ")" << std::endl;
    }
  } else std::cout << "L1_2MU10 feature container empty" << std::endl;
*/


  //Fill Trigger List
  if(m_doTrigList){
    auto chainGroup = m_trigDecTool->getChainGroup(".*");
    for(auto &trig : chainGroup->getListOfTriggers()) {
      auto cg = m_trigDecTool->getChainGroup(trig);
      if (cg->isPassed()) {
        m_triggerCounts[trig]++;
      } else {
        m_triggerCounts[trig] += 0;
      }
    }
  }
 
}

int DisplacedDimuonAnalysis::processTruth(){
  //initialization
  std::vector<const xAOD::TruthParticle*> muVtx;
  bool foundTPVtx(false);
  int numTruthVtx(0);

  // Start loop over all truth vertices, then find the ones we are interested in
  const xAOD::TruthVertexContainer* truthVertices = m_containerTool->TruthVertices();
  for (const auto truthVtx: *truthVertices){

    //check if this is the true primary vertex and if so add to ntuple
    if (AnalysisTool::isTPVtx(truthVtx) && !foundTPVtx) {
      TVector3 TPVtx = {truthVtx->x(),truthVtx->y(),truthVtx->z()};
      m_ntupleTool->setTPV(TPVtx);
      foundTPVtx = true;
    }

    //clear muVtx vector
    for (auto it = muVtx.begin();it != muVtx.end(); ++it) { delete *it;}
    muVtx.clear();
            
    // get the pointer to the TruthParticle parent of the vertex
    const xAOD::TruthParticle* incoming = truthVtx->incomingParticle(0);
    // require that the vertex has a single parent that is either a SM Z boson, PDG = 23, or a dark photon/dark Z (Z_D), PDG = 1023 (or sometimes 32), and at least two daughters
    if (truthVtx->nIncomingParticles() != 1 ) continue;
    if ((incoming->absPdgId() != 23) && (incoming->absPdgId() != 1023) && (incoming->absPdgId() != 32)) continue;
    if (incoming->nChildren() < 2) continue;
    numTruthVtx++;
    
    // Set the LLP (PDG ID = 6000006) or NLSP (PDG ID = 1000022).
    const xAOD::TruthParticle* incomingParent =  AnalysisTool::getMuonParent(incoming); 
    const xAOD::TruthParticle* longLivedParticle(NULL);
    if (incomingParent) {
      if (incomingParent->absPdgId() == 6000006 || incomingParent->absPdgId() == 1000022) longLivedParticle = incomingParent;
      else longLivedParticle = incoming;
    } else longLivedParticle = incoming;

    //write the vertex children to ntuple
    for (unsigned int i = 0;i < incoming->nChildren();i++) {
      xAOD::TruthParticle* tMu = new xAOD::TruthParticle();
      float tmpD0 = AnalysisTool::d0(incoming->child(i));
      float tmpZ0 = AnalysisTool::z0(incoming->child(i));
      tMu->makePrivateStore(incoming->child(i));
      m_ntupleTool->addTruthMuon(tMu,tmpD0,tmpZ0,incoming->pdgId(),incoming->barcode());
      muVtx.push_back(tMu);
    }
    
    float vx = truthVtx->x();
    float vy = truthVtx->y();
    float vz = truthVtx->z();
    float openAng = AnalysisTool::openAngle(muVtx[0],muVtx[1]);
    float invMass = AnalysisTool::invMass(muVtx[0],muVtx[1]);

    // sign the vertex according to the angle between the flight vector and the momentum vector of the Z/Z_D
    float sgnT(1.);
    if (vx*incoming->px()+vy*incoming->py() < 0 ) sgnT = -1.;

    //prepare track parameters for truth vertex
    std::vector<std::pair<float,float>> params;
    params.push_back( std::make_pair(AnalysisTool::d0(muVtx[0]),AnalysisTool::z0(muVtx[0])));
    params.push_back( std::make_pair(AnalysisTool::d0(muVtx[1]),AnalysisTool::z0(muVtx[1]))); 
      
    //determine lead muon index
    int iLead = muVtx[0]->pt() > muVtx[1]->pt() ? 0 : 1;
    int iSub = muVtx[0]->pt() > muVtx[1]->pt() ? 1 : 0;
    
    //check if vertex passes LJ trigger
    const std::string chain("HLT_mu20_msonly_mu6noL1_msonly_nscan05_noComb");
    Trig::FeatureContainer f = m_trigDecTool->features(chain);
    std::vector< Trig::Feature<xAOD::MuonContainer> > muColls = f.containerFeature<xAOD::MuonContainer>();
    int LJMatch(0);
    if(muColls.size()>0) {
      const Trig::Feature<xAOD::MuonContainer>& mcf = muColls[0];
      const xAOD::MuonContainer* mc = mcf.cptr();
      xAOD::MuonContainer::const_iterator mIt = mc->begin();
      float minLDR(9999.),minSDR(9999.);
      for (;mIt != mc->end(); ++mIt ) {
        const xAOD::Muon* muon = *mIt;
        float tmpLDR = AnalysisTool::deltaR(muVtx[iLead],muon);
        float tmpSDR = AnalysisTool::deltaR(muVtx[iSub],muon);
        minLDR = tmpLDR < minLDR ? tmpLDR : minLDR;
        minSDR = tmpSDR < minSDR ? tmpSDR : minSDR;
      }
      if (minLDR < 0.1 && minSDR < 0.1) LJMatch = 1;
      /*
      if (openAng > 0.5 && LJMatch == 1){
        std::cout << "openAngTrue: " << openAng << " openAngTrig: " << AnalysisTool::openAngle((*mc)[0],(*mc)[1]) << std::endl;
        for (unsigned int f=0;f<mc->size();f++){
          std::cout << "mu" << f << "Trig(" << (*mc)[f]->pt() * 0.001 << "," << (*mc)[f]->eta() << "," << (*mc)[f]->phi() << ")" << std::endl;
        }
        std::cout << "mu1Truth(" << muVtx[0]->pt() * 0.001 << "," << muVtx[0]->eta() << "," << muVtx[0]->phi() << ")" << std::endl;
        std::cout << "mu2Truth(" << muVtx[1]->pt() * 0.001 << "," << muVtx[1]->eta() << "," << muVtx[1]->phi() << ")" << std::endl;
      } 
      */
    }

    //check if vertex passes mu60 trigger 
    const std::string chain2("HLT_mu60_0eta105_msonly");
    Trig::FeatureContainer f2 = m_trigDecTool->features(chain2);
    std::vector< Trig::Feature<xAOD::MuonContainer> > muColls2 = f2.containerFeature<xAOD::MuonContainer>();
    int mu60Match(0);
    if(muColls2.size()>0) {
      const Trig::Feature<xAOD::MuonContainer>& mcf = muColls2[0];
      const xAOD::MuonContainer* mc = mcf.cptr();
      xAOD::MuonContainer::const_iterator mIt = mc->begin();
      /*
      if (muVtx[0]->pt() * 0.001 < 60. && muVtx[1]->pt() * 0.001 < 60.) {
        for (unsigned int f=0;f<mc->size();f++){
          std::cout << "mu" << f << "Trig(" << (*mc)[f]->pt() * 0.001 << "," << (*mc)[f]->eta() << "," << (*mc)[f]->phi() << ")" << std::endl;
        }
        std::cout << "mu1Truth(" << muVtx[0]->pt() * 0.001 << "," << muVtx[0]->eta() << "," << muVtx[0]->phi() << ")" << std::endl;
        std::cout << "mu2Truth(" << muVtx[1]->pt() * 0.001 << "," << muVtx[1]->eta() << "," << muVtx[1]->phi() << ")" << std::endl;
      }
      */
      float minLDR(9999.);
      for (;mIt != mc->end(); ++mIt ) {
        const xAOD::Muon* muon = *mIt;
        float tmpLDR = AnalysisTool::deltaR(muVtx[iLead],muon);
        minLDR = tmpLDR < minLDR ? tmpLDR : minLDR;
      }
      if (minLDR < 0.1) {
        //std::cout << "was a match" << std::endl;
        mu60Match = 1;
      }
    }
   
    //Make a truth vertex object and then add to nTuple
    TruthVertex tmpVtx = TruthVertex(muVtx,params,TVector3(vx,vy,vz),sgnT,openAng,invMass,incoming->pdgId(),incoming->barcode(),iLead,iSub);
    m_ntupleTool->addTruthVtx(tmpVtx,longLivedParticle,LJMatch,mu60Match);

  } //end truthVtx loop

  for (auto it = muVtx.begin();it != muVtx.end(); ++it) { delete *it;}
  return numTruthVtx;
}

std::vector<std::shared_ptr<AO::Muon>> DisplacedDimuonAnalysis::processMSTrackParticles(){

  //identify BIB muons
  float BIBphi1(999.),BIBphi2(999.);
  const xAOD::MuonSegmentContainer* muSeg = m_containerTool->muonSegments();
  for (const auto tmpSeg: *muSeg){
    float r1 = 0.1*sqrt(pow(tmpSeg->x(),2)+pow(tmpSeg->y(),2));
    float phi1 = atan2(tmpSeg->y(),tmpSeg->x());
    float thetaDir1 = atan2(sqrt(pow(tmpSeg->px(),2)+pow(tmpSeg->py(),2)),tmpSeg->pz());
    float thetaPos1 = atan2(sqrt(pow(tmpSeg->x(),2)+pow(tmpSeg->y(),2)),tmpSeg->z()); 
    float etaPos1 = -log(tan(thetaPos1/2.0));
    if (fabs(thetaPos1-thetaDir1) < 0.087222) continue; //if less than 5 degrees continue
    for (const auto tmpSeg2: *muSeg){
      float r2 = 0.1*sqrt(pow(tmpSeg2->x(),2)+pow(tmpSeg2->y(),2));
      float phi2 = atan2(tmpSeg2->y(),tmpSeg2->x());
      float thetaDir2 = atan2(sqrt(pow(tmpSeg2->px(),2)+pow(tmpSeg2->py(),2)),tmpSeg2->pz());
      float thetaPos2 = atan2(sqrt(pow(tmpSeg2->x(),2)+pow(tmpSeg2->y(),2)),tmpSeg2->z());
      float etaPos2 = -log(tan(thetaPos2/2.0));
      if (fabs(thetaPos2-thetaDir2) < 0.087222) continue; //if less than 5 degrees continue
      if (fabs(tmpSeg->t0() - tmpSeg2->t0()) < 25) continue;
      if (fabs(r1-r2) > 20.) continue;
      if (fabs(phi1) < 3.0 && fabs(phi1) > 0.1) continue;
      if (fabs(phi2) < 3.0 && fabs(phi2) > 0.1) continue;
      if (fabs(etaPos1) < 1.0 || fabs(etaPos2) < 1.0) continue;
      float zSign1 = std::copysign(1.0,tmpSeg->z());
      float zSign2 = std::copysign(1.0,tmpSeg2->z());
      if (zSign1 == zSign2) continue;
      BIBphi1 = phi1;
      BIBphi2 = phi2;
    }
  }
  
  //Start by processing combined muons 
  typedef ROOT::Math::SMatrix<double,5,5,ROOT::Math::MatRepSym<double,5> > SMatrixSym5;
  typedef ROOT::Math::SVector<double,5> SVector5;
  const xAOD::MuonContainer* CombMuons = m_containerTool->muons();
  if (CombMuons) {
    for (const auto muon: *CombMuons){
      float combPt = muon->pt();
      if (combPt < 3.0 * 1000.0 || combPt > 6500. * 1000.0) continue;

      const xAOD::TrackParticle * id = muon->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);
      if (!id) continue;
      const xAOD::TrackParticle * me = muon->trackParticle(xAOD::Muon::TrackParticleType::ExtrapolatedMuonSpectrometerTrackParticle);     
      if (!me) continue;

      //get ID cov and TP
      xAOD::ParametersCovMatrix_t cM = id->definingParametersCovMatrix();
      ROOT::Math::SVector<double,15> vID;
      vID[0] = cM(0,0);
      vID[1] = cM(1,0);
      vID[2] = cM(1,1);
      vID[3] = cM(2,0);
      vID[4] = cM(2,1);
      vID[5] = cM(2,2);
      vID[6] = cM(3,0);
      vID[7] = cM(3,1);
      vID[8] = cM(3,2);
      vID[9] = cM(3,3);
      vID[10] = cM(4,0);
      vID[11] = cM(4,1);
      vID[12] = cM(4,2);
      vID[13] = cM(4,3);
      vID[14] = cM(4,4);
      SMatrixSym5 covID(vID);

      SVector5 tpID;
      tpID[0] = id->d0();
      tpID[1] = id->z0();
      tpID[2] = id->phi0();
      tpID[3] = id->theta();
      tpID[4] = id->qOverP();

      //get cov and TP for me track
      cM = me->definingParametersCovMatrix();
      ROOT::Math::SVector<double,15> vME;
      vME[0] = cM(0,0);
      vME[1] = cM(1,0);
      vME[2] = cM(1,1);
      vME[3] = cM(2,0);
      vME[4] = cM(2,1);
      vME[5] = cM(2,2);
      vME[6] = cM(3,0);
      vME[7] = cM(3,1);
      vME[8] = cM(3,2);
      vME[9] = cM(3,3);
      vME[10] = cM(4,0);
      vME[11] = cM(4,1);
      vME[12] = cM(4,2);
      vME[13] = cM(4,3);
      vME[14] = cM(4,4);
      SMatrixSym5 covME(vME);

      SVector5 tpME;
      tpME[0] = me->d0();
      tpME[1] = me->z0();
      tpME[2] = me->phi0();
      tpME[3] = me->theta();
      tpME[4] = me->qOverP();
 
      SMatrixSym5 covSum = covID + covME;
      int ifail(0);
      SMatrixSym5 invCovSum = covSum.Inverse(ifail);
      if (ifail != 0) continue;

      double diffPhi = tpME[2] - tpID[2];
      if (diffPhi>M_PI) tpME[2] -= 2.*M_PI;
      if (diffPhi<-1.*M_PI) tpME[2] += 2.*M_PI;

      SVector5 diffPars = tpID - tpME;
      double chi2 = ROOT::Math::Similarity(diffPars,invCovSum);

      float combIsoDPV = doMuonIso(muon,true,true);
      float combIsoDAll = doMuonIso(muon,true,false);
      float combIsoIPV = doMuonIso(muon,false,true);
      float combIsoIAll = doMuonIso(muon,false,false);
      float combJMOR = doJetMuOverlap(muon);
      const xAOD::TrackParticle * combTP = muon->primaryTrackParticle();
      float combD0 = (combTP) ? combTP->d0() : -999.0;
      float combZ0 = (combTP) ? combTP->z0() : -999.0;

      //truth info for combined muon
      int combPDG(-1),combBarCode(-1);
      const xAOD::TruthParticle* combTruthMu = (m_containerTool->m_isMC && muon) ? AnalysisTool::truthLink(muon) : NULL;
      if (combTruthMu) {
        combPDG = combTruthMu->pdgId();
        combBarCode = combTruthMu->barcode();
      } 

      m_ntupleTool->addCombMuon(combPt,muon->phi(),muon->eta(),combD0,combZ0,combIsoDPV,combIsoDAll,combIsoIPV,combIsoIAll,combJMOR,(int) muon->author(),chi2,combPDG,combBarCode);      
    }  //comb muon for loop
  }  // if comb muons exist

  //process MuSA tracks
  const xAOD::TrackParticleContainer* trkColl = m_containerTool->MSTrackParticles();
  std::vector<std::shared_ptr<AO::Muon>> TPMuons;
  int nMuSA(0);

  for (const auto tmpMu: *trkColl){
    //make sure pt measurement is valid
    if (tmpMu->pt() > 6500. * 1000.) continue;

    //BIB removal
    bool isBIB(false);
    if (BIBphi1 < 999. && (fabs(tmpMu->phi()) > 3.0 || fabs(tmpMu->phi()) < 0.1) && fabs(tmpMu->eta()) > 1) isBIB = true;
    if (isBIB) continue;

    //remove const
    xAOD::TrackParticle* origMu = const_cast<xAOD::TrackParticle*>(tmpMu);
    //copy aux store object
    xAOD::TrackParticle MuObj = *origMu;
    //get pointer to copied muon TP
    xAOD::TrackParticle* Mu = &MuObj;

    double d0IP = Mu->auxdata< double >("d0IP");
    double z0IP = Mu->auxdata< double >("z0IP");
    double phiIP = Mu->auxdata< double >("phiIP");
    double thetaIP = Mu->auxdata< double >("thetaIP");
    double qOverPIP = Mu->auxdata< double >("qOverPIP");
    Mu->setDefiningParameters(d0IP,z0IP,phiIP,thetaIP,qOverPIP);
    Mu->setParametersOrigin(0.,0.,0.);  

    xAOD::ParametersCovMatrix_t cM = Mu->definingParametersCovMatrix();
    std::vector<float> covVec; 
    covVec.push_back(Mu->auxdata< double >("d0CovIP"));
    covVec.push_back(cM(1,0));
    covVec.push_back(Mu->auxdata< double >("z0CovIP"));
    covVec.push_back(cM(2,0));
    covVec.push_back(cM(2,1));
    covVec.push_back(Mu->auxdata< double >("phiCovIP"));
    covVec.push_back(cM(3,0));
    covVec.push_back(cM(3,1));
    covVec.push_back(cM(3,2));
    covVec.push_back(Mu->auxdata< double >("thetaCovIP"));
    covVec.push_back(cM(4,0));
    covVec.push_back(cM(4,1));
    covVec.push_back(cM(4,2));
    covVec.push_back(cM(4,3));
    covVec.push_back(Mu->auxdata< double >("qOverPCovIP"));
    Mu->setDefiningParametersCovMatrixVec(covVec);

    //initial track selection
    if (Mu->pt() < 3. * 1000.) continue;    
    if (Mu->charge() == 0 ) continue;
    //minimum number of trigger hits requirement (phi,eta)
    if (!AnalysisTool::layerRequirement(Mu,3,3)) continue;
    nMuSA++;
 
    //get Iso and muJetOR variables
    float isoDPV = doMuonIso(Mu,true,true);
    float isoDAll = doMuonIso(Mu,true,false);
    float isoIPV = doMuonIso(Mu,false,true);
    float isoIAll = doMuonIso(Mu,false,false);
    float minDRjetMu = doJetMuOverlap(Mu);

    //Get truth matching info
    int PDG(-1),barCode(-1),parPDG(-1),parBarCode(-1);
    int isTM(0);
    const xAOD::TruthParticle* truthMu = (m_containerTool->m_isMC && Mu) ? AnalysisTool::truthLink(Mu) : NULL;
    if (truthMu) {
      PDG = truthMu->pdgId();
      barCode = truthMu->barcode();
      const xAOD::TruthParticle* muParent =  AnalysisTool::getMuonParent(truthMu);
      if (muParent) {
        isTM = 1;
        parPDG = muParent->pdgId();
        parBarCode = muParent->barcode();
      }
    }

    // make AO Muon
    auto TP = std::make_shared<AO::Muon>(AO::Muon(*Mu,              //modified MSTP
                                                  *origMu,          //original MSTP
                                                  isTM,             //truth matched?
                                                  PDG,              //truth matched PDG
                                                  barCode,          //truth matched barcode
                                                  parPDG,           //truth matched parent PDG
                                                  parBarCode,       //truth matched parent barcode
                                                  isoDPV,           //muon isolation direct PV
						  isoDAll,          //muon isolation direct All
						  isoIPV,           //muon isolation indirect PV
						  isoIAll,          //muon isolation indirect All
                                                  minDRjetMu));     //JMOR
    m_ntupleTool->addMuon(*TP);
    TPMuons.push_back(std::move(TP));
  }//end loop over MSTP (Mu)

  unsigned int sizeMSTP(TPMuons.size());
  if (sizeMSTP > 1) {
    for (unsigned int i = 0;i<sizeMSTP-1;i++) {
      for (unsigned int j=i+1;j<sizeMSTP;j++){
        xAOD::TrackParticle* mu1 = TPMuons[i]->tp();
        xAOD::TrackParticle* mu2 = TPMuons[j]->tp();
        float minDR(0.02);
        //if (TPMuons[i]->dR() < minDR || TPMuons[j]->dR() < minDR) continue;
        float sumEta = mu1->eta() + mu2->eta();
        float deltaPhi = TVector2::Phi_mpi_pi(mu1->phi() - mu2->phi());
        float cosDR = sqrt(pow(sumEta,2)+pow(deltaPhi - TMath::Pi(),2));
        if (cosDR < 0.04) {
          //std::cout << "cosmic muon found" << std::endl;
          xAOD::TrackParticle* topMu = (mu1->phi() > 0) ? mu1 : mu2;
          xAOD::TrackParticle* botMu = (mu1->phi() > 0) ? mu2 : mu1;         
          //std::cout << "top leg: (vx,vy,vz,pt,phi,eta) (" << topMu->vx() *0.1 << "," << topMu->vy() * 0.1 << "," << topMu->vz() * 0.1 << 
          //"," << topMu->pt()*0.001 << "," << topMu->phi() << "," << topMu->eta() << ")" <<  std::endl;
          //std::cout << "bottom leg: (vx,vy,vz,pt,phi,eta) (" << botMu->vx() *0.1 << "," << botMu->vy() * 0.1 << "," << botMu->vz() * 0.1 <<
          //"," << botMu->pt()*0.001 << "," << botMu->phi() << "," << botMu->eta() << ")" <<  std::endl;
          for (const auto tmpSeg: *muSeg){
            float pt = sqrt(pow(tmpSeg->px()*0.001,2) + pow(tmpSeg->py()*0.001,2));
            float phi = atan2(tmpSeg->py(),tmpSeg->px());
            float eta = asinh(tmpSeg->pz()*0.001/pt);
            //std::cout << "seg: (x,y,z,pt,phi,eta,t0) (" << tmpSeg->x() *0.1 << "," << tmpSeg->y()*0.1 << "," << tmpSeg->z()*0.1 << 
          //"," << pt << "," << phi << "," << eta << "," << tmpSeg->t0() << ")" << std::endl; 
          }
        }
      }
    }
  }



  return TPMuons;
}

int DisplacedDimuonAnalysis::processVertices(std::vector<std::shared_ptr<AO::Muon>> TPMuons){

  if (TPMuons.empty()) return false;
  
  //Initialize
  int nVtx(0);
  for (unsigned int i=0;i < TPMuons.size() - 1;i++){
    if (!TPMuons[i]) {
      std::cout << "no TPMuons[" << i << "]" << std::endl;
      continue;
    }
    for (unsigned int j=1+i;j < TPMuons.size();j++){
      if (!TPMuons[j]) {
        std::cout << "no TPMuons[" << j << "]" << std::endl;
        continue;	
      }
      
      int leadIndex = (TPMuons[i]->pt() > TPMuons[j]->pt()) ? i : j;
      int subIndex = (TPMuons[i]->pt() > TPMuons[j]->pt()) ? j : i;
      RecVertex vtx = m_vertexTool->createVertex(TPMuons[leadIndex],TPMuons[subIndex],leadIndex,subIndex); 
      m_ntupleTool->addVtx(vtx);
      nVtx++;    
      
    }
  }
  
  return nVtx; 
}

float DisplacedDimuonAnalysis::doJetMuOverlap(xAOD::TrackParticle* Mu){
  const xAOD::JetContainer* jetCol = m_containerTool->jets();
  float minDRjetMu(99.);
  for (const auto jet: *jetCol){
   xAOD::Jet* newJet(NULL); 
   m_jetCalibTool->calibratedCopy(*jet,newJet);
    bool isClean = m_cleaningTool->accept(*newJet);
    if (!isClean) continue;
    float newjvt = m_hjvtagup->updateJvt(*newJet);
    //WP corresponds to 92% efficiency
    if (newjvt < 0.59 && newJet->pt() < 60 * 1000. && TMath::Abs(newJet->eta()) < 2.4){
      delete newJet;
      continue;
    }
    if (newJet->pt() < 10. * 1000.) {
      delete newJet;
      continue;
    }
    float tmpMinDRjetMu = AnalysisTool::deltaR(newJet,Mu);
    minDRjetMu = (tmpMinDRjetMu < minDRjetMu) ? tmpMinDRjetMu : minDRjetMu;
    delete newJet;
  }
  return minDRjetMu;
}

float DisplacedDimuonAnalysis::doJetMuOverlap(const xAOD::Muon* Mu){
  const xAOD::JetContainer* jetCol = m_containerTool->jets();
  float minDRjetMu(99.);
  for (const auto jet: *jetCol){
    xAOD::Jet* newJet(NULL);
    m_jetCalibTool->calibratedCopy(*jet,newJet);
    bool isClean = m_cleaningTool->accept(*newJet);
    if (!isClean) continue;
    float newjvt = m_hjvtagup->updateJvt(*newJet);
    if (newjvt < 0.59 && newJet->pt() < 60. * 1000. && TMath::Abs(newJet->eta()) < 2.4) {
      delete newJet;
      continue;
    }
    if (newJet->pt() < 10. * 1000.) {
      delete newJet;
      continue;
    }
    float tmpMinDRjetMu = AnalysisTool::deltaR(newJet,Mu);
    minDRjetMu = (tmpMinDRjetMu < minDRjetMu) ? tmpMinDRjetMu : minDRjetMu;
    delete newJet;
  }
  return minDRjetMu;
}

float DisplacedDimuonAnalysis::doMuonIso(xAOD::TrackParticle* Mu,bool direct,bool doPV) {

  float cDR(9999.),tmpDR(9999.);
  int cIndex(-1);
  const xAOD::MuonContainer* muons = m_containerTool->muons();
  if (direct){
    for (unsigned int i=0;i<muons->size();i++) { 
      const xAOD::Muon* combMu = (*muons)[i];
      if ((*muons)[i]->pt() *0.001 < 3.0) continue;
      tmpDR  = AnalysisTool::deltaR(Mu,combMu);
      if (tmpDR < cDR) {
        cDR = tmpDR;
        cIndex = i;
      }
    }
  }
  const xAOD::TrackParticle* combID = (cDR < 0.1) ? (*muons)[cIndex]->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle) : NULL;

  float sumIsoPt(0.);
  const xAOD::Vertex* pv(NULL);
  if (doPV) {
    const xAOD::VertexContainer * tmpPVContainer = m_containerTool->PV();
    if (tmpPVContainer) {
      for (const auto vx: * tmpPVContainer) {
        if (vx->vertexType() == xAOD::VxType::PriVtx){
          pv = vx;
          break;
        }
      }
    }
  }

  if (pv){
    for (unsigned int i=0;i<pv->nTrackParticles();i++){
      const xAOD::TrackParticle* tmpTP = pv->trackParticle(i);
      if (!tmpTP) continue;
      if (tmpTP->pt() < 500. ) continue;
      float tmpIdMuDR = AnalysisTool::deltaR(tmpTP,Mu);
      
      if (direct) {
        if (combID) {
          if (tmpTP->pt() == combID->pt() && tmpTP->phi() == combID->phi() && tmpTP->eta() == combID->eta()) continue;
        }
      } else {
        float MuPt(Mu->pt()),IdPt(tmpTP->pt()),deltaPt(0.3);
        if (IdPt > MuPt*(1-deltaPt) && IdPt < MuPt *(1+deltaPt) && tmpIdMuDR < 0.1 ) continue;
      }
      if (tmpIdMuDR < 0.4) sumIsoPt += tmpTP->pt();

      
    }
  }
  else {
    const xAOD::TrackParticleContainer* IDtrks = m_containerTool->inDetTrackParticles();
    for (const auto tmpTP: *IDtrks) {
      if (!tmpTP) continue;
      if (tmpTP->pt() < 500. ) continue;
      float tmpIdMuDR = AnalysisTool::deltaR(tmpTP,Mu);

      if (direct) {
        if (combID) {
          if (tmpTP->pt() == combID->pt() && tmpTP->phi() == combID->phi() && tmpTP->eta() == combID->eta()) continue;
        }
      } else {
        float MuPt(Mu->pt()),IdPt(tmpTP->pt()),deltaPt(0.3);
        if (IdPt > MuPt*(1-deltaPt) && IdPt < MuPt *(1+deltaPt) && tmpIdMuDR < 0.1 ) continue;
      }
      if (tmpIdMuDR < 0.4) sumIsoPt += tmpTP->pt();
    }
  }
  return sumIsoPt/Mu->pt();
}

float DisplacedDimuonAnalysis::doMuonIso(const xAOD::Muon* Mu,bool direct,bool doPV) {
  const xAOD::TrackParticle* combID = Mu->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);
  float sumIsoPt(0.);
  const xAOD::Vertex* pv(NULL);
  if (doPV){
    const xAOD::VertexContainer * tmpPVContainer = m_containerTool->PV();
    if (tmpPVContainer) {
      for (const auto vx: * tmpPVContainer) {
        if (vx->vertexType() == xAOD::VxType::PriVtx){
          pv = vx;
          break;
        }
      }
    }
  }

  if (pv){
    for (unsigned int i=0;i<pv->nTrackParticles();i++){
      const xAOD::TrackParticle* tmpTP = pv->trackParticle(i);
      if (!tmpTP) continue;
      if (tmpTP->pt() < 500. ) continue;
      float tmpIdMuDR = AnalysisTool::deltaR(tmpTP,Mu);

      if (direct) {
        if (combID) {
          if (tmpTP->pt() == combID->pt() && tmpTP->phi() == combID->phi() && tmpTP->eta() == combID->eta()) continue;
        }
      } else {
        float MuPt(Mu->pt()),IdPt(tmpTP->pt()),deltaPt(0.3);
        if (IdPt > MuPt*(1-deltaPt) && IdPt < MuPt *(1+deltaPt) && tmpIdMuDR < 0.1 ) continue;
      }
      if (tmpIdMuDR < 0.4) sumIsoPt += tmpTP->pt();
    }
  } else {
    const xAOD::TrackParticleContainer* IDtrks = m_containerTool->inDetTrackParticles();
    for (const auto tmpTP: *IDtrks) {
      if (!tmpTP) continue;
      if (tmpTP->pt() < 500. ) continue;
      float tmpIdMuDR = AnalysisTool::deltaR(tmpTP,Mu);

      if (direct) {
        if (combID) {
          if (tmpTP->pt() == combID->pt() && tmpTP->phi() == combID->phi() && tmpTP->eta() == combID->eta()) continue;
        }
      } else {
        float MuPt(Mu->pt()),IdPt(tmpTP->pt()),deltaPt(0.3);
        if (IdPt > MuPt*(1-deltaPt) && IdPt < MuPt *(1+deltaPt) && tmpIdMuDR < 0.1 ) continue;
      }
      if (tmpIdMuDR < 0.4) sumIsoPt += tmpTP->pt();
    }
  }
  return sumIsoPt/Mu->pt();
}

TH1F * DisplacedDimuonAnalysis::Hist1D(std::string name,std::string xAxis, int nbins, Float_t first, Float_t last){
  TH1F* hist = new TH1F(name.c_str(),"",nbins,first,last);
  hist->GetXaxis()->SetTitle(xAxis.c_str());
  wk()->addOutput(hist);
  return hist;
}

void DisplacedDimuonAnalysis::doEff1D(TH1F * hNum,TH1F * hDen, TH1F *hEff){
  hEff->Divide(hNum,hDen);
  for (int i=1;i<=hEff->GetNbinsX();i++){
    float num = hNum->GetBinContent(i);
    float den = hDen->GetBinContent(i);
    float eff = hEff->GetBinContent(i);
    float err = (num > 0 && den > 0) ? eff*sqrt(1/num  + 1/den ) : 0;
    hEff->SetBinError(i,err);
  }
  return;
}

void DisplacedDimuonAnalysis::calcL1METQ(int Ex, int Ey, int& METQ, bool& Overflow) {

  static const unsigned int m_nBits = 6;
  static const unsigned int m_nRanges = 4;
  static const unsigned int m_mask = 0x3F;

  //Greater of 2 values determines range of bits used
  unsigned int absEx = abs(Ex);
  unsigned int absEy = abs(Ey);
  int max = (absEx > absEy ? absEx : absEy);

  //If Ex/Ey overflows the LUT input range, trigger fires all thresholds. Indicate this with overflow flag and an out of range ETmiss value 
  if ( max >= (1<<(m_nBits+m_nRanges-1)) ) {
    METQ = 16777216; // 4096**2
    Overflow = true;
  }
  // Otherwise, emulate precision by checking which ET range we are in and zeroing bits below that. 
  else {
    for (unsigned int range = 0; range < m_nRanges; ++range) {
      if ( max < (1<<(m_nBits+range)) ) {
        absEx &= (m_mask<<range);
        absEy &= (m_mask<<range);
        break;
      }
    }
    METQ = absEx*absEx + absEy*absEy;
    Overflow = false;
  }
}

void DisplacedDimuonAnalysis::calcL1MET(int Ex, int Ey, float& MET, bool& Overflow) {
  int METQ;
  calcL1METQ(Ex,Ey,METQ,Overflow);
  MET = sqrt(static_cast<float>(METQ));
}

void DisplacedDimuonAnalysis::printChainGroup(std::string cgS) {
  auto cg = m_trigDecTool->getChainGroup(cgS.c_str());
  auto fc = cg->features();
  auto muFeatureContainers = fc.containerFeature<xAOD::MuonContainer>();
  std::cout << std::endl;
  std::cout << "feature container: " << cgS << " entries: " <<  muFeatureContainers.size() << std::endl;
  int fcCount(0);
  for (auto mcont : muFeatureContainers) {
    int mcCount(0);
    for (auto m : *mcont.cptr()) {
      std::cout << "(cont,entry): (" << fcCount << "," << mcCount << ") pt: " << m->pt()*0.001 << " eta: " << m->eta() << " phi: " << m->phi() << " muonType: " << m->muonType() << " author: " << m->author() << std::endl;
      mcCount++;
    }
    fcCount++;
  }
}

bool DisplacedDimuonAnalysis::passLJTrig(std::string cgS){
  bool tmpTrigDec = m_trigDecTool->isPassed(cgS.c_str());
  if (!tmpTrigDec) return false;
  auto cg = m_trigDecTool->getChainGroup(cgS.c_str());
  auto fc = cg->features();
  auto muFeatureContainers = fc.containerFeature<xAOD::MuonContainer>();
  int subMuTrigCount(0);
  for (auto mcont : muFeatureContainers) {
    for (auto m : *mcont.cptr()) {
      float muTrigEta = fabs(m->eta());
      float muTrigPt = m->pt();
      float muTrigThreshold(99999.);
      if (muTrigEta < 1.05) muTrigThreshold = 11.31 * 1000.;
      else if (muTrigEta < 1.5) muTrigThreshold = 10.52 * 1000.;
      else if (muTrigEta < 2.0) muTrigThreshold = 12.00 * 1000.;
      else muTrigThreshold = 13.24;
      if (muTrigPt > muTrigThreshold) subMuTrigCount++;
    }
  }
  return (subMuTrigCount > 1);
}
