#include "../DisplacedDimuonAnalysis/AnalysisTool.h"
#include "TMath.h"

bool AnalysisTool::isTPVtx(const xAOD::TruthVertex* truthVtx){
  bool hasGluino(false),hasHiggs(false);
  for(unsigned int i=0;i<truthVtx->nIncomingParticles();i++){
    const xAOD::TruthParticle* incoming = truthVtx->incomingParticle(i);
    if (incoming->pdgId() == 25) hasHiggs = true;
    if (incoming->pdgId() == 1000021) hasGluino = true;
  }
  bool hasChi(false);
  int numT(0);
  for(unsigned int i=0;i<truthVtx->nOutgoingParticles();i++){
    const xAOD::TruthParticle* outgoing = truthVtx->outgoingParticle(i);
    if(outgoing->absPdgId() == 6000006) numT++;
    if(outgoing->pdgId() == 1000022) hasChi = true;
  }
  if ((hasChi && hasGluino) || numT == 2 || hasHiggs) return true;
  else return false;
}

const xAOD::TruthParticle* AnalysisTool::getTrueZ(const xAOD::TruthParticle* trueMu1, const xAOD::TruthParticle* trueMu2) {
  const xAOD::TruthParticle* nullParent(0);
  int muPDG1(-1);
  int muPDG2(-1);
  if (trueMu1 && trueMu2) {
    muPDG1 = trueMu1->absPdgId();
    muPDG2 = trueMu2->absPdgId();
  } else {
    return nullParent;
  }

  int muParPDG1(-1);
  int muParBarCode1(-1);
  int muParBarCode2(-1);
    
  const xAOD::TruthParticle* muParent1 = getMuonParent(trueMu1);
  const xAOD::TruthParticle* muParent2 = getMuonParent(trueMu2);

  if (muParent1 && muParent2) {
    muParPDG1 = muParent1->absPdgId();
    muParBarCode1 = muParent1->barcode();      
    muParBarCode2 = muParent2->barcode();
  }
  if ((muPDG1 == 13) && (muPDG2 == 13) && (muParBarCode1 != -1) && (muParBarCode1 == muParBarCode2) && (muParPDG1 == 23 || muParPDG1 == 1023 || muParPDG1 == 32)) {
    return muParent1;
  }
  return nullParent;
}

const xAOD::TruthParticle* AnalysisTool::getMuonParent(const xAOD::TruthParticle* truthMu) {
  if (truthMu->parent(0) == 0) {
    return truthMu;
  } else {
    if (truthMu->absPdgId() == truthMu->parent(0)->absPdgId() ) {
      return getMuonParent(truthMu->parent(0));
    } else {
      return truthMu->parent(0);
    }
  }
}

const xAOD::TruthParticle* AnalysisTool::truthLink(const xAOD::TrackParticle* track){
  const xAOD::TruthParticle* truth(NULL);
  if(track->auxdata< ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink")){
    ElementLink< xAOD::TruthParticleContainer > truthLink = track->auxdata< ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
    truth = *truthLink;
  }
  return truth;
}

const xAOD::TruthParticle* AnalysisTool::truthLink(const xAOD::Muon* combMu){
  const xAOD::TrackParticle* combMuTP = combinedTrackLink(combMu);
  const xAOD::TruthParticle* truth = combMuTP ? truthLink(combMuTP) : NULL;
  return truth;
}

const xAOD::TrackParticle* AnalysisTool::combinedTrackLink(const xAOD::Muon* combMu){
  const xAOD::TrackParticle* combTP(NULL);
  if(combMu->auxdata< ElementLink<xAOD::TrackParticleContainer> >("combinedTrackParticleLink")){
    ElementLink< xAOD::TrackParticleContainer > trackLink1 = combMu->auxdata< ElementLink<xAOD::TrackParticleContainer> >("combinedTrackParticleLink");
    combTP = *trackLink1;
  }
  return combTP;
}

bool AnalysisTool::layerRequirement(const xAOD::TrackParticle* Mu,int phiLayers,int etaLayers){
  uint8_t numOfPhiLayers(0),numOfTriggerEtaLayers(0),numOfPrecisionLayers(0);
  if (!Mu->summaryValue(numOfPhiLayers,xAOD::SummaryType::numberOfPhiLayers) || !Mu->summaryValue(numOfPrecisionLayers,xAOD::SummaryType::numberOfPrecisionLayers) || !Mu->summaryValue(numOfTriggerEtaLayers,xAOD::SummaryType::numberOfTriggerEtaLayers) ){
    std::cout << "summary values not included, returning false" << std::endl;
    return false;
  }
  if ((int)numOfPhiLayers < phiLayers || (int)numOfTriggerEtaLayers < etaLayers) return false;
  else return true;
}


float AnalysisTool::d0(const xAOD::TrackParticle* tp){
  float md0 = tp->d0();
  float mphi = tp->phi();
  float mVx0 = tp->vx();
  float mVy0 = tp->vy();
  float vx = mVx0+md0*cos(mphi);
  float vy = mVy0+md0*sin(mphi);
  float pt = tp->pt();
  float px = pt*cos(mphi);
  float py = pt*sin(mphi);
  return fabs(px*vy-vx*py)/pt;
}

float AnalysisTool::d0(const xAOD::TruthParticle* tp){
  if (!tp->hasProdVtx()) return -999.;
  float vx = tp->prodVtx()->x();
  float vy = tp->prodVtx()->y();
  float pt = tp->pt();
  float px = pt*cos(tp->phi());
  float py = pt*sin(tp->phi());
  return fabs(px*vy-vx*py)/pt;
}

float AnalysisTool::z0(const xAOD::TrackParticle* tp){
  float md0 = tp->d0();
  float mphi = tp->phi();
  float meta = tp->eta();
  float mVx0 = tp->vx();
  float mVy0 = tp->vy();
  float mVz0 = tp->vz();
  float vx = mVx0+md0*cos(mphi);
  float vy = mVy0+md0*sin(mphi);
  float vr = sqrt(vx*vx+vy*vy);
  float vz = mVz0 + tp->z0();
  float pt = tp->pt();
  float px = pt*cos(mphi);
  float py = pt*sin(mphi);
  float pz = pt*sinh(meta);
  float d0 = AnalysisTool::d0(tp);

  float XdotP2D = vx*px+vy*py;
  float t0 = -2.0*XdotP2D / pow(pt,2);

  //return vz - (vr*pz)/pt;
  return vz + pz*t0;
}

float AnalysisTool::z0(const xAOD::TruthParticle* tp){
  if (!tp->hasProdVtx()) return -999.;
  float d0 = AnalysisTool::d0(tp);
  float vx = tp->prodVtx()->x();
  float vy = tp->prodVtx()->y();
  float vr = sqrt(vx*vx+vy*vy);
  float vz = tp->prodVtx()->z();
  float pt = tp->pt();
  float px = tp->px();
  float py = tp->py();
  float pz = tp->pz();

  float XdotP2D = vx*px+vy*py; 
  float t0 = -2.0*XdotP2D / pow(pt,2); 

//return vz - (vr*pz)/pt;
  return vz + pz*t0;
}

float AnalysisTool::openAngle(const xAOD::TruthParticle* tp1,const xAOD::TruthParticle* tp2){
  xAOD::TruthParticle::FourMom_t v1 = tp1->p4();
  xAOD::TruthParticle::FourMom_t v2 = tp2->p4();
  return v1.Angle(v2.Vect());
}

float AnalysisTool::openAngle(const xAOD::Muon* tp1,const xAOD::Muon* tp2){
  xAOD::Muon::FourMom_t v1 = tp1->p4();
  xAOD::Muon::FourMom_t v2 = tp2->p4();
  return v1.Angle(v2.Vect());
}

float AnalysisTool::invMass(const xAOD::TruthParticle* t1,const xAOD::TruthParticle* t2){
  xAOD::TruthParticle::FourMom_t m1 = t1->p4();
  xAOD::TruthParticle::FourMom_t m2 = t2->p4();
  xAOD::TruthParticle::FourMom_t mTotal = m1+m2;
  return mTotal.M();
}

float AnalysisTool::invMass(const xAOD::TrackParticle* t1,const xAOD::TrackParticle* t2){
  xAOD::TrackParticle::FourMom_t m1 = t1->p4();
  xAOD::TrackParticle::FourMom_t m2 = t2->p4();
  xAOD::TrackParticle::FourMom_t mTotal = m1+m2;
  return mTotal.M();
}

float AnalysisTool::deltaR(const xAOD::Jet* j1,const xAOD::TrackParticle* t1){
  xAOD::Jet::FourMom_t m1 = j1->p4();
  xAOD::TrackParticle::FourMom_t m2 = t1->p4();
  return m1.DeltaR(m2);
}

float AnalysisTool::deltaR(const xAOD::Jet* j1,const xAOD::Muon* t1){
  xAOD::Jet::FourMom_t m1 = j1->p4();
  xAOD::Muon::FourMom_t m2 = t1->p4();
  return m1.DeltaR(m2);
}

float AnalysisTool::deltaR(const xAOD::TrackParticle* t1,const xAOD::TrackParticle* t2){
  xAOD::TrackParticle::FourMom_t m1 = t1->p4();
  xAOD::TrackParticle::FourMom_t m2 = t2->p4();
  return m1.DeltaR(m2);
}

float AnalysisTool::deltaR(const xAOD::TrackParticle* t1,const xAOD::Muon* t2){
  xAOD::TrackParticle::FourMom_t m1 = t1->p4();
  xAOD::Muon::FourMom_t m2 = t2->p4();
  return m1.DeltaR(m2);
}

float AnalysisTool::deltaR(const xAOD::Muon* t1,const xAOD::Muon* t2){
  xAOD::Muon::FourMom_t m1 = t1->p4();
  xAOD::Muon::FourMom_t m2 = t2->p4();
  return m1.DeltaR(m2);
}

float AnalysisTool::deltaR(const xAOD::TruthParticle* t1,const xAOD::Muon* t2){
  xAOD::TruthParticle::FourMom_t m1 = t1->p4();
  xAOD::Muon::FourMom_t m2 = t2->p4();
  return m1.DeltaR(m2);
}

