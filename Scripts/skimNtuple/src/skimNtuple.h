//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jan 20 11:27:49 2017 by ROOT version 6.06/02
// from TTree tree/tree
// found on file: data16PeriodB.merge.NTUP.root
//////////////////////////////////////////////////////////

#ifndef skimNtuple_h
#define skimNtuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"

using namespace std;

class skimNtuple {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           EventNumber;
   Int_t           RunNumber;
   Int_t           BCID;
   Int_t           LB;
   Float_t         evtWeight;
   Float_t         sumWeights;
   Float_t         prw;
   Float_t         EtMiss;
   Float_t         EtMissPhi;
   Float_t         TPV_x;
   Float_t         TPV_y;
   Float_t         TPV_z;
   Float_t         PV_z;
   Int_t           PV_ntrk;
   Int_t           PU_nvtx;
   Int_t           nMSOnly;
   Int_t           NDV;
   Int_t           NDVT;
   Int_t           narrowScanTrig;
   Int_t           METTrig;
   Int_t           singleMuTrig;
   Int_t           threeMuTrig;
   Int_t           L1_MU20Trig;
   Int_t           L1_3MU6Trig;
   vector<float>   *trkt_pt;
   vector<float>   *trkt_phi;
   vector<float>   *trkt_eta;
   vector<float>   *trkt_d0;
   vector<float>   *trkt_z0;
   vector<float>   *trkt_charge;
   vector<int>     *trkt_PDG;
   vector<int>     *trkt_barCode;
   vector<int>     *trkt_parPDG;
   vector<int>     *trkt_parBarCode;
   vector<float>   *DVT_x;
   vector<float>   *DVT_y;
   vector<float>   *DVT_z;
   vector<float>   *DVT_r;
   vector<float>   *DVT_l;
   vector<float>   *DVT_m;
   vector<float>   *DVT_openAng;
   vector<int>     *DVT_parPDG;
   vector<int>     *DVT_parBarCode;
   vector<int>     *DVT_muIndexLead;
   vector<int>     *DVT_muIndexSub;
   vector<double>  *DVT_pLLP;
   vector<double>  *DVT_eLLP;
   vector<double>  *DVT_lLLP;
   vector<float>   *trk_pt;
   vector<float>   *trk_phi;
   vector<float>   *trk_eta;
   vector<float>   *trk_d0;
   vector<float>   *trk_z0;
   vector<float>   *trk_m;
   vector<float>   *trk_charge;
   vector<float>   *trk_covd0;
   vector<float>   *trk_covz0;
   vector<float>   *trk_covPhi;
   vector<float>   *trk_covTheta;
   vector<float>   *trk_covP;
   vector<float>   *trk_dR;
   Int_t           trk_isTM;
   vector<int>     *trk_PDG;
   vector<int>     *trk_barCode;
   vector<int>     *trk_parPDG;
   vector<int>     *trk_parBarCode;
   vector<float>   *trk_iso;
   vector<float>   *trk_jetDR;
   vector<int>     *trk_closestAuthor;
   vector<unsigned char> *trk_nPrecLayers;
   vector<unsigned char> *trk_nPhiLayers;
   vector<unsigned char> *trk_nEtaLayers;
   vector<float>   *trk_vx;
   vector<float>   *trk_vy;
   vector<float>   *trk_vz;
   vector<float>   *DV_x;
   vector<float>   *DV_y;
   vector<float>   *DV_z;
   vector<float>   *DV_r;
   vector<float>   *DV_l;
   vector<float>   *DV_m;
   vector<float>   *DV_charge;
   vector<float>   *DV_openAng;
   vector<float>   *DV_distV;
   vector<int>     *DV_muIndexLead;
   vector<int>     *DV_muIndexSub;

   // List of branches
   TBranch        *b_EventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_LB;   //!
   TBranch        *b_evtWeight;   //!
   TBranch        *b_sumWeights;   //!
   TBranch        *b_prw;   //!
   TBranch        *b_EtMiss;   //!
   TBranch        *b_EtMissPhi;   //!
   TBranch        *b_TPV_x;   //!
   TBranch        *b_TPV_y;   //!
   TBranch        *b_TPV_z;   //!
   TBranch        *b_PV_z;   //!
   TBranch        *b_PV_ntrk;   //!
   TBranch        *b_PU_nvtx;   //!
   TBranch        *b_nMSOnly;   //!
   TBranch        *b_NDV;   //!
   TBranch        *b_NDVT;   //!
   TBranch        *b_narrowScanTrig;   //!
   TBranch        *b_METTrig;   //!
   TBranch        *b_singleMuTrig;   //!
   TBranch        *b_threeMuTrig;   //!
   TBranch        *b_L1_MU20Trig;   //!
   TBranch        *b_L1_3MU6Trig;   //!
   TBranch        *b_trkt_pt;   //!
   TBranch        *b_trkt_phi;   //!
   TBranch        *b_trkt_eta;   //!
   TBranch        *b_trkt_d0;   //!
   TBranch        *b_trkt_z0;   //!
   TBranch        *b_trkt_charge;   //!
   TBranch        *b_trkt_PDG;   //!
   TBranch        *b_trkt_barCode;   //!
   TBranch        *b_trkt_parPDG;   //!
   TBranch        *b_trkt_parBarCode;   //!
   TBranch        *b_DVT_x;   //!
   TBranch        *b_DVT_y;   //!
   TBranch        *b_DVT_z;   //!
   TBranch        *b_DVT_r;   //!
   TBranch        *b_DVT_l;   //!
   TBranch        *b_DVT_m;   //!
   TBranch        *b_DVT_openAng;   //!
   TBranch        *b_DVT_parPDG;   //!
   TBranch        *b_DVT_parBarCode;   //!
   TBranch        *b_DVT_muIndexLead;   //!
   TBranch        *b_DVT_muIndexSub;   //!
   TBranch        *b_DVT_pLLP;   //!
   TBranch        *b_DVT_eLLP;   //!
   TBranch        *b_DVT_lLLP;   //!
   TBranch        *b_trk_pt;   //!
   TBranch        *b_trk_phi;   //!
   TBranch        *b_trk_eta;   //!
   TBranch        *b_trk_d0;   //!
   TBranch        *b_trk_z0;   //!
   TBranch        *b_trk_m;   //!
   TBranch        *b_trk_charge;   //!
   TBranch        *b_trk_covd0;   //!
   TBranch        *b_trk_covz0;   //!
   TBranch        *b_trk_covPhi;   //!
   TBranch        *b_trk_covTheta;   //!
   TBranch        *b_trk_covP;   //!
   TBranch        *b_trk_dR;   //!
   TBranch        *b_trk_isTM;   //!
   TBranch        *b_trk_PDG;   //!
   TBranch        *b_trk_barCode;   //!
   TBranch        *b_trk_parPDG;   //!
   TBranch        *b_trk_parBarCode;   //!
   TBranch        *b_trk_iso;   //!
   TBranch        *b_trk_jetDR;   //!
   TBranch        *b_trk_closestAuthor;   //!
   TBranch        *b_trk_nPrecLayers;   //!
   TBranch        *b_trk_nPhiLayers;   //!
   TBranch        *b_trk_nEtaLayers;   //!
   TBranch        *b_trk_vx;   //!
   TBranch        *b_trk_vy;   //!
   TBranch        *b_trk_vz;   //!
   TBranch        *b_DV_x;   //!
   TBranch        *b_DV_y;   //!
   TBranch        *b_DV_z;   //!
   TBranch        *b_DV_r;   //!
   TBranch        *b_DV_l;   //!
   TBranch        *b_DV_m;   //!
   TBranch        *b_DV_charge;   //!
   TBranch        *b_DV_openAng;   //!
   TBranch        *b_DV_distV;   //!
   TBranch        *b_DV_muIndexLead;   //!
   TBranch        *b_DV_muIndexSub;   //!

   skimNtuple(TTree *tree=0);
   virtual ~skimNtuple();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(std::string tmpStr);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef skimNtuple_cxx
skimNtuple::skimNtuple(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data16PeriodB.merge.NTUP.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data16PeriodB.merge.NTUP.root");
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

skimNtuple::~skimNtuple()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t skimNtuple::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t skimNtuple::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void skimNtuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   trkt_pt = 0;
   trkt_phi = 0;
   trkt_eta = 0;
   trkt_d0 = 0;
   trkt_z0 = 0;
   trkt_charge = 0;
   trkt_PDG = 0;
   trkt_barCode = 0;
   trkt_parPDG = 0;
   trkt_parBarCode = 0;
   DVT_x = 0;
   DVT_y = 0;
   DVT_z = 0;
   DVT_r = 0;
   DVT_l = 0;
   DVT_m = 0;
   DVT_openAng = 0;
   DVT_parPDG = 0;
   DVT_parBarCode = 0;
   DVT_muIndexLead = 0;
   DVT_muIndexSub = 0;
   DVT_pLLP = 0;
   DVT_eLLP = 0;
   DVT_lLLP = 0;
   trk_pt = 0;
   trk_phi = 0;
   trk_eta = 0;
   trk_d0 = 0;
   trk_z0 = 0;
   trk_m = 0;
   trk_charge = 0;
   trk_covd0 = 0;
   trk_covz0 = 0;
   trk_covPhi = 0;
   trk_covTheta = 0;
   trk_covP = 0;
   trk_dR = 0;
   trk_PDG = 0;
   trk_barCode = 0;
   trk_parPDG = 0;
   trk_parBarCode = 0;
   trk_iso = 0;
   trk_jetDR = 0;
   trk_closestAuthor = 0;
   trk_nPrecLayers = 0;
   trk_nPhiLayers = 0;
   trk_nEtaLayers = 0;
   trk_vx = 0;
   trk_vy = 0;
   trk_vz = 0;
   DV_x = 0;
   DV_y = 0;
   DV_z = 0;
   DV_r = 0;
   DV_l = 0;
   DV_m = 0;
   DV_charge = 0;
   DV_openAng = 0;
   DV_distV = 0;
   DV_muIndexLead = 0;
   DV_muIndexSub = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("LB", &LB, &b_LB);
   fChain->SetBranchAddress("evtWeight", &evtWeight, &b_evtWeight);
   fChain->SetBranchAddress("sumWeights", &sumWeights, &b_sumWeights);
   fChain->SetBranchAddress("prw", &prw, &b_prw);
   fChain->SetBranchAddress("EtMiss", &EtMiss, &b_EtMiss);
   fChain->SetBranchAddress("EtMissPhi", &EtMissPhi, &b_EtMissPhi);
   fChain->SetBranchAddress("TPV_x", &TPV_x, &b_TPV_x);
   fChain->SetBranchAddress("TPV_y", &TPV_y, &b_TPV_y);
   fChain->SetBranchAddress("TPV_z", &TPV_z, &b_TPV_z);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
   fChain->SetBranchAddress("PV_ntrk", &PV_ntrk, &b_PV_ntrk);
   fChain->SetBranchAddress("PU_nvtx", &PU_nvtx, &b_PU_nvtx);
   fChain->SetBranchAddress("nMSOnly", &nMSOnly, &b_nMSOnly);
   fChain->SetBranchAddress("NDV", &NDV, &b_NDV);
   fChain->SetBranchAddress("NDVT", &NDVT, &b_NDVT);
   fChain->SetBranchAddress("narrowScanTrig", &narrowScanTrig, &b_narrowScanTrig);
   fChain->SetBranchAddress("METTrig", &METTrig, &b_METTrig);
   fChain->SetBranchAddress("singleMuTrig", &singleMuTrig, &b_singleMuTrig);
   fChain->SetBranchAddress("threeMuTrig", &threeMuTrig, &b_threeMuTrig);
   fChain->SetBranchAddress("L1_MU20Trig", &L1_MU20Trig, &b_L1_MU20Trig);
   fChain->SetBranchAddress("L1_3MU6Trig", &L1_3MU6Trig, &b_L1_3MU6Trig);
   fChain->SetBranchAddress("trkt_pt", &trkt_pt, &b_trkt_pt);
   fChain->SetBranchAddress("trkt_phi", &trkt_phi, &b_trkt_phi);
   fChain->SetBranchAddress("trkt_eta", &trkt_eta, &b_trkt_eta);
   fChain->SetBranchAddress("trkt_d0", &trkt_d0, &b_trkt_d0);
   fChain->SetBranchAddress("trkt_z0", &trkt_z0, &b_trkt_z0);
   fChain->SetBranchAddress("trkt_charge", &trkt_charge, &b_trkt_charge);
   fChain->SetBranchAddress("trkt_PDG", &trkt_PDG, &b_trkt_PDG);
   fChain->SetBranchAddress("trkt_barCode", &trkt_barCode, &b_trkt_barCode);
   fChain->SetBranchAddress("trkt_parPDG", &trkt_parPDG, &b_trkt_parPDG);
   fChain->SetBranchAddress("trkt_parBarCode", &trkt_parBarCode, &b_trkt_parBarCode);
   fChain->SetBranchAddress("DVT_x", &DVT_x, &b_DVT_x);
   fChain->SetBranchAddress("DVT_y", &DVT_y, &b_DVT_y);
   fChain->SetBranchAddress("DVT_z", &DVT_z, &b_DVT_z);
   fChain->SetBranchAddress("DVT_r", &DVT_r, &b_DVT_r);
   fChain->SetBranchAddress("DVT_l", &DVT_l, &b_DVT_l);
   fChain->SetBranchAddress("DVT_m", &DVT_m, &b_DVT_m);
   fChain->SetBranchAddress("DVT_openAng", &DVT_openAng, &b_DVT_openAng);
   fChain->SetBranchAddress("DVT_parPDG", &DVT_parPDG, &b_DVT_parPDG);
   fChain->SetBranchAddress("DVT_parBarCode", &DVT_parBarCode, &b_DVT_parBarCode);
   fChain->SetBranchAddress("DVT_muIndexLead", &DVT_muIndexLead, &b_DVT_muIndexLead);
   fChain->SetBranchAddress("DVT_muIndexSub", &DVT_muIndexSub, &b_DVT_muIndexSub);
   fChain->SetBranchAddress("DVT_pLLP", &DVT_pLLP, &b_DVT_pLLP);
   fChain->SetBranchAddress("DVT_eLLP", &DVT_eLLP, &b_DVT_eLLP);
   fChain->SetBranchAddress("DVT_lLLP", &DVT_lLLP, &b_DVT_lLLP);
   fChain->SetBranchAddress("trk_pt", &trk_pt, &b_trk_pt);
   fChain->SetBranchAddress("trk_phi", &trk_phi, &b_trk_phi);
   fChain->SetBranchAddress("trk_eta", &trk_eta, &b_trk_eta);
   fChain->SetBranchAddress("trk_d0", &trk_d0, &b_trk_d0);
   fChain->SetBranchAddress("trk_z0", &trk_z0, &b_trk_z0);
   fChain->SetBranchAddress("trk_m", &trk_m, &b_trk_m);
   fChain->SetBranchAddress("trk_charge", &trk_charge, &b_trk_charge);
   fChain->SetBranchAddress("trk_covd0", &trk_covd0, &b_trk_covd0);
   fChain->SetBranchAddress("trk_covz0", &trk_covz0, &b_trk_covz0);
   fChain->SetBranchAddress("trk_covPhi", &trk_covPhi, &b_trk_covPhi);
   fChain->SetBranchAddress("trk_covTheta", &trk_covTheta, &b_trk_covTheta);
   fChain->SetBranchAddress("trk_covP", &trk_covP, &b_trk_covP);
   fChain->SetBranchAddress("trk_dR", &trk_dR, &b_trk_dR);
   fChain->SetBranchAddress("trk_isTM", &trk_isTM, &b_trk_isTM);
   fChain->SetBranchAddress("trk_PDG", &trk_PDG, &b_trk_PDG);
   fChain->SetBranchAddress("trk_barCode", &trk_barCode, &b_trk_barCode);
   fChain->SetBranchAddress("trk_parPDG", &trk_parPDG, &b_trk_parPDG);
   fChain->SetBranchAddress("trk_parBarCode", &trk_parBarCode, &b_trk_parBarCode);
   fChain->SetBranchAddress("trk_iso", &trk_iso, &b_trk_iso);
   fChain->SetBranchAddress("trk_jetDR", &trk_jetDR, &b_trk_jetDR);
   fChain->SetBranchAddress("trk_closestAuthor", &trk_closestAuthor, &b_trk_closestAuthor);
   fChain->SetBranchAddress("trk_nPrecLayers", &trk_nPrecLayers, &b_trk_nPrecLayers);
   fChain->SetBranchAddress("trk_nPhiLayers", &trk_nPhiLayers, &b_trk_nPhiLayers);
   fChain->SetBranchAddress("trk_nEtaLayers", &trk_nEtaLayers, &b_trk_nEtaLayers);
   fChain->SetBranchAddress("trk_vx", &trk_vx, &b_trk_vx);
   fChain->SetBranchAddress("trk_vy", &trk_vy, &b_trk_vy);
   fChain->SetBranchAddress("trk_vz", &trk_vz, &b_trk_vz);
   fChain->SetBranchAddress("DV_x", &DV_x, &b_DV_x);
   fChain->SetBranchAddress("DV_y", &DV_y, &b_DV_y);
   fChain->SetBranchAddress("DV_z", &DV_z, &b_DV_z);
   fChain->SetBranchAddress("DV_r", &DV_r, &b_DV_r);
   fChain->SetBranchAddress("DV_l", &DV_l, &b_DV_l);
   fChain->SetBranchAddress("DV_m", &DV_m, &b_DV_m);
   fChain->SetBranchAddress("DV_charge", &DV_charge, &b_DV_charge);
   fChain->SetBranchAddress("DV_openAng", &DV_openAng, &b_DV_openAng);
   fChain->SetBranchAddress("DV_distV", &DV_distV, &b_DV_distV);
   fChain->SetBranchAddress("DV_muIndexLead", &DV_muIndexLead, &b_DV_muIndexLead);
   fChain->SetBranchAddress("DV_muIndexSub", &DV_muIndexSub, &b_DV_muIndexSub);
   Notify();
}

Bool_t skimNtuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void skimNtuple::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t skimNtuple::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef skimNtuple_cxx
