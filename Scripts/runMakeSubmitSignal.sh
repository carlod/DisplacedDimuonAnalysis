#%%%%%%%%%%%%%%%%%%%%
#SIGNAL MC SAMPLES  %
#%%%%%%%%%%%%%%%%%%%%

./makeSubmit.sh GGMm300ct1000 100 run
./makeSubmit.sh GGMm300ct5000 100 run
./makeSubmit.sh GGMm700ct1000 100 run
./makeSubmit.sh GGMm700ct5000 100 run
./makeSubmit.sh GGMm1000ct1000 100 run
./makeSubmit.sh GGMm1000ct5000 100 run

./makeSubmit.sh Zdm20ct50 100 run
./makeSubmit.sh Zdm20ct50AllMu 100 run
./makeSubmit.sh Zdm40ct50 100 run
./makeSubmit.sh Zdm40ct50AllMu 100 run
./makeSubmit.sh Zdm40ct500 100 run
./makeSubmit.sh Zdm40ct500AllMu 100 run
./makeSubmit.sh Zdm60ct50 100 run
./makeSubmit.sh Zdm60ct50AllMu 100 run
./makeSubmit.sh Zdm60ct500 100 run
./makeSubmit.sh Zdm60ct500AllMu 100 run

