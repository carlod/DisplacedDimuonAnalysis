#%%%%%%%%%%%%%%%
#DATA SAMPLES  %
#%%%%%%%%%%%%%%%

./makeSubmit.sh data16PeriodB 500 run
./makeSubmit.sh data16PeriodC 500 run

#%%%%%%%%%%%%%%%%
#SM MC SAMPLES  %
#%%%%%%%%%%%%%%%%

./makeSubmit.sh Zmumu 100 run

./makeSubmit.sh ttbar 100 run

./makeSubmit.sh DY6M10 100 run
./makeSubmit.sh DY10M60 100 run

./makeSubmit.sh DY10M40_0Pt70BVeto 100 run
./makeSubmit.sh DY10M40_0Pt70BFilter 100 run
./makeSubmit.sh DY10M40_70Pt140BVeto 100 run
./makeSubmit.sh DY10M40_70Pt140BFilter 100 run
./makeSubmit.sh DY10M40_140Pt400BVeto 100 run
./makeSubmit.sh DY10M40_140Pt400BFilter 100 run
./makeSubmit.sh DY10M40_400PtECMBVeto 100 run
./makeSubmit.sh DY10M40_400PtECMBFilter 100 run
./makeSubmit.sh DY70M120 100 run

./makeSubmit.sh Wplus 100 run
./makeSubmit.sh Wminus 100 run

./makeSubmit.sh ZZvvll 100 run
./makeSubmit.sh ZZllll 100 run
./makeSubmit.sh WWlvlv 100 run
./makeSubmit.sh WZlvll 100 run

./makeSubmit.sh STtchanT 100 run
./makeSubmit.sh STtchanTB 100 run
./makeSubmit.sh STwtchanT 100 run
./makeSubmit.sh STwtchanTB 100 run
./makeSubmit.sh STschanT 100 run
./makeSubmit.sh STschanTB 100 run

#./makeSubmit.sh Upsilon1SMu 100 run
#./makeSubmit.sh Upsilon2SMu 100 run
#./makeSubmit.sh Upsilon3SMu 100 run

#./makeSubmit.sh JZ0Wmu 100 run
#./makeSubmit.sh JZ1WAmu 100 run
#./makeSubmit.sh JZRW1Bmu 100 run
#./makeSubmit.sh JZRW2mu 100 run
#./makeSubmit.sh JZRW3mu 100 run
#./makeSubmit.sh JZRW4mu 100 run

#%%%%%%%%%%%%%%%%%%%%
#SIGNAL MC SAMPLES  %
#%%%%%%%%%%%%%%%%%%%%

