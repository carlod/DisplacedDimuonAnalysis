#!/bin/bash

if [ $# != 2 ] && [ $# != 3 ]; then
  echo "$0 sample(VLQct1,VLQct5,Zmumu,ttbar,Wplus,Wminus,Wall,ZZ,signal,background,all) filesPerJob(#) [run]"
  exit 1
fi

PATHBASEMC=/titan/atlas/common/xAOD/
PATHBASEDATA=/titan/atlas/common/xAOD/data16_13TeV/EXOT20_v2/

#%%%%%%%%%%%%%%%%%%%
#DATA SAMPLES 2016 %
#%%%%%%%%%%%%%%%%%%%

data16PeriodA=data16_13TeV.periodA.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodB=data16_13TeV.periodB.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodC=data16_13TeV.periodC.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodD=data16_13TeV.periodD.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodE=data16_13TeV.periodE.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodF=data16_13TeV.periodF.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodG=data16_13TeV.periodG.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodI=data16_13TeV.periodI.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodK=data16_13TeV.periodK.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950
data16PeriodL=data16_13TeV.periodL.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950

#%%%%%%%%%%%%%%%%%%%%%%
#STANDARD MODEL BKGD  %
#%%%%%%%%%%%%%%%%%%%%%%

Zmumu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_EXOT20.e3601_s2576_s2132_r7725_r7676_p2888
ttbar=mc15c_13TeV/EXOT20_v2/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_EXOT20.e3698_s2608_s2183_r7725_r7676_p2888

STtchanT=mc15c_13TeV/EXOT20_v2/mc15_13TeV.410011.PowhegPythiaEvtGen_P2012_singletop_tchan_lept_top.merge.DAOD_EXOT20.e3824_s2608_s2183_r7725_r7676_p2888
STtchanTB=mc15c_13TeV/EXOT20_v2/mc15_13TeV.410012.PowhegPythiaEvtGen_P2012_singletop_tchan_lept_antitop.merge.DAOD_EXOT20.e3824_s2608_s2183_r7725_r7676_p2888
STwtchanT=mc15c_13TeV/EXOT20_v2/mc15_13TeV.410015.PowhegPythiaEvtGen_P2012_Wt_dilepton_top.merge.DAOD_EXOT20.e3753_a766_a818_r7676_p2888
STwtchanTB=mc15c_13TeV/EXOT20_v2/mc15_13TeV.410016.PowhegPythiaEvtGen_P2012_Wt_dilepton_antitop.merge.DAOD_EXOT20.e3753_a766_a818_r7676_p2888
STschanT=mc15c_13TeV/EXOT20_v2/mc15_13TeV.410025.PowhegPythiaEvtGen_P2012_SingleTopSchan_noAllHad_top.merge.DAOD_EXOT20.e3998_s2608_s2183_r7725_r7676_p2888
STschanTB=mc15c_13TeV/EXOT20_v2/mc15_13TeV.410026.PowhegPythiaEvtGen_P2012_SingleTopSchan_noAllHad_antitop.merge.DAOD_EXOT20.e3998_s2608_s2183_r7725_r7676_p2888

DY6M10=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.DAOD_EXOT20.e4770_s2726_r7772_r7676_p2888
DY10M60=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_EXOT20.e4770_s2726_r7772_r7676_p2888
DY10M40_0Pt70BVeto=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361476.Sherpa_CT10_Zmumu_Mll10to40_Pt0_70_BVeto.merge.DAOD_EXOT20.e4198_s2608_s2183_r7772_r7676_p2888
DY10M40_0Pt70BFilter=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361477.Sherpa_CT10_Zmumu_Mll10to40_Pt0_70_BFilter.merge.DAOD_EXOT20.e4198_s2608_s2183_r7772_r7676_p2888
DY10M40_70Pt140BVeto=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361478.Sherpa_CT10_Zmumu_Mll10to40_Pt70_140_BVeto.merge.DAOD_EXOT20.e4198_s2608_s2183_r7772_r7676_p2888
DY10M40_70Pt140BFilter=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361479.Sherpa_CT10_Zmumu_Mll10to40_Pt70_140_BFilter.merge.DAOD_EXOT20.e4198_s2608_s2183_r7772_r7676_p2888
DY10M40_140Pt400BVeto=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361480.Sherpa_CT10_Zmumu_Mll10to40_Pt140_400_BVeto.merge.DAOD_EXOT20.e4198_s2608_s2183_r7772_r7676_p2888
#DY10M40_140Pt400BFilter=mc15c_13TeV/EXOT20_v2/
DY10M40_400PtECMBVeto=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361482.Sherpa_CT10_Zmumu_Mll10to40_Pt400_E_CMS_BVeto.merge.DAOD_EXOT20.e4198_s2608_s2183_r7772_r7676_p2888
DY10M40_400PtECMBFilter=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361483.Sherpa_CT10_Zmumu_Mll10to40_Pt400_E_CMS_BFilter.merge.DAOD_EXOT20.e4198_s2608_s2183_r7772_r7676_p2888
DY70M120=mc15c_13TeV/EXOT20_v2/mc15_13TeV.301560.Pythia8EvtGen_A14NNPDF23LO_DYmumu_70M120.merge.DAOD_EXOT20.e3882_s2608_s2183_r7772_r7676_p2888

Wplus=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.merge.DAOD_EXOT20.e3601_s2576_s2132_r7725_r7676_p2888
Wminus=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361104.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusmunu.merge.DAOD_EXOT20.e3601_s2576_s2132_r7725_r7676_p2888

ZZvvll=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.merge.DAOD_EXOT20.e4475_s2726_r7772_r7676_p2888
ZZllll=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361603.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZllll_mll4.merge.DAOD_EXOT20.e4475_s2726_r7772_r7676_p2888
WWlvlv=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.merge.DAOD_EXOT20.e4616_s2726_r7772_r7676_p2888
WZlvll=mc15c_13TeV/EXOT20_v2/mc15_13TeV.361601.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvll_mll4.merge.DAOD_EXOT20.e4475_s2726_r7772_r7676_p2888

Upsilon1SMu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.424102.Pythia8B_A14_CTEQ6L1_Upsilon1S_mu4mu4.merge.DAOD_EXOT20.e4293_s2608_s2183_r7772_r7676_p2888
Upsilon2SMu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.424105.Pythia8B_A14_CTEQ6L1_Upsilon2S_mu4mu4.merge.DAOD_EXOT20.e5104_s2726_r7772_r7676_p2888
Upsilon3SMu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.424106.Pythia8B_A14_CTEQ6L1_Upsilon3S_mu4mu4.merge.DAOD_EXOT20.e5104_s2726_r7772_r7676_p2888

JZ0Wmu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.427000.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W_mufilter.merge.DAOD_EXOT20.e3968_s2608_s2183_r7725_r7676_p2888
JZ1WAmu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.427030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WA_mufilter.merge.DAOD_EXOT20.e3968_s2608_s2183_r7725_r7676_p2888
JZRW1Bmu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.427031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZRW1B_mufilter.merge.DAOD_EXOT20.e3968_s2608_s2183_r7725_r7676_p2888
#JZRW2mu=mc15c_13TeV/EXOT20_v2/
JZRW3mu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.427033.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZRW3_mufilter.merge.DAOD_EXOT20.e3968_s2608_s2183_r7725_r7676_p2888
JZRW4mu=mc15c_13TeV/EXOT20_v2/mc15_13TeV.427034.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZRW4_mufilter.merge.DAOD_EXOT20.e3968_s2608_s2183_r7725_r7676_p2888

#%%%%%%%%%%%%%%%%
#SIGNAL SAMPLES %
#%%%%%%%%%%%%%%%%

VLQm11ct1=mc15_13TeV/DispDileptonDerivation/signal/mc15_13TeV.302064.MadGraphPythia8EvtGen_A14NNPDF23LO_VLQm11ct1.merge.AOD.e3965_a766_a767_r6264
VLQm11ct5=mc15_13TeV/DispDileptonDerivation/signal/mc15_13TeV.302065.MadGraphPythia8EvtGen_A14NNPDF23LO_VLQm11ct5.merge.AOD.e3965_a766_a767_r6264
VLQm7ct1=mc15_13TeV/DispDileptonDerivation/signal/mc15_13TeV.302062.MadGraphPythia8EvtGen_A14NNPDF23LO_VLQm7ct1.merge.AOD.e3965_s2698_r6869_r6282
VLQm7ct5=mc15_13TeV/DispDileptonDerivation/signal/mc15_13TeV.302063.MadGraphPythia8EvtGen_A14NNPDF23LO_VLQm7ct5.merge.AOD.e3965_s2698_r6869_r6282
VLQm3ct1=mc15_13TeV/DispDileptonDerivation/signal/mc15_13TeV.302060.MadGraphPythia8EvtGen_A14NNPDF23LO_VLQm3ct1.merge.AOD.e3965_s2698_r6869_r6282
VLQm3ct5=mc15_13TeV/DispDileptonDerivation/signal/mc15_13TeV.302061.MadGraphPythia8EvtGen_A14NNPDF23LO_VLQm3ct5.merge.AOD.e3965_s2698_r6869_r6282

GGMm300ct1000=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305631.MGPy8EG_A14N23LO_GG_ZG_1100_300_1000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0
GGMm300ct5000=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305632.MGPy8EG_A14N23LO_GG_ZG_1100_300_5000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0
GGMm700ct1000=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305633.MGPy8EG_A14N23LO_GG_ZG_1100_700_1000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0
GGMm700ct5000=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305634.MGPy8EG_A14N23LO_GG_ZG_1100_700_5000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0
GGMm1000ct1000=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305635.MGPy8EG_A14N23LO_GG_ZG_1100_1000_1000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0
GGMm1000ct5000=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305636.MGPy8EG_A14N23LO_GG_ZG_1100_1000_5000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0

Zdm20ct50=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305018.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m20ct50.merge.AOD.e4827_a766_a821_r7676_s3_EXT0
Zdm20ct50AllMu=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305019.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m20ct50.merge.AOD.e4827_a766_a821_r7676_s3_EXT0
Zdm40ct50=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305020.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m40ct50.merge.AOD.e4827_a766_a821_r7676_s3_EXT0
Zdm40ct50AllMu=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305021.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m40ct50.merge.AOD.e4827_a766_a821_r7676_s3_EXT0
Zdm40ct500=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305022.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m40ct500.merge.AOD.e4827_a766_a810_r6282_s3_EXT0
Zdm40ct500AllMu=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305023.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m40ct500.merge.AOD.e4827_a766_a810_r6282_s3_EXT0
Zdm60ct50=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305024.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m60ct50.merge.AOD.e4827_a766_a810_r6282_s3_EXT0
Zdm60ct50AllMu=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305025.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m60ct50.merge.AOD.e4827_a766_a810_r6282_s3_EXT0
Zdm60ct500=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305026.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m60ct500.merge.AOD.e4827_a766_a810_r6282_s3_EXT0
Zdm60ct500AllMu=mc15c_13TeV/EXOT20_v2/user.nrbern.mc15_13TeV.305027.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m60ct500.merge.AOD.e4827_a766_a810_r6282_s3_EXT0

ISDATA=0

if [ "$1" = "data16PeriodA" ];
  then SAMPLE=($data16PeriodA);
  OUTNAME=("data16PeriodA");
  ISDATA=1
elif [ "$1" = "data16PeriodB" ];
  then SAMPLE=($data16PeriodB);
  OUTNAME=("data16PeriodB");
  ISDATA=1
elif [ "$1" = "data16PeriodC" ];
  then SAMPLE=($data16PeriodC);
  OUTNAME=("data16PeriodC");
  ISDATA=1
elif [ "$1" = "data16PeriodD" ];
  then SAMPLE=($data16PeriodD);
  OUTNAME=("data16PeriodD");
  ISDATA=1
elif [ "$1" = "data16PeriodE" ];
  then SAMPLE=($data16PeriodE);
  OUTNAME=("data16PeriodE");
  ISDATA=1
elif [ "$1" = "data16PeriodF" ];
  then SAMPLE=($data16PeriodF);
  OUTNAME=("data16PeriodF");
  ISDATA=1
elif [ "$1" = "data16PeriodG" ];
  then SAMPLE=($data16PeriodG);
  OUTNAME=("data16PeriodG");
  ISDATA=1
elif [ "$1" = "data16PeriodI" ];
  then SAMPLE=($data16PeriodI);
  OUTNAME=("data16PeriodI");
  ISDATA=1
elif [ "$1" = "data16PeriodK" ];
  then SAMPLE=($data16PeriodK);
  OUTNAME=("data16PeriodK");
  ISDATA=1
elif [ "$1" = "data16PeriodL" ];
  then SAMPLE=($data16PeriodL);
  OUTNAME=("data16PeriodL");
  ISDATA=1
elif [ "$1" = "dataPeriodD" ];
  then SAMPLE=($dataPeriodD);
  OUTNAME=("dataPeriodD");
  ISDATA=1
elif [ "$1" = "dataPeriodE" ];
  then SAMPLE=($dataPeriodE);
  OUTNAME=("dataPeriodE");
  ISDATA=1
elif [ "$1" = "dataPeriodF" ];
  then SAMPLE=($dataPeriodF);
  OUTNAME=("dataPeriodF");
  ISDATA=1
elif [ "$1" = "dataPeriodG" ];
  then SAMPLE=($dataPeriodG);
  OUTNAME=("dataPeriodG");
  ISDATA=1
elif [ "$1" = "dataPeriodH" ];
  then SAMPLE=($dataPeriodH);
  OUTNAME=("dataPeriodH");
  ISDATA=1
elif [ "$1" = "dataPeriodJ" ];
  then SAMPLE=($dataPeriodJ);
  OUTNAME=("dataPeriodJ");
  ISDATA=1
elif [ "$1" = "Zmumu" ];
  then SAMPLE=($Zmumu);
  OUTNAME=("Zmumu");
elif [ "$1" = "ttbar" ];
  then SAMPLE=($ttbar);
  OUTNAME=("ttbar");
elif [ "$1" = "STtchanT" ];
  then SAMPLE=($STtchanT);
  OUTNAME=("STtchanT");
elif [ "$1" = "STtchanTB" ];
  then SAMPLE=($STtchanTB);
  OUTNAME=("STtchanTB");
elif [ "$1" = "STwtchanT" ];
  then SAMPLE=($STwtchanT);
  OUTNAME=("STwtchanT");
elif [ "$1" = "STwtchanTB" ];
  then SAMPLE=($STwtchanTB);
  OUTNAME=("STwtchanTB");
elif [ "$1" = "STschanT" ];
  then SAMPLE=($STschanT);
  OUTNAME=("STschanT");
elif [ "$1" = "STschanTB" ];
  then SAMPLE=($STschanTB);
  OUTNAME=("STschanTB");
elif [ "$1" = "DY6M10" ];
  then SAMPLE=($DY6M10);
  OUTNAME=("DY6M10");
elif [ "$1" = "DY10M60" ];
  then SAMPLE=($DY10M60);
  OUTNAME=("DY10M60");
elif [ "$1" = "DY10M40_0Pt70BVeto" ];
  then SAMPLE=($DY10M40_0Pt70BVeto);
  OUTNAME=("DY10M40_0Pt70BVeto");
elif [ "$1" = "DY10M40_0Pt70BFilter" ];
  then SAMPLE=($DY10M40_0Pt70BFilter);
  OUTNAME=("DY10M40_0Pt70BFilter");
elif [ "$1" = "DY10M40_70Pt140BVeto" ];
  then SAMPLE=($DY10M40_70Pt140BVeto);
  OUTNAME=("DY10M40_70Pt140BVeto");
elif [ "$1" = "DY10M40_70Pt140BFilter" ];
  then SAMPLE=($DY10M40_70Pt140BFilter);
  OUTNAME=("DY10M40_70Pt140BFilter");
elif [ "$1" = "DY10M40_140Pt400BVeto" ];
  then SAMPLE=($DY10M40_140Pt400BVeto);
  OUTNAME=("DY10M40_140Pt400BVeto");
elif [ "$1" = "DY10M40_140Pt400BFilter" ];
  then SAMPLE=($DY10M40_140Pt400BFilter);
  OUTNAME=("DY10M40_140Pt400BFilter");
elif [ "$1" = "DY10M40_400PtECMBVeto" ];
  then SAMPLE=($DY10M40_400PtECMBVeto);
  OUTNAME=("DY10M40_400PtECMBVeto");
elif [ "$1" = "DY10M40_400PtECMBFilter" ];
  then SAMPLE=($DY10M40_400PtECMBFilter);
  OUTNAME=("DY10M40_400PtECMBFilter");
elif [ "$1" = "DY70M120" ];
  then SAMPLE=($DY70M120);
  OUTNAME=("DY70M120");
elif [ "$1" = "Wplus" ];
  then SAMPLE=($Wplus);
  OUTNAME=("Wplus");
elif [ "$1" = "Wminus" ];
  then SAMPLE=($Wminus);
  OUTNAME=("Wminus");
elif [ "$1" = "Wall" ];
  then SAMPLE=($Wplus $Wminus);
  OUTNAME=("Wall");
elif [ "$1" = "ZZvvll" ];
  then SAMPLE=($ZZvvll);
  OUTNAME=("ZZvvll");
elif [ "$1" = "ZZllll" ];
  then SAMPLE=($ZZllll);
  OUTNAME=("ZZllll");
elif [ "$1" = "WWlvlv" ];
  then SAMPLE=($WWlvlv);
  OUTNAME=("WWlvlv");
elif [ "$1" = "WZlvll" ];
  then SAMPLE=($WZlvll);
  OUTNAME=("WZlvll");
elif [ "$1" = "Upsilon1SMu" ];
  then SAMPLE=($Upsilon1SMu);
  OUTNAME=("Upsilon1SMu");
elif [ "$1" = "Upsilon2SMu" ];
  then SAMPLE=($Upsilon2SMu);
  OUTNAME=("Upsilon2SMu");
elif [ "$1" = "Upsilon3SMu" ];
  then SAMPLE=($Upsilon3SMu);
  OUTNAME=("Upsilon3SMu");
elif [ "$1" = "JZ0Wmu" ];
  then SAMPLE=($JZ0Wmu);
  OUTNAME=("JZ0Wmu");
elif [ "$1" = "JZ1WAmu" ];
  then SAMPLE=($JZ1WAmu);
  OUTNAME=("JZ1WAmu");
elif [ "$1" = "JZRW1Bmu" ];
  then SAMPLE=($JZRW1Bmu);
  OUTNAME=("JZRW1Bmu");
elif [ "$1" = "JZRW2mu" ];
  then SAMPLE=($JZRW2mu);
  OUTNAME=("JZRW2mu");
elif [ "$1" = "JZRW3mu" ];
  then SAMPLE=($JZRW3mu);
  OUTNAME=("JZRW3mu");
elif [ "$1" = "JZRW4mu" ];
  then SAMPLE=($JZRW4mu);
  OUTNAME=("JZRW4mu");
elif [ "$1" = "GGMm300ct1000" ];
  then SAMPLE=($GGMm300ct1000);
  OUTNAME=("GGMm300ct1000");
elif [ "$1" = "GGMm300ct5000" ];
  then SAMPLE=($GGMm300ct5000);
  OUTNAME=("GGMm300ct5000");
elif [ "$1" = "GGMm700ct1000" ];
  then SAMPLE=($GGMm700ct1000);
  OUTNAME=("GGMm700ct1000");
elif [ "$1" = "GGMm700ct5000" ];
  then SAMPLE=($GGMm700ct5000);
  OUTNAME=("GGMm700ct5000");
elif [ "$1" = "GGMm1000ct1000" ];
  then SAMPLE=($GGMm1000ct1000);
  OUTNAME=("GGMm1000ct1000");
elif [ "$1" = "GGMm1000ct5000" ];
  then SAMPLE=($GGMm1000ct5000);
  OUTNAME=("GGMm1000ct5000");
elif [ "$1" = "VLQct5" ];
  then SAMPLE=($VLQct5);
  OUTNAME=("VLQct5");
elif [ "$1" = "VLQm7ct1" ];
  then SAMPLE=($VLQm7ct1);
  OUTNAME=("VLQm7ct1");
elif [ "$1" = "VLQm7ct5" ];
  then SAMPLE=($VLQm7ct5);
  OUTNAME=("VLQm7ct5");
elif [ "$1" = "VLQm3ct1" ];
  then SAMPLE=($VLQm3ct1);
  OUTNAME=("VLQm3ct1");
elif [ "$1" = "VLQm3ct5" ];
  then SAMPLE=($VLQm3ct5);
  OUTNAME=("VLQm3ct5");
elif [ "$1" = "VLQm11ct1" ];
  then SAMPLE=($VLQm11ct1);
  OUTNAME=("VLQm11ct1");
elif [ "$1" = "VLQm11ct5" ];
  then SAMPLE=($VLQm11ct5);
  OUTNAME=("VLQm11ct5");
elif [ "$1" = "Zdm20ct50" ];
  then SAMPLE=($Zdm20ct50);
  OUTNAME=("Zdm20ct50");
elif [ "$1" = "Zdm20ct50AllMu" ];
  then SAMPLE=($Zdm20ct50AllMu);
  OUTNAME=("Zdm20ct50AllMu");
elif [ "$1" = "Zdm40ct50" ];
  then SAMPLE=($Zdm40ct50);
  OUTNAME=("Zdm40ct50");
elif [ "$1" = "Zdm40ct50AllMu" ];
  then SAMPLE=($Zdm40ct50AllMu);
  OUTNAME=("Zdm40ct50AllMu");
elif [ "$1" = "Zdm40ct500" ];
  then SAMPLE=($Zdm40ct500);
  OUTNAME=("Zdm40ct500");
elif [ "$1" = "Zdm40ct500AllMu" ];
  then SAMPLE=($Zdm40ct500AllMu);
  OUTNAME=("Zdm40ct500AllMu");
elif [ "$1" = "Zdm60ct50" ];
  then SAMPLE=($Zdm60ct50);
  OUTNAME=("Zdm60ct50");
elif [ "$1" = "Zdm60ct50AllMu" ];
  then SAMPLE=($Zdm60ct50AllMu);
  OUTNAME=("Zdm60ct50AllMu");
elif [ "$1" = "Zdm60ct500" ];
  then SAMPLE=($Zdm60ct500);
  OUTNAME=("Zdm60ct500");
elif [ "$1" = "Zdm60ct500AllMu" ];
  then SAMPLE=($Zdm60ct500AllMu);
  OUTNAME=("Zdm60ct500AllMu");
fi

for i in ${!SAMPLE[*]};
  do
  jj=1;
  PATHVAR=${PATHBASEMC}${SAMPLE[$i]}
  if [ $ISDATA = 1 ]; then
    PATHVAR=${PATHBASEDATA}${SAMPLE[$i]}
  fi
  echo $PATHVAR
  FINALOUT="testRun Torque ${OUTNAME[$i]}_$jj ${PATHVAR}"
  COUNTERVAR=1
  NUMFILES=$2
  OUTPUT=submit_${OUTNAME[$i]}.sh

  `rm ${OUTPUT}`
  `touch ${OUTPUT}`

  for entry in "${PATHVAR}"/*
    do
    if [ $ISDATA = 0 ]; then
      FINALOUT+=" $(echo $entry | cut -d"/" -f9)";
    elif [ $ISDATA = 1 ]; then
      FINALOUT+=" $(echo $entry | cut -d"/" -f9)";
    fi
    if [ $((COUNTERVAR%NUMFILES)) = 0 ];
      then echo $FINALOUT >> ${OUTPUT};
      jj=$((jj+1));
      FINALOUT="testRun Torque ${OUTNAME[$i]}_$jj ${PATHVAR}"; 
    fi 
    COUNTERVAR=$((COUNTERVAR+1))
  done

  if [ "$FINALOUT" != "testRun Torque ${OUTNAME[$i]}_$jj ${PATHVAR}" ];
    then echo $FINALOUT >> ${OUTPUT};
  fi

  if [ $# == 3 ]; then
    eval "cd ../../"
    eval "source DisplacedDimuonAnalysis/Scripts/${OUTPUT}"
    eval "rm DisplacedDimuonAnalysis/Scripts/${OUTPUT}"
    eval "cd DisplacedDimuonAnalysis/Scripts"
  fi

done 

