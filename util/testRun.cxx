#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include "xAODRootAccess/Init.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/Sample.h"
#include "SampleHandler/SampleLocal.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/TorqueDriver.h"
#include "EventLoop/CondorDriver.h"
#include "EventLoop/ProofDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/DiskListXRD.h"
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include "DisplacedDimuonAnalysis/DisplacedDimuonAnalysis.h"
#include "TLegend.h"
#include "THStack.h"
#include "TFile.h"
#include <TSystem.h>
#include <memory>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <iterator>

int main( int argc, char* argv[] ) {

   //turn data submission off
   //xAOD::TFileAccessTracer::enableDataSubmission(false);

   //exit if there is the wrong number of arguments
   if(argc < 5 && argv[1] != std::string("Grid")) {
     std::cout << "ERROR: wrong number of arguments" << std::endl;
     std::cout << "argument format (local): testRun outputDirectory sampleType (dataCosmic, dataPeriodD, dataPeriodG, DY10M40_0Pt70BVeto, DY10M40_0Pt70BFilter, DY10M40_70Pt140BVeto, DY10M40_70Pt140BFilter, Upsilon3p5mu, JZ0Wmu, JZ1WAmu, JZRW1Bmu, JZRW2mu, JZRW3mu, JZRW4mu, DY6M10, DY10M60, DY70M120, VLQ, VLQct1, VLQct5, VLQm3ct1, VLQm7ct1, Zdm20ct05, Zdm40ct05, Zdm40ct05mm, Zdm40ct5mm, Zdm60ct05, Zdm60ct5mm, Zmumu, ttbar, Wplus, Wminus,ZZ) sampleLength (short,long) driverType (Direct, DirectBNL, Proof)" << std::endl;
     std::cout << "argument format (batch): testRun Torque(Condor) outputName dirPath fileName [filename2 filename3 ...]" << std::endl;
     return  0.;
   }

   //first the local setup for Direct or Proof drivers
   if (argv[1] != std::string("Torque") && argv[1] != std::string("Condor") && argv[1] != std::string("Grid")) {
     std::cout << "local run" << std::endl;
   
     std::string submitDir = argv[1];
     std::string sampleType = argv[2];
     std::string sampleLength = argv[3];
     std::string driverType = argv[4];
     
     //Check that arguments are valid 
     if (sampleType != "GGMm300ct1" && sampleType != "GGMm300ct5" && sampleType != "GGMm700ct1" && sampleType != "GGMm700ct5" && sampleType != "GGMm1000ct1" && sampleType != "GGMm1000ct5" && sampleType != "data16PeriodB" && sampleType != "data16PeriodC" && sampleType != "dataCosmic" && sampleType != "dataPeriodD" && sampleType != "dataPeriodE" && sampleType != "dataPeriodF" && sampleType != "dataPeriodG" && sampleType != "dataPeriodH" && sampleType != "dataPeriodJ" && sampleType != "DY10M40_0Pt70BVeto" && sampleType != "DY10M40_0Pt70BFilter" && sampleType != "DY10M40_70Pt140BVeto" && sampleType != "DY10M40_70Pt140BFilter" && sampleType != "Upsilon3p5mu" && sampleType != "JZ0Wmu" && sampleType != "JZ1WAmu" && sampleType != "JZRW1Bmu" && sampleType != "JZRW2mu" && sampleType != "JZRW3mu" && sampleType != "JZRW4mu" && sampleType != "DY6M10" && sampleType != "DY10M60" && sampleType != "DY70M120" && sampleType != "Zdm20ct05" && sampleType != "Zdm20ct05mm" && sampleType != "Zdm40ct05" && sampleType != "Zdm40ct05mm" && sampleType != "Zdm40ct5" && sampleType != "Zdm40ct5mm" && sampleType != "Zdm60ct05" && sampleType != "Zdm60ct05mm" && sampleType != "Zdm60ct5" && sampleType != "Zdm60ct5mm" && sampleType != "Zmumu" && sampleType != "ttbar" && sampleType != "Wplus" && sampleType != "Wminus" && sampleType != "ZZ"){
       std::cout << "ERROR: unknown value for sampleLength" << std::endl;
       std::cout << "acceptable values: VLQ, VLQct1, VLQct5, Zmumu, ttbar" << std::endl;
       return 0.;
     }
     if (sampleLength != "short" && sampleLength != "long"){
       std::cout << "ERROR: unknown value for sampleLength" << std::endl;
       std::cout << "acceptable values: short, long" << std::endl;
       return 0.;
     }
     if (driverType != "Direct" && driverType != "Proof"){
       std::cout << "ERROR: unknown value for driverType" << std::endl;
       std::cout << "acceptable values: Direct, Proof" << std::endl;
       return 0.;
     }  

     // Set up the job for xAOD access:
     xAOD::Init().ignore();

    // create a new sample handler to describe the data files we use
    SH::SampleHandler sh;

    std::string prelimPath("");
    std::string samplePath("");
    
    //determine correct path for sampleType
    if (driverType != std::string("DirectBNL")) {
      // running on titan

      prelimPath = "/titan/atlas/common/xAOD/";
      if (sampleType.find("dataP") != std::string::npos) prelimPath += "data15_13TeV/DispDileptonDerivation";
      else if (sampleType.find("data16P") != std::string::npos) prelimPath += "data16_13TeV/EXOT20_v2";
      //else prelimPath += "mc15c_13TeV/DispDileptonDerivation";
      else prelimPath += "mc15c_13TeV";

      if (sampleType == "dataCosmic"){
        //prelimPath = "/data2/nrbern/COSMICS";
	//samplePath = "/data15_cos.00258274.physics_IDCosmic.merge.AOD.r6764_p2358";
        samplePath = "/data16_cos.00295658.physics_CosmicMuons.merge.AOD.f681_m1565"; 
      } else if (sampleType == "data16PeriodB"){
        samplePath = "/data16_13TeV.periodB.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950";
        //samplePath = "/data16_13TeV.periodL.physics_Main.PhysCont.DAOD_EXOT20.grp16_v01_p2950";
      } else if (sampleType == "data16PeriodC"){
        samplePath = "/data16_13TeV.periodC.physics_Main.merge.DAOD_EXOT20.f711_m1620_p2889";
      } else if (sampleType == "dataPeriodD"){
	samplePath = "/user.carlod.data15_13TeV.periodD.physics_Main.PhysCont.AOD.repro20_v02_t0_EXT0";
      } else if (sampleType == "dataPeriodE"){
        samplePath = "/user.carlod.data15_13TeV.periodE.physics_Main.PhysCont.AOD.repro20_v02_t0_EXT0";
      } else if (sampleType == "dataPeriodF"){
        samplePath = "/user.carlod.data15_13TeV.periodF.physics_Main.PhysCont.AOD.repro20_v02_t0_EXT0";
      } else if (sampleType == "dataPeriodG"){
        prelimPath = "/data2/nrbern";
        samplePath = "/data15_13TeV.00281075.physics_Main.merge.AOD.r7562_p2521";
        //samplePath = "/user.carlod.data15_13TeV.periodG.physics_Main.PhysCont.AOD.repro20_v02_t0_EXT0";
        //samplePath = "/user.nrbern.data15_13TeV.periodG.physics_Main.PhysCont.AOD.repro20_v02_s1_EXT0";
        //samplePath = "/data15_13TeV.00280862.physics_Main.merge.AOD.r7562_p2521";
      } else if (sampleType == "dataPeriodH"){
        samplePath = "/user.carlod.data15_13TeV.periodH.physics_Main.PhysCont.AOD.repro20_v02_t0_EXT0";
      } else if (sampleType == "dataPeriodJ"){
        samplePath = "/user.carlod.data15_13TeV.periodJ.physics_Main.PhysCont.AOD.repro20_v02_t0_EXT0";
      } else if (sampleType == "Upsilon3p5mu"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
 	samplePath = "/background/user.nrbern.mc15_13TeV.300101.Pythia8BPhotospp_A14_CTEQ6L1_pp_Upsilon1S_mu3p5mu3p5.merge.AOD.e4676_a766_a807_r6282_t0_EXT0";
      } else if (sampleType == "JZ0Wmu"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath = "/background/user.nrbern.mc15_13TeV.427000.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W_mufilter.merge.AOD.e3968_s2608_s2183_r7326_r6282_EXT0";
      } else if (sampleType == "JZ1WAmu"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath = "/background/user.nrbern.mc15_13TeV.427030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WA_mufilter.merge.AOD.e3968_s2608_s2183_r7326_r6282_EXT0";
      } else if (sampleType == "JZRW1Bmu"){
        samplePath = "/background/user.nrbern.mc15_13TeV.427031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZRW1B_mufilter.merge.AOD.e3968_s2608_s2183_r7725_r7676_v1_EXT0";
      } else if (sampleType == "JZRW2mu"){
	samplePath ="/background/user.nrbern.mc15_13TeV.427032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZRW2_mufilter.merge.AOD.e3968_s2608_s2183_r7725_r7676_v1_EXT0";
      } else if (sampleType == "JZRW3mu"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath ="/background/user.nrbern.mc15_13TeV.427033.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZRW3_mufilter.merge.AOD.e3968_s2608_s2183_r7326_r6282_EXT0";
      } else if (sampleType == "JZRW4mu"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath = "/background/user.nrbern.mc15_13TeV.427034.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZRW4_mufilter.merge.AOD.e3968_s2608_s2183_r7326_r6282_EXT0";
      } else if (sampleType == "DY6M10"){
        samplePath = "/background/user.nrbern.mc15_13TeV.361666.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_6M10.merge.AOD.e4770_s2726_r7772_r7676_v1_EXT0";
      } else if (sampleType == "DY10M60"){
        samplePath = "/background/user.nrbern.mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.AOD.e4770_s2726_r7772_r7676_v1_EXT0";
      } else if (sampleType == "DY10M40_0Pt70BVeto"){
        samplePath = "/background/user.nrbern.mc15_13TeV.361476.Sherpa_CT10_Zmumu_Mll10to40_Pt0_70_BVeto.merge.AOD.e4198_s2608_s2183_r7772_r7676_v1_EXT0";
      } else if (sampleType == "DY10M40_0Pt70BFilter"){
	samplePath = "/background/user.nrbern.mc15_13TeV.361477.Sherpa_CT10_Zmumu_Mll10to40_Pt0_70_BFilter.merge.AOD.e4198_s2608_s2183_r7772_r7676_v1_EXT0";
      } else if (sampleType == "DY10M40_70Pt140BVeto"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath = "/background/user.nrbern.mc15_13TeV.361478.Sherpa_CT10_Zmumu_Mll10to40_Pt70_140_BVeto.merge.AOD.e4198_s2608_s2183_r7326_r6282_EXT0";
      } else if (sampleType == "DY10M40_70Pt140BFilter"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath = "/background/user.nrbern.mc15_13TeV.361479.Sherpa_CT10_Zmumu_Mll10to40_Pt70_140_BFilter.merge.AOD.e4198_s2608_s2183_r7326_r6282_EXT0"; 
      } else if (sampleType == "DY70M120"){
        samplePath = "/background/user.nrbern.mc15_13TeV.301560.Pythia8EvtGen_A14NNPDF23LO_DYmumu_70M120.merge.AOD.e3882_s2608_s2183_r7772_r7676_v1_EXT0";
      } else if (sampleType == "GGMm300ct1"){
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305631.MGPy8EG_A14N23LO_GG_ZG_1100_300_1000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "GGMm300ct5"){
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305632.MGPy8EG_A14N23LO_GG_ZG_1100_300_5000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "GGMm700ct1"){
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305633.MGPy8EG_A14N23LO_GG_ZG_1100_700_1000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "GGMm700ct5"){
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305634.MGPy8EG_A14N23LO_GG_ZG_1100_700_5000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "GGMm1000ct1"){
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305635.MGPy8EG_A14N23LO_GG_ZG_1100_1000_1000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "GGMm1000ct5"){
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305636.MGPy8EG_A14N23LO_GG_ZG_1100_1000_5000.merge.AOD.e5151_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "Zdm20ct05") {
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305018.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m20ct50.merge.AOD.e4827_a766_a821_r7676_s3_EXT0"; 
      } else if (sampleType == "Zdm20ct05mm") {
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
	samplePath = "/user.nrbern.mc15_13TeV.305019.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m20ct50.merge.AOD.e4827_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "Zdm40ct05") {
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305020.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m40ct50.merge.AOD.e4827_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "Zdm40ct05mm") {
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
	samplePath = "/user.nrbern.mc15_13TeV.305021.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m40ct50.merge.AOD.e4827_a766_a821_r7676_s3_EXT0";
      } else if (sampleType == "Zdm40ct5") {
	prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305022.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m40ct500.merge.AOD.e4827_a766_a810_r6282_s3_EXT0";
      } else if (sampleType == "Zdm40ct5mm") {
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305023.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m40ct500.merge.AOD.e4827_a766_a810_r6282_s3_EXT0";
      } else if (sampleType == "Zdm60ct05") {
	prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305024.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m60ct50.merge.AOD.e4827_a766_a810_r6282_s3_EXT0";
      } else if (sampleType == "Zdm60ct05mm") {
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305025.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m60ct50.merge.AOD.e4827_a766_a810_r6282_s3_EXT0";
      } else if (sampleType == "Zdm60ct5") {
        prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305026.MadGraphPythia8EvtGen_A14N23LO_zpzpmmff_m60ct500.merge.AOD.e4827_a766_a810_r6282_s3_EXT0";
      } else if (sampleType == "Zdm60ct5mm") {
	prelimPath = "/titan/atlas/common/xAOD/mc15c_13TeV/EXOT20_v2";
        samplePath = "/user.nrbern.mc15_13TeV.305027.MadGraphPythia8EvtGen_A14N23LO_zpzpmmmm_m60ct500.merge.AOD.e4827_a766_a810_r6282_s3_EXT0";
      } else if (sampleType == "Zmumu"){
        samplePath = "/EXOT20_v2/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_EXOT20.e3601_s2576_s2132_r7725_r7676_p2888/";
      } else if (sampleType == "ttbar"){
        samplePath = "/EXOT20/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_EXOT20.e3698_s2608_s2183_r7725_r7676_p2823";
      } else if (sampleType == "Wplus"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath = "/background/user.nrbern.mc15_13TeV.361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu.merge.AOD.e3601_s2576_s2132_r6725_r6282_EXT0";
      } else if (sampleType == "Wminus"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath = "/background/user.nrbern.mc15_13TeV.361104.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusmunu.merge.AOD.e3601_s2576_s2132_r6725_r6282_EXT0";
      } else if (sampleType == "ZZ"){
        prelimPath = "/titan/atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation";
	samplePath = "/background/user.carlod.mc15_13TeV.361063.Sherpa_CT10_llll.merge.AOD.e3836_s2608_s2183_r6869_r6282_EXT0";
      }
    } else {
      // running at BNL

      prelimPath = "/pnfs/usatlas.bnl.gov/users/carlod/rucio/user.carlod";

      if (sampleType == "Zmumu"){
	samplePath = "/user.carlod.mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e3601_s2576_s2132_r7267_r6282_EXT0";
      }
 
    }
    
    std::string fullPath = prelimPath + samplePath;

    //Fill the samplehandler for Direct Driver
    if (driverType == "Direct" || driverType == "DirectBNL"){
      std::string lsCmd = std::string("ls ") + fullPath;
      std::string inputHistos(gSystem->GetFromPipe(lsCmd.c_str()));
      std::istringstream iss(inputHistos.c_str());
      std::string firstFile;
      iss >> firstFile;
      const char* inputFilePath = gSystem->ExpandPathName (fullPath.c_str());
      SH::DiskListLocal list (inputFilePath);
      if (sampleLength == "long") SH::ScanDir().scan(sh, list);
      else SH::ScanDir().filePattern(firstFile.c_str()).scan(sh,list);
    //Next fill the samplehandler for Proof Driver
    } else if (driverType == "Proof"){
      // Only set up for titan, for now
      std::string lsCmd = std::string("xrdfs titan.physics.umass.edu ls /atlas/common/xAOD/mc15_13TeV/DispDileptonDerivation") + samplePath;
      std::string inputHistos(gSystem->GetFromPipe(lsCmd.c_str()));
      std::istringstream iss(inputHistos.c_str());
      std::string firstFile;
      iss >> firstFile;
      std::string delimiter = "/";
      size_t pos =0;
      std::string token;
      while ((pos = firstFile.find(delimiter)) != std::string::npos) {
        token = firstFile.substr(0,pos);
        std::cout << token << std::endl;
        firstFile.erase(0,pos + delimiter.length());
      }
      std::cout << firstFile << std::endl;
      SH::DiskListXRD list ("titan.physics.umass.edu",(fullPath.erase(0,6)).c_str(),true);
      if (sampleLength == "long") SH::ScanDir().scan(sh, list);
      else SH::ScanDir().filePattern(firstFile.c_str()).scan(sh,list);
    }

    // set the name of the tree in our files
    sh.setMetaString ("nc_tree", "CollectionTree");

    // print out the samples we found
    sh.print ();

    // this is the basic description of our job
    EL::Job job;
    job.sampleHandler (sh);
    sh.setMetaDouble (EL::Job::optMaxEvents,50000);

    // define an output and an ntuple associated to that output
    EL::OutputStream output("myOutput");
    job.outputAdd(output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc("myOutput");
    job.algsAdd(ntuple);

    // add our algorithm to the job
    DisplacedDimuonAnalysis *alg = new DisplacedDimuonAnalysis();
    
    job.algsAdd (alg);

    alg->outputName = "myOutput";
    alg->currentSampleName = sampleType; 

    //Submit the job
    if (driverType == "Proof") {
      EL::ProofDriver driver;
      driver.proofMaster = "lite://";
      driver.numWorkers = 8;
      driver.makeParOptions = "--lite";
      driver.submit (job, submitDir);
    } else {
      EL::DirectDriver driver;
      driver.submit (job, submitDir);
    } 

    //Setup for batch jobs
   } else if (argv[1] == std::string("Torque") || argv[1] == std::string("Condor")){
    std::cout << "batch run" << std::endl;
    
    //Note that arguments to testRun are different for batch!
    std::string dirPath = argv[3];
    std::string dirPath2("");
    
    if (argv[1] == std::string("Torque") ) dirPath2 = std::string("root://titan.physics.umass.edu/") + std::string(dirPath).erase(0,6);
    else dirPath2 = dirPath;
    
    std::string fileName = argv[4];
    std::string outputDir = std::string("DisplacedDimuonAnalysis/Output/") + argv[2];

    // Set up the job for xAOD access:
    xAOD::Init().ignore();
    
    // create a new sample handler to describe the data files we use
    SH::SampleHandler sh;

    //fill the samplehandler
    for (int i=4;i<argc;i++){
      std::string fullPath = dirPath2 + std::string("/") + std::string(argv[i]);
      std::string sampleName = std::string("sample") + std::to_string(i-3);
      std::auto_ptr<SH::SampleLocal> sample (new SH::SampleLocal(sampleName));
      sample->add (fullPath);
      sh.add (sample.release());
    }

    // set the name of the tree in our files
    sh.setMetaString ("nc_tree", "CollectionTree");

    // print out the samples we found
    sh.print ();

    // this is the basic description of our job
    EL::Job job;
    job.sampleHandler (sh);
    
    // define an output and an ntuple associated to that output
    EL::OutputStream output("myOutput");
    job.outputAdd(output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc("myOutput");
    job.algsAdd(ntuple);

    // add our algorithm to the job
    DisplacedDimuonAnalysis *alg = new DisplacedDimuonAnalysis();


    job.algsAdd (alg);

    alg->outputName = "myOutput";
    alg->currentSampleName = argv[2];

    //submit the job
    if (argv[1] == std::string("Torque") ) {
      EL::TorqueDriver driver;
      driver.shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet";
      //driver.options()->setString(EL::Job::optSubmitFlags,"-l walltime=00:02:30");
      driver.submitOnly(job,outputDir);
    } else {
      
      EL::CondorDriver driver;
      //shellInit not needed. The "getenv=true" option imports the user's full environment to each job
      driver.shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet";
      job.options()->setString(EL::Job::optCondorConf, "getenv = true\naccounting_group = group_atlas.umass\nRequirements = ( TARGET.Memory >= 3002 )");
      driver.submitOnly(job,outputDir);

    }

  } else if (argv[1] == std::string("Grid")) {
    //Grid jobs
   
    xAOD::Init().ignore();

    // create a new sample handler to describe the data files we use
    SH::SampleHandler sh;

    sh.setMetaString ("nc_tree", "CollectionTree");
    std::string sampleName = argv[3]; 
    SH::scanRucio (sh,sampleName);
    // print out the samples we found
    sh.print ();

    EL::Job job;
    job.sampleHandler (sh);

    // define an output and an ntuple associated to that output
    EL::OutputStream output("myOutput");
    job.outputAdd(output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc("myOutput");
    job.algsAdd(ntuple);

    DisplacedDimuonAnalysis *alg = new DisplacedDimuonAnalysis();

    job.algsAdd (alg);

    alg->outputName = "myOutput";
    alg->currentSampleName = argv[3];
    std::string submitDir = argv[2];
 
    EL::PrunDriver driver;
    driver.options()->setString("nc_outputSampleName", "user.nrbern.test.%in:name[2]%.%in:name[6]%"); 
    driver.options()->setString(EL::Job::optGridNFilesPerJob,  "50");
    driver.options()->setDouble(EL::Job::optGridMergeOutput, 1); 
    driver.submitOnly(job,submitDir);
  }
 
  return 0;
}
