#ifndef ANALYSISTOOL_H
#define ANALYSISTOOL_H

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"

class AnalysisTool {

  public:
    static bool isTPVtx(const xAOD::TruthVertex* truthVtx);
    static const xAOD::TruthParticle* getTrueZ(const xAOD::TruthParticle* trueMu1, const xAOD::TruthParticle* trueMu2);
    static const xAOD::TruthParticle* getMuonParent(const xAOD::TruthParticle* truth);
    static const xAOD::TruthParticle* truthLink(const xAOD::TrackParticle* track);
    static const xAOD::TruthParticle* truthLink(const xAOD::Muon* combMu);
    static const xAOD::TrackParticle* combinedTrackLink(const xAOD::Muon* combMu);
    static bool  layerRequirement(const xAOD::TrackParticle* Mu,int phiLayers, int etaLayers);
    static float invMass(const xAOD::TruthParticle* t1,const xAOD::TruthParticle* t2);
    static float invMass(const xAOD::TrackParticle* t1,const xAOD::TrackParticle* t2);
    static float d0(const xAOD::TrackParticle* muon); 
    static float d0(const xAOD::TruthParticle* tp);
    static float z0(const xAOD::TrackParticle* muon);
    static float z0(const xAOD::TruthParticle* tp);
    static float openAngle(const xAOD::TruthParticle* tp1,const xAOD::TruthParticle* tp2);
    static float openAngle(const xAOD::Muon* tp1,const xAOD::Muon* tp2);
    static float deltaR(const xAOD::Jet* j1,const xAOD::TrackParticle* t1);
    static float deltaR(const xAOD::Jet* j1,const xAOD::Muon* t1);
    static float deltaR(const xAOD::TrackParticle* t1,const xAOD::TrackParticle* t2);
    static float deltaR(const xAOD::TrackParticle* t1,const xAOD::Muon* t2);
    static float modDeltaR(const xAOD::TrackParticle* t1,const xAOD::Muon* t2);
    static float deltaR(const xAOD::Muon* t1, const xAOD::Muon* t2);
    static float deltaR(const xAOD::TruthParticle* t1,const xAOD::Muon* t2);

};

#endif
