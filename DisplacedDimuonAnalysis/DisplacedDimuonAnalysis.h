#ifndef DISPDIMUON_ANALYSIS_H
#define DISPDIMUON_ANALYSIS_H

#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include <TH1.h>
#include <TH2F.h>
#include <TH2Poly.h>
#include <TTree.h>
#include <map>
#include <fstream>
#include <iostream>
#include <vector>
#include "TTree.h"
#include "TVector3.h"
#include "TFile.h"
#include "TDirectory.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TStopwatch.h"

// GRL
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

//JVT
#include "JetMomentTools/JetVertexTaggerTool.h"

//Jet Cleaning
#include "JetSelectorTools/JetCleaningTool.h"

//PRW
#include "PileupReweighting/PileupReweightingTool.h"

//JER
#include "JetResolution/JERTool.h"
#include "JetResolution/JERSmearingTool.h"

//JES calibration
#include "JetCalibTools/JetCalibrationTool.h"

#include "../DisplacedDimuonAnalysis/AnalysisTool.h"
#include "../DisplacedDimuonAnalysis/VertexTool.h"
#include "../DisplacedDimuonAnalysis/ContainerTool.h"
#include "../DisplacedDimuonAnalysis/NtupleTool.h"
#include "../DisplacedDimuonAnalysis/HistogramTool.h"

class ContainerTool;
class AnalysisTool;
class VertexTool;
class NtupleTool;

namespace xAOD{
  class TEvent;
}

namespace TrigConf{
  class xAODConfigTool;
} 

namespace Trig{
  class TrigDecisionTool;
  class TrigMuonMatching;
}

namespace CP{
  class MuonCalibrationAndSmearingTool; 
  class MuonSelectionTool; 
}

struct trigMuon {

  trigMuon(int index, float pt,float eta,float phi,int muonType) : m_index(index),m_pt(pt),m_eta(eta),m_phi(phi),m_muonType(muonType) {}
  trigMuon(const trigMuon& t) : m_index(t.index()),m_pt(t.pt()),m_eta(t.eta()),m_phi(t.phi()),m_muonType(t.muonType()) {}
  int index() const{return m_index;}
  float pt() const{return m_pt;}
  float eta() const{return m_eta;}
  float phi() const{return m_phi;}
  int muonType() const{return m_muonType;}
  void print(){ std::cout << "index: " << m_index << " pt: " << m_pt << " eta: " << m_eta << " phi: " << m_phi << " muonType: " << m_muonType << std::endl;}

  int m_index;
  float m_pt;
  float m_eta;
  float m_phi;
  int m_muonType;
  
};

class DisplacedDimuonAnalysis : public EL::Algorithm
{
public:

  //sampleName
  std::string currentSampleName;

  //An Ntuple
  std::string outputName;

  //member variables
  xAOD::TEvent *m_event;  //!
  int m_evt_num;
  trigMap m_trigMap;
  bool m_debug;
  bool m_doTrigList;
  bool m_isBatch;
  double m_sumWeights;
  bool m_filledSumWeights;
  bool m_doMSonlyMuonMomentumCalibration;
  std::map<std::string,int> m_triggerCounts;
  std::string m_dsName; 
  
  // #ifndef __CINT__
  CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool; //! 
  CP::MuonSelectionTool* m_muonSelectionTool; //!
  JetCleaningTool           *m_cleaningTool;  //!
  JetVertexTaggerTool       *m_pjvtag;        //!
  ToolHandle<IJetUpdateJvt>  m_hjvtagup;      //!
  JERTool                   *m_JERTool;       //!
  JERSmearingTool           *m_JERSmearingTool; //!
  ToolHandle<IJERTool>       m_jerHandle;     //!
  JetCalibrationTool        *m_jetCalibTool;  //!
  CP::PileupReweightingTool *m_prwTool;       //!
  GoodRunsListSelectionTool *m_grlTool;       //!
  AnalysisTool              *m_analysisTool;  //!
  VertexTool                *m_vertexTool;    //!
  ContainerTool             *m_containerTool; //!
  TrigConf::xAODConfigTool  *m_configTool;    //!
  Trig::TrigDecisionTool    *m_trigDecTool;   //!
  Trig::TrigMuonMatching    *m_matchTool;     //!
  TFile                     *m_outputFile;    //!
  NtupleTool                *m_ntupleTool;    //!
  // #endif // not __CINT__

  // this is a standard constructor
  DisplacedDimuonAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  //class methods
  void triggerSelection();
  int processTruth();
  std::vector<std::shared_ptr<AO::Muon>> processMSTrackParticles();
  int processVertices(std::vector<std::shared_ptr<AO::Muon>> TPMuons);
  
  //helper methods
  float doJetMuOverlap(xAOD::TrackParticle* mu);
  float doJetMuOverlap(const xAOD::Muon* mu);
  float doMuonIso(xAOD::TrackParticle* Mu,bool direct,bool doPV);
  float doMuonIso(const xAOD::Muon* Mu,bool direct,bool doPV);
  TH1F * Hist1D(std::string name,std::string xAxis, int nbins, Float_t first, Float_t last);
  void doEff1D(TH1F*, TH1F*, TH1F*);
  void calcL1METQ(int Ex, int Ey, int& METQ, bool& Overflow);
  void calcL1MET(int Ex, int Ey, float& MET, bool& Overflow);
  void printChainGroup(std::string cgS);
  bool passLJTrig(std::string cgS);

  // this is needed to distribute the algorithm to the workers
  ClassDef(DisplacedDimuonAnalysis, 1);
};

#endif
