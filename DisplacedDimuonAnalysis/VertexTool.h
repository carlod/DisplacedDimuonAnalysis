#ifndef VERTEXTOOL_H
#define VERTEXTOOL_H

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "TVector3.h"
#include <map>

#include "../DisplacedDimuonAnalysis/Muon.h"

typedef std::map<std::string,int> trigMap;

struct RecVertex {

  //constructor
  RecVertex(std::vector<std::shared_ptr<AO::Muon>>& p, TVector3 vtx,float sgnVtx, float openA,float distB,float m, float q,int iLead,int iSub) :m_muons(std::move(p)),m_vtx(vtx),m_sgnVtx(sgnVtx),m_openA(openA),m_distB(distB),m_invMass(m),m_q(q),m_iLead(iLead),m_iSub(iSub) {
  
    for(auto& it : m_muons){
      m_particles.push_back(it->tp());
      m_particlesMS.push_back(it->tpMS());
    }
    m_jetDRLead = m_muons[0]->jetDR();
    m_jetDRSub = m_muons[1]->jetDR();
  }

  //member functions
  std::vector<const xAOD::TrackParticle*> particles(){return m_particles;}
  const xAOD::TrackParticle* pLead(){return m_particles[0];}
  const xAOD::TrackParticle* pSub(){return m_particles[1];}
  const xAOD::TrackParticle* pMSLead(){return m_particlesMS[0];}
  const xAOD::TrackParticle* pMSSub(){return m_particlesMS[1];}
  TVector3 vertex(){return m_vtx;}
  float x(){return m_vtx.X();}
  float y(){return m_vtx.Y();}
  float z(){return m_vtx.Z();}
  float r(){return sqrt(pow(m_vtx.X(),2)+pow(m_vtx.Y(),2));}
  float l(){return sqrt(pow(this->r(),2)+pow(m_vtx.Z(),2));}
  float sgn(){return m_sgnVtx;}
  float openA(){return m_openA;}
  float distB(){return m_distB;}
  float invMass(){return m_invMass;}
  float charge(){return m_q;}
  float jetDRLead() {return m_jetDRLead;}
  float jetDRSub() {return m_jetDRSub;}
  int iLead() {return m_iLead;}
  int iSub() {return m_iSub;}

  //member variables
  std::vector<const xAOD::TrackParticle*> m_particles;
  std::vector<const xAOD::TrackParticle*> m_particlesMS;
  std::vector<std::shared_ptr<AO::Muon>> m_muons;
  TVector3 m_vtx;
  float m_sgnVtx;
  float m_openA;
  float m_distB;
  float m_invMass;
  float m_q;
  float m_jetDRLead;
  float m_jetDRSub;
  int m_iLead;
  int m_iSub;

};

struct TruthVertex {

TruthVertex(std::vector<const xAOD::TruthParticle*> p,std::vector<std::pair<float,float>> param, TVector3 vtx,float sgnVtx, float openA,float m, int parPDG, int parBarCode,int iLead,int iSub) :m_particles(p),m_param(param),m_vtx(vtx),m_sgnVtx(sgnVtx),m_openA(openA),m_invMass(m),m_parPDG(parPDG),m_parBarCode(parBarCode),m_iLead(iLead),m_iSub(iSub) {}

  std::vector<const xAOD::TruthParticle*> particles(){return m_particles;}
  const xAOD::TruthParticle* p1(){return m_particles[0];}
  const xAOD::TruthParticle* p2(){return m_particles[1];}
  std::pair<float,float> param1(){return m_param[0];}
  std::pair<float,float> param2(){return m_param[1];}
  float d01(){return m_param[0].first;}
  float z01(){return m_param[0].second;}
  float d02(){return m_param[1].first;}
  float z02(){return m_param[1].second;}
  TVector3 vertex(){return m_vtx;}
  float x(){return m_vtx.X();}
  float y(){return m_vtx.Y();}
  float r(){return sqrt(pow(m_vtx.X(),2)+pow(m_vtx.Y(),2));}
  float z(){return m_vtx.Z();}
  float l(){return sqrt(pow(this->r(),2)+pow(m_vtx.Z(),2));}
  float sgn(){return m_sgnVtx;}
  float openA(){return m_openA;}
  float invMass(){return m_invMass;}
  int parPDG(){return m_parPDG;}
  int parBarCode() {return m_parBarCode;}
  int iLead() {return m_iLead;}
  int iSub() {return m_iSub;}

  std::vector<const xAOD::TruthParticle*> m_particles;
  std::vector<std::pair<float,float>> m_param;
  TVector3 m_vtx;
  float m_sgnVtx;
  float m_openA;
  float m_invMass;
  int m_parPDG;
  int m_parBarCode;
  int m_iLead;
  int m_iSub;
  
};

class VertexTool {
  public:
  RecVertex createVertex(std::shared_ptr<AO::Muon>& tLead,std::shared_ptr<AO::Muon>& tSub,int iLead,int iSub);
};

#endif
