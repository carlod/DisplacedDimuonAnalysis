#ifndef HISTOGRAMTOOL_H
#define HISTOGRAMTOOL_H

#include <iostream>
#include <map> 
#include <vector>
#include "TFile.h"
#include "TH1F.h"

using namespace std;

class HistogramTool {

  public:
    HistogramTool(std::vector<TH1F *>& histV);
    void fillHist(string hname,string title, int nbinsx, Float_t xlow, Float_t xhigh, Float_t x, Float_t xw = 1.0);
    std::map<std::string,TH1F *>& theMap(){return m_histMap;}
    std::vector<TH1F *>& theVect(){return m_histVect;}

  private:
    int m_currentHist;
    std::vector<TH1F *>& m_histVect;
    std::map<std::string,TH1F *> m_histMap;
};

#endif
