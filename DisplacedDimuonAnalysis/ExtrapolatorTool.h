#ifndef EXTRAPOLATORTOOL_H
#define EXTRAPOLATORTOOL_H

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "TVector3.h"
#include "TVector.h"
#include "TMatrixD.h"
#include <Math/SMatrix.h>

typedef ROOT::Math::SMatrix<double,6,6> mat66;
typedef ROOT::Math::SMatrix<double,5,6> mat56;
typedef ROOT::Math::SMatrix<double,6,5> mat65;
typedef ROOT::Math::SMatrix<double,5,5> mat55;
typedef std::vector<TVector3> trajectory;

struct track {

  track(float pt,float theta,float phi,float vx, float vy, float vz,float charge,mat66 * cov,float d0 = 0.0, float z0 = 0.0);
  track(TVector3 x,TVector3 p, float charge,trajectory muTraj,mat66 * cov,float d0 = 0.0,float z0 = 0.0);
  track(TVector3 x,TVector3 p, float charge,mat66 * cov,float d0 = 0.0,float z0 = 0.0);
  void print();
  
  TVector3& x(){return m_x;}
  TVector3& dp(){return m_dp;}
  
  TVector3& vx(){return m_vx;}
  TVector3& p(){return m_p;}

  trajectory& traj(){return m_traj;}

  float q(){return m_q;}
  float d0(){return m_d0;}
  float z0(){return m_z0;}

  mat66& cov(){return m_cov;}
  
  TVector3 m_x;
  TVector3 m_dp;
  
  TVector3 m_vx;
  TVector3 m_p;

  trajectory m_traj;

  float m_q;
  float m_d0;
  float m_z0;

  mat66 m_cov;

};

class ExtrapolatorTool {

  public:

  bool m_doSE; 
  bool m_debug;

  trajectory expressAtIP(xAOD::TrackParticle* mu,bool doSE);
  track TPToTrack(xAOD::TrackParticle* mu);
  trajectory trackToTP(track mu,xAOD::TrackParticle* MSTP);
  track doCaloProp(track mu);
  track doIDProp(track mu);
  TVector3 getXvecT(TVector3 x,TVector3 dp,float t);
  TVector3 getXvecTheta(TVector3 x_t0,TVector3 pHat,double theta,double roc,float q);  
  TMatrixD rotMatrix(double theta);
  float R(float x,float y);
  
  //cov prop methods
  void initMomentumJ(TVector *param,mat55 * mat);
  void initPCJ(TVector * param,mat65 * mat);
  void initCPJ(TVector3 x,TVector3 p,mat56 * mat);
  void initCaloJ(track * ct,mat66 * mat,double t0);
  void initIDJ(track * cT,mat66 * mat,float thetaI,float deltaT,float B);
  double BatRZ(double r, double z);
  double ROC(double pt,double B);

};

#endif
