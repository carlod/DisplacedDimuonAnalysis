#ifndef CONTAINERTOOL_H
#define CONTAINERTOOL_H

#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigger/MuonRoIContainer.h"

namespace xAOD{
  class TEvent;
}

class ContainerTool {
  public:
    bool isGood(xAOD::TEvent * theEvent);
    const xAOD::EventInfo* EventInfo(){return m_eventInfo;}
    const xAOD::TruthVertexContainer* TruthVertices(){return m_truthVertices;}
    const xAOD::TruthParticleContainer* TruthMuons(){return m_truthMuons;}
    const xAOD::TruthParticleContainer* TruthParticles(){return m_truthParticles;}
    const xAOD::MuonSegmentContainer* muonSegments(){return m_muonSegments;}
    const xAOD::TrackParticleContainer* MSTrackParticles(){return m_MSTrackParticles;}
    const xAOD::TrackParticleContainer* exTrackParticles(){return m_exTrackParticles;}
    const xAOD::TrackParticleContainer* combTrackParticles(){return m_combTrackParticles;}
    const xAOD::TrackParticleContainer* inDetTrackParticles(){return m_inDetTrackParticles;}
    const xAOD::JetContainer* jets(){return m_jets;}
    const xAOD::MuonContainer* muons(){return m_muons;}
    const xAOD::MissingETContainer* MET(){return m_MET;}
    const xAOD::EnergySumRoI* LVL1EnergySumRoI(){return m_LVL1EnergySumRoI;}
    const xAOD::TrigMissingETContainer* HLT_MET_mht(){return m_HLT_MET_mht;}
    const xAOD::TrigMissingETContainer* HLT_MET_cell(){return m_HLT_MET_cell;} 
    const xAOD::MuonContainer* HLT_muon(){return m_HLT_muon;}
    const xAOD::MuonRoIContainer* L1_muonRoI(){return m_L1_muonRoI;}
    const xAOD::VertexContainer* PV(){return m_PrimaryVertices;}

    const xAOD::EventInfo* m_eventInfo;
    const xAOD::TruthVertexContainer* m_truthVertices;
    const xAOD::TruthParticleContainer* m_truthMuons;
    const xAOD::TruthParticleContainer* m_truthParticles;
    const xAOD::TrackParticleContainer* m_MSTrackParticles;
    const xAOD::TrackParticleContainer* m_exTrackParticles;
    const xAOD::TrackParticleContainer* m_combTrackParticles;
    const xAOD::TrackParticleContainer* m_inDetTrackParticles;
    const xAOD::JetContainer* m_jets;
    const xAOD::MuonSegmentContainer* m_muonSegments;
    const xAOD::MuonContainer* m_muons;
    const xAOD::MissingETContainer* m_MET;
    const xAOD::EnergySumRoI* m_LVL1EnergySumRoI;
    const xAOD::TrigMissingETContainer* m_HLT_MET_mht;
    const xAOD::TrigMissingETContainer* m_HLT_MET_cell;
    const xAOD::MuonContainer* m_HLT_muon;
    const xAOD::MuonRoIContainer* m_L1_muonRoI;
    const xAOD::VertexContainer* m_PrimaryVertices;


    bool m_isMC;
    bool m_isGood;
};

#endif
