#ifndef MUON_H
#define MUON_H

#include "xAODTracking/TrackParticle.h"

namespace AO {

  class Muon {
  
    public:
      Muon(xAOD::TrackParticle TP,xAOD::TrackParticle TPMS,int isTM,int PDG,int barCode,int parPDG,int parBarCode,float isoDPV,float isoDAll,float isoIPV,float isoIAll,float jetDR) :
          m_TP(TP),m_TPMS(TPMS),m_isTM(isTM),m_PDG(PDG),
          m_barCode(barCode),m_parPDG(parPDG),m_parBarCode(parBarCode),m_isoDPV(isoDPV),
          m_isoDAll(isoDAll),m_isoIPV(isoIPV),m_isoIAll(isoIAll),m_jetDR(jetDR) {
      
        uint8_t tmpPrecLayers = 0;
        m_TP.summaryValue(tmpPrecLayers,xAOD::SummaryType::numberOfPrecisionLayers);
        m_nPrecLayers = (int) tmpPrecLayers;
        uint8_t tmpPhiLayers = 0;
        m_TP.summaryValue(tmpPhiLayers,xAOD::SummaryType::numberOfPhiLayers);
        m_nPhiLayers = (int) tmpPhiLayers;
        uint8_t tmpEtaLayers = 0;
        m_TP.summaryValue(tmpEtaLayers,xAOD::SummaryType::numberOfTriggerEtaLayers);
        m_nEtaLayers = (int) tmpEtaLayers;
        
      }

      xAOD::TrackParticle* tp(){return &m_TP;}
      xAOD::TrackParticle* tpMS(){return &m_TPMS;}
      float pt(){return m_TP.pt();}
      float phi(){return m_TP.phi();}
      float eta(){return m_TP.eta();}
      float d0(){return m_TP.d0();}
      float z0(){return m_TP.z0();}
      float m(){return m_TP.m();}
      float charge(){return m_TP.charge();}
      xAOD::ParametersCovMatrix_t covMat(){return m_TPMS.definingParametersCovMatrix();}
      int isTM(){return m_isTM;}
      int PDG(){return m_PDG;}
      int barCode(){return m_barCode;}
      int parPDG(){return m_parPDG;}
      int parBarCode(){return m_parBarCode;}
      float isoDPV(){return m_isoDPV;}
      float isoDAll(){return m_isoDAll;}
      float isoIPV(){return m_isoIPV;}
      float isoIAll(){return m_isoIAll;}
      float jetDR(){return m_jetDR;}
      int nPrecLayers(){return m_nPrecLayers;}
      int nPhiLayers(){return m_nPhiLayers;}
      int nEtaLayers(){return m_nEtaLayers;}
      float vx(){return m_TP.vx();}
      float vy(){return m_TP.vy();}
      float vz(){return m_TP.vz();}

  private:
    Muon() {}
    xAOD::TrackParticle m_TP; 
    xAOD::TrackParticle m_TPMS;
    int m_isTM;
    int m_PDG;
    int m_barCode;
    int m_parPDG;
    int m_parBarCode;
    float m_isoDPV;
    float m_isoDAll;
    float m_isoIPV;
    float m_isoIAll;
    float m_jetDR;
    int m_nPrecLayers;
    int m_nPhiLayers;
    int m_nEtaLayers;

  };

}

#endif
