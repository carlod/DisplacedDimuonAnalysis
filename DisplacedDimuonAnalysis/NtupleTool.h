#ifndef NTUPLETOOL_H
#define NTUPLETOOL_H

#include "VertexTool.h"
#include "TFile.h"
#include "TTree.h"

#include <vector>

class NtupleTool {

  public:
    NtupleTool(TFile* outputFile);

    void initForWrite(TTree* tree);
    void setEvtVars(int evtNum,int runNum,int BCID,int LB,float evtW,float sumW,float prw,float MET, float METPhi);
    void setEvtVtxVars(float PVz,int nTrkPV,int nPU,int nMuons,int nRecVtx,int nTruthVtx);
    void setTPV(TVector3 TVtx);
    void setTrigDec(trigMap tmpMap);
    void addMET(float trigMHT,float trigMHTPhi,float mht,float mhtphi);
    void addIDTrk(const xAOD::TrackParticle * tp,int vtxType);
    void addCombMuon(float pt,float phi,float eta,float d0,float z0,float isoDPV,float isoDAll,float isoIPV,float isoIAll,float JMOR,int author,double chi2,int PDG,int barcode);
    void addMuon(AO::Muon muon);
    void addTruthMuon(xAOD::TruthParticle * truth,float d0,float z0,int parPDG,int parBarCode);
    void addVtx(RecVertex& theVertex);
    void addTruthVtx(TruthVertex& theVertex, const xAOD::TruthParticle* longLivedParticle,int LJMatch,int mu60Match);
    void Fill(){m_tree->Fill();}
    void Clear();
    
  private:

    TTree * m_tree;

    //Event Vars
    int m_evt_num; 
    int m_run_num;
    int m_bcid;
    int m_lumiBlock;
    float m_evtWeight;
    float m_sumWeights;
    float m_prw;
    float m_eTMiss; 
    float m_eTMissPhi;
    
    //Event Vertex Vars
    float m_tpv_x;
    float m_tpv_y;
    float m_tpv_z;
    float m_pv_z;
    int m_pv_ntrk;
    int m_nPUvtx;
    int m_nMuons;
    int m_nRecVtx; 
    int m_nTruthVtx;
    
    //Trigger Vars
    int m_narrowScanTrig;
    int m_METTrig;
    int m_singleMuTrig;
    int m_threeMuTrig;
    int m_L1_MU20Trig;
    int m_L1_3MU6Trig;
   
    //MET Vars
    float m_trig_mht;
    float m_trig_mht_phi;
    float m_mht;
    float m_mht_phi;

    //ID Track Vars
    std::vector<float> m_id_pt;
    std::vector<float> m_id_phi;
    std::vector<float> m_id_eta;
    std::vector<float> m_id_d0;
    std::vector<float> m_id_z0;
    std::vector<float> m_id_charge;
    std::vector<float> m_id_chi2;
    std::vector<float> m_id_dof;
    std::vector<int> m_id_vtxType; 

    //Truth Track Vars
    std::vector<float> m_trkt_pt; 
    std::vector<float> m_trkt_phi; 
    std::vector<float> m_trkt_eta; 
    std::vector<float> m_trkt_d0; 
    std::vector<float> m_trkt_z0; 
    std::vector<float> m_trkt_charge;
    std::vector<int> m_trkt_PDG;
    std::vector<int> m_trkt_barCode;
    std::vector<int> m_trkt_status;
    std::vector<int> m_trkt_parPDG;
    std::vector<int> m_trkt_parBarCode;

    //Truth DV Vars
    std::vector<int> m_dvt_LJMatch;
    std::vector<int> m_dvt_mu60Match;
    std::vector<float> m_dvt_x; 
    std::vector<float> m_dvt_y; 
    std::vector<float> m_dvt_z; 
    std::vector<float> m_dvt_r; 
    std::vector<float> m_dvt_l; 
    std::vector<float> m_dvt_m; 
    std::vector<float> m_dvt_openAng; 
    std::vector<int> m_dvt_parPDG;
    std::vector<int> m_dvt_parBarCode;
    std::vector<int> m_dvt_muIndexLead;
    std::vector<int> m_dvt_muIndexSub;
    
    std::vector<double> m_dvt_pLLP;
    std::vector<double> m_dvt_eLLP;
    std::vector<double> m_dvt_lLLP;

    //Comb Muon Vars
    std::vector<float> m_comb_pt;
    std::vector<float> m_comb_phi;
    std::vector<float> m_comb_eta;
    std::vector<float> m_comb_d0;
    std::vector<float> m_comb_z0;
    std::vector<float> m_comb_isoDPV;
    std::vector<float> m_comb_isoDAll;
    std::vector<float> m_comb_isoIPV;
    std::vector<float> m_comb_isoIAll;
    std::vector<float> m_comb_JMOR;
    std::vector<int> m_comb_author;
    std::vector<double> m_comb_chi2;
    std::vector<int> m_comb_PDG;
    std::vector<int> m_comb_barcode;

    //Track Vars
    std::vector<float> m_trk_pt; 
    std::vector<float> m_trk_phi; 
    std::vector<float> m_trk_eta; 
    std::vector<float> m_trk_d0; 
    std::vector<float> m_trk_z0; 
    std::vector<float> m_trk_m;
    std::vector<float> m_trk_charge;
    std::vector<float> m_trk_covd0;
    std::vector<float> m_trk_covz0;
    std::vector<float> m_trk_covPhi;
    std::vector<float> m_trk_covTheta;
    std::vector<float> m_trk_covP;
    std::vector<int> m_trk_isTM;
    std::vector<int> m_trk_PDG;
    std::vector<int> m_trk_barCode;
    std::vector<int> m_trk_parPDG;
    std::vector<int> m_trk_parBarCode;
    std::vector<float> m_trk_isoDPV;
    std::vector<float> m_trk_isoDAll;
    std::vector<float> m_trk_isoIPV;
    std::vector<float> m_trk_isoIAll;
    std::vector<float> m_trk_jetDR;
    std::vector<uint8_t> m_trk_nPrecLayers;
    std::vector<uint8_t> m_trk_nPhiLayers;
    std::vector<uint8_t> m_trk_nEtaLayers;
    std::vector<float> m_trk_vx;
    std::vector<float> m_trk_vy;
    std::vector<float> m_trk_vz;

    //DV Vars
    std::vector<float> m_dv_x; 
    std::vector<float> m_dv_y; 
    std::vector<float> m_dv_z; 
    std::vector<float> m_dv_r; 
    std::vector<float> m_dv_l; 
    std::vector<float> m_dv_m; 
    std::vector<float> m_dv_charge; 
    std::vector<float> m_dv_openAng; 
    std::vector<float> m_dv_distV; 
    std::vector<int> m_dv_muIndexLead;
    std::vector<int> m_dv_muIndexSub;

};

#endif
