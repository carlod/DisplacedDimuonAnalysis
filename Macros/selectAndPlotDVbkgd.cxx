#include "TString.h"
#include "TChain.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "RooRealVar.h"
#include "RooDataHist.h"
#include "RooPlot.h"
#include "RooTreeData.h"
#include "RooLinkedList.h"

void selectAndPlotDVbkgd(TString inputPath, TString fileName, TString sampleType) {

// Read in the ntuple(s)
  TChain DVTree("tree");
  DVTree.Add(inputPath+fileName);
  
  // Very Loose, Loose, Tight and Truth-Match cuts
  // Note that the VeryLoose selection is the CR and the Tight selection is the SR
  //TCut TruthMatch = "DV_parBarCode > 0";
  TCut TruthMatch = "fabs(DV_muPDG1) == 13 && fabs(DV_muPDG2) == 13";
  TCut MatchToTruth = "DV_parBarCode == DVT_parBarCode";

  TCut Base = "(METTrig || MuTrig || HLT3Mu6Trig || J80Met80Trig || Mu20Mu6Trig) && DV_openAng > 0.01 && DV_m > 10.0 && DV_m < 2000.0 && fabs(DV_r) < 400. && fabs(DV_z) < 600. && fabs(DV_muEta1) < 2.5 && fabs(DV_muEta2) < 2.5 && DV_charge == 0 && DV_distV < 100.0";
  TCut VeryLoose =  Base&&"DV_muDR1 > 0.02 && DV_muDR2 > 0.02 && (DV_muDR1 < 0.05 || DV_muDR2 < 0.05)";
  TCut Loose = VeryLoose&&TruthMatch;
  TCut Tight = Base&&"DV_muDR1 > 0.05 && DV_muDR2 > 0.05";

  // Make 1D histos with, Loose and Tight selection
  TH1F* DVrVL = new TH1F("DVrVL", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  TH1F* absDVrVL = new TH1F("absDVrVL", "|r| DV [cm]", 100, 0.0, 400.0);
  TH1F* DVzVL = new TH1F("DVzVL", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  TH1F* DVlVL = new TH1F("DVlVL", "l DV [cm]", 100, 0.0, 600.0);

  DVrVL->SetXTitle("r_{vtx} [cm]");
  DVrVL->SetYTitle("Events");

  absDVrVL->SetXTitle("|r| [cm]");
  absDVrVL->SetYTitle("Events");

  DVzVL->SetXTitle("z_{vtx} [cm]");
  DVzVL->SetYTitle("Events");

  DVlVL->SetXTitle("L_{vtx} [cm]");
  DVlVL->SetYTitle("Events");


  TH1F* DVrL = new TH1F("DVrL", "r_{vtx} [cm]", 100, -400.0, 400.0);
  TH1F* absDVrL = new TH1F("absDVrL", "|r| [cm]", 100, 0.0, 400.0);
  TH1F* DVzL = new TH1F("DVzL", "z_{vtx} [cm]", 100, -600.0, 600.0);
  TH1F* DVlL = new TH1F("DVlL", "l [cm]", 100, 0.0, 600.0);
 
  DVrL->SetXTitle("r_{vtx} [cm]");
  DVrL->SetYTitle("Events");

  absDVrL->SetXTitle("|r| [cm]");
  absDVrL->SetYTitle("Events");

  DVzL->SetXTitle("z_{vtx} [cm]");
  DVzL->SetYTitle("Events");

  DVlL->SetXTitle("L_{vtx} [cm]");
  DVlL->SetYTitle("Events");


  TH1F* DVrT = new TH1F("DVrT", "r_{vtx} [cm]", 100, -400.0, 400.0);
  TH1F* absDVrT = new TH1F("absDVrT", "|r| [cm]", 100, 0.0, 400.0);
  TH1F* DVzT = new TH1F("DVzT", "z [cm]", 100, -600.0, 600.0);
  TH1F* DVlT = new TH1F("DVlT", "l [cm]", 100, 0.0, 600.0);
 
  DVrT->SetXTitle("r_{vtx} DV  [cm]");
  DVrT->SetYTitle("Events");

  absDVrT->SetXTitle("|r| [cm]");
  absDVrT->SetYTitle("Events");

  DVzT->SetXTitle("z_{vtx} [cm]");
  DVzT->SetYTitle("Events");

  DVlT->SetXTitle("L_{vtx} [cm]");
  DVlT->SetYTitle("Events");

  // More histos, Very Loose
  TH1F* DVmVL = new TH1F("DVmVL", "m [GeV]", 100, 0.0, 200.0);
  TH1F* DVdistVL = new TH1F("DVdistVL", "distV [cm]", 100, 0.0, 200.0);
  TH2F* DVrVSzVL = new TH2F("DVrVSz", "r_{vtx} VS z", 80, -600.0, 600.0, 80, -400.0, 400.0);
  TH1F* DVopenAngVL = new TH1F("DVopenAngVL", "openAngle", 100, 0., 3.14);

  DVmVL->SetXTitle("m_{vtx} [GeV]");
  DVmVL->SetYTitle("Events");

  DVdistVL->SetXTitle("Min distance between tracks [cm]");
  DVdistVL->SetYTitle("Events");

  DVrVSzVL->SetXTitle("z_{vtx} [cm]");
  DVrVSzVL->SetYTitle("r_{vtx} [cm]");

  DVopenAngVL->SetYTitle("Events");
  DVopenAngVL->SetXTitle("#theta_{#mu#mu} [rad]");

  // Make 1d histos of some reconstructed muon quantities (pt, phi, eta, d0, z0, etc.)
  TH1F* DVmuPtVL = new TH1F("DVmuPtVL", "DVmuPtVL", 100, 0., 200.0);
  DVmuPtVL->SetYTitle("Events");
  DVmuPtVL->SetXTitle("#mu p_{T} [GeV]");

  TH1F* DVmuPhiVL = new TH1F("DVmuPhiVL", "DVmuPhiVL", 50, -3.14, 3.14);
  DVmuPhiVL->SetYTitle("Events");
  DVmuPhiVL->SetXTitle("#phi_{#mu} [rad]");

  TH1F* DVmuEtaVL = new TH1F("DVmuEtaVL", "DVmuEtaVL", 50, -5.0, 5.0);
  DVmuEtaVL->SetYTitle("Events");
  DVmuEtaVL->SetXTitle("#eta_{#mu}");

  TH1F* DVmuD0VL = new TH1F("DVmuD0VL", "DVmuD0VL", 100, 0., 400.);
  DVmuD0VL->SetYTitle("Events");
  DVmuD0VL->SetXTitle("#mu d_{0} [cm]");

  TH1F* DVmuZ0VL = new TH1F("DVmuZ0VL", "DVmuZ0VL", 100, 0., 400.);
  DVmuZ0VL->SetYTitle("Events");
  DVmuZ0VL->SetXTitle("#mu z_{0} [cm]");


  // More histos, Loose
  TH1F* DVmL = new TH1F("DVmL", "m [GeV]", 100, 0.0, 200.0);
  TH1F* DVdistL = new TH1F("DVdistL", "distV [cm]", 100, 0.0, 200.0);
  TH2F* DVrVSzL = new TH2F("DVrVSzL", "r VS z", 80, -600.0, 600.0, 80, -400.0, 400.0);
  TH1F* DVopenAngL = new TH1F("DVopenAngL", "openAngle", 100, 0., 3.14);

  DVmL->SetXTitle("m_{vtx} [GeV]");
  DVmL->SetYTitle("Events");

  DVdistL->SetXTitle("Min distance between tracks [cm]");
  DVdistL->SetYTitle("Events");

  DVrVSzL->SetXTitle("z_{vtx} [cm]");
  DVrVSzL->SetYTitle("r_{vtx} [cm]");

  DVopenAngL->SetYTitle("Events");
  DVopenAngL->SetXTitle("#theta_{#mu#mu} [rad]");

  // Make 1d histos of some reconstructed muon quantities (pt, phi, eta, d0, z0, etc.)
  TH1F* DVmuPtL = new TH1F("DVmuPtL", "DVmuPtL", 100, 0., 200.0);
  DVmuPtL->SetYTitle("Events");
  DVmuPtL->SetXTitle("#mu p_{T} [GeV]");

  TH1F* DVmuPhiL = new TH1F("DVmuPhiL", "DVmuPhiL", 50, -3.14, 3.14);
  DVmuPhiL->SetYTitle("Events");
  DVmuPhiL->SetXTitle("#phi_{#mu} [rad]");

  TH1F* DVmuEtaL = new TH1F("DVmuEtaL", "DVmuEtaL", 50, -5.0, 5.0);
  DVmuEtaL->SetYTitle("Events");
  DVmuEtaL->SetXTitle("#eta_{#mu}");

  TH1F* DVmuD0L = new TH1F("DVmuD0L", "DVmuD0L", 100, 0., 400.);
  DVmuD0L->SetYTitle("Events");
  DVmuD0L->SetXTitle("#mu d_{0} [cm]");

  TH1F* DVmuZ0L = new TH1F("DVmuZ0L", "DVmuZ0L", 100, 0., 400.);
  DVmuZ0L->SetYTitle("Events");
  DVmuZ0L->SetXTitle("#mu z_{0} [cm]");


  // More histos, Tight
  TH1F* DVmT = new TH1F("DVmT", "m [GeV]", 100, 0.0, 200.0);
  TH1F* DVdistT = new TH1F("DVdistT", "distV [cm]", 100, 0.0, 200.0);
  TH2F* DVrVSzT = new TH2F("DVrVSzT", "r VS z", 80, -600.0, 600.0, 80, -400.0, 400.0);
  TH1F* DVopenAngT = new TH1F("DVopenAngT", "openAngle", 100, 0., 3.14);

  DVmT->SetXTitle("m_{vtx} [GeV]");
  DVmT->SetYTitle("Events");

  DVdistT->SetXTitle("Min distance between tracks [cm]");
  DVdistT->SetYTitle("Events");

  DVrVSzT->SetXTitle("z_{vtx} [cm]");
  DVrVSzT->SetYTitle("r_{vtx} [cm]");

  DVopenAngT->SetYTitle("Events");
  DVopenAngT->SetXTitle("#theta_{#mu#mu} [rad]");

  // Make 1d histos of some reconstructed muon quantities (pt, phi, eta, d0, z0, etc.)
  TH1F* DVmuPtT = new TH1F("DVmuPtT", "DVmuPtT", 100, 0., 200.0);
  DVmuPtT->SetYTitle("Events");
  DVmuPtT->SetXTitle("#mu p_{T} [GeV]");

  TH1F* DVmuPhiT = new TH1F("DVmuPhiT", "DVmuPhiT", 50, -3.14, 3.14);
  DVmuPhiT->SetYTitle("Events");
  DVmuPhiT->SetXTitle("#phi_{#mu} [rad]");

  TH1F* DVmuEtaT = new TH1F("DVmuEtaT", "DVmuEtaT", 50, -5.0, 5.0);
  DVmuEtaT->SetYTitle("Events");
  DVmuEtaT->SetXTitle("#eta_{#mu}");

  TH1F* DVmuD0T = new TH1F("DVmuD0T", "DVmuD0T", 100, 0., 400.);
  DVmuD0T->SetYTitle("Events");
  DVmuD0T->SetXTitle("#mu d_{0} [cm]");

  TH1F* DVmuZ0T = new TH1F("DVmuZ0T", "DVmuZ0T", 100, 0., 400.);
  DVmuZ0T->SetYTitle("Events");
  DVmuZ0T->SetXTitle("#mu z_{0} [cm]");

  // More histos, Tight and Truth-Match to a Z->mumu

  TH1F* DVrTT = new TH1F("DVrTT", "r_{vtx} [cm]", 100, -400.0, 400.0);
  TH1F* absDVrTT = new TH1F("absDVrTT", "|r| [cm]", 100, 0.0, 400.0);
  TH1F* DVzTT = new TH1F("DVzTT", "z [cm]", 100, -600.0, 600.0);
  TH1F* DVlTT = new TH1F("DVlTT", "l [cm]", 100, 0.0, 600.0);
 
  DVrTT->SetXTitle("r_{vtx} DV  [cm]");
  DVrTT->SetYTitle("Events");

  absDVrTT->SetXTitle("|r| [cm]");
  absDVrTT->SetYTitle("Events");

  DVzTT->SetXTitle("z_{vtx} [cm]");
  DVzTT->SetYTitle("Events");

  DVlTT->SetXTitle("L_{vtx} [cm]");
  DVlTT->SetYTitle("Events");

  TH1F* DVmTT = new TH1F("DVmTT", "m [GeV]", 100, 0.0, 200.0);
  TH1F* DVdistTT = new TH1F("DVdistTT", "distV [cm]", 100, 0.0, 200.0);
  TH2F* DVrVSzTT = new TH2F("DVrVSzTT", "r VS z", 80, -600.0, 600.0, 80, -400.0, 400.0);
  TH1F* DVopenAngTT = new TH1F("DVopenAngTT", "openAngle", 100, 0., 3.14);

  DVmTT->SetXTitle("m_{vtx} [GeV]");
  DVmTT->SetYTitle("Events");

  DVdistTT->SetXTitle("Min distance between tracks [cm]");
  DVdistTT->SetYTitle("Events");

  DVrVSzTT->SetXTitle("z_{vtx} [cm]");
  DVrVSzTT->SetYTitle("r_{vtx} [cm]");

  DVopenAngTT->SetYTitle("Events");
  DVopenAngTT->SetXTitle("#theta_{#mu#mu} [rad]");

  // Make 1d histos of some reconstructed muon quantities (pt, phi, eta, d0, z0, etc.)
  TH1F* DVmuPtTT = new TH1F("DVmuPtTT", "DVmuPtTT", 100, 0., 200.0);
  DVmuPtTT->SetYTitle("Events");
  DVmuPtTT->SetXTitle("#mu p_{T} [GeV]");

  TH1F* DVmuPhiTT = new TH1F("DVmuPhiTT", "DVmuPhiTT", 50, -3.14, 3.14);
  DVmuPhiTT->SetYTitle("Events");
  DVmuPhiTT->SetXTitle("#phi_{#mu} [rad]");

  TH1F* DVmuEtaTT = new TH1F("DVmuEtaTT", "DVmuEtaTT", 50, -5.0, 5.0);
  DVmuEtaTT->SetYTitle("Events");
  DVmuEtaTT->SetXTitle("#eta_{#mu}");

  TH1F* DVmuD0TT = new TH1F("DVmuD0TT", "DVmuD0TT", 100, 0., 400.);
  DVmuD0TT->SetYTitle("Events");
  DVmuD0TT->SetXTitle("#mu d_{0} [cm]");

  TH1F* DVmuZ0TT = new TH1F("DVmuZ0TT", "DVmuZ0TT", 100, 0., 400.);
  DVmuZ0TT->SetYTitle("Events");
  DVmuZ0TT->SetXTitle("#mu z_{0} [cm]");
  
  // Fill the histos
  DVTree.Draw("DV_r>>DVrVL", VeryLoose);
  DVTree.Draw("fabs(DV_r)>>absDVrVL", VeryLoose);
  DVTree.Draw("DV_z>>DVzVL", VeryLoose);
  DVTree.Draw("DV_l>>DVlVL", VeryLoose);

  DVTree.Draw("DV_m>>DVmVL", VeryLoose);
  DVTree.Draw("DV_distV*0.1>>DVdistVL", VeryLoose);
  DVTree.Draw("DV_openAng>>DVopenAngVL", VeryLoose);

  DVTree.Draw("((DV_y/fabs(DV_y))*DV_r):(DV_z)>>DVrVSzVL", VeryLoose);

  DVTree.Draw("DV_muPt1>>DVmuPtVL", VeryLoose);
  DVTree.Draw("DV_muPt2>>+DVmuPtVL", VeryLoose);

  DVTree.Draw("DV_muPhi1>>DVmuPhiVL", VeryLoose);
  DVTree.Draw("DV_muPhi2>>+DVmuPhiVL", VeryLoose);

  DVTree.Draw("DV_muEta1>>DVmuEtaVL", VeryLoose);
  DVTree.Draw("DV_muEta2>>+DVmuEtaVL", VeryLoose);

  DVTree.Draw("DV_muD01>>DVmuD0VL", VeryLoose);
  DVTree.Draw("DV_muD02>>+DVmuD0VL", VeryLoose);

  DVTree.Draw("DV_muZ01>>DVmuZ0VL", VeryLoose);
  DVTree.Draw("DV_muZ02>>+DVmuZ0VL", VeryLoose);


  DVTree.Draw("DV_r>>DVrL", Loose);
  DVTree.Draw("fabs(DV_r)>>absDVrL", Loose);
  DVTree.Draw("DV_z>>DVzL", Loose);
  DVTree.Draw("DV_l>>DVlL", Loose);

  DVTree.Draw("DV_m>>DVmL", Loose);
  DVTree.Draw("DV_distV*0.1>>DVdistL", Loose);
  DVTree.Draw("DV_openAng>>DVopenAngL", Loose);

  DVTree.Draw("((DV_y/fabs(DV_y))*DV_r):(DV_z)>>DVrVSzL", Loose);

  DVTree.Draw("DV_muPt1>>DVmuPtL", Loose);
  DVTree.Draw("DV_muPt2>>+DVmuPtL", Loose);

  DVTree.Draw("DV_muPhi1>>DVmuPhiL", Loose);
  DVTree.Draw("DV_muPhi2>>+DVmuPhiL", Loose);

  DVTree.Draw("DV_muEta1>>DVmuEtaL", Loose);
  DVTree.Draw("DV_muEta2>>+DVmuEtaL", Loose);

  DVTree.Draw("DV_muD01>>DVmuD0L", Loose);
  DVTree.Draw("DV_muD02>>+DVmuD0L", Loose);

  DVTree.Draw("DV_muZ01>>DVmuZ0L", Loose);
  DVTree.Draw("DV_muZ02>>+DVmuZ0L", Loose);


  DVTree.Draw("DV_r>>DVrT", Tight);
  DVTree.Draw("fabs(DV_r)>>absDVrT", Tight);
  DVTree.Draw("DV_z>>DVzT", Tight);
  DVTree.Draw("DV_l>>DVlT", Tight);

  DVTree.Draw("DV_m>>DVmT", Tight);
  DVTree.Draw("DV_distV*0.1>>DVdistT", Tight);
  DVTree.Draw("DV_openAng>>DVopenAngT", Tight);

  DVTree.Draw("((DV_y/fabs(DV_y))*DV_r):(DV_z)>>DVrVSzT", Tight);

  DVTree.Draw("DV_muPt1>>DVmuPtT", Tight);
  DVTree.Draw("DV_muPt2>>+DVmuPtT", Tight);

  DVTree.Draw("DV_muPhi1>>DVmuPhiT", Tight);
  DVTree.Draw("DV_muPhi2>>+DVmuPhiT", Tight);

  DVTree.Draw("DV_muEta1>>DVmuEtaT", Tight);
  DVTree.Draw("DV_muEta2>>+DVmuEtaT", Tight);

  DVTree.Draw("DV_muD01>>DVmuD0T", Tight);
  DVTree.Draw("DV_muD02>>+DVmuD0T", Tight);

  DVTree.Draw("DV_muZ01>>DVmuZ0T", Tight);
  DVTree.Draw("DV_muZ02>>+DVmuZ0T", Tight);

  DVTree.Draw("DV_r>>DVrTT", Tight&&TruthMatch);
  DVTree.Draw("fabs(DV_r)>>absDVrTT", Tight&&TruthMatch);
  DVTree.Draw("DV_z>>DVzTT", Tight&&TruthMatch);
  DVTree.Draw("DV_l>>DVlTT", Tight&&TruthMatch);

  DVTree.Draw("DV_m>>DVmTT", Tight&&TruthMatch);
  DVTree.Draw("DV_distV*0.1>>DVdistTT", Tight&&TruthMatch);
  DVTree.Draw("DV_openAng>>DVopenAngTT", Tight&&TruthMatch);

  DVTree.Draw("((DV_y/fabs(DV_y))*DV_r):(DV_z)>>DVrVSzTT", Tight&&TruthMatch);

  DVTree.Draw("DV_muPt1>>DVmuPtTT", Tight&&TruthMatch);
  DVTree.Draw("DV_muPt2>>+DVmuPtTT", Tight&&TruthMatch);

  DVTree.Draw("DV_muPhi1>>DVmuPhiTT", Tight&&TruthMatch);
  DVTree.Draw("DV_muPhi2>>+DVmuPhiTT", Tight&&TruthMatch);

  DVTree.Draw("DV_muEta1>>DVmuEtaTT", Tight&&TruthMatch);
  DVTree.Draw("DV_muEta2>>+DVmuEtaTT", Tight&&TruthMatch);

  DVTree.Draw("DV_muD01>>DVmuD0TT", Tight&&TruthMatch);
  DVTree.Draw("DV_muD02>>+DVmuD0TT", Tight&&TruthMatch);

  DVTree.Draw("DV_muZ01>>DVmuZ0TT", Tight&&TruthMatch);
  DVTree.Draw("DV_muZ02>>+DVmuZ0TT", Tight&&TruthMatch);

  // Turn on the stats box for the 1D histos
  DVrVL->SetStats(1);
  DVzVL->SetStats(1);
  DVlVL->SetStats(1);
  DVmVL->SetStats(1);
  DVdistVL->SetStats(1);
  DVopenAngVL->SetStats(1);
  DVmuPtVL->SetStats(1);
  DVmuPhiVL->SetStats(1);
  DVmuEtaVL->SetStats(1);
  DVmuD0VL->SetStats(1);
  DVmuZ0VL->SetStats(1);

  DVrL->SetStats(1);
  DVzL->SetStats(1);
  DVlL->SetStats(1);
  DVmL->SetStats(1);
  DVdistL->SetStats(1);
  DVopenAngL->SetStats(1);
  DVmuPtL->SetStats(1);
  DVmuPhiL->SetStats(1);
  DVmuEtaL->SetStats(1);
  DVmuD0L->SetStats(1);
  DVmuZ0L->SetStats(1);

  DVrT->SetStats(1);
  DVzT->SetStats(1);
  DVlT->SetStats(1);
  DVmT->SetStats(1);
  DVdistT->SetStats(1);
  DVopenAngT->SetStats(1);
  DVmuPtT->SetStats(1);
  DVmuPhiT->SetStats(1);
  DVmuEtaT->SetStats(1);
  DVmuD0T->SetStats(1);
  DVmuZ0T->SetStats(1);

  DVrTT->SetStats(1);
  DVzTT->SetStats(1);
  DVlTT->SetStats(1);
  DVmTT->SetStats(1);
  DVdistTT->SetStats(1);
  DVopenAngTT->SetStats(1);
  DVmuPtTT->SetStats(1);
  DVmuPhiTT->SetStats(1);
  DVmuEtaTT->SetStats(1);
  DVmuD0TT->SetStats(1);
  DVmuZ0TT->SetStats(1);

  gStyle->SetOptStat("ourme");
  DVrVSzVL->SetMarkerStyle(20);
  DVrVSzL->SetMarkerStyle(20);
  DVrVSzT->SetMarkerStyle(20);
  DVrVSzTT->SetMarkerStyle(20);

  TCanvas* c1 = new TCanvas("c1", "c1", 1200, 800);

  // Make sure to leave a right margin for the 2D color plots
  c1->SetRightMargin(0.15);
  

  // Loose selection

  DVrL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-r-"+sampleType+".pdf");
  c1->Clear();
  
  absDVrL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-absr-"+sampleType+".pdf");
  c1->Clear();
  
  DVzL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-z-"+sampleType+".pdf");
  c1->Clear();
  
  DVlL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-l-"+sampleType+".pdf");
  c1->Clear();
  
  DVmL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-m-"+sampleType+".pdf");
  c1->Clear();
  
  DVdistL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-distV-"+sampleType+".pdf");
  c1->Clear();
  
  DVopenAngL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-openAng-"+sampleType+".pdf");
  c1->Clear();
  
  DVrVSzL->Draw("SCAT");

  c1->SaveAs(inputPath+"/DVPlotsL-rVSz-"+sampleType+".pdf");
  c1->Clear();
    
  DVmuPtL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-muPt-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuPhiL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-muPhi-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuEtaL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-muEta-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuD0L->Draw();
  c1->SaveAs(inputPath+"/DVPlotsL-muD0-"+sampleType+".pdf");
  c1->Clear();

  // Tight selection

  DVrT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-r-"+sampleType+".pdf");
  c1->Clear();
  
  absDVrT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-absr-"+sampleType+".pdf");
  c1->Clear();
  
  DVzT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-z-"+sampleType+".pdf");
  c1->Clear();
  
  DVlT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-l-"+sampleType+".pdf");
  c1->Clear();
  
  DVmT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-m-"+sampleType+".pdf");
  c1->Clear();
  
  DVdistT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-distV-"+sampleType+".pdf");
  c1->Clear();
  
  DVopenAngT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-openAng-"+sampleType+".pdf");
  c1->Clear();
  
  DVrVSzT->Draw("SCAT");

  c1->SaveAs(inputPath+"/DVPlotsT-rVSz-"+sampleType+".pdf");
  c1->Clear();
    
  DVmuPtT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-muPt-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuPhiT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-muPhi-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuEtaT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-muEta-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuD0T->Draw();
  c1->SaveAs(inputPath+"/DVPlotsT-muD0-"+sampleType+".pdf");
  c1->Clear();

  // Tight && TruthMatch selection

  DVrTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-r-"+sampleType+".pdf");
  c1->Clear();
  
  absDVrTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-absr-"+sampleType+".pdf");
  c1->Clear();
  
  DVzTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-z-"+sampleType+".pdf");
  c1->Clear();
  
  DVlTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-l-"+sampleType+".pdf");
  c1->Clear();
  
  DVmTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-m-"+sampleType+".pdf");
  c1->Clear();
  
  DVdistTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-distV-"+sampleType+".pdf");
  c1->Clear();
  
  DVopenAngTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-openAng-"+sampleType+".pdf");
  c1->Clear();
  
  DVrVSzTT->Draw("SCAT");

  c1->SaveAs(inputPath+"/DVPlotsTT-rVSz-"+sampleType+".pdf");
  c1->Clear();
    
  DVmuPtTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-muPt-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuPhiTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-muPhi-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuEtaTT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-muEta-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuD0TT->Draw();
  c1->SaveAs(inputPath+"/DVPlotsTT-muD0-"+sampleType+".pdf");
  c1->Clear();

  // Very Loose selection
  
  DVmVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-m-"+sampleType+".pdf");
  c1->Clear();
  
  DVdistVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-distV-"+sampleType+".pdf");
  c1->Clear();
  
  DVopenAngVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-openAng-"+sampleType+".pdf");
  c1->Clear();
  
  DVrVSzVL->Draw("SCAT");

  c1->SaveAs(inputPath+"/DVPlotsVL-rVSz-"+sampleType+".pdf");
  c1->Clear();
    
  DVmuPtVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-muPt-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuPhiVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-muPhi-"+sampleType+".pdf");
  c1->Clear();
  
  DVmuEtaVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-muEta-"+sampleType+".pdf");
  c1->Clear();
  
  c1->SetLogy();
  DVrVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-r-"+sampleType+".pdf");
  c1->Clear();
  
  c1->SetLogy();
  absDVrVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-absr-"+sampleType+".pdf");
  c1->Clear();
  
  c1->SetLogy();
  DVzVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-z-"+sampleType+".pdf");
  c1->Clear();
  
  c1->SetLogy();
  DVlVL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-l-"+sampleType+".pdf");
  c1->Clear();

  c1->SetLogy();
  DVmuD0VL->Draw();
  c1->SaveAs(inputPath+"/DVPlotsVL-muD0-"+sampleType+".pdf");
  c1->Clear();
      

}

