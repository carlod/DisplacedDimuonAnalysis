#include "TString.h"
#include "TChain.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "RooRealVar.h"
#include "RooDataHist.h"
#include "RooPlot.h"
#include "RooTreeData.h"
#include "RooLinkedList.h"
#include "TLatex.h"
#include <iostream>
#include <fstream>

void selectAndPlotDV(TString inputPath, TString fileName, TString sampleType) {
 
  // Read in the ntuple(s):  will want to pass the directory of the input root files as string inputPath, and sample type as sampleType
  // Run from root prompt:  e.g.  .x selectAndPlot.cxx("VLQTest", "VLQ")
  TChain DVTree("tree");
  DVTree.Add(inputPath+fileName);

  // Create a file to store all the histos
  //TFile* outputHistos = new TFile(inputPath+"/Histos-"+sampleType+".root", "new");

  // Loose, Tight and Truth-Match cuts
  // Below emulates an HLT_2MU15 trigger seeded by the L1_MU20 trigger
  //TCut Trigger = "Mu20Trig && DV_muPt1 > 15.0 && DV_muPt2 > 15.0";
  TCut Trigger = "(METTrig || MuTrig || HLT3Mu6Trig || J80Met80Trig || Mu20Mu6Trig)";
  TCut TriggerLowMass = "(HLT3Mu6Trig || Mu20Mu6Trig)";
  TCut Loose = "DV_openAng > 0.01 && DV_m > 10.0 && DV_m < 2000.0 && fabs(DV_r) < 400. && fabs(DV_z) < 600. && fabs(DV_muEta1) < 2.5 && fabs(DV_muEta2) < 2.5";
  TCut AddTight = "DV_charge == 0 && DV_distV < 100.0 && DV_muDR1 > 0.05 && DV_muDR2 > 0.05";
  TCut TruthMatch = "DV_parBarCode > 0";
  TCut MatchToTruth = "DV_parBarCode == DVT_parBarCode";
 
  cout<<"Booking the histograms"<<endl;

  // Make 1D histos of decay lengths (r, z and l = sqrt(r*r + z*z)), with various selection criteria
  TH1F* DVr = new TH1F("DVr", "r DV [cm]", 100, -600.0, 600.0);
  DVr->SetXTitle("r DV Loose [cm]");
  DVr->SetYTitle("Events");
  DVTree.Draw("DV_r>>DVr", Trigger&&Loose);

  TH1F* absDVr = new TH1F("absDVr", "|r| DV [cm]", 100, 0.0, 600.0);
  absDVr->SetXTitle("|r| DV Loose [cm]");
  absDVr->SetYTitle("Events");
  DVTree.Draw("fabs(DV_r)>>absDVr", Trigger&&Loose);

  TH1F* DVz = new TH1F("DVz", "z DV [cm]", 100, -600.0, 600.0);
  DVz->SetXTitle("z DV Loose [cm]");
  DVz->SetYTitle("Events");
  DVTree.Draw("DV_z>>DVz", Trigger&&Loose);

  TH1F* DVl = new TH1F("DVl", "l DV [cm]", 100, 0.0, 600.0);
  DVl->SetXTitle("L DV Loose [cm]");
  DVl->SetYTitle("Events");
  DVTree.Draw("DV_l>>DVl", Trigger&&Loose);

  // Make 1D histos of decay lengths, with "tight" selection
  TH1F* DVrTight = new TH1F("DVrTight", "r DV [cm]", 100, -600.0, 600.0);
  DVrTight->SetXTitle("r DV [cm]");
  DVrTight->SetYTitle("Events");
  DVTree.Draw("DV_r>>DVrTight", Trigger&&Loose&&AddTight);

  TH1F* absDVrTight = new TH1F("absDVrTight", "|r| DV [cm]", 100, 0.0, 600.0);
  absDVrTight->SetXTitle("|r| DV [cm]");
  absDVrTight->SetYTitle("Events");
  DVTree.Draw("fabs(DV_r)>>absDVrTight", Trigger&&Loose&&AddTight);

  TH1F* DVzTight = new TH1F("DVzTight", "z DV [cm]", 100, -600.0, 600.0);
  DVzTight->SetXTitle("z DV [cm]");
  DVzTight->SetYTitle("Events");
  DVTree.Draw("DV_z>>DVzTight", Trigger&&Loose&&AddTight);

  TH1F* DVlTight = new TH1F("DVlTight", "l DV [cm]", 100, 0.0, 600.0);
  DVlTight->SetXTitle("L DV [cm]");
  DVlTight->SetYTitle("Events");
  DVTree.Draw("DV_l>>DVlTight", Trigger&&Loose&&AddTight);

  // make 1D histos of decay lengths, with tight selection and truth-matching requirement
  TH1F* DVrtMatch = new TH1F("DVrtMatch", "r DV [cm]", 100, -600.0, 600.0);
  DVrtMatch->SetXTitle("r DV [cm]");
  DVrtMatch->SetYTitle("Events");
  DVTree.Draw("DV_r>>DVrtMatch", Trigger&&Loose&&AddTight&&TruthMatch);

  TH1F* absDVrtMatch = new TH1F("absDVrtMatch", "|r| DV [cm]", 100, 0.0, 600.0);
  absDVrtMatch->SetXTitle("|r| DV [cm]");
  absDVrtMatch->SetYTitle("Events");
  DVTree.Draw("fabs(DV_r)>>absDVrtMatch", Trigger&&Loose&&AddTight&&TruthMatch);

  TH1F* DVztMatch = new TH1F("DVztMatch", "z DV [cm]", 100, -600.0, 600.0);
  DVztMatch->SetXTitle("z DV [cm]");
  DVztMatch->SetYTitle("Events");
  DVTree.Draw("DV_z>>DVztMatch", Trigger&&Loose&&AddTight&&TruthMatch);
 
  TH1F* DVltMatch = new TH1F("DVltMatch", "l DV [cm]", 100, 0.0, 600.0);
  DVltMatch->SetXTitle("L DV [cm]");
  DVltMatch->SetYTitle("Events");
  DVTree.Draw("DV_l>>DVltMatch", Trigger&&Loose&&AddTight&&TruthMatch);

  // Make 1D histos of mass, with "tight" selection
  TH1F* DVmasstMatch = new TH1F("DVmasstMatch", "Mass DV [GeV]", 100, 0.0, 200.0);
  DVmasstMatch->SetXTitle("Mass DV [GeV]");
  DVmasstMatch->SetYTitle("Events");
  DVTree.Draw("DV_m>>DVmasstMatch", Trigger&&Loose&&AddTight&&TruthMatch);

  // DV l, z and r residuals
  TH1F* DVrResid = new TH1F("DVrResid", "#Delta# r DV [cm]", 100, -50.0, 50.0);
  DVrResid->SetXTitle("r-r_{true} [cm]");
  DVrResid->SetYTitle("Events");
  DVTree.Draw("(fabs(DV_r)-DVT_r)>>DVrResid", Trigger&&Loose&&AddTight&&TruthMatch&&MatchToTruth);

  TH1F* DVzResid = new TH1F("DVzResid", "#Delta# z DV [cm]", 100, -50.0, 50.0);
  DVzResid->SetXTitle("z-z_{true} [cm]");
  DVzResid->SetYTitle("Events");
  DVTree.Draw("(DV_z-DVT_z)>>DVzResid", Trigger&&Loose&&AddTight&&TruthMatch&&MatchToTruth);

  TH1F* DVlResid = new TH1F("DVlResid", "#Delta# l DV [cm]", 100, -50.0, 50.0);
  DVlResid->SetXTitle("L-L_{true} [cm]");
  DVlResid->SetYTitle("Events");
  DVTree.Draw("(DV_l-DVT_l)>>DVlResid", Trigger&&Loose&&AddTight&&TruthMatch&&MatchToTruth);

  // Make a 2D histo of z v/s r
  TH2F* zVSr = new TH2F("zVSr", "z VS r", 60, -600.0, 600.0, 40, -400.0, 400.0);
  zVSr->SetXTitle("z DV [cm]");
  zVSr->SetYTitle("r DV [cm]");
  DVTree.Draw("((DV_y/fabs(DV_y))*DV_r):(DV_z)>>zVSr", Trigger&&Loose&&AddTight&&TruthMatch);

  TH2F* zVSrTrue = new TH2F("zVSrTrue", "z VS r True", 40, -400.0, 400.0, 40, -400.0, 400.0);
  zVSrTrue->SetXTitle("True z DV [cm]");
  zVSrTrue->SetYTitle("True r DV [cm]");
  DVTree.Draw("((DVT_y/fabs(DVT_y))*DVT_r):(DVT_z)>>zVSrTrue", Trigger);

  TH2F* zVSrTrueMatch = new TH2F("zVSrTrueMatch", "z VS r True",  40, -400.0, 400.0, 40, -400.0, 400.0);
  zVSrTrueMatch->SetXTitle("True z DV [cm]");
  zVSrTrueMatch->SetYTitle("True r DV [cm]");
  DVTree.Draw("((DVT_y/fabs(DVT_y))*DVT_r):(DVT_z)>>zVSrTrueMatch", Trigger&&Loose&&AddTight&&MatchToTruth);

  TH2F* effDVrVSz = new TH2F("effDVrVSz", "z VS r True",  40, -400.0, 400.0, 40, -400.0, 400.0);
  effDVrVSz->SetXTitle("True z DV [cm]");
  effDVrVSz->SetYTitle("True r DV [cm]");
  effDVrVSz->Divide(zVSrTrueMatch,zVSrTrue,1.0,1.0);

  // Make a 2D histo of reconstructed l v/s true l
  TH2F* recVStrue = new TH2F("recVStrue", "recVStrue", 80, 0.0, 600.0, 80, 0.0, 600.0);
  recVStrue->SetXTitle("L_{true} [cm]");
  recVStrue->SetYTitle("L [cm]");
  DVTree.Draw("(DV_l):(DVT_l)>>recVStrue", Trigger&&Loose&&AddTight&&TruthMatch&&MatchToTruth);

  // Make a 2D histo of l residual v/s true l
  TH2F* residVStrue = new TH2F("residVStrue", "residVStrue", 80, 0.0, 600.0, 100, -50.0, 50.0);
  residVStrue->SetXTitle("L_{true} [cm]");
  residVStrue->SetYTitle("L-L_{true} [cm]");
  DVTree.Draw("((DV_l-DVT_l)):(DVT_l)>>residVStrue", Trigger&&Loose&&AddTight&&TruthMatch&&MatchToTruth);
  TProfile* residVStrueProfile = residVStrue->ProfileX("residVStrueProfile", 1, -1, "s");
  residVStrueProfile->SetYTitle("L residual");

  // Make a 2D histo of l residual v/s dimuon opening angle
  TH2F* residVSOpenAngle = new TH2F("residVSOpenAngle", "residVSOpenAngle", 100, 0.0, 3.14, 100, -50., 50.);
  residVSOpenAngle->SetXTitle("#theta_{#mu#mu} [rad]");
  residVSOpenAngle->SetYTitle("L-L_{true} [cm]");
  DVTree.Draw("((DV_l-DVT_l)):(DVT_openAng)>>residVSOpenAngle", Trigger&&Loose&&AddTight&&TruthMatch&&MatchToTruth);
  TProfile* residVSOpenAngleProfile = residVSOpenAngle->ProfileX("residVSOpenAngleProfile", 1, -1, "s");
  residVSOpenAngleProfile->SetYTitle("L residual");

  // Make a 2D histo of reconstructed r v/s d0
  TH2F* rVSd0 = new TH2F("rVSd0", "rVSd0", 100, 0.0, 200.0, 100, 0.0, 400.0);
  rVSd0->SetXTitle("DV d_{0} [cm]");
  rVSd0->SetYTitle("DV r [cm]");
  DVTree.Draw("fabs(DV_r):(DV_muD01*0.1)>>rVSd0", Trigger&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("fabs(DV_r):(DV_muD02*0.1)>>+rVSd0", Trigger&&Loose&&AddTight&&TruthMatch);

  // Make a 2D histo of true r v/s pT of the leading muon of the vertex
  TH2F* rVSpT = new TH2F("rVSpT", "rVSpT", 100, 0.0, 1000.0, 100, 0.0, 400.0);
  rVSpT->SetXTitle("p_{T} of leading truth muon [GeV]");
  rVSpT->SetYTitle("r_{true} [cm]");
  DVTree.Draw("fabs(DVT_r):(DVT_muPt1)>>rVSpT", "DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("fabs(DVT_r):(DVT_muPt2)>>+rVSpT", "DVT_muPt1 < DVT_muPt2");

  // Make 1d histos of some reconstructed muon quantities (pt, phi, eta, d0, z0, etc.)
  TH1F* muPt = new TH1F("muPt", "muPt", 100, 0., 1000.0);
  muPt->SetYTitle("Events");
  muPt->SetXTitle("#mu p_{T} [GeV]");
  DVTree.Draw("DV_muPt1>>muPt", Trigger&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muPt2>>+muPt", Trigger&&Loose&&AddTight&&TruthMatch);

  TH1F* muPhi = new TH1F("muPhi", "muPhi", 100, -3.14, 3.14);
  muPhi->SetYTitle("Events");
  muPhi->SetXTitle("#phi_{#mu} [rad]");
  DVTree.Draw("DV_muPhi1>>muPhi", Trigger&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muPhi2>>+muPhi", Trigger&&Loose&&AddTight&&TruthMatch);

  TH1F* muEta = new TH1F("muEta", "muEta", 100, -5.0, 5.0);
  muEta->SetYTitle("Events");
  muEta->SetXTitle("#eta_{#mu}");
  DVTree.Draw("DV_muEta1>>muEta", Trigger&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muEta2>>+muEta", Trigger&&Loose&&AddTight&&TruthMatch);

  //TH1F* muD0 = new TH1F("muD0", "muD0", 100, 0., 400.);
  TH1F* muD0 = new TH1F("muD0", "muD0", 100, 0., 200.);
  muD0->SetYTitle("Events");
  muD0->SetXTitle("#mu d_{0} [cm]");
  DVTree.Draw("DV_muD01*0.1>>muD0", Trigger&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muD02*0.1>>+muD0", Trigger&&Loose&&AddTight&&TruthMatch);

  TH1F* muZ0 = new TH1F("muZ0", "muZ0", 100, 0., 400.);
  muZ0->SetYTitle("Events");
  muZ0->SetXTitle("#mu |z_{0}| [cm]");
  DVTree.Draw("fabs(DV_muZ01)*0.1>>muZ0", Trigger&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("fabs(DV_muZ02)*0.1>>+muZ0", Trigger&&Loose&&AddTight&&TruthMatch);

  TH1F* openAngle = new TH1F("openAngle", "openAngle", 100, 0., 3.14);
  openAngle->SetYTitle("Events");
  openAngle->SetXTitle("#theta_{#mu #mu} [rad]");
  DVTree.Draw("DV_openAng>>openAngle", Trigger&&Loose&&AddTight&&TruthMatch);

  TH1F* DOCA = new TH1F("DOCA", "DOCA", 100, 0., 15.0);
  DOCA->SetYTitle("Events");
  DOCA->SetXTitle("DV distance of closest approach [cm]");
  DVTree.Draw("DV_distV*0.1>>DOCA", Trigger&&Loose&&AddTight&&TruthMatch);

  TH1F* deltaR = new TH1F("deltaR", "deltaR", 100, 0.0, 0.5);
  deltaR->SetYTitle("Events");
  deltaR->SetXTitle("#DeltaR(comb,MS)");
  DVTree.Draw("DV_muDR1>>deltaR", Trigger&&Loose&&TruthMatch&&"DV_charge == 0 && DV_distV < 100.0");
  DVTree.Draw("DV_muDR2>>+deltaR", Trigger&&Loose&&TruthMatch&&"DV_charge == 0 && DV_distV < 100.0");

  // Now define some efficiency histograms (trigger wrt all truth DV, DV wrt triggered events and triggered wrt DV)

  cout<<"Making trigger efficiencies wrt truth"<<endl;

  TH1F* trueRNoTrig = new TH1F("trueRNoTrig", "trueRNoTrig", 50, 0., 600.);
  DVTree.Draw("DVT_r>>trueRNoTrig");
  trueRNoTrig->Sumw2();

  TH1F* trueLNoTrig = new TH1F("trueLNoTrig", "trueLNoTrig", 50, 0., 600.);
  DVTree.Draw("DVT_l>>trueLNoTrig");
  trueLNoTrig->Sumw2();

  TH1F* truePtNoTrig = new TH1F("truePtNoTrig", "truePtNoTrig", 50, 0., 600.);
  DVTree.Draw("DVT_muPt1>>truePtNoTrig", "DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muPt2>>+truePtNoTrig", "DVT_muPt1 < DVT_muPt2");
  truePtNoTrig->Sumw2();

  //TH1F* trueD0NoTrig = new TH1F("trueD0NoTrig", "trueD0NoTrig", 50, 0., 600.);
  TH1F* trueD0NoTrig = new TH1F("trueD0NoTrig", "trueD0NoTrig", 50, 0., 200.);
  DVTree.Draw("DVT_muD01*0.1>>trueD0NoTrig", "DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muD02*0.1>>+trueD0NoTrig", "DVT_muPt1 < DVT_muPt2");
  trueD0NoTrig->Sumw2();

  TH1F* trueOpenAngleNoTrig = new TH1F("trueOpenAngleNoTrig", "trueOpenAngleNoTrig", 100, 0., 3.14);
  trueOpenAngleNoTrig->SetYTitle("Events");
  trueOpenAngleNoTrig->SetXTitle("true #theta_{#mu #mu} [rad]");
  DVTree.Draw("DVT_openAng>>trueOpenAngleNoTrig");
  trueOpenAngleNoTrig->Sumw2();

  // MET trigger eff. v/s true r
  TH1F* trueRMETTrig = new TH1F("trueRMETTrig", "trueRMETTrig", 50, 0., 600.);
  TH1F* effMETTrigVSr = new TH1F("effMETTrigVSr", "effMETTrigVSr", 50, 0., 600.);
  trueRMETTrig->Sumw2();
  effMETTrigVSr->SetMarkerStyle(20);
  effMETTrigVSr->SetXTitle("r_{true} [cm]");
  effMETTrigVSr->SetYTitle("E_{T}^{miss} trigger efficiency");
  DVTree.Draw("DVT_r>>trueRMETTrig", "METTrig");
  effMETTrigVSr->Divide(trueRMETTrig,trueRNoTrig,1.0,1.0,"B");

  // MET trigger eff. v/s true L
  TH1F* trueLMETTrig = new TH1F("trueLMETTrig", "trueLMETTrig", 50, 0., 600.);
  TH1F* effMETTrigVSl = new TH1F("effMETTrigVSl", "effMETTrigVSl", 50, 0., 600.);
  trueLMETTrig->Sumw2();
  DVTree.Draw("DVT_l>>trueLMETTrig", "METTrig");
  effMETTrigVSl->Divide(trueLMETTrig,trueLNoTrig,1.0,1.0,"B");
  effMETTrigVSl->SetMarkerStyle(20);
  effMETTrigVSl->SetXTitle("L_{true} [cm]");
  effMETTrigVSl->SetYTitle("E_{T}^{miss} trigger efficiency");

  // MET trigger eff. v/s true d0
  //TH1F* trueD0METTrig = new TH1F("trueD0METTrig", "trueD0METTrig", 50, 0., 600.);
  //TH1F* effMETTrigVSd0 = new TH1F("effMETTrigVSd0", "effMETTrigVSdo", 50, 0., 600.);
  TH1F* trueD0METTrig = new TH1F("trueD0METTrig", "trueD0METTrig", 50, 0., 200.);
  TH1F* effMETTrigVSd0 = new TH1F("effMETTrigVSd0", "effMETTrigVSdo", 50, 0., 200.);
  trueD0METTrig->Sumw2();
  DVTree.Draw("DVT_muD01*0.1>>trueD0METTrig", "METTrig && DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muD02*0.1>>+trueD0METTrig", "METTrig && DVT_muPt1 < DVT_muPt2");
  effMETTrigVSd0->Divide(trueD0METTrig,trueD0NoTrig,1.0,1.0,"B");
  effMETTrigVSd0->SetMarkerStyle(20);
  effMETTrigVSd0->SetXTitle("d_{0} of leading truth muon [cm]");
  effMETTrigVSd0->SetYTitle("E_{T}^{miss} trigger efficiency");

  // MET trigger eff. v/s true pT
  TH1F* truePtMETTrig = new TH1F("truePtMETTrig", "truePtMETTrig", 50, 0., 600.);
  TH1F* effMETTrigVSpT = new TH1F("effMETTrigVSpT", "effMETTrigVSpT", 50., 0., 600.);
  truePtMETTrig->Sumw2();
  effMETTrigVSpT->SetMarkerStyle(20);
  effMETTrigVSpT->SetXTitle("p_{T} of leading truth muon [GeV]");
  effMETTrigVSpT->SetYTitle("E_{T}^{miss} trigger efficiency");
  DVTree.Draw("DVT_muPt1>>truePtMETTrig", "METTrig && DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muPt2>>+truePtMETTrig", "METTrig && DVT_muPt1 < DVT_muPt2");
  effMETTrigVSpT->Divide(truePtMETTrig,truePtNoTrig,1.0,1.0,"B");

  // MS60 trigger eff. v/s true pT
  TH1F* truePtMSTrig = new TH1F("truePtMSTrig", "truePtMSTrig", 50, 0., 600.);
  TH1F* effMSTrigVSpT = new TH1F("effMSTrigVSpT", "effMSTrigVSpT", 50., 0., 600.);
  DVTree.Draw("DVT_muPt1>>truePtMSTrig", "MuTrig && DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muPt2>>+truePtMSTrig", "MuTrig && DVT_muPt1 < DVT_muPt2");
  truePtMSTrig->Sumw2();
  effMSTrigVSpT->Divide(truePtMSTrig,truePtNoTrig,1.0,1.0,"B");
  effMSTrigVSpT->SetMarkerStyle(20);
  effMSTrigVSpT->SetXTitle("p_{T} of leading truth muon [GeV]");
  effMSTrigVSpT->SetYTitle("MS60 trigger efficiency");

  // MS60 trigger eff. v/s true d0
  //TH1F* trueD0MSTrig = new TH1F("trueD0MSTrig", "trueD0MSTrig", 50, 0., 600.);
  //TH1F* effMSTrigVSd0 = new TH1F("effMSTrigVSd0", "effMSTrigVSd0", 50, 0., 600.);
  TH1F* trueD0MSTrig = new TH1F("trueD0MSTrig", "trueD0MSTrig", 50, 0., 200.);
  TH1F* effMSTrigVSd0 = new TH1F("effMSTrigVSd0", "effMSTrigVSd0", 50, 0., 200.);
  trueD0MSTrig->Sumw2();
  DVTree.Draw("DVT_muD01*0.1>>trueD0MSTrig", "MuTrig && DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muD02*0.1>>+trueD0MSTrig", "MuTrig && DVT_muPt1 < DVT_muPt2");
  effMSTrigVSd0->Divide(trueD0MSTrig,trueD0NoTrig,1.0,1.0,"B");
  effMSTrigVSd0->SetMarkerStyle(20);
  effMSTrigVSd0->SetXTitle("d_{0} of leading truth muon [cm]");
  effMSTrigVSd0->SetYTitle("MS60 trigger efficiency");

  // MS60 trigger eff. v/s true r
  TH1F* trueRMSTrig = new TH1F("trueRMSTrig", "trueRMSTrig", 50, 0., 600.);
  TH1F* effMSTrigVSr = new TH1F("effMSTrigVSr", "effMSTrigVSr", 50, 0., 600.);
  DVTree.Draw("DVT_r>>trueRMSTrig", "MuTrig");
  trueRMSTrig->Sumw2();
  effMSTrigVSr->Divide(trueRMSTrig,trueRNoTrig,1.0,1.0,"B");
  effMSTrigVSr->SetMarkerStyle(20);
  effMSTrigVSr->SetXTitle("r_{true} [cm]");
  effMSTrigVSr->SetYTitle("MS60 trigger efficiency");

  // MS60 trigger eff. v/s true l
  TH1F* trueLMSTrig = new TH1F("trueLMSTrig", "trueLMSTrig", 50, 0., 600.);
  TH1F* effMSTrigVSl = new TH1F("effMSTrigVSl", "effMSTrigVSl", 50, 0., 600.);
  DVTree.Draw("DVT_l>>trueLMSTrig", "MuTrig");
  trueLMSTrig->Sumw2();
  effMSTrigVSl->Divide(trueLMSTrig,trueLNoTrig,1.0,1.0,"B");
  effMSTrigVSl->SetMarkerStyle(20);
  effMSTrigVSl->SetXTitle("L_{true} [cm]");
  effMSTrigVSl->SetYTitle("MS60 trigger efficiency");

  // L1_MU20 trigger eff. v/s true d0
  //TH1F* trueD0Mu20Trig = new TH1F("trueD0Mu20Trig", "trueD0Mu20Trig", 50, 0., 600.);
  //TH1F* effMu20TrigVSd0 = new TH1F("effMu20TrigVSd0", "effMu20TrigVSd0", 50, 0., 600.);
  TH1F* trueD0Mu20Trig = new TH1F("trueD0Mu20Trig", "trueD0Mu20Trig", 50, 0., 200.);
  TH1F* effMu20TrigVSd0 = new TH1F("effMu20TrigVSd0", "effMu20TrigVSd0", 50, 0., 200.);
  trueD0Mu20Trig->Sumw2();
  DVTree.Draw("DVT_muD01*0.1>>trueD0Mu20Trig", "Mu20Trig && DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muD02*0.1>>+trueD0Mu20Trig", "Mu20Trig && DVT_muPt1 < DVT_muPt2");
  effMu20TrigVSd0->Divide(trueD0Mu20Trig,trueD0NoTrig,1.0,1.0,"B");
  effMu20TrigVSd0->SetMarkerStyle(20);
  effMu20TrigVSd0->SetXTitle("d_{0} of leading truth muon [cm]");
  effMu20TrigVSd0->SetYTitle("L1_MU20 trigger efficiency");

  // Ratio of MS60 to L1_MU20 trig efficiencies
  //TH1F* effMu20MSTrigVSd0 = new TH1F("effMu20MSTrigVSd0", "effMu20MSTrigVSd0", 50, 0., 600.);
  TH1F* effMu20MSTrigVSd0 = new TH1F("effMu20MSTrigVSd0", "effMu20MSTrigVSd0", 50, 0., 200.);
  effMu20MSTrigVSd0->Divide(trueD0MSTrig, trueD0Mu20Trig,1.0,1.0,"B");
  effMu20MSTrigVSd0->SetMarkerStyle(20);
  effMu20MSTrigVSd0->SetXTitle("d_{0} of leading truth muon [cm]");
  effMu20MSTrigVSd0->SetYTitle("#varepsilon_{MS}/#varepsilon_{L1}");

  // L1_MU20 trigger eff. v/s true r
  TH1F* trueRMu20Trig = new TH1F("trueRMu20Trig", "trueRMu20Trig", 50, 0., 600.);
  TH1F* effMu20TrigVSr = new TH1F("effMu20TrigVSr", "effMu20TrigVSr", 50, 0., 600.);
  trueRMu20Trig->Sumw2();
  DVTree.Draw("DVT_r>>trueRMu20Trig", "Mu20Trig");
  effMu20TrigVSr->Divide(trueRMu20Trig,trueRNoTrig,1.0,1.0,"B");
  effMu20TrigVSr->SetMarkerStyle(20);
  effMu20TrigVSr->SetXTitle("r_{true} [cm]");
  effMu20TrigVSr->SetYTitle("L1_MU20 trigger efficiency");

  // L1_MU20 trigger eff. v/s true L
  TH1F* trueLMu20Trig = new TH1F("trueLMu20Trig", "trueLMu20Trig", 50, 0., 600.);
  TH1F* effMu20TrigVSl = new TH1F("effMu20TrigVSl", "effMu20TrigVSl", 50, 0., 600.);
  trueLMu20Trig->Sumw2();
  DVTree.Draw("DVT_l>>trueLMu20Trig", "Mu20Trig");
  effMu20TrigVSl->Divide(trueLMu20Trig,trueLNoTrig,1.0,1.0,"B");
  effMu20TrigVSl->SetMarkerStyle(20);
  effMu20TrigVSl->SetXTitle("L_{true} [cm]");
  effMu20TrigVSl->SetYTitle("L1_MU20 trigger efficiency");

  // Low mass trigger trigger eff. v/s true pT
  TH1F* truePtLowMassTrig = new TH1F("truePtLowMassTrig", "truePtLowMassTrig", 50, 0., 600.);
  TH1F* effLowMassTrigVSpT = new TH1F("effLowMassTrigVSpT", "effLowMassTrigVSpT", 50., 0., 600.);
  DVTree.Draw("DVT_muPt1>>truePtLowMassTrig", TriggerLowMass && " DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muPt2>>+truePtLowMassTrig", TriggerLowMass && " DVT_muPt1 < DVT_muPt2");
  truePtLowMassTrig->Sumw2();
  effLowMassTrigVSpT->Divide(truePtLowMassTrig,truePtNoTrig,1.0,1.0,"B");
  effLowMassTrigVSpT->SetMarkerStyle(20);
  effLowMassTrigVSpT->SetXTitle("p_{T} of leading truth muon [GeV]");
  effLowMassTrigVSpT->SetYTitle("Low mass trigger efficiency");

  // Low mass trigger trigger eff. v/s true d0
  //TH1F* trueD0LowMassTrig = new TH1F("trueD0LowMassTrig", "trueD0LowMassTrig", 50, 0., 600.);
  //TH1F* effLowMassTrigVSd0 = new TH1F("effLowMassTrigVSd0", "effLowMassTrigVSd0", 50, 0., 600.);
  TH1F* trueD0LowMassTrig = new TH1F("trueD0LowMassTrig", "trueD0LowMassTrig", 50, 0., 200.);
  TH1F* effLowMassTrigVSd0 = new TH1F("effLowMassTrigVSd0", "effLowMassTrigVSd0", 50, 0., 200.);
  trueD0LowMassTrig->Sumw2();
  DVTree.Draw("DVT_muD01*0.1>>trueD0LowMassTrig", TriggerLowMass && " DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muD02*0.1>>+trueD0LowMassTrig", TriggerLowMass && " DVT_muPt1 < DVT_muPt2");
  effLowMassTrigVSd0->Divide(trueD0LowMassTrig,trueD0NoTrig,1.0,1.0,"B");
  effLowMassTrigVSd0->SetMarkerStyle(20);
  effLowMassTrigVSd0->SetXTitle("d_{0} of leading truth muon [cm]");
  effLowMassTrigVSd0->SetYTitle("Low mass trigger efficiency");

  // Low mass trigger trigger eff. v/s true r
  TH1F* trueRLowMassTrig = new TH1F("trueRLowMassTrig", "trueRLowMassTrig", 50, 0., 600.);
  TH1F* effLowMassTrigVSr = new TH1F("effLowMassTrigVSr", "effLowMassTrigVSr", 50, 0., 600.);
  DVTree.Draw("DVT_r>>trueRLowMassTrig", TriggerLowMass);
  trueRLowMassTrig->Sumw2();
  effLowMassTrigVSr->Divide(trueRLowMassTrig,trueRNoTrig,1.0,1.0,"B");
  effLowMassTrigVSr->SetMarkerStyle(20);
  effLowMassTrigVSr->SetXTitle("r_{true} [cm]");
  effLowMassTrigVSr->SetYTitle("Low mass trigger efficiency");

  // Low mass trigger trigger eff. v/s true l
  TH1F* trueLLowMassTrig = new TH1F("trueLLowMassTrig", "trueLLowMassTrig", 50, 0., 600.);
  TH1F* effLowMassTrigVSl = new TH1F("effLowMassTrigVSl", "effLowMassTrigVSl", 50, 0., 600.);
  DVTree.Draw("DVT_l>>trueLLowMassTrig", TriggerLowMass);
  trueLLowMassTrig->Sumw2();
  effLowMassTrigVSl->Divide(trueLLowMassTrig,trueLNoTrig,1.0,1.0,"B");
  effLowMassTrigVSl->SetMarkerStyle(20);
  effLowMassTrigVSl->SetXTitle("L_{true} [cm]");
  effLowMassTrigVSl->SetYTitle("Low mass trigger efficiency");

  // Low mass trigger eff. v/s true opening angle of dimuon pair
  TH1F* trueOpenAngleLowMassTrig = new TH1F("trueOpenAngleLowMassTrig", "trueOpenAngleLowMassTrig", 100, 0., 3.14);
  TH1F* effLowMassTrigVSopenAngle = new TH1F("effLowMassTrigVSopenAngle", "effLowMassTrigVSopenAngle", 100, 0., 3.14);
  trueOpenAngleLowMassTrig->Sumw2();
  DVTree.Draw("DVT_openAng>>trueOpenAngleLowMassTrig", TriggerLowMass);
  effLowMassTrigVSopenAngle->Divide(trueOpenAngleLowMassTrig,trueOpenAngleNoTrig,1.0,1.0,"B");
  effLowMassTrigVSopenAngle->SetMarkerStyle(20);
  effLowMassTrigVSopenAngle->SetXTitle("true #theta_{#mu #mu} [rad]");
  effLowMassTrigVSopenAngle->SetYTitle("Low mass trigger efficiency");

  // Full trigger eff. v/s true d0
  //TH1F* trueD0AllTrig = new TH1F("trueD0AllTrig", "trueD0AllTrig", 50, 0., 600.);
  //TH1F* effAllTrigVSd0 = new TH1F("effAllTrigVSd0", "effAllTrigVSd0", 50, 0., 600.);
  TH1F* trueD0AllTrig = new TH1F("trueD0AllTrig", "trueD0AllTrig", 50, 0., 200.);
  TH1F* effAllTrigVSd0 = new TH1F("effAllTrigVSd0", "effAllTrigVSd0", 50, 0., 200.);
  trueD0AllTrig->Sumw2();
  DVTree.Draw("DVT_muD01*0.1>>trueD0AllTrig", Trigger&&"DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muD02*0.1>>+trueD0AllTrig", Trigger&&"DVT_muPt1 < DVT_muPt2");
  effAllTrigVSd0->Divide(trueD0AllTrig,trueD0NoTrig,1.0,1.0,"B");
  effAllTrigVSd0->SetMarkerStyle(20);
  effAllTrigVSd0->SetXTitle("d_{0} of leading truth muon [cm]");
  effAllTrigVSd0->SetYTitle("total trigger efficiency");

  // Full trigger eff. v/s true L
  TH1F* trueLAllTrig = new TH1F("trueLAllTrig", "trueLAllTrig", 50, 0., 600.);
  TH1F* effAllTrigVSl = new TH1F("effAllTrigVSl", "effAllTrigVSl", 50, 0., 600.);
  trueLAllTrig->Sumw2();
  DVTree.Draw("DVT_l>>trueLAllTrig", Trigger);
  effAllTrigVSl->Divide(trueLAllTrig,trueLNoTrig,1.0,1.0,"B");
  effAllTrigVSl->SetMarkerStyle(20);
  effAllTrigVSl->SetXTitle("L_{true} [cm]");
  effAllTrigVSl->SetYTitle("total trigger efficiency");

  // Full trigger eff. v/s true pT
  TH1F* truePtAllTrig = new TH1F("truePtAllTrig", "truePtAllTrig", 50, 0., 600.);
  TH1F* effAllTrigVSpT = new TH1F("effAllTrigVSpT", "effAllTrigVSpT", 50, 0., 600.);
  truePtAllTrig->Sumw2();
  DVTree.Draw("DVT_muPt1>>truePtAllTrig", Trigger&&"DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muPt2>>+truePtAllTrig", Trigger&&"DVT_muPt1 < DVT_muPt2");
  effAllTrigVSpT->Divide(truePtAllTrig,truePtNoTrig,1.0,1.0,"B");
  effAllTrigVSpT->SetMarkerStyle(20);
  effAllTrigVSpT->SetXTitle("p_{T} of leading truth muon [GeV]");
  effAllTrigVSpT->SetYTitle("total trigger efficiency");

  // Full trigger eff. v/s true opening angle of dimuon pair
  TH1F* trueOpenAngleAllTrig = new TH1F("trueOpenAngleAllTrig", "trueOpenAngleAllTrig", 100, 0., 3.14);
  TH1F* effAllTrigVSopenAngle = new TH1F("effAllTrigVSopenAngle", "effAllTrigVSopenAngle", 100, 0., 3.14);
  trueOpenAngleAllTrig->Sumw2();
  DVTree.Draw("DVT_openAng>>trueOpenAngleAllTrig", Trigger);
  effAllTrigVSopenAngle->Divide(trueOpenAngleAllTrig,trueOpenAngleNoTrig,1.0,1.0,"B");
  effAllTrigVSopenAngle->SetMarkerStyle(20);
  effAllTrigVSopenAngle->SetXTitle("true #theta_{#mu #mu} [rad]");
  effAllTrigVSopenAngle->SetYTitle("total trigger efficiency");

  // Now for ratio of triggered to all reco DV

  cout<<"Making trigger efficiencies wrt reco DV"<<endl;

  TH1F* recoRNoTrig = new TH1F("recoRNoTrig", "recoRNoTrig", 50, 0., 600.);
  DVTree.Draw("DV_r>>recoRNoTrig", Loose&&AddTight&&TruthMatch);
  recoRNoTrig->Sumw2();

  TH1F* recoLNoTrig = new TH1F("recoLNoTrig", "recoLNoTrig", 50, 0., 600.);
  DVTree.Draw("DV_l>>recoLNoTrig", Loose&&AddTight&&TruthMatch);
  recoLNoTrig->Sumw2();

  TH1F* recoPtNoTrig = new TH1F("recoPtNoTrig", "recoPtNoTrig", 50, 0., 600.);
  DVTree.Draw("DV_muPt1>>recoPtNoTrig", Loose&&AddTight&&TruthMatch&&"DV_muPt1 > DV_muPt2");
  DVTree.Draw("DV_muPt2>>+recoPtNoTrig", Loose&&AddTight&&TruthMatch&&"DV_muPt1 < DV_muPt2");
  recoPtNoTrig->Sumw2();

  //TH1F* recoD0NoTrig = new TH1F("recoD0NoTrig", "recoD0NoTrig", 50, 0., 600.);
  TH1F* recoD0NoTrig = new TH1F("recoD0NoTrig", "recoD0NoTrig", 50, 0., 200.);
  DVTree.Draw("DV_muD01*0.1>>recoD0NoTrig", Loose&&AddTight&&TruthMatch&&"DV_muPt1 > DV_muPt2");
  DVTree.Draw("DV_muD02*0.1>>+recoD0NoTrig", Loose&&AddTight&&TruthMatch&&"DV_muPt1 < DV_muPt2");
  recoD0NoTrig->Sumw2();

  TH1F* recoOpenAngleNoTrig = new TH1F("recoOpenAngleNoTrig", "recoOpenAngleNoTrig", 100, 0., 3.14);
  recoOpenAngleNoTrig->SetYTitle("Events");
  recoOpenAngleNoTrig->SetXTitle("#theta_{#mu #mu} [rad]");
  DVTree.Draw("DV_openAng>>recoOpenAngleNoTrig", Loose&&AddTight&&TruthMatch);
  recoOpenAngleNoTrig->Sumw2();

  // MET trigger eff. v/s reco r
  TH1F* recoRMETTrig = new TH1F("recoRMETTrig", "recoRMETTrig", 50, 0., 600.);
  TH1F* effMETTrigVSRecor = new TH1F("effMETTrigVSRecor", "effMETTrigVSRecor", 50, 0., 600.);
  recoRMETTrig->Sumw2();
  effMETTrigVSRecor->SetMarkerStyle(20);
  effMETTrigVSRecor->SetXTitle("r [cm]");
  effMETTrigVSRecor->SetYTitle("E_{T}^{miss} trigger efficiency");
  DVTree.Draw("DV_r>>recoRMETTrig", "METTrig"&&Loose&&AddTight&&TruthMatch);
  effMETTrigVSRecor->Divide(recoRMETTrig,recoRNoTrig,1.0,1.0,"B");

  // MET trigger eff. v/s reco L
  TH1F* recoLMETTrig = new TH1F("recoLMETTrig", "recoLMETTrig", 50, 0., 600.);
  TH1F* effMETTrigVSRecol = new TH1F("effMETTrigVSRecol", "effMETTrigVSRecol", 50, 0., 600.);
  recoLMETTrig->Sumw2();
  DVTree.Draw("DV_l>>recoLMETTrig", "METTrig"&&Loose&&AddTight&&TruthMatch);
  effMETTrigVSRecol->Divide(recoLMETTrig,recoLNoTrig,1.0,1.0,"B");
  effMETTrigVSRecol->SetMarkerStyle(20);
  effMETTrigVSRecol->SetXTitle("L [cm]");
  effMETTrigVSRecol->SetYTitle("E_{T}^{miss} trigger efficiency");

  // MET trigger eff. v/s reco d0
  //TH1F* recoD0METTrig = new TH1F("recoD0METTrig", "recoD0METTrig", 50, 0., 600.);
  //TH1F* effMETTrigVSRecod0 = new TH1F("effMETTrigVSRecod0", "effMETTrigVSRecodo", 50, 0., 600.);
  TH1F* recoD0METTrig = new TH1F("recoD0METTrig", "recoD0METTrig", 50, 0., 200.);
  TH1F* effMETTrigVSRecod0 = new TH1F("effMETTrigVSRecod0", "effMETTrigVSRecodo", 50, 0., 200.);
  recoD0METTrig->Sumw2();
  DVTree.Draw("DV_muD01*0.1>>recoD0METTrig", "METTrig && DV_muPt1 > DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muD02*0.1>>+recoD0METTrig", "METTrig && DV_muPt1 < DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  effMETTrigVSRecod0->Divide(recoD0METTrig,recoD0NoTrig,1.0,1.0,"B");
  effMETTrigVSRecod0->SetMarkerStyle(20);
  effMETTrigVSRecod0->SetXTitle("d_{0} of leading muon [cm]");
  effMETTrigVSRecod0->SetYTitle("E_{T}^{miss} trigger efficiency");

  // MET trigger eff. v/s reco pT
  TH1F* recoPtMETTrig = new TH1F("recoPtMETTrig", "recoPtMETTrig", 50, 0., 600.);
  TH1F* effMETTrigVSRecopT = new TH1F("effMETTrigVSRecopT", "effMETTrigVSRecopT", 50., 0., 600.);
  recoPtMETTrig->Sumw2();
  effMETTrigVSRecopT->SetMarkerStyle(20);
  effMETTrigVSRecopT->SetXTitle("p_{T} of leading muon [GeV]");
  effMETTrigVSRecopT->SetYTitle("E_{T}^{miss} trigger efficiency");
  DVTree.Draw("DV_muPt1>>recoPtMETTrig", "METTrig && DV_muPt1 > DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muPt2>>+recoPtMETTrig", "METTrig && DV_muPt1 < DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  effMETTrigVSRecopT->Divide(recoPtMETTrig,recoPtNoTrig,1.0,1.0,"B");

  // MS60 trigger eff. v/s reco pT
  TH1F* recoPtMSTrig = new TH1F("recoPtMSTrig", "recoPtMSTrig", 50, 0., 600.);
  TH1F* effMSTrigVSRecopT = new TH1F("effMSTrigVSRecopT", "effMSTrigVSRecopT", 50., 0., 600.);
  DVTree.Draw("DV_muPt1>>recoPtMSTrig", "MuTrig && DV_muPt1 > DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muPt2>>+recoPtMSTrig", "MuTrig && DV_muPt1 < DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  recoPtMSTrig->Sumw2();
  effMSTrigVSRecopT->Divide(recoPtMSTrig,recoPtNoTrig,1.0,1.0,"B");
  effMSTrigVSRecopT->SetMarkerStyle(20);
  effMSTrigVSRecopT->SetXTitle("p_{T} of leading muon [GeV]");
  effMSTrigVSRecopT->SetYTitle("MS60 trigger efficiency");

  // MS60 trigger eff. v/s reco d0
  //TH1F* recoD0MSTrig = new TH1F("recoD0MSTrig", "recoD0MSTrig", 50, 0., 600.);
  //TH1F* effMSTrigVSRecod0 = new TH1F("effMSTrigVSRecod0", "effMSTrigVSRecod0", 50, 0., 600.);
  TH1F* recoD0MSTrig = new TH1F("recoD0MSTrig", "recoD0MSTrig", 50, 0., 200.);
  TH1F* effMSTrigVSRecod0 = new TH1F("effMSTrigVSRecod0", "effMSTrigVSRecod0", 50, 0., 200.);
  recoD0MSTrig->Sumw2();
  DVTree.Draw("DV_muD01*0.1>>recoD0MSTrig", "MuTrig && DV_muPt1 > DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muD02*0.1>>+recoD0MSTrig", "MuTrig && DV_muPt1 < DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  effMSTrigVSRecod0->Divide(recoD0MSTrig,recoD0NoTrig,1.0,1.0,"B");
  effMSTrigVSRecod0->SetMarkerStyle(20);
  effMSTrigVSRecod0->SetXTitle("d_{0} of leading muon [cm]");
  effMSTrigVSRecod0->SetYTitle("MS60 trigger efficiency");

  // MS60 trigger eff. v/s reco r
  TH1F* recoRMSTrig = new TH1F("recoRMSTrig", "recoRMSTrig", 50, 0., 600.);
  TH1F* effMSTrigVSRecor = new TH1F("effMSTrigVSRecor", "effMSTrigVSRecor", 50, 0., 600.);
  DVTree.Draw("DV_r>>recoRMSTrig", "MuTrig"&&Loose&&AddTight&&TruthMatch);
  recoRMSTrig->Sumw2();
  effMSTrigVSRecor->Divide(recoRMSTrig,recoRNoTrig,1.0,1.0,"B");
  effMSTrigVSRecor->SetMarkerStyle(20);
  effMSTrigVSRecor->SetXTitle("r [cm]");
  effMSTrigVSRecor->SetYTitle("MS60 trigger efficiency");

  // MS60 trigger eff. v/s reco l
  TH1F* recoLMSTrig = new TH1F("recoLMSTrig", "recoLMSTrig", 50, 0., 600.);
  TH1F* effMSTrigVSRecol = new TH1F("effMSTrigVSRecol", "effMSTrigVSRecol", 50, 0., 600.);
  DVTree.Draw("DV_l>>recoLMSTrig", "MuTrig"&&Loose&&AddTight&&TruthMatch);
  recoLMSTrig->Sumw2();
  effMSTrigVSRecol->Divide(recoLMSTrig,recoLNoTrig,1.0,1.0,"B");
  effMSTrigVSRecol->SetMarkerStyle(20);
  effMSTrigVSRecol->SetXTitle("L [cm]");
  effMSTrigVSRecol->SetYTitle("MS60 trigger efficiency");

  // L1_MU20 trigger eff. v/s reco d0
  //TH1F* recoD0Mu20Trig = new TH1F("recoD0Mu20Trig", "recoD0Mu20Trig", 50, 0., 600.);
  //TH1F* effMu20TrigVSRecod0 = new TH1F("effMu20TrigVSRecod0", "effMu20TrigVSRecod0", 50, 0., 600.);
  TH1F* recoD0Mu20Trig = new TH1F("recoD0Mu20Trig", "recoD0Mu20Trig", 50, 0., 200.);
  TH1F* effMu20TrigVSRecod0 = new TH1F("effMu20TrigVSRecod0", "effMu20TrigVSRecod0", 50, 0., 200.);
  recoD0Mu20Trig->Sumw2();
  DVTree.Draw("DV_muD01*0.1>>recoD0Mu20Trig", "Mu20Trig && DV_muPt1 > DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  DVTree.Draw("DV_muD02*0.1>>+recoD0Mu20Trig", "Mu20Trig && DV_muPt1 < DV_muPt2"&&Loose&&AddTight&&TruthMatch);
  effMu20TrigVSRecod0->Divide(recoD0Mu20Trig,recoD0NoTrig,1.0,1.0,"B");
  effMu20TrigVSRecod0->SetMarkerStyle(20);
  effMu20TrigVSRecod0->SetXTitle("d_{0} of leading muon [cm]");
  effMu20TrigVSRecod0->SetYTitle("L1_MU20 trigger efficiency");

  // L1_MU20 trigger eff. v/s reco r
  TH1F* recoRMu20Trig = new TH1F("recoRMu20Trig", "recoRMu20Trig", 50, 0., 600.);
  TH1F* effMu20TrigVSRecor = new TH1F("effMu20TrigVSRecor", "effMu20TrigVSRecor", 50, 0., 600.);
  recoRMu20Trig->Sumw2();
  DVTree.Draw("DV_r>>recoRMu20Trig", "Mu20Trig"&&Loose&&AddTight&&TruthMatch);
  effMu20TrigVSRecor->Divide(recoRMu20Trig,recoRNoTrig,1.0,1.0,"B");
  effMu20TrigVSRecor->SetMarkerStyle(20);
  effMu20TrigVSRecor->SetXTitle("r [cm]");
  effMu20TrigVSRecor->SetYTitle("L1_MU20 trigger efficiency");

  // L1_MU20 trigger eff. v/s reco L
  TH1F* recoLMu20Trig = new TH1F("recoLMu20Trig", "recoLMu20Trig", 50, 0., 600.);
  TH1F* effMu20TrigVSRecol = new TH1F("effMu20TrigVSRecol", "effMu20TrigVSRecol", 50, 0., 600.);
  recoLMu20Trig->Sumw2();
  DVTree.Draw("DV_l>>recoLMu20Trig", "Mu20Trig"&&Loose&&AddTight&&TruthMatch);
  effMu20TrigVSRecol->Divide(recoLMu20Trig,recoLNoTrig,1.0,1.0,"B");
  effMu20TrigVSRecol->SetMarkerStyle(20);
  effMu20TrigVSRecol->SetXTitle("L [cm]");
  effMu20TrigVSRecol->SetYTitle("L1_MU20 trigger efficiency");

  // Full trigger eff. v/s reco d0
  //TH1F* recoD0AllTrig = new TH1F("recoD0AllTrig", "recoD0AllTrig", 50, 0., 600.);
  //TH1F* effAllTrigVSRecod0 = new TH1F("effAllTrigVSRecod0", "effAllTrigVSRecod0", 50, 0., 600.);
  TH1F* recoD0AllTrig = new TH1F("recoD0AllTrig", "recoD0AllTrig", 50, 0., 200.);
  TH1F* effAllTrigVSRecod0 = new TH1F("effAllTrigVSRecod0", "effAllTrigVSRecod0", 50, 0., 200.);
  recoD0AllTrig->Sumw2();
  DVTree.Draw("DV_muD01*0.1>>recoD0AllTrig", Loose&&AddTight&&TruthMatch&&Trigger&&"DV_muPt1 > DV_muPt2");
  DVTree.Draw("DV_muD02*0.1>>+recoD0AllTrig", Loose&&AddTight&&TruthMatch&&Trigger&&"DV_muPt1 < DV_muPt2");
  effAllTrigVSRecod0->Divide(recoD0AllTrig,recoD0NoTrig,1.0,1.0,"B");
  effAllTrigVSRecod0->SetMarkerStyle(20);
  effAllTrigVSRecod0->SetXTitle("d_{0} of leading muon [cm]");
  effAllTrigVSRecod0->SetYTitle("total trigger efficiency");

  // Full trigger eff. v/s reco L
  TH1F* recoLAllTrig = new TH1F("recoLAllTrig", "recoLAllTrig", 50, 0., 600.);
  TH1F* effAllTrigVSRecol = new TH1F("effAllTrigVSRecol", "effAllTrigVSRecol", 50, 0., 600.);
  recoLAllTrig->Sumw2();
  DVTree.Draw("DV_l>>recoLAllTrig", Trigger&&Loose&&AddTight&&TruthMatch);
  effAllTrigVSRecol->Divide(recoLAllTrig,recoLNoTrig,1.0,1.0,"B");
  effAllTrigVSRecol->SetMarkerStyle(20);
  effAllTrigVSRecol->SetXTitle("L [cm]");
  effAllTrigVSRecol->SetYTitle("total trigger efficiency");

  // Full trigger eff. v/s reco pT
  TH1F* recoPtAllTrig = new TH1F("recoPtAllTrig", "recoPtAllTrig", 50, 0., 600.);
  TH1F* effAllTrigVSRecopT = new TH1F("effAllTrigVSRecopT", "effAllTrigVSRecopT", 50, 0., 600.);
  recoPtAllTrig->Sumw2();
  DVTree.Draw("DV_muPt1>>recoPtAllTrig", Loose&&AddTight&&TruthMatch&&Trigger&&"DV_muPt1 > DV_muPt2");
  DVTree.Draw("DV_muPt2>>+recoPtAllTrig", Loose&&AddTight&&TruthMatch&&Trigger&&"DV_muPt1 < DV_muPt2");
  effAllTrigVSRecopT->Divide(recoPtAllTrig,recoPtNoTrig,1.0,1.0,"B");
  effAllTrigVSRecopT->SetMarkerStyle(20);
  effAllTrigVSRecopT->SetXTitle("p_{T} of leading muon [GeV]");
  effAllTrigVSRecopT->SetYTitle("total trigger efficiency");

  // Full trigger eff. v/s reco opening angle of dimuon pair
  TH1F* recoOpenAngleAllTrig = new TH1F("recoOpenAngleAllTrig", "recoOpenAngleAllTrig", 100, 0., 3.14);
  TH1F* effAllTrigVSRecoopenAngle = new TH1F("effAllTrigVSRecoopenAngle", "effAllTrigVSRecoopenAngle", 100, 0., 3.14);
  recoOpenAngleAllTrig->Sumw2();
  DVTree.Draw("DV_openAng>>recoOpenAngleAllTrig", Trigger&&Loose&&AddTight&&TruthMatch);
  effAllTrigVSRecoopenAngle->Divide(recoOpenAngleAllTrig,recoOpenAngleNoTrig,1.0,1.0,"B");
  effAllTrigVSRecoopenAngle->SetMarkerStyle(20);
  effAllTrigVSRecoopenAngle->SetXTitle("#theta_{#mu #mu} [rad]");
  effAllTrigVSRecoopenAngle->SetYTitle("total trigger efficiency");


  // DV eff. v/s true r, z, L, d0 and pT of leading muon - relative to triggered events
  // Include a "zoomed" L plot
  TH1F* trueRDVTrig = new TH1F("trueRDVTrig","trueRDVTrig", 100, 0., 400.);
  trueRDVTrig->Sumw2();
  DVTree.Draw("DVT_r>>trueRDVTrig", Trigger);

  TH1F* trueZDVTrig = new TH1F("trueZDVTrig","trueZDVTrig", 100, -600., 600.);
  trueZDVTrig->Sumw2();
  DVTree.Draw("DVT_z>>trueZDVTrig", Trigger);

  TH1F* trueLDVTrig = new TH1F("trueLDVTrig","trueLDVTrig", 100, 0., 600.);
  trueLDVTrig->Sumw2();
  DVTree.Draw("DVT_l>>trueLDVTrig", Trigger);

  TH1F* trueLDVTrigZoom = new TH1F("trueLDVTrigZoom","trueLDVTrigZoom",40,0.0,20.0);
  trueLDVTrigZoom->Sumw2();
  DVTree.Draw("DVT_l>>trueLDVTrigZoom", Trigger);

  //TH1F* trueD0DVTrig = new TH1F("trueD0DVTrig","trueD0DVTrig", 100, 0., 400.);
  TH1F* trueD0DVTrig = new TH1F("trueD0DVTrig","trueD0DVTrig", 50, 0., 200.);
  trueD0DVTrig->Sumw2();
  DVTree.Draw("DVT_muD01*0.1>>trueD0DVTrig", "DVT_muPt1 > DVT_muPt2"&&Trigger);
  DVTree.Draw("DVT_muD02*0.1>>+trueD0DVTrig", "DVT_muPt1 < DVT_muPt2"&&Trigger);
 
  TH1F* truePtDVTrig = new TH1F("truePtDVTrig","truePtDVTrig", 100, 0., 600.);
  truePtDVTrig->Sumw2();
  DVTree.Draw("DVT_muPt1>>truePtDVTrig", "DVT_muPt1 > DVT_muPt2"&&Trigger);
  DVTree.Draw("DVT_muPt2>>+truePtDVTrig", "DVT_muPt1 < DVT_muPt2"&&Trigger);

  TH1F* trueRDV = new TH1F("trueRDV","trueRDV", 100, 0., 400.);
  trueRDV->Sumw2();
  DVTree.Draw("DVT_r>>trueRDV", Trigger&&Loose&&AddTight&&MatchToTruth);
  TH1F* effRDV = new TH1F("effRDV","effRDV", 100, 0., 400.);
  effRDV->Divide(trueRDV,trueRDVTrig,1.0,1.0,"B");
  effRDV->SetMarkerStyle(20);
  effRDV->SetXTitle("r^{true}_{vtx} [cm]");
  effRDV->SetYTitle("DV efficiency");

  TH1F* trueZDV = new TH1F("trueZDV","trueZDV", 100, -600., 600.);
  trueZDV->Sumw2();
  DVTree.Draw("DVT_z>>trueZDV", Trigger&&Loose&&AddTight&&MatchToTruth);
  TH1F* effZDV = new TH1F("effZDV","effZDV", 100, -600., 600.);
  effZDV->Divide(trueZDV,trueZDVTrig,1.0,1.0,"B");
  effZDV->SetMarkerStyle(20);
  effZDV->SetXTitle("z^{true}_{vtx} [cm]");
  effZDV->SetYTitle("DV efficiency");
 
  TH1F* trueLDV = new TH1F("trueLDV","trueLDV", 100, 0., 600.);
  trueLDV->Sumw2();
  DVTree.Draw("DVT_l>>trueLDV", Trigger&&Loose&&AddTight&&MatchToTruth);
  TH1F* effLDV = new TH1F("effLDV","effLDV", 100, 0., 600.);
  effLDV->Divide(trueLDV,trueLDVTrig,1.0,1.0,"B");
  effLDV->SetMarkerStyle(20);
  effLDV->SetXTitle("L^{true}_{vtx} [cm]");
  effLDV->SetYTitle("DV efficiency");

  TH1F* trueLDVZoom = new TH1F("trueLDVZoom","trueLDVZoom",40,0.0,20.0);
  trueLDVZoom->Sumw2();
  DVTree.Draw("DVT_l>>trueLDVZoom", Trigger&&Loose&&AddTight&&MatchToTruth);
  TH1F* effLDVZoom = new TH1F("effLDVZoom","effLDVZoom",40, 0.0, 20.0);
  effLDVZoom->Divide(trueLDVZoom,trueLDVTrigZoom,1.0,1.0,"B");
  effLDVZoom->SetMarkerStyle(20);
  effLDVZoom->SetXTitle("L^{true}_{vtx} [cm]");
  effLDVZoom->SetYTitle("DV efficiency");

  TH1F* trueOpenAngleDV = new TH1F("trueOpenAngleDV","trueOpenAngleDV", 100, 0., 3.14);
  trueOpenAngleDV->Sumw2();
  DVTree.Draw("DVT_openAng>>trueOpenAngleDV", Trigger&&Loose&&AddTight&&MatchToTruth);
  TH1F* effOpenAngleDV = new TH1F("effOpenAngleDV","effOpenAngleDV", 100, 0., 3.14);
  effOpenAngleDV->Divide(trueOpenAngleDV,trueOpenAngleAllTrig,1.0,1.0,"B");
  effOpenAngleDV->SetMarkerStyle(20);
  effOpenAngleDV->SetXTitle("true #theta_{#mu #mu} [rad]");
  effOpenAngleDV->SetYTitle("DV efficiency");

  //TH1F* trueD0DV = new TH1F("trueD0DV","trueD0DV", 100, 0., 400.);
  TH1F* trueD0DV = new TH1F("trueD0DV","trueD0DV", 50, 0., 200.);
  trueD0DV->Sumw2();
  DVTree.Draw("DVT_muD01*0.1>>trueD0DV", Trigger&&Loose&&AddTight&&MatchToTruth&&"DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muD02*0.1>>+trueD0DV", Trigger&&Loose&&AddTight&&MatchToTruth&&"DVT_muPt1 < DVT_muPt2");
  //TH1F* effD0DV = new TH1F("effD0DV","effD0DV", 100, 0., 400.);
  TH1F* effD0DV = new TH1F("effD0DV","effD0DV", 50, 0., 200.);
  effD0DV->Divide(trueD0DV,trueD0DVTrig,1.0,1.0,"B");
  effD0DV->SetMarkerStyle(20);
  effD0DV->SetXTitle("d_{0} of leading truth muon [cm]");
  effD0DV->SetYTitle("DV efficiency");
  
  TH1F* truePtDV = new TH1F("truePtDV","truePtDV", 100, 0., 600.);
  truePtDV->Sumw2();
  DVTree.Draw("DVT_muPt1>>truePtDV", Trigger&&Loose&&AddTight&&MatchToTruth&&"DVT_muPt1 > DVT_muPt2");
  DVTree.Draw("DVT_muPt2>>+truePtDV", Trigger&&Loose&&AddTight&&MatchToTruth&&"DVT_muPt1 < DVT_muPt2");
  TH1F* effPtDV = new TH1F("effPtDV","effPtDV", 100, 0., 600.);
  effPtDV->Divide(truePtDV,truePtDVTrig,1.0,1.0,"B");
  effPtDV->SetMarkerStyle(20);
  effPtDV->SetXTitle("p_{T} of leading truth muon [GeV]");
  effPtDV->SetYTitle("DV efficiency");

  cout<<"Done making all plots"<<endl;
  
  // Save all the histograms to an output Root file
  //outputHistos->Write();
  //outputHistos->Close();

  // Turn on the stats box for the 1D histos
  DVrTight->SetStats(1);
  DVzTight->SetStats(1);
  DVlTight->SetStats(1);
  DVrtMatch->SetStats(1);
  DVztMatch->SetStats(1);
  DVltMatch->SetStats(1);
  DVmasstMatch->SetStats(1);
  DVrResid->SetStats(1);
  DVzResid->SetStats(1);
  DVlResid->SetStats(1);
  muPt->SetStats(1);
  muPhi->SetStats(1);
  muEta->SetStats(1);
  muD0->SetStats(1);
  muZ0->SetStats(1);
  openAngle->SetStats(1);
  DOCA->SetStats(1);
  deltaR->SetStats(1);

  // We will have to make sure the canvas is big enough
  TCanvas* c1 = new TCanvas("c1", "c1", 1200, 800);

  // Make sure to leave a right margin for the scale for the 2D color plots
  c1->SetRightMargin(0.15);

  gStyle->SetOptStat("ourme");

  cout<<"Making PDF files of plots"<<endl;

  c1->Divide(3,2);

  c1->cd(1);
  DVrTight->Draw();
  c1->cd(2);
  DVzTight->Draw();
  c1->cd(3);
  DVlTight->Draw();
  c1->cd(4);
  DVrtMatch->Draw();
  c1->cd(5);
  DVztMatch->Draw();
  c1->cd(6);
  DVltMatch->Draw();
  c1->SaveAs(inputPath+"/DVPlots-"+sampleType+".pdf");

  // Do some simple Gaussian fits to the residuals
  TF1* rfitFunc = new TF1("rfitFunc", "gaus", -20.0, 20.0);
  TF1* zfitFunc = new TF1("zfitFunc", "gaus", -6.0, 6.0);
  TF1* lfitFunc = new TF1("lfitFunc", "gaus", -10.0, 10.0);

  gStyle->SetOptFit(11);
  rfitFunc->SetLineColor(2);
  zfitFunc->SetLineColor(2);
  lfitFunc->SetLineColor(2);
  rfitFunc->SetParNames("A","Mean","sigma");
  zfitFunc->SetParNames("A","Mean","sigma");
  lfitFunc->SetParNames("A","Mean","sigma");
  DVrResid->Fit("rfitFunc","R");
  DVzResid->Fit("zfitFunc","R");
  DVlResid->Fit("lfitFunc","R");

  c1->Clear();
  c1->Divide(2,2);

  c1->cd(1);
  DVrResid->Draw();
  c1->cd(2);
  DVzResid->Draw();
  c1->cd(3);
  DVlResid->Draw();
  c1->cd(4);
  gStyle->SetPalette(57);
  gStyle->SetNumberContours(40);
  zVSr->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVResidPlots-"+sampleType+".pdf");

  c1->Clear();
  c1->SetRightMargin(0.05);
  gStyle->SetPalette(57);
  gStyle->SetNumberContours(120);
  zVSr->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVrVSz-"+sampleType+".pdf");

  c1->Clear();
  muPt->Draw();
  c1->SaveAs(inputPath+"/DVMuPt-"+sampleType+".pdf");

  c1->Clear();
  muEta->Draw();
  c1->SaveAs(inputPath+"/DVMuEta-"+sampleType+".pdf");

  c1->Clear();
  muPhi->Draw();
  c1->SaveAs(inputPath+"/DVMuPhi-"+sampleType+".pdf");

  c1->Clear();
  muD0->Draw();
  c1->SaveAs(inputPath+"/DVMuD0-"+sampleType+".pdf");

  c1->Clear();
  muZ0->Draw();
  c1->SaveAs(inputPath+"/DVMuZ0-"+sampleType+".pdf");

  c1->Clear();
  DOCA->Draw();
  c1->SaveAs(inputPath+"/DVDOCA-"+sampleType+".pdf");

  c1->Clear();
  openAngle->Draw();
  c1->SaveAs(inputPath+"/DVOpenAngle-"+sampleType+".pdf");

  c1->Clear();
  deltaR->Draw();
  c1->SaveAs(inputPath+"/DVdeltaR-"+sampleType+".pdf");

  c1->Clear();
  recVStrue->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVrecVStrue-"+sampleType+".pdf");

  c1->Clear();
  DVlResid->Draw();
  c1->SaveAs(inputPath+"/DVlResid-"+sampleType+".pdf");

  c1->Clear();
  DVrResid->Draw();
  c1->SaveAs(inputPath+"/DVrResid-"+sampleType+".pdf");
  
  c1->Clear();
  DVzResid->Draw();
  c1->SaveAs(inputPath+"/DVzResid-"+sampleType+".pdf");

  c1->Clear();
  residVStrue->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVresidVStrue-"+sampleType+".pdf");

  c1->Clear();
  residVStrueProfile->Draw("EP");
  c1->SaveAs(inputPath+"/DVresidVStrueProfile-"+sampleType+".pdf");

  c1->Clear();
  residVSOpenAngle->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVresidVSOpenAngle-"+sampleType+".pdf");

  c1->Clear();
  residVSOpenAngleProfile->Draw("EP");
  c1->SaveAs(inputPath+"/DVresidVSOpenAngleProfile-"+sampleType+".pdf");

  c1->Clear();
  rVSd0->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVrVSd0-"+sampleType+".pdf");

  c1->Clear();
  rVSpT->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVrVSpT-"+sampleType+".pdf");

  c1->Clear();
  DVmasstMatch->Draw();
  c1->SaveAs(inputPath+"/DVmass-"+sampleType+".pdf");

  c1->Clear();
  effRDV->SetMaximum(1.0);
  effRDV->SetMinimum(0.0);
  effRDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffR-"+sampleType+".pdf");

  c1->Clear();
  effZDV->SetMaximum(1.0);
  effZDV->SetMinimum(0.0);
  effZDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffZ-"+sampleType+".pdf");

  c1->Clear();
  effLDV->SetMaximum(1.0);
  effLDV->SetMinimum(0.0);
  effLDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffL-"+sampleType+".pdf");

  c1->Clear();
  effLDVZoom->SetMaximum(1.0);
  effLDVZoom->SetMinimum(0.0);
  effLDVZoom->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffLZoom-"+sampleType+".pdf");

  c1->Clear();
  effOpenAngleDV->SetMaximum(1.0);
  effOpenAngleDV->SetMinimum(0.0);
  effOpenAngleDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffOpenAngle-"+sampleType+".pdf");

  c1->Clear();
  effD0DV->SetMaximum(1.0);
  effD0DV->SetMinimum(0.0);
  effD0DV->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffD0-"+sampleType+".pdf");

  c1->Clear();
  effPtDV->SetMaximum(1.0);
  effPtDV->SetMinimum(0.0);
  effPtDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffPt-"+sampleType+".pdf");

  c1->Clear();
  effMETTrigVSr->SetMaximum(1.0);
  effMETTrigVSr->SetMinimum(0.0);
  effMETTrigVSr->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMETTrigVSr-"+sampleType+".pdf");

  c1->Clear();
  effMETTrigVSl->SetMaximum(1.0);
  effMETTrigVSl->SetMinimum(0.0);
  effMETTrigVSl->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMETTrigVSl-"+sampleType+".pdf");

  c1->Clear();
  effMETTrigVSd0->SetMaximum(1.0);
  effMETTrigVSd0->SetMinimum(0.0);
  effMETTrigVSd0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMETTrigVSd0-"+sampleType+".pdf");

  c1->Clear();
  effMETTrigVSpT->SetMaximum(1.0);
  effMETTrigVSpT->SetMinimum(0.0);
  effMETTrigVSpT->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMETTrigVSpT-"+sampleType+".pdf");

  c1->Clear();
  effMSTrigVSpT->SetMaximum(1.0);
  effMSTrigVSpT->SetMinimum(0.0);
  effMSTrigVSpT->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMSTrigVSpT-"+sampleType+".pdf");

  c1->Clear();
  effMSTrigVSd0->SetMaximum(1.0);
  effMSTrigVSd0->SetMinimum(0.0);
  effMSTrigVSd0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMSTrigVSd0-"+sampleType+".pdf");

  c1->Clear();
  effMSTrigVSr->SetMaximum(1.0);
  effMSTrigVSr->SetMinimum(0.0);
  effMSTrigVSr->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMSTrigVSr-"+sampleType+".pdf");

  c1->Clear();
  effMSTrigVSl->SetMaximum(1.0);
  effMSTrigVSl->SetMinimum(0.0);
  effMSTrigVSl->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMSTrigVSl-"+sampleType+".pdf");

  c1->Clear();
  effMu20TrigVSd0->SetMaximum(1.0);
  effMu20TrigVSd0->SetMinimum(0.0);
  effMu20TrigVSd0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMu20TrigVSd0-"+sampleType+".pdf");

  c1->Clear();
  effMu20TrigVSr->SetMaximum(1.0);
  effMu20TrigVSr->SetMinimum(0.0);
  effMu20TrigVSr->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMu20TrigVSr-"+sampleType+".pdf");

  c1->Clear();
  effMu20TrigVSl->SetMaximum(1.0);
  effMu20TrigVSl->SetMinimum(0.0);
  effMu20TrigVSl->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMu20TrigVSl-"+sampleType+".pdf");

  c1->Clear();
  effMu20MSTrigVSd0->SetMaximum(1.0);
  effMu20MSTrigVSd0->SetMinimum(0.0);
  effMu20MSTrigVSd0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMu20MSTrigVSd0-"+sampleType+".pdf");

  c1->Clear();
  effLowMassTrigVSpT->SetMaximum(1.0);
  effLowMassTrigVSpT->SetMinimum(0.0);
  effLowMassTrigVSpT->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffLowMassTrigVSpT-"+sampleType+".pdf");

  c1->Clear();
  effLowMassTrigVSd0->SetMaximum(1.0);
  effLowMassTrigVSd0->SetMinimum(0.0);
  effLowMassTrigVSd0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffLowMassTrigVSd0-"+sampleType+".pdf");

  c1->Clear();
  effLowMassTrigVSr->SetMaximum(1.0);
  effLowMassTrigVSr->SetMinimum(0.0);
  effLowMassTrigVSr->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffLowMassTrigVSr-"+sampleType+".pdf");

  c1->Clear();
  effLowMassTrigVSl->SetMaximum(1.0);
  effLowMassTrigVSl->SetMinimum(0.0);
  effLowMassTrigVSl->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffLowMassTrigVSl-"+sampleType+".pdf");

  c1->Clear();
  effLowMassTrigVSopenAngle->SetMaximum(1.0);
  effLowMassTrigVSopenAngle->SetMinimum(0.0);
  effLowMassTrigVSopenAngle->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffLowMassTrigVSopenAngle-"+sampleType+".pdf");

  c1->Clear();
  effAllTrigVSl->SetMaximum(1.0);
  effAllTrigVSl->SetMinimum(0.0);
  effAllTrigVSl->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffAllTrigVSl-"+sampleType+".pdf");

  c1->Clear();
  effAllTrigVSd0->SetMaximum(1.0);
  effAllTrigVSd0->SetMinimum(0.0);
  effAllTrigVSd0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffAllTrigVSd0-"+sampleType+".pdf");

  c1->Clear();
  effAllTrigVSpT->SetMaximum(1.0);
  effAllTrigVSpT->SetMinimum(0.0);
  effAllTrigVSpT->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffAllTrigVSpT-"+sampleType+".pdf");

  c1->Clear();
  effAllTrigVSopenAngle->SetMaximum(1.0);
  effAllTrigVSopenAngle->SetMinimum(0.0);
  effAllTrigVSopenAngle->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffAllTrigVSopenAngle-"+sampleType+".pdf");


  c1->Clear();
  effMETTrigVSRecor->SetMaximum(1.0);
  effMETTrigVSRecor->SetMinimum(0.0);
  effMETTrigVSRecor->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMETTrigVSRecor-"+sampleType+".pdf");

  c1->Clear();
  effMETTrigVSRecol->SetMaximum(1.0);
  effMETTrigVSRecol->SetMinimum(0.0);
  effMETTrigVSRecol->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMETTrigVSRecol-"+sampleType+".pdf");

  c1->Clear();
  effMETTrigVSRecod0->SetMaximum(1.0);
  effMETTrigVSRecod0->SetMinimum(0.0);
  effMETTrigVSRecod0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMETTrigVSRecod0-"+sampleType+".pdf");

  c1->Clear();
  effMETTrigVSRecopT->SetMaximum(1.0);
  effMETTrigVSRecopT->SetMinimum(0.0);
  effMETTrigVSRecopT->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMETTrigVSRecopT-"+sampleType+".pdf");

  c1->Clear();
  effMSTrigVSRecopT->SetMaximum(1.0);
  effMSTrigVSRecopT->SetMinimum(0.0);
  effMSTrigVSRecopT->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMSTrigVSRecopT-"+sampleType+".pdf");

  c1->Clear();
  effMSTrigVSRecod0->SetMaximum(1.0);
  effMSTrigVSRecod0->SetMinimum(0.0);
  effMSTrigVSRecod0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMSTrigVSRecod0-"+sampleType+".pdf");

  c1->Clear();
  effMSTrigVSRecor->SetMaximum(1.0);
  effMSTrigVSRecor->SetMinimum(0.0);
  effMSTrigVSRecor->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMSTrigVSRecor-"+sampleType+".pdf");

  c1->Clear();
  effMSTrigVSRecol->SetMaximum(1.0);
  effMSTrigVSRecol->SetMinimum(0.0);
  effMSTrigVSRecol->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMSTrigVSRecol-"+sampleType+".pdf");

  c1->Clear();
  effMu20TrigVSRecod0->SetMaximum(1.0);
  effMu20TrigVSRecod0->SetMinimum(0.0);
  effMu20TrigVSRecod0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMu20TrigVSRecod0-"+sampleType+".pdf");

  c1->Clear();
  effMu20TrigVSRecor->SetMaximum(1.0);
  effMu20TrigVSRecor->SetMinimum(0.0);
  effMu20TrigVSRecor->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMu20TrigVSRecor-"+sampleType+".pdf");

  c1->Clear();
  effMu20TrigVSRecol->SetMaximum(1.0);
  effMu20TrigVSRecol->SetMinimum(0.0);
  effMu20TrigVSRecol->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffMu20TrigVSRecol-"+sampleType+".pdf");

  c1->Clear();
  effAllTrigVSRecol->SetMaximum(1.0);
  effAllTrigVSRecol->SetMinimum(0.0);
  effAllTrigVSRecol->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffAllTrigVSRecol-"+sampleType+".pdf");

  c1->Clear();
  effAllTrigVSRecod0->SetMaximum(1.0);
  effAllTrigVSRecod0->SetMinimum(0.0);
  effAllTrigVSRecod0->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffAllTrigVSRecod0-"+sampleType+".pdf");

  c1->Clear();
  effAllTrigVSRecopT->SetMaximum(1.0);
  effAllTrigVSRecopT->SetMinimum(0.0);
  effAllTrigVSRecopT->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffAllTrigVSRecopT-"+sampleType+".pdf");

  c1->Clear();
  effAllTrigVSRecoopenAngle->SetMaximum(1.0);
  effAllTrigVSRecoopenAngle->SetMinimum(0.0);
  effAllTrigVSRecoopenAngle->Draw("EP");
  c1->SaveAs(inputPath+"/DVeffAllTrigVSRecoopenAngle-"+sampleType+".pdf");

  c1->Clear();
  gStyle->SetNumberContours(40);
  effDVrVSz->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVeffDVrVSz-"+sampleType+".pdf");

  c1->Clear();
  
  c1->SetLogy();
  DVrtMatch->Draw();
  c1->SaveAs(inputPath+"/DVrtMatch-"+sampleType+".pdf");
  c1->Clear();
  c1->SetLogy();
  DVztMatch->Draw();
  c1->SaveAs(inputPath+"/DVztMatch-"+sampleType+".pdf");
  c1->Clear();
  c1->SetLogy();
  DVltMatch->Draw();
  c1->SaveAs(inputPath+"/DVltMatch-"+sampleType+".pdf");
  c1->Clear();
  c1->SetLogy();
  muD0->Draw();
  c1->SaveAs(inputPath+"/DVd0tMatch-"+sampleType+".pdf");
  c1->Clear();
  c1->SetLogy();
  muZ0->Draw();
  c1->SaveAs(inputPath+"/DVz0tMatch-"+sampleType+".pdf");


}

