//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Apr 21 21:50:08 2016 by ROOT version 6.04/02
// from TTree tree/tree
// found on file: Zdm20ct05.merge.NTUP.root
//////////////////////////////////////////////////////////

#ifndef lifetimeAnalysis_h
#define lifetimeAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include <TH1.h>
#include <TVector3.h>

class lifetimeAnalysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types

   // Declaration of leaf types
   Int_t           EventNumber;
   Int_t           NDV;
   Int_t           NDVT;
   Int_t           MuTrig;
   Int_t           METTrig;
   Int_t           L13Mu6Trig;
   Float_t           trigEta;
   Float_t           trigPhi;
   Int_t           Mu20Trig;
   Int_t           Mu10Trig;
   Int_t           HLT3Mu6Trig;
   Int_t           J80Met80Trig;
   Int_t           Mu20Mu6Trig;
   Float_t         EtMiss;
   Float_t         EtMissPhi;
   Float_t         evtWeight;
   Float_t         sumWeights;
   Float_t         prw;
   Int_t           nMuSA;
   Int_t           nMSOnly;
   Int_t           PV_ntrk;
   Int_t           PU_nvtx;

   std::vector<float>   *DVT_r;
   std::vector<float>   *DVT_z;
   std::vector<float>   *DVT_x;
   std::vector<float>   *DVT_y;
   std::vector<float>   *DVT_l;
   std::vector<float>   *DVT_m;
   std::vector<float>   *DVT_openAng;
   std::vector<int>     *DVT_parPDG;
   std::vector<int>     *DVT_parBarCode;
   std::vector<float>   *DVT_muPt1;
   std::vector<float>   *DVT_muPhi1;
   std::vector<float>   *DVT_muEta1;
   std::vector<float>   *DVT_muD01;
   std::vector<float>   *DVT_muZ01;
   std::vector<int>     *DVT_muPDG1;
   std::vector<int>     *DVT_muBarCode1;
   std::vector<float>   *DVT_muPt2;
   std::vector<float>   *DVT_muPhi2;
   std::vector<float>   *DVT_muEta2;
   std::vector<float>   *DVT_muD02;
   std::vector<float>   *DVT_muZ02;
   std::vector<int>     *DVT_muPDG2;
   std::vector<int>     *DVT_muBarCode2;
   std::vector<double>   *DVT_pLLP;
   std::vector<double>   *DVT_eLLP;
   std::vector<double>   *DVT_lLLP;
     
   std::vector<int>     *DV_truthMatch;
   std::vector<float>   *DV_r;
   std::vector<float>   *DV_z;
   std::vector<float>   *DV_x;
   std::vector<float>   *DV_y;
   std::vector<float>   *DV_l;
   std::vector<float>   *DV_m;
   std::vector<float>   *DV_charge;
   std::vector<float>   *DV_openAng;
   std::vector<float>   *DV_distV;
   std::vector<int>     *DV_parBarCode;
   std::vector<float>   *DV_muPt1;
   std::vector<float>   *DV_muPhi1;
   std::vector<float>   *DV_muEta1;
   std::vector<float>   *DV_muD01;
   std::vector<float>   *DV_muZ01;
   std::vector<float>   *DV_muDR1;
   std::vector<int>     *DV_muPDG1;
   std::vector<int>     *DV_muParPDG1;
   std::vector<float>   *DV_muIso1;
   std::vector<float>   *DV_muJetDR1;
   std::vector<uint8_t> *DV_muNPrecLayers1;
   std::vector<uint8_t> *DV_muNPrecHoleLayers1;
   std::vector<uint8_t> *DV_muNPhiLayers1;
   std::vector<uint8_t> *DV_muNPhiHoleLayers1;
   std::vector<uint8_t> *DV_muNEtaLayers1;
   std::vector<uint8_t> *DV_muNEtaHoleLayers1;
   std::vector<float>   *DV_muVx1;
   std::vector<float>   *DV_muVy1;
   std::vector<float>   *DV_muVz1;
   std::vector<float>   *DV_muPt2;
   std::vector<float>   *DV_muPhi2;
   std::vector<float>   *DV_muEta2;
   std::vector<float>   *DV_muD02;
   std::vector<float>   *DV_muZ02;
   std::vector<float>   *DV_muDR2;
   std::vector<int>     *DV_muPDG2;
   std::vector<int>     *DV_muParPDG2;
   std::vector<float>   *DV_muIso2;
   std::vector<float>   *DV_muJetDR2;
   std::vector<uint8_t> *DV_muNPrecLayers2;
   std::vector<uint8_t> *DV_muNPrecHoleLayers2;
   std::vector<uint8_t> *DV_muNPhiLayers2;
   std::vector<uint8_t> *DV_muNPhiHoleLayers2;
   std::vector<uint8_t> *DV_muNEtaLayers2;
   std::vector<uint8_t> *DV_muNEtaHoleLayers2;
   std::vector<float>   *DV_muVx2;
   std::vector<float>   *DV_muVy2;
   std::vector<float>   *DV_muVz2;
   std::vector<float>   *TPV_x;
   std::vector<float>   *TPV_y;
   std::vector<float>   *TPV_z;
   std::vector<float>   *PV_x;
   std::vector<float>   *PV_y;
   std::vector<float>   *PV_z;

   // List of branches
   TBranch        *b_EventNumber;   //!
   TBranch        *b_NDV;   //!
   TBranch        *b_NDVT;   //!
   TBranch        *b_MuTrig;   //!
   TBranch        *b_METTrig;   //!
   TBranch        *b_trigEta;    //!
   TBranch        *b_trigPhi;    //!
   TBranch        *b_L13Mu6Trig; //!
   TBranch        *b_Mu20Trig;   //!
   TBranch        *b_Mu10Trig;   //!
   TBranch        *b_HLT3Mu6Trig;   //!
   TBranch        *b_J80Met80Trig;   //!
   TBranch        *b_Mu20Mu6Trig;   //!
   TBranch        *b_EtMiss;   //!
   TBranch        *b_EtMissPhi;   //!
   TBranch        *b_evtWeight;   //!
   TBranch        *b_sumWeights;   //!
   TBranch        *b_prw;   //!
   TBranch        *b_nMuSA;   //!
   TBranch        *b_nMSOnly; //!
   TBranch        *b_PV_ntrk; //!
   TBranch        *b_PU_nvtx; //!
   TBranch        *b_DVT_r;   //!
   TBranch        *b_DVT_z;   //!
   TBranch        *b_DVT_x;   //!
   TBranch        *b_DVT_y;   //!
   TBranch        *b_DVT_l;   //!
   TBranch        *b_DVT_m;   //!
   TBranch        *b_DVT_openAng;   //!
   TBranch        *b_DVT_parPDG;   //!
   TBranch        *b_DVT_parBarCode;   //!
   TBranch        *b_DVT_muPt1;   //!
   TBranch        *b_DVT_muPhi1;   //!
   TBranch        *b_DVT_muEta1;   //!
   TBranch        *b_DVT_muD01;   //!
   TBranch        *b_DVT_muZ01;   //!
   TBranch        *b_DVT_muPDG1;   //!
   TBranch        *b_DVT_muBarCode1;   //!
   TBranch        *b_DVT_muPt2;   //!
   TBranch        *b_DVT_muPhi2;   //!
   TBranch        *b_DVT_muEta2;   //!
   TBranch        *b_DVT_muD02;   //!
   TBranch        *b_DVT_muZ02;   //!
   TBranch        *b_DVT_muPDG2;   //!
   TBranch        *b_DVT_muBarCode2;   //!
   TBranch        *b_DVT_pLLP;  //!
   TBranch        *b_DVT_eLLP;  //!
   TBranch        *b_DVT_lLLP;  //!
   TBranch        *b_DV_truthMatch;   //!
   TBranch        *b_DV_r;   //!
   TBranch        *b_DV_z;   //!
   TBranch        *b_DV_x;   //!
   TBranch        *b_DV_y;   //!
   TBranch        *b_DV_l;   //!
   TBranch        *b_DV_m;   //!
   TBranch        *b_DV_charge;   //!
   TBranch        *b_DV_openAng;   //!
   TBranch        *b_DV_distV;   //!
   TBranch        *b_DV_parBarCode;   //!
   TBranch        *b_DV_muPt1;   //!
   TBranch        *b_DV_muPhi1;   //!
   TBranch        *b_DV_muEta1;   //!
   TBranch        *b_DV_muD01;   //!
   TBranch        *b_DV_muZ01;   //!
   TBranch        *b_DV_muDR1;   //!
   TBranch        *b_DV_muPDG1;   //!
   TBranch        *b_DV_muParPDG1;   //!
   TBranch        *b_DV_muIso1;    //!
   TBranch        *b_DV_muJetDR1;    //!
   TBranch        *b_DV_muNPrecLayers1;    //!
   TBranch        *b_DV_muNPrecHoleLayers1;    //!
   TBranch        *b_DV_muNPhiLayers1;    //!
   TBranch        *b_DV_muNPhiHoleLayers1;    //!
   TBranch        *b_DV_muNEtaLayers1;    //!
   TBranch        *b_DV_muNEtaHoleLayers1;    //!
   TBranch        *b_DV_muVx1;   //!
   TBranch        *b_DV_muVy1;   //!
   TBranch        *b_DV_muVz1;   //!
   TBranch        *b_DV_muPt2;   //!
   TBranch        *b_DV_muPhi2;   //!
   TBranch        *b_DV_muEta2;   //!
   TBranch        *b_DV_muD02;   //!
   TBranch        *b_DV_muZ02;   //!
   TBranch        *b_DV_muDR2;   //!
   TBranch        *b_DV_muPDG2;   //!
   TBranch        *b_DV_muParPDG2;   //!
   TBranch        *b_DV_muIso2;    //!
   TBranch        *b_DV_muJetDR2;    //!
   TBranch        *b_DV_muNPrecLayers2;    //!
   TBranch        *b_DV_muNPrecHoleLayers2;    //!
   TBranch        *b_DV_muNPhiLayers2;    //!
   TBranch        *b_DV_muNPhiHoleLayers2;    //!
   TBranch        *b_DV_muNEtaLayers2;    //!
   TBranch        *b_DV_muNEtaHoleLayers2;    //!
   TBranch        *b_DV_muVx2;   //!
   TBranch        *b_DV_muVy2;   //!
   TBranch        *b_DV_muVz2;   //!
   TBranch        *b_TPV_x;   //!
   TBranch        *b_TPV_y;   //!
   TBranch        *b_TPV_z;   //!
   TBranch        *b_PV_x;   //!
   TBranch        *b_PV_y;   //!
   TBranch        *b_PV_z;   //!     
   
   lifetimeAnalysis(TTree *tree=0);
   virtual ~lifetimeAnalysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(std::vector<double>& iLifetimes, TH1D* histoNum, TH1D* histoDen, TH1F* histoNumLvtx, TH1F* histoDenLvtx,
			 TH1F* histoNumd0, TH1F* histoDend0, TH1F* histoNumpT, TH1F* histoDenpT,
			 TH1F* histoNumOpenAngle, TH1F* histoDenOpenAngle,
			 TH1F* histoNumPV, TH1F* histoDenPV,
			 TH1F* histoResidr, TH1F* histoResidz, TH1F* histoResidl, TH1F* histoResidd0,
			 TH1F* histoResidpT, TH1F* histoResidphi, TH1F* histoResideta,
			 double sampleLifetime, int runNum);
   virtual float    makePoissonModel(int nObserved, double nBackground, double sigmaBackground, double& ULminus, double& ULplus, int limitMode);
   bool removeChambers(TVector3 crossPoint,float eta,float phi,int numPrec);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef lifetimeAnalysis_cxx
lifetimeAnalysis::lifetimeAnalysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("InputFiles/Zdm20ct50.merge.NTUP.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("InputFiles/Zdm20ct50.merge.NTUP.root");
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

lifetimeAnalysis::~lifetimeAnalysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t lifetimeAnalysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t lifetimeAnalysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void lifetimeAnalysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   DVT_r = 0;
   DVT_z = 0;
   DVT_x = 0;
   DVT_y = 0;
   DVT_l = 0;
   DVT_m = 0;
   DVT_openAng = 0;
   DVT_parPDG = 0;
   DVT_parBarCode = 0;
   DVT_muPt1 = 0;
   DVT_muPhi1 = 0;
   DVT_muEta1 = 0;
   DVT_muD01 = 0;
   DVT_muZ01 = 0;
   DVT_muPDG1 = 0;
   DVT_muBarCode1 = 0;
   DVT_muPt2 = 0;
   DVT_muPhi2 = 0;
   DVT_muEta2 = 0;
   DVT_muD02 = 0;
   DVT_muZ02 = 0;
   DVT_muPDG2 = 0;
   DVT_muBarCode2 = 0;
   DVT_pLLP = 0;
   DVT_eLLP = 0;
   DVT_lLLP = 0;
   DV_truthMatch = 0;
   DV_r = 0;
   DV_z = 0;
   DV_x = 0;
   DV_y = 0;
   DV_l = 0;
   DV_m = 0;
   DV_charge = 0;
   DV_openAng = 0;
   DV_distV = 0;
   DV_parBarCode = 0;
   DV_muPt1 = 0;
   DV_muPhi1 = 0;
   DV_muEta1 = 0;
   DV_muD01 = 0;
   DV_muZ01 = 0;
   DV_muDR1 = 0;
   DV_muPDG1 = 0;
   DV_muParPDG1 = 0;
   DV_muIso1 = 0;
   DV_muJetDR1 = 0;
   DV_muNPrecLayers1 = 0;
   DV_muNPrecHoleLayers1 = 0;
   DV_muNPhiLayers1 = 0;
   DV_muNPhiHoleLayers1 = 0;
   DV_muNEtaLayers1 = 0;
   DV_muNEtaHoleLayers1 = 0;
   DV_muVx1 = 0;
   DV_muVy1 = 0;
   DV_muVz1 = 0;
   DV_muPt2 = 0;
   DV_muPhi2 = 0;
   DV_muEta2 = 0;
   DV_muD02 = 0;
   DV_muZ02 = 0;
   DV_muDR2 = 0;
   DV_muPDG2 = 0;
   DV_muParPDG2 = 0;
   DV_muIso2 = 0;
   DV_muJetDR2 = 0;
   DV_muNPrecLayers2 = 0;
   DV_muNPrecHoleLayers2 = 0;
   DV_muNPhiLayers2 = 0;
   DV_muNPhiHoleLayers2 = 0;
   DV_muNEtaLayers2 = 0;
   DV_muNEtaHoleLayers2 = 0;
   DV_muVx2 = 0;
   DV_muVy2 = 0;
   DV_muVz2 = 0;
   TPV_x = 0;
   TPV_y = 0;
   TPV_z = 0;
   PV_x = 0;
   PV_y = 0;
   PV_z = 0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("NDV", &NDV, &b_NDV);
   fChain->SetBranchAddress("NDVT", &NDVT, &b_NDVT);
   fChain->SetBranchAddress("MuTrig", &MuTrig, &b_MuTrig);
   fChain->SetBranchAddress("METTrig", &METTrig, &b_METTrig);
   fChain->SetBranchAddress("L13Mu6Trig", &L13Mu6Trig, &b_L13Mu6Trig);
   fChain->SetBranchAddress("trigEta", &trigEta, &b_trigEta);
   fChain->SetBranchAddress("trigPhi", &trigPhi, &b_trigPhi);
   fChain->SetBranchAddress("Mu20Trig", &Mu20Trig, &b_Mu20Trig);
   fChain->SetBranchAddress("Mu10Trig", &Mu10Trig, &b_Mu10Trig);
   fChain->SetBranchAddress("HLT3Mu6Trig", &HLT3Mu6Trig, &b_HLT3Mu6Trig);
   fChain->SetBranchAddress("J80Met80Trig", &J80Met80Trig, &b_J80Met80Trig);
   fChain->SetBranchAddress("Mu20Mu6Trig", &Mu20Mu6Trig, &b_Mu20Mu6Trig);
   fChain->SetBranchAddress("EtMiss", &EtMiss, &b_EtMiss);
   fChain->SetBranchAddress("EtMissPhi", &EtMissPhi, &b_EtMissPhi);
   fChain->SetBranchAddress("evtWeight", &evtWeight, &b_evtWeight);
   fChain->SetBranchAddress("sumWeights", &sumWeights, &b_sumWeights);
   fChain->SetBranchAddress("prw", &prw, &b_prw);
   fChain->SetBranchAddress("nMuSA", &nMuSA, &b_nMuSA);
   fChain->SetBranchAddress("nMSOnly", &nMSOnly, &b_nMSOnly);
   fChain->SetBranchAddress("PV_ntrk", &PV_ntrk, &b_PV_ntrk);
   fChain->SetBranchAddress("PU_nvtx", &PU_nvtx, &b_PU_nvtx);
   fChain->SetBranchAddress("DVT_r", &DVT_r, &b_DVT_r);
   fChain->SetBranchAddress("DVT_z", &DVT_z, &b_DVT_z);
   fChain->SetBranchAddress("DVT_x", &DVT_x, &b_DVT_x);
   fChain->SetBranchAddress("DVT_y", &DVT_y, &b_DVT_y);
   fChain->SetBranchAddress("DVT_l", &DVT_l, &b_DVT_l);
   fChain->SetBranchAddress("DVT_m", &DVT_m, &b_DVT_m);
   fChain->SetBranchAddress("DVT_openAng", &DVT_openAng, &b_DVT_openAng);
   fChain->SetBranchAddress("DVT_parPDG", &DVT_parPDG, &b_DVT_parPDG);
   fChain->SetBranchAddress("DVT_parBarCode", &DVT_parBarCode, &b_DVT_parBarCode);
   fChain->SetBranchAddress("DVT_muPt1", &DVT_muPt1, &b_DVT_muPt1);
   fChain->SetBranchAddress("DVT_muPhi1", &DVT_muPhi1, &b_DVT_muPhi1);
   fChain->SetBranchAddress("DVT_muEta1", &DVT_muEta1, &b_DVT_muEta1);
   fChain->SetBranchAddress("DVT_muD01", &DVT_muD01, &b_DVT_muD01);
   fChain->SetBranchAddress("DVT_muZ01", &DVT_muZ01, &b_DVT_muZ01);
   fChain->SetBranchAddress("DVT_muPDG1", &DVT_muPDG1, &b_DVT_muPDG1);
   fChain->SetBranchAddress("DVT_muBarCode1", &DVT_muBarCode1, &b_DVT_muBarCode1);
   fChain->SetBranchAddress("DVT_muPt2", &DVT_muPt2, &b_DVT_muPt2);
   fChain->SetBranchAddress("DVT_muPhi2", &DVT_muPhi2, &b_DVT_muPhi2);
   fChain->SetBranchAddress("DVT_muEta2", &DVT_muEta2, &b_DVT_muEta2);
   fChain->SetBranchAddress("DVT_muD02", &DVT_muD02, &b_DVT_muD02);
   fChain->SetBranchAddress("DVT_muZ02", &DVT_muZ02, &b_DVT_muZ02);
   fChain->SetBranchAddress("DVT_muPDG2", &DVT_muPDG2, &b_DVT_muPDG2);
   fChain->SetBranchAddress("DVT_muBarCode2", &DVT_muBarCode2, &b_DVT_muBarCode2);
   fChain->SetBranchAddress("DVT_pLLP", &DVT_pLLP, &b_DVT_pLLP);
   fChain->SetBranchAddress("DVT_eLLP", &DVT_eLLP, &b_DVT_eLLP);
   fChain->SetBranchAddress("DVT_lLLP", &DVT_lLLP, &b_DVT_lLLP);
   fChain->SetBranchAddress("DV_truthMatch", &DV_truthMatch, &b_DV_truthMatch);
   fChain->SetBranchAddress("DV_r", &DV_r, &b_DV_r);
   fChain->SetBranchAddress("DV_z", &DV_z, &b_DV_z);
   fChain->SetBranchAddress("DV_x", &DV_x, &b_DV_x);
   fChain->SetBranchAddress("DV_y", &DV_y, &b_DV_y);
   fChain->SetBranchAddress("DV_l", &DV_l, &b_DV_l);
   fChain->SetBranchAddress("DV_m", &DV_m, &b_DV_m);
   fChain->SetBranchAddress("DV_charge", &DV_charge, &b_DV_charge);
   fChain->SetBranchAddress("DV_openAng", &DV_openAng, &b_DV_openAng);
   fChain->SetBranchAddress("DV_distV", &DV_distV, &b_DV_distV);
   fChain->SetBranchAddress("DV_parBarCode", &DV_parBarCode, &b_DV_parBarCode);
   fChain->SetBranchAddress("DV_muPt1", &DV_muPt1, &b_DV_muPt1);
   fChain->SetBranchAddress("DV_muPhi1", &DV_muPhi1, &b_DV_muPhi1);
   fChain->SetBranchAddress("DV_muEta1", &DV_muEta1, &b_DV_muEta1);
   fChain->SetBranchAddress("DV_muD01", &DV_muD01, &b_DV_muD01);
   fChain->SetBranchAddress("DV_muZ01", &DV_muZ01, &b_DV_muZ01);
   fChain->SetBranchAddress("DV_muDR1", &DV_muDR1, &b_DV_muDR1);
   fChain->SetBranchAddress("DV_muPDG1", &DV_muPDG1, &b_DV_muPDG1);
   fChain->SetBranchAddress("DV_muParPDG1", &DV_muParPDG1, &b_DV_muParPDG1);
   fChain->SetBranchAddress("DV_muIso1", &DV_muIso1, &b_DV_muIso1);
   fChain->SetBranchAddress("DV_muJetDR1", &DV_muJetDR1, &b_DV_muJetDR1);
   fChain->SetBranchAddress("DV_muNPrecLayers1", &DV_muNPrecLayers1, &b_DV_muNPrecLayers1);
   fChain->SetBranchAddress("DV_muNPrecHoleLayers1", &DV_muNPrecHoleLayers1, &b_DV_muNPrecHoleLayers1);
   fChain->SetBranchAddress("DV_muNPhiLayers1", &DV_muNPhiLayers1, &b_DV_muNPhiLayers1);
   fChain->SetBranchAddress("DV_muNPhiHoleLayers1", &DV_muNPhiHoleLayers1, &b_DV_muNPhiHoleLayers1);
   fChain->SetBranchAddress("DV_muNEtaLayers1", &DV_muNEtaLayers1, &b_DV_muNEtaLayers1);
   fChain->SetBranchAddress("DV_muNEtaHoleLayers1", &DV_muNEtaHoleLayers1, &b_DV_muNEtaHoleLayers1);
   fChain->SetBranchAddress("DV_muVx1", &DV_muVx1, &b_DV_muVx1);
   fChain->SetBranchAddress("DV_muVy1", &DV_muVy1, &b_DV_muVy1);
   fChain->SetBranchAddress("DV_muVz1", &DV_muVz1, &b_DV_muVz1);
   fChain->SetBranchAddress("DV_muPt2", &DV_muPt2, &b_DV_muPt2);
   fChain->SetBranchAddress("DV_muPhi2", &DV_muPhi2, &b_DV_muPhi2);
   fChain->SetBranchAddress("DV_muEta2", &DV_muEta2, &b_DV_muEta2);
   fChain->SetBranchAddress("DV_muD02", &DV_muD02, &b_DV_muD02);
   fChain->SetBranchAddress("DV_muZ02", &DV_muZ02, &b_DV_muZ02);
   fChain->SetBranchAddress("DV_muDR2", &DV_muDR2, &b_DV_muDR2);
   fChain->SetBranchAddress("DV_muPDG2", &DV_muPDG2, &b_DV_muPDG2);
   fChain->SetBranchAddress("DV_muParPDG2", &DV_muParPDG2, &b_DV_muParPDG2);
   fChain->SetBranchAddress("DV_muIso2", &DV_muIso2, &b_DV_muIso2);
   fChain->SetBranchAddress("DV_muJetDR2", &DV_muJetDR2, &b_DV_muJetDR2);
   fChain->SetBranchAddress("DV_muNPrecLayers2", &DV_muNPrecLayers2, &b_DV_muNPrecLayers2);
   fChain->SetBranchAddress("DV_muNPrecHoleLayers2", &DV_muNPrecHoleLayers2, &b_DV_muNPrecHoleLayers2);
   fChain->SetBranchAddress("DV_muNPhiLayers2", &DV_muNPhiLayers2, &b_DV_muNPhiLayers2);
   fChain->SetBranchAddress("DV_muNPhiHoleLayers2", &DV_muNPhiHoleLayers2, &b_DV_muNPhiHoleLayers2);
   fChain->SetBranchAddress("DV_muNEtaLayers2", &DV_muNEtaLayers2, &b_DV_muNEtaLayers2);
   fChain->SetBranchAddress("DV_muNEtaHoleLayers2", &DV_muNEtaHoleLayers2, &b_DV_muNEtaHoleLayers2);
   fChain->SetBranchAddress("DV_muVx2", &DV_muVx2, &b_DV_muVx2);
   fChain->SetBranchAddress("DV_muVy2", &DV_muVy2, &b_DV_muVy2);
   fChain->SetBranchAddress("DV_muVz2", &DV_muVz2, &b_DV_muVz2);
   fChain->SetBranchAddress("TPV_x", &TPV_x, &b_TPV_x);
   fChain->SetBranchAddress("TPV_y", &TPV_y, &b_TPV_y);
   fChain->SetBranchAddress("TPV_z", &TPV_z, &b_TPV_z);
   fChain->SetBranchAddress("PV_x", &PV_x, &b_PV_x);
   fChain->SetBranchAddress("PV_y", &PV_y, &b_PV_y);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);

   Notify();
}

Bool_t lifetimeAnalysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void lifetimeAnalysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t lifetimeAnalysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef lifetimeAnalysis_cxx
