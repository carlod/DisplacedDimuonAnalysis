#ifndef ntupleAnalysis_h
#define ntupleAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include <TH1.h>
#include <TVector3.h>
#include "plotManager.h"
#include "analysisManager.h"

namespace CF{
  enum Cutflow {trig,PV,multitrk,Pt,D0,chambers,SigD0,FidVol,iso,JMOR,trkSep,invMass,OSVtx,cosmic,DR,maxSize};
}

namespace TrigSF{
  enum SFState {OFF,NOM,SYSUP,SYSDOWN};
}

class ntupleAnalysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           EventNumber;
   Int_t           RunNumber;
   Int_t           BCID;
   Int_t           LB;
   Float_t         evtWeight;
   Float_t         sumWeights;
   Float_t         prw;
   Float_t         EtMiss;
   Float_t         EtMissPhi;
   Float_t         TPV_x;
   Float_t         TPV_y;
   Float_t         TPV_z;
   Float_t         PV_z;
   Int_t           PV_ntrk;
   Int_t           PU_nvtx;
   Int_t           nMSOnly;
   Int_t           NDV;
   Int_t           NDVT;
   Int_t           narrowScanTrig;
   Int_t           METTrig;
   Int_t           singleMuTrig;
   Int_t           threeMuTrig;
   Int_t           L1_MU20Trig;
   Int_t           L1_3MU6Trig;
   Float_t         trigMHT;
   Float_t         trigMHTPhi;
   Float_t         MHT;
   Float_t         MHTPhi;
   
   std::vector<float>   *id_pt;
   std::vector<float>   *id_phi;
   std::vector<float>   *id_eta;
   std::vector<float>   *id_d0;
   std::vector<float>   *id_z0;
   std::vector<float>   *id_charge;
   std::vector<float>   *id_chi2;
   std::vector<float>   *id_dof;
   std::vector<float>   *id_vtxType;
   
   std::vector<float>   *trkt_pt;
   std::vector<float>   *trkt_phi;
   std::vector<float>   *trkt_eta;
   std::vector<float>   *trkt_d0;
   std::vector<float>   *trkt_z0;
   std::vector<float>   *trkt_charge;
   std::vector<int>     *trkt_PDG;
   std::vector<int>     *trkt_barCode;
   
   std::vector<int>     *trkt_status;
   
   std::vector<int>     *trkt_parPDG;
   std::vector<int>     *trkt_parBarCode;
   std::vector<float>   *DVT_x;
   std::vector<float>   *DVT_y;
   std::vector<float>   *DVT_z;
   std::vector<float>   *DVT_r;
   std::vector<float>   *DVT_l;
   std::vector<float>   *DVT_m;
   std::vector<float>   *DVT_openAng;
   std::vector<int>     *DVT_parPDG;
   std::vector<int>     *DVT_parBarCode;
   std::vector<int>     *DVT_muIndexLead;
   std::vector<int>     *DVT_muIndexSub;
   std::vector<double>  *DVT_pLLP;
   std::vector<double>  *DVT_eLLP;
   std::vector<double>  *DVT_lLLP;
   
   std::vector<float>   *comb_pt;
   std::vector<float>   *comb_phi;
   std::vector<float>   *comb_eta;
   std::vector<float>   *comb_d0;
   std::vector<float>   *comb_z0;
   std::vector<float>   *comb_isoDPV;
   std::vector<float>   *comb_isoDAll;
   std::vector<float>   *comb_isoIPV;
   std::vector<float>   *comb_isoIAll;
   std::vector<float>   *comb_JMOR;
   std::vector<int>   *comb_author;
   std::vector<float>   *comb_chi2;
   std::vector<float>   *comb_PDG;
   std::vector<float>   *comb_barcode;
   
   std::vector<float>   *trk_pt;
   std::vector<float>   *trk_phi;
   std::vector<float>   *trk_eta;
   std::vector<float>   *trk_d0;
   std::vector<float>   *trk_z0;
   std::vector<float>   *trk_m;
   std::vector<float>   *trk_charge;
   std::vector<float>   *trk_covd0;
   std::vector<float>   *trk_covz0;
   std::vector<float>   *trk_covPhi;
   std::vector<float>   *trk_covTheta;
   std::vector<float>   *trk_covP;
   
   Int_t           trk_isTM;
   std::vector<int>     *trk_PDG;
   std::vector<int>     *trk_barCode;
   std::vector<int>     *trk_parPDG;
   std::vector<int>     *trk_parBarCode;
   
   std::vector<float>   *trk_isoDPV;
   std::vector<float>   *trk_isoDAll;
   std::vector<float>   *trk_isoIPV;
   std::vector<float>   *trk_isoIAll;
   
   std::vector<float>   *trk_jetDR;
   
   std::vector<unsigned char> *trk_nPrecLayers;
   std::vector<unsigned char> *trk_nPhiLayers;
   std::vector<unsigned char> *trk_nEtaLayers;
   std::vector<float>   *trk_vx;
   std::vector<float>   *trk_vy;
   std::vector<float>   *trk_vz;
   std::vector<float>   *DV_x;
   std::vector<float>   *DV_y;
   std::vector<float>   *DV_z;
   std::vector<float>   *DV_r;
   std::vector<float>   *DV_l;
   std::vector<float>   *DV_m;
   std::vector<float>   *DV_charge;
   std::vector<float>   *DV_openAng;
   std::vector<float>   *DV_distV;
   std::vector<int>     *DV_muIndexLead;
   std::vector<int>     *DV_muIndexSub;

   // List of branches
   TBranch        *b_EventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_LB;   //!
   TBranch        *b_evtWeight;   //!
   TBranch        *b_sumWeights;   //!
   TBranch        *b_prw;   //!
   TBranch        *b_EtMiss;   //!
   TBranch        *b_EtMissPhi;   //!
   TBranch        *b_TPV_x;   //!
   TBranch        *b_TPV_y;   //!
   TBranch        *b_TPV_z;   //!
   TBranch        *b_PV_z;   //!
   TBranch        *b_PV_ntrk;   //!
   TBranch        *b_PU_nvtx;   //!
   TBranch        *b_nMSOnly;   //!
   TBranch        *b_NDV;   //!
   TBranch        *b_NDVT;   //!
   TBranch        *b_narrowScanTrig;   //!
   TBranch        *b_METTrig;   //!
   TBranch        *b_singleMuTrig;   //!
   TBranch        *b_threeMuTrig;   //!
   TBranch        *b_L1_MU20Trig;   //!
   TBranch        *b_L1_3MU6Trig;   //!
   TBranch        *b_trigMHT;   //!
   TBranch        *b_trigMHTPhi;   //!
   TBranch        *b_MHT;   //!
   TBranch        *b_MHTPhi;   //!
   
   TBranch        *b_id_pt;   //!
   TBranch        *b_id_phi;   //!
   TBranch        *b_id_eta;   //!
   TBranch        *b_id_d0;   //!
   TBranch        *b_id_z0;   //!
   TBranch        *b_id_charge;   //!
   TBranch        *b_id_chi2;   //!
   TBranch        *b_id_dof;   //!
   TBranch        *b_id_vtxType;   //!
   
   TBranch        *b_trkt_pt;   //!
   TBranch        *b_trkt_phi;   //!
   TBranch        *b_trkt_eta;   //!
   TBranch        *b_trkt_d0;   //!
   TBranch        *b_trkt_z0;   //!
   TBranch        *b_trkt_charge;   //!
   TBranch        *b_trkt_PDG;   //!
   TBranch        *b_trkt_barCode;   //!
   
   TBranch        *b_trkt_status;   //!
   
   TBranch        *b_trkt_parPDG;   //!
   TBranch        *b_trkt_parBarCode;   //!
   TBranch        *b_DVT_x;   //!
   TBranch        *b_DVT_y;   //!
   TBranch        *b_DVT_z;   //!
   TBranch        *b_DVT_r;   //!
   TBranch        *b_DVT_l;   //!
   TBranch        *b_DVT_m;   //!
   TBranch        *b_DVT_openAng;   //!
   TBranch        *b_DVT_parPDG;   //!
   TBranch        *b_DVT_parBarCode;   //!
   TBranch        *b_DVT_muIndexLead;   //!
   TBranch        *b_DVT_muIndexSub;   //!
   TBranch        *b_DVT_pLLP;   //!
   TBranch        *b_DVT_eLLP;   //!
   TBranch        *b_DVT_lLLP;   //!
   
   TBranch        *b_comb_pt;   //!
   TBranch        *b_comb_phi;   //!
   TBranch        *b_comb_eta;   //!
   TBranch        *b_comb_d0;   //!
   TBranch        *b_comb_z0;   //!
   TBranch        *b_comb_isoDPV;   //!
   TBranch        *b_comb_isoDAll;   //!
   TBranch        *b_comb_isoIPV;   //!
   TBranch        *b_comb_isoIAll;   //!
   TBranch        *b_comb_JMOR;   //!
   TBranch        *b_comb_author;   //!
   TBranch        *b_comb_chi2;   //!
   TBranch        *b_comb_PDG;   //!
   TBranch        *b_comb_barcode;   //!
   
   TBranch        *b_trk_pt;   //!
   TBranch        *b_trk_phi;   //!
   TBranch        *b_trk_eta;   //!
   TBranch        *b_trk_d0;   //!
   TBranch        *b_trk_z0;   //!
   TBranch        *b_trk_m;   //!
   TBranch        *b_trk_charge;   //!
   TBranch        *b_trk_covd0;   //!
   TBranch        *b_trk_covz0;   //!
   TBranch        *b_trk_covPhi;   //!
   TBranch        *b_trk_covTheta;   //!
   TBranch        *b_trk_covP;   //!
   
   TBranch        *b_trk_isTM;   //!
   TBranch        *b_trk_PDG;   //!
   TBranch        *b_trk_barCode;   //!
   TBranch        *b_trk_parPDG;   //!
   TBranch        *b_trk_parBarCode;   //!
   
   TBranch        *b_trk_isoDPV;   //!
   TBranch        *b_trk_isoDAll;   //!
   TBranch        *b_trk_isoIPV;   //!
   TBranch        *b_trk_isoIAll;   //!
   
   TBranch        *b_trk_jetDR;   //!
   
   TBranch        *b_trk_nPrecLayers;   //!
   TBranch        *b_trk_nPhiLayers;   //!
   TBranch        *b_trk_nEtaLayers;   //!
   TBranch        *b_trk_vx;   //!
   TBranch        *b_trk_vy;   //!
   TBranch        *b_trk_vz;   //!
   TBranch        *b_DV_x;   //!
   TBranch        *b_DV_y;   //!
   TBranch        *b_DV_z;   //!
   TBranch        *b_DV_r;   //!
   TBranch        *b_DV_l;   //!
   TBranch        *b_DV_m;   //!
   TBranch        *b_DV_charge;   //!
   TBranch        *b_DV_openAng;   //!
   TBranch        *b_DV_distV;   //!
   TBranch        *b_DV_muIndexLead;   //!
   TBranch        *b_DV_muIndexSub;   //!
 
   ntupleAnalysis(TTree *tree=0);
   virtual ~ntupleAnalysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(plotManager* pMan,std::vector<TH1F*>& histoVec,std::vector<TH1F*>& histoVecNW,std::vector<TH1F*>& histoVecVar,std::vector<TH1F*>& histoVecVarNW,std::vector<TH2F*>& histoVec2D,std::vector<TH2F*>& histoVec2DNW,std::vector<TH2F*>& histoVec2DVar,std::vector<TH2F*>& histoVec2DVarNW,int runNum,bool isMC,bool isSig, std::string sampleName,float theSF,std::string theRegion,std::vector<TH1F*>& SF1DHist,std::vector<TH2F*>& SF2DHist);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   bool removeChambers(float eta,float phi,int numPrec);
   bool trigRemoveChambers(float eta,float phi);
   float getNormDP(float DVx,float DVy,float DVz,float pt,float eta,float phi);
};

#endif

#ifdef ntupleAnalysis_cxx
ntupleAnalysis::ntupleAnalysis(TTree *tree) : fChain(0) 
{}

ntupleAnalysis::~ntupleAnalysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntupleAnalysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntupleAnalysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntupleAnalysis::Init(TTree *tree)
{

   // Set object pointer
   id_pt = 0;
   id_phi = 0;
   id_eta = 0;
   id_d0 = 0;
   id_z0 = 0;
   id_charge = 0;
   id_chi2 = 0;
   id_dof = 0;
   id_vtxType = 0;
   
   trkt_pt = 0;
   trkt_phi = 0;
   trkt_eta = 0;
   trkt_d0 = 0;
   trkt_z0 = 0;
   trkt_charge = 0;
   trkt_PDG = 0;
   trkt_barCode = 0;
   
   trkt_status = 0;
   
   trkt_parPDG = 0;
   trkt_parBarCode = 0;
   DVT_x = 0;
   DVT_y = 0;
   DVT_z = 0;
   DVT_r = 0;
   DVT_l = 0;
   DVT_m = 0;
   DVT_openAng = 0;
   DVT_parPDG = 0;
   DVT_parBarCode = 0;
   DVT_muIndexLead = 0;
   DVT_muIndexSub = 0;
   DVT_pLLP = 0;
   DVT_eLLP = 0;
   DVT_lLLP = 0;

   comb_pt = 0;
   comb_phi = 0;
   comb_eta = 0;
   comb_d0 = 0;
   comb_z0 = 0;
   comb_isoDPV = 0;
   comb_isoDAll = 0;
   comb_isoIPV = 0;
   comb_isoIAll = 0;
   comb_JMOR = 0;
   comb_author = 0;
   comb_chi2 = 0;
   comb_PDG = 0;
   comb_barcode = 0;

   trk_pt = 0;
   trk_phi = 0;
   trk_eta = 0;
   trk_d0 = 0;
   trk_z0 = 0;
   trk_m = 0;
   trk_charge = 0;
   trk_covd0 = 0;
   trk_covz0 = 0;
   trk_covPhi = 0;
   trk_covTheta = 0;
   trk_covP = 0;
   
   trk_PDG = 0;
   trk_barCode = 0;
   trk_parPDG = 0;
   trk_parBarCode = 0;
   
   trk_isoDPV = 0;
   trk_isoDAll = 0;
   trk_isoIPV = 0;
   trk_isoIAll = 0;
   
   trk_jetDR = 0;
   
   trk_nPrecLayers = 0;
   trk_nPhiLayers = 0;
   trk_nEtaLayers = 0;
   trk_vx = 0;
   trk_vy = 0;
   trk_vz = 0;
   DV_x = 0;
   DV_y = 0;
   DV_z = 0;
   DV_r = 0;
   DV_l = 0;
   DV_m = 0;
   DV_charge = 0;
   DV_openAng = 0;
   DV_distV = 0;
   DV_muIndexLead = 0;
   DV_muIndexSub = 0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("LB", &LB, &b_LB);
   fChain->SetBranchAddress("evtWeight", &evtWeight, &b_evtWeight);
   fChain->SetBranchAddress("sumWeights", &sumWeights, &b_sumWeights);
   fChain->SetBranchAddress("prw", &prw, &b_prw);
   fChain->SetBranchAddress("EtMiss", &EtMiss, &b_EtMiss);
   fChain->SetBranchAddress("EtMissPhi", &EtMissPhi, &b_EtMissPhi);
   fChain->SetBranchAddress("TPV_x", &TPV_x, &b_TPV_x);
   fChain->SetBranchAddress("TPV_y", &TPV_y, &b_TPV_y);
   fChain->SetBranchAddress("TPV_z", &TPV_z, &b_TPV_z);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
   fChain->SetBranchAddress("PV_ntrk", &PV_ntrk, &b_PV_ntrk);
   fChain->SetBranchAddress("PU_nvtx", &PU_nvtx, &b_PU_nvtx);
   fChain->SetBranchAddress("nMSOnly", &nMSOnly, &b_nMSOnly);
   fChain->SetBranchAddress("NDV", &NDV, &b_NDV);
   fChain->SetBranchAddress("NDVT", &NDVT, &b_NDVT);
   fChain->SetBranchAddress("narrowScanTrig", &narrowScanTrig, &b_narrowScanTrig);
   fChain->SetBranchAddress("METTrig", &METTrig, &b_METTrig);
   fChain->SetBranchAddress("singleMuTrig", &singleMuTrig, &b_singleMuTrig);
   fChain->SetBranchAddress("threeMuTrig", &threeMuTrig, &b_threeMuTrig);
   fChain->SetBranchAddress("L1_MU20Trig", &L1_MU20Trig, &b_L1_MU20Trig);
   fChain->SetBranchAddress("L1_3MU6Trig", &L1_3MU6Trig, &b_L1_3MU6Trig);
   fChain->SetBranchAddress("trigMHT", &trigMHT, &b_trigMHT);
   fChain->SetBranchAddress("trigMHTPhi", &trigMHTPhi, &b_trigMHTPhi);
   fChain->SetBranchAddress("MHT", &MHT, &b_MHT);
   fChain->SetBranchAddress("MHTPhi", &MHTPhi, &b_MHTPhi);
   
   fChain->SetBranchAddress("id_pt", &id_pt, &b_id_pt);
   fChain->SetBranchAddress("id_phi", &id_phi, &b_id_phi);
   fChain->SetBranchAddress("id_eta", &id_eta, &b_id_eta);
   fChain->SetBranchAddress("id_d0", &id_d0, &b_id_d0);
   fChain->SetBranchAddress("id_z0", &id_z0, &b_id_z0);
   fChain->SetBranchAddress("id_charge", &id_charge, &b_id_charge);
   fChain->SetBranchAddress("id_chi2", &id_chi2, &b_id_chi2);
   fChain->SetBranchAddress("id_dof", &id_dof, &b_id_dof);
   fChain->SetBranchAddress("id_vtxType", &id_vtxType, &b_id_vtxType);
   
   fChain->SetBranchAddress("trkt_pt", &trkt_pt, &b_trkt_pt);
   fChain->SetBranchAddress("trkt_phi", &trkt_phi, &b_trkt_phi);
   fChain->SetBranchAddress("trkt_eta", &trkt_eta, &b_trkt_eta);
   fChain->SetBranchAddress("trkt_d0", &trkt_d0, &b_trkt_d0);
   fChain->SetBranchAddress("trkt_z0", &trkt_z0, &b_trkt_z0);
   fChain->SetBranchAddress("trkt_charge", &trkt_charge, &b_trkt_charge);
   fChain->SetBranchAddress("trkt_PDG", &trkt_PDG, &b_trkt_PDG);
   fChain->SetBranchAddress("trkt_barCode", &trkt_barCode, &b_trkt_barCode);
   
   fChain->SetBranchAddress("trkt_status", &trkt_status, &b_trkt_status);
   
   fChain->SetBranchAddress("trkt_parPDG", &trkt_parPDG, &b_trkt_parPDG);
   fChain->SetBranchAddress("trkt_parBarCode", &trkt_parBarCode, &b_trkt_parBarCode);
   fChain->SetBranchAddress("DVT_x", &DVT_x, &b_DVT_x);
   fChain->SetBranchAddress("DVT_y", &DVT_y, &b_DVT_y);
   fChain->SetBranchAddress("DVT_z", &DVT_z, &b_DVT_z);
   fChain->SetBranchAddress("DVT_r", &DVT_r, &b_DVT_r);
   fChain->SetBranchAddress("DVT_l", &DVT_l, &b_DVT_l);
   fChain->SetBranchAddress("DVT_m", &DVT_m, &b_DVT_m);
   fChain->SetBranchAddress("DVT_openAng", &DVT_openAng, &b_DVT_openAng);
   fChain->SetBranchAddress("DVT_parPDG", &DVT_parPDG, &b_DVT_parPDG);
   fChain->SetBranchAddress("DVT_parBarCode", &DVT_parBarCode, &b_DVT_parBarCode);
   fChain->SetBranchAddress("DVT_muIndexLead", &DVT_muIndexLead, &b_DVT_muIndexLead);
   fChain->SetBranchAddress("DVT_muIndexSub", &DVT_muIndexSub, &b_DVT_muIndexSub);
   fChain->SetBranchAddress("DVT_pLLP", &DVT_pLLP, &b_DVT_pLLP);
   fChain->SetBranchAddress("DVT_eLLP", &DVT_eLLP, &b_DVT_eLLP);
   fChain->SetBranchAddress("DVT_lLLP", &DVT_lLLP, &b_DVT_lLLP);
   
   fChain->SetBranchAddress("comb_pt", &comb_pt, &b_comb_pt);
   fChain->SetBranchAddress("comb_phi", &comb_phi, &b_comb_phi);
   fChain->SetBranchAddress("comb_eta", &comb_eta, &b_comb_eta);
   fChain->SetBranchAddress("comb_d0", &comb_d0, &b_comb_d0);
   fChain->SetBranchAddress("comb_z0", &comb_z0, &b_comb_z0);
   fChain->SetBranchAddress("comb_isoDPV", &comb_isoDPV, &b_comb_isoDPV);
   fChain->SetBranchAddress("comb_isoDAll", &comb_isoDAll, &b_comb_isoDAll);
   fChain->SetBranchAddress("comb_isoIPV", &comb_isoIPV, &b_comb_isoIPV);
   fChain->SetBranchAddress("comb_isoIAll", &comb_isoIAll, &b_comb_isoIAll);
   fChain->SetBranchAddress("comb_JMOR", &comb_JMOR, &b_comb_JMOR);
   fChain->SetBranchAddress("comb_author", &comb_author, &b_comb_author);
   fChain->SetBranchAddress("comb_chi2", &comb_chi2, &b_comb_chi2);
   fChain->SetBranchAddress("comb_PDG", &comb_PDG, &b_comb_PDG);
   fChain->SetBranchAddress("comb_barcode", &comb_barcode, &b_comb_barcode);
   
   fChain->SetBranchAddress("trk_pt", &trk_pt, &b_trk_pt);
   fChain->SetBranchAddress("trk_phi", &trk_phi, &b_trk_phi);
   fChain->SetBranchAddress("trk_eta", &trk_eta, &b_trk_eta);
   fChain->SetBranchAddress("trk_d0", &trk_d0, &b_trk_d0);
   fChain->SetBranchAddress("trk_z0", &trk_z0, &b_trk_z0);
   fChain->SetBranchAddress("trk_m", &trk_m, &b_trk_m);
   fChain->SetBranchAddress("trk_charge", &trk_charge, &b_trk_charge);
   fChain->SetBranchAddress("trk_covd0", &trk_covd0, &b_trk_covd0);
   fChain->SetBranchAddress("trk_covz0", &trk_covz0, &b_trk_covz0);
   fChain->SetBranchAddress("trk_covPhi", &trk_covPhi, &b_trk_covPhi);
   fChain->SetBranchAddress("trk_covTheta", &trk_covTheta, &b_trk_covTheta);
   fChain->SetBranchAddress("trk_covP", &trk_covP, &b_trk_covP);
   
   fChain->SetBranchAddress("trk_isTM", &trk_isTM, &b_trk_isTM);
   fChain->SetBranchAddress("trk_PDG", &trk_PDG, &b_trk_PDG);
   fChain->SetBranchAddress("trk_barCode", &trk_barCode, &b_trk_barCode);
   fChain->SetBranchAddress("trk_parPDG", &trk_parPDG, &b_trk_parPDG);
   fChain->SetBranchAddress("trk_parBarCode", &trk_parBarCode, &b_trk_parBarCode);
   
   fChain->SetBranchAddress("trk_isoDPV", &trk_isoDPV, &b_trk_isoDPV);
   fChain->SetBranchAddress("trk_isoDAll", &trk_isoDAll, &b_trk_isoDAll);
   fChain->SetBranchAddress("trk_isoIPV", &trk_isoIPV, &b_trk_isoIPV);
   fChain->SetBranchAddress("trk_isoIAll", &trk_isoIAll, &b_trk_isoIAll);
   
   fChain->SetBranchAddress("trk_jetDR", &trk_jetDR, &b_trk_jetDR);
   
   fChain->SetBranchAddress("trk_nPrecLayers", &trk_nPrecLayers, &b_trk_nPrecLayers);
   fChain->SetBranchAddress("trk_nPhiLayers", &trk_nPhiLayers, &b_trk_nPhiLayers);
   fChain->SetBranchAddress("trk_nEtaLayers", &trk_nEtaLayers, &b_trk_nEtaLayers);
   fChain->SetBranchAddress("trk_vx", &trk_vx, &b_trk_vx);
   fChain->SetBranchAddress("trk_vy", &trk_vy, &b_trk_vy);
   fChain->SetBranchAddress("trk_vz", &trk_vz, &b_trk_vz);
   fChain->SetBranchAddress("DV_x", &DV_x, &b_DV_x);
   fChain->SetBranchAddress("DV_y", &DV_y, &b_DV_y);
   fChain->SetBranchAddress("DV_z", &DV_z, &b_DV_z);
   fChain->SetBranchAddress("DV_r", &DV_r, &b_DV_r);
   fChain->SetBranchAddress("DV_l", &DV_l, &b_DV_l);
   fChain->SetBranchAddress("DV_m", &DV_m, &b_DV_m);
   fChain->SetBranchAddress("DV_charge", &DV_charge, &b_DV_charge);
   fChain->SetBranchAddress("DV_openAng", &DV_openAng, &b_DV_openAng);
   fChain->SetBranchAddress("DV_distV", &DV_distV, &b_DV_distV);
   fChain->SetBranchAddress("DV_muIndexLead", &DV_muIndexLead, &b_DV_muIndexLead);
   fChain->SetBranchAddress("DV_muIndexSub", &DV_muIndexSub, &b_DV_muIndexSub);
   Notify();
}

Bool_t ntupleAnalysis::Notify()
{return kTRUE;}

void ntupleAnalysis::Show(Long64_t entry)
{
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntupleAnalysis::Cut(Long64_t entry)
{return 1;}
#endif // #ifdef ntupleAnalysis_cxx
