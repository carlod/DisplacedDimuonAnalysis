#ifndef plotManager_h
#define plotManager_h

#include <vector>
#include <string>
#include <iostream>
#include <THStack.h>
#include <TH1.h>
#include <TH2F.h>

enum PlotType { 
  r, 
  z, 
  l, 
  m, 
  numMSTP,
  numVtx,
  leadD0, 
  subD0,
  minD0,
  D0,
  leadZ0, 
  subZ0, 
  leadEta,
  subEta, 
  leadPhi,
  subPhi, 
  zoomPhi,
  leadPt,
  AleadPt,
  BleadPt,
  CleadPt,
  DleadPt,
  subPt,
  AsubPt,
  BsubPt,
  CsubPt,
  DsubPt,
  leadD0Sig,
  subD0Sig,
  leadD0SigZoom,
  subD0SigZoom,
  D0Res,
  D0Pull,
  leadZ0Sig,
  subZ0Sig,
  Z0Res,
  Z0Pull,
  leadThetaSig, 
  subThetaSig,
  ThetaRes,
  ThetaPull, 
  leadPhiSig,
  subPhiSig,
  PhiRes,
  PhiPull,
  leadPSig,
  subPSig,
  PSigoverP,
  PRes,
  PPull,
  leadDR, 
  subDR,
  minDR, 
  maxDR,
  mu60Trig,
  METTrig,
  threeMu6Trig,
  minD,
  deltaPhi,
  deltaPhiZoom,
  sumEta,
  cosmicDR,
  cosmicDRFull,
  vtxCharge,
  minTrkVy,
  leadIso,
  subIso,
  maxIso,
  leadJMOR,
  subJMOR,
  minJMOR,
  AminDR,
  BminDR,
  CminDR,
  DminDR,
  closestAuthor,
  nPrecLayers,
  nPhiLayers,
  nEtaLayers,
  vx,
  vy,
  vr,
  vz,
  PU_nvtx,
  RunNumber,
  dataPeriod,
  invMRes,
  invMSTACO,
  etaDR,
  zPVRes
  
};

enum PlotTypeVar {
  rVar,
  zVar,
  lVar,
  leadD0Var,
  subD0Var,
  minD0Var,
  leadZ0Var,
  subZ0Var,
  minZ0Var,
  mVar,
  leadDRVar,
  subDRVar,
  maxDRVar,
  minDRVar,
  DRPStaco,
  DRMStaco,
  DRPmuID,
  DRMmuID,
  DRPStaco_SS,
  DRMStaco_SS,
  DRPmuID_SS,
  DRMmuID_SS,
  etaDRVar,
  minIsoOS,
  minIsoSS,
  invMResVar,
  invMSTACOVar,
  
};

enum PlotType2D {
  QCD,
  etaSigma,
  D0Sig,
  Z0Sig,
  PhiSig,
  ThetaSig,
  PSig
};

enum PlotType2DVar {
  trigEtaPhiVar,
  AtrigEtaPhiVarL,
  BtrigEtaPhiVarL,
  CtrigEtaPhiVarL,
  DtrigEtaPhiVarL,
  AtrigEtaPhiVarS,
  BtrigEtaPhiVarS,
  CtrigEtaPhiVarS,
  DtrigEtaPhiVarS,
  DRPtVar,
  DRDRVar,
  DRDRSSVar,
  DRDRmuS,
  DRDRSmu,
  DRDRSS,
  DRDRmumu_SS,
  DRDRmuS_SS,
  DRDRSmu_SS,
  DRDRSS_SS,
  DRDRmumu,
  isoCompVar
};

class plotInfo {

  public:
  
  plotInfo(std::string _name,std::string _axisName,int _nBins,float _minBin,float _maxBin,int _isLog) : 
  name(_name),axisName(_axisName),nBins(_nBins),minBin(_minBin),maxBin(_maxBin),isLog(_isLog) {}

  std::string name;
  std::string axisName;
  int nBins;
  float minBin;
  float maxBin;
  int isLog;

};

class plotInfoVar {

  public:
  
  plotInfoVar(std::string _name,std::string _axisName,int _nBins,Float_t * _binsX,int _isLog) : 
  name(_name),axisName(_axisName),nBins(_nBins),isLog(_isLog) {
    binsX = new Float_t[_nBins+1];
    for (unsigned int j=0;j<_nBins+1;j++){binsX[j] = _binsX[j];}
  }

  std::string name;
  std::string axisName;
  int nBins;
  Float_t * binsX;
  int isLog;
  
  

};

class plotInfo2D {

  public:
  
  plotInfo2D(std::string _name,std::string _axisNameX,std::string _axisNameY,int _nBinsX,float _minBinX,float _maxBinX,int _nBinsY,float _minBinY,float _maxBinY,int _isLog) : 
  name(_name),axisNameX(_axisNameX),axisNameY(_axisNameY),nBinsX(_nBinsX),minBinX(_minBinX),maxBinX(_maxBinX),nBinsY(_nBinsY),minBinY(_minBinY),maxBinY(_maxBinY),isLog(_isLog) {}

  std::string name;
  std::string axisNameX;
  std::string axisNameY;
  int nBinsX;
  float minBinX;
  float maxBinX;
  int nBinsY;
  float minBinY;
  float maxBinY;
  int isLog;

};

class plotInfo2DVar {

  public: 
  
  plotInfo2DVar(std::string _name,std::string _axisNameX,std::string _axisNameY,int _nBinsX,Double_t * _binsX,int _nBinsY,Double_t * _binsY,int _isLog) : 
  name(_name),axisNameX(_axisNameX),axisNameY(_axisNameY),nBinsX(_nBinsX),nBinsY(_nBinsY),minBinY(-1.),maxBinY(-1.),isLog(_isLog) {
    binsX = new Double_t[_nBinsX+1];
    for (unsigned int j=0;j<_nBinsX+1;j++){binsX[j] = _binsX[j];}
    binsY = new Double_t[_nBinsY+1];
    for (unsigned int j=0;j<_nBinsY+1;j++){binsY[j] = _binsY[j];}
  }
  
  plotInfo2DVar(std::string _name,std::string _axisNameX,std::string _axisNameY,int _nBinsX,Double_t * _binsX,int _nBinsY,float _minBinY,float _maxBinY,int _isLog) : 
  name(_name),axisNameX(_axisNameX),axisNameY(_axisNameY),nBinsX(_nBinsX),nBinsY(_nBinsY),binsY(NULL),minBinY(_minBinY),maxBinY(_maxBinY),isLog(_isLog) {
    binsX = new Double_t[_nBinsX+1];
    for (unsigned int j=0;j<_nBinsX+1;j++){binsX[j] = _binsX[j];}
  }
  
  std::string name;
  std::string axisNameX;
  std::string axisNameY;
  int nBinsX;
  Double_t * binsX;
  int nBinsY;
  Double_t * binsY;
  float minBinY;
  float maxBinY;
  int isLog;

};

class plotManager {

  public :
  //member variables
  std::vector<plotInfo> m_plotInfoVec;
  std::vector<plotInfoVar> m_plotInfoVecVar;
  std::vector<plotInfo2D> m_plotInfoVec2D;
  std::vector<plotInfo2DVar> m_plotInfoVec2DVar;
  int m_numPlotsVar;
  int m_numPlots;
  int m_numPlots2D;
  int m_numPlots2DVar;
  
  //methods
  plotManager(bool doSR,bool doYS);
  void initHistVec(std::vector<TH1F*>& histoVec,int runNum,std::string theRegion,bool isNW);
  void initStackVec(std::vector<THStack*>& stackVec,std::string stackName);
  void initHistVecVar(std::vector<TH1F*>& histoVec,int runNum,std::string theRegion,bool isNW);
  void initStackVecVar(std::vector<THStack*>& stackVec,std::string stackName);
  void initHistVec2D(std::vector<TH2F*>& histoVec2D,int runNum,std::string theRegion,bool isNW);
  void initHistVec2DVar(std::vector<TH2F*>& histoVec2D,int runNum,std::string theRegion,bool isNW);
  void initStackVec2D(std::vector<TH2F*>& stackVec2D,std::string stackName);
  void initStackVec2DVar(std::vector<TH2F*>& stackVec2D,std::string stackName);
  void initErrVec(std::vector<TH1F*>& histoVecErr,std::string histName,int runNum);
  void initErrVecVar(std::vector<TH1F*>& histoVecErr,std::string histName,int runNum);
  void initLogVec(std::vector<int>& theLogs,std::vector<int>& theLogsVar);

};

#endif 
