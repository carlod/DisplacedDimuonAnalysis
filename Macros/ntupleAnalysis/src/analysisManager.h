#ifndef analysisManager_h
#define analysisManager_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include "plotManager.h"

class analysisManager {

  public :
  
  //member variables
  
  //1D background stacks
  std::vector<THStack*> m_theStacks;
  std::vector<THStack*> m_theStacksVar;
  
  //2D background "stacks"
  std::vector<TH2F*> m_theStacks2D;
  std::vector<TH2F*> m_theStacks2DVar;
  
  //1D signal 
  std::vector<std::vector<TH1F*>> m_sampleVec;
  std::vector<std::vector<TH1F*>> m_sampleVecNW;
  std::vector<std::vector<TH1F*>> m_sampleVecVar;
  std::vector<std::vector<TH1F*>> m_sampleVecVarNW;
  
  //2D signal
  std::vector<std::vector<TH2F*>> m_sampleVec2D;
  std::vector<std::vector<TH2F*>> m_sampleVec2DNW;
  std::vector<std::vector<TH2F*>> m_sampleVec2DVar;
  std::vector<std::vector<TH2F*>> m_sampleVec2DVarNW;
  
  std::vector<float> m_theSFs;
  std::vector<float> m_theLumi;
  std::vector<std::string> m_theSamples;
  std::vector<float> m_theColors;
  
  std::vector<float> m_sigSFs;
  std::vector<float> m_sigLumi;
  std::vector<float> m_sigColors;
  std::vector<std::string> m_sigSamples;
  
  std::vector<int> m_theLogs;
  std::vector<int> m_theLogsVar;
  int m_totalDYnum; 
  int m_totalWnum; 
  int m_totalDBnum; 
  int m_totalUnum; 
  int m_totalSTnum;
  int m_totalJZnum; 
  int m_bkgdSize;
  int m_sigSize;
  
  plotManager* m_pMan;

  //methods
  analysisManager(plotManager* pMan,float intLumi);
  void createStack(std::string stackName);
  std::vector<TH1F*> makeErrorHist(std::string histName,int runNum);
  std::vector<TH1F*> makeErrorHistVar(std::string histName,int runNum);
  void fillStackandPrint(std::string regionName,std::vector<TH1F*>& histoVecErr,std::vector<TH1F*>& histoVecErrVar);
  void drawAndSavePlots(std::string regionName, std::vector<TH1F*> dataVec,std::vector<TH1F*> dataVecVar,std::vector<std::vector<TH1F*>> signalVec,std::vector<std::vector<TH1F*>> signalVecVar, std::vector<TH1F*> histoVecErr,std::vector<TH1F*> histoVecErrVar,std::string outputDir);
  void makeSFMap(std::vector<TH1F*> dataVec);

};

#endif 
