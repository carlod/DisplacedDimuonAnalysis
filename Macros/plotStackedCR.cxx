#include "TString.h"
#include "TChain.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "RooRealVar.h"
#include "RooDataHist.h"
#include "RooPlot.h"
#include "RooTreeData.h"
#include "RooLinkedList.h"

void plotStackedCR() {

// Read in the ntuple(s).  You'll need to put in the proper paths and filenames for the four MC backgrounds and data ntuples
  TChain ZmumuTree("tree");
  ZmumuTree.Add("Zmumu2-20/data-myOutput/AOD.root");

  TChain ttbarTree("tree");
  ttbarTree.Add("ttbar2-19/data-myOutput/AOD.root");

  TChain ZZTree("tree");
  ZZTree.Add("ZZ-18batch/data-myOutput/AOD.root");

  TChain WallTree("tree");
  WallTree.Add("Wall-19/data-myOutput/AOD.root");

  TChain DataTree("tree");
  DataTree.Add("PeriodG-21/dataPeriodG.merge.NTUP.root");

  // Output histos
  TFile* outputHistos = new TFile("StackedHistos.root", "new");

  // Scale to 0.869 fb-1.  Change this to whatever the data lumi. is.
  double SFZmumu = 0.869/10.2237875;
  double SFttbar = 0.869/109.427;
  double SFZZ = 0.869/230.991;
  double SFWall = 0.869/(2.43036+2.3273);

  cout<<"Z->mumu SF: "<<SFZmumu<<endl;
  cout<<"ttbar   SF: "<<SFttbar<<endl;
  cout<<"ZZ->4mu SF: "<<SFZZ<<endl;
  cout<<"W->munu SF: "<<SFWall<<endl;
  cout<<endl;
  
  // ControlRegion, SignalRegion and Truth-Match cuts
  //TCut TruthMatch = "DV_parBarCode > 0";
  TCut TruthMatch = "fabs(DV_muPDG1) == 13 && fabs(DV_muPDG2) == 13";
  TCut MatchToTruth = "DV_parBarCode == DVT_parBarCode";

  TCut Base = "(METTrig || MuTrig || HLT3Mu6Trig || J80Met80Trig || Mu20Mu6Trig) && DV_openAng > 0.01 && DV_m > 10.0 && DV_m < 2000.0 && fabs(DV_r) < 400. && fabs(DV_z) < 600. && fabs(DV_muEta1) < 2.5 && fabs(DV_muEta2) < 2.5 && DV_charge == 0 && DV_distV < 100.0";
  TCut ControlRegion =  Base&&"DV_muDR1 > 0.02 && DV_muDR2 > 0.02 && (DV_muDR1 < 0.05 || DV_muDR2 < 0.05)";
  TCut ControlRegionTM = ControlRegion&&TruthMatch;
  TCut SignalRegion = Base&&"DV_muDR1 > 0.05 && DV_muDR2 > 0.05";

  // Make 1D stack histos:  CR

  THStack* rStackCR = new THStack("rStackCR","rStackCR;r_{vtx} [cm];Events");

  THStack* zStackCR = new THStack("zStackCR","zStackCR;z_{vtx} [cm];Events");

  THStack* lStackCR = new THStack("lStackCR","lStackCR;L_{vtx} [cm];Events");

  THStack* mStackCR = new THStack("mStackCR","mStackCR;m_{vtx} [GeV];Events");

  THStack* muPtStackCR = new THStack("muPtStackCR","muPtStackCR;#mu p_{T} [GeV];Events");

  THStack* muD0StackCR = new THStack("muD0StackCR","muD0StackCR;#mu d_{0} [cm];Events");

  THStack* muZ0StackCR = new THStack("muZ0StackCR","muZ0StackCR;#mu z_{0} [cm];Events");

  THStack* muEtaStackCR = new THStack("muEtaStackCR","muEtaStackCR;#mu #eta;Events");

  // Make 1D stack histos:  SR

  THStack* rStackSR = new THStack("rStackSR","rStackSR;r_{vtx} [cm];Events");

  THStack* zStackSR = new THStack("zStackSR","zStackSR;z_{vtx} [cm];Events");

  THStack* lStackSR = new THStack("lStackSR","lStackSR;L_{vtx} [cm];Events");

  THStack* mStackSR = new THStack("mStackSR","mStackSR;m_{vtx} [GeV];Events");

  THStack* muPtStackSR = new THStack("muPtStackSR","muPtStackSR;#mu p_{T} [GeV];Events");

  THStack* muD0StackSR = new THStack("muD0StackSR","muD0StackSR;#mu d_{0} [cm];Events");

  THStack* muZ0StackSR = new THStack("muZ0StackSR","muZ0StackSR;#mu z_{0} [cm];Events");

  THStack* muEtaStackSR = new THStack("muEtaStackSR","muEtaStackSR;#mu #eta;Events");

  cout<<"Defined all stacked histos"<<endl;

  // Make each 1D histo and fill

  // W+jets
  TH1F* WallrCR = new TH1F("WallrCR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  WallrCR->SetFillColor(kGreen);
  WallTree.Draw("DV_r>>WallrCR", ControlRegion);
  //WallrCR->Sumw2();
  WallrCR->Scale(SFWall);
  rStackCR->Add(WallrCR);

  TH1F* WallzCR = new TH1F("WallzCR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  WallTree.Draw("DV_z>>WallzCR", ControlRegion);
  //WallzCR->Sumw2();
  WallzCR->Scale(SFWall);
  WallzCR->SetFillColor(kGreen);
  zStackCR->Add(WallzCR);

  TH1F* WalllCR = new TH1F("WalllCR", "l DV [cm]", 100, 0.0, 600.0);
  WallTree.Draw("DV_l>>WalllCR", ControlRegion);
  //WalllCR->Sumw2();
  WalllCR->Scale(SFWall);
  WalllCR->SetFillColor(kGreen);
  lStackCR->Add(WalllCR);

  TH1F* WallmCR = new TH1F("WallmCR", "m [GeV]", 100, 0.0, 200.0);
  WallTree.Draw("DV_m>>WallmCR", ControlRegion);
  //WallmCR->Sumw2();
  WallmCR->Scale(SFWall);
  WallmCR->SetFillColor(kGreen);
  mStackCR->Add(WallmCR);

  TH1F* WallmuPtCR = new TH1F("WallmuPtCR", "WallmuPtCR", 100, 0., 200.0);
  WallTree.Draw("DV_muPt1>>WallmuPtCR", ControlRegion);
  WallTree.Draw("DV_muPt2>>+WallmuPtCR", ControlRegion);
  //WallmuPtCR->Sumw2();
  WallmuPtCR->Scale(SFWall);
  WallmuPtCR->SetFillColor(kGreen);
  muPtStackCR->Add(WallmuPtCR);

  TH1F* WallmuD0CR = new TH1F("WallmuD0CR", "WallmuD0CR", 100, 0., 400.);
  WallTree.Draw("DV_muD01>>WallmuD0CR", ControlRegion);
  WallTree.Draw("DV_muD02>>+WallmuD0CR", ControlRegion);
  //WallmuD0CR->Sumw2();
  WallmuD0CR->Scale(SFWall);
  WallmuD0CR->SetFillColor(kGreen);
  muD0StackCR->Add(WallmuD0CR);

  TH1F* WallmuZ0CR = new TH1F("WallmuZ0CR", "WallmuZ0CR", 100, 0., 400.);
  WallTree.Draw("DV_muZ01>>WallmuZ0CR", ControlRegion);
  WallTree.Draw("DV_muZ02>>+WallmuZ0CR", ControlRegion);
  //WallmuZ0CR->Sumw2();
  WallmuZ0CR->Scale(SFWall);
  WallmuZ0CR->SetFillColor(kGreen);
  muZ0StackCR->Add(WallmuZ0CR);

  TH1F* WallmuEtaCR = new TH1F("WallmuEtaCR", "WallmuEtaCR", 50, -2.5, 2.5);
  WallTree.Draw("DV_muEta1>>WallmuEtaCR", ControlRegion);
  WallTree.Draw("DV_muEta2>>+WallmuEtaCR", ControlRegion);
  //WallmuEtaCR->Sumw2();
  WallmuEtaCR->Scale(SFWall);
  WallmuEtaCR->SetFillColor(kGreen);
  muEtaStackCR->Add(WallmuEtaCR);

  TH1F* WallrSR = new TH1F("WallrSR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  WallTree.Draw("DV_r>>WallrSR", SignalRegion);
  //WallrSR->Sumw2();
  WallrSR->Scale(SFWall);
  WallrSR->SetFillColor(kGreen);
  rStackSR->Add(WallrSR);

  TH1F* WallzSR = new TH1F("WallzSR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  WallTree.Draw("DV_z>>WallzSR", SignalRegion);
  //WallzSR->Sumw2();
  WallzSR->Scale(SFWall);
  WallzSR->SetFillColor(kGreen);
  zStackSR->Add(WallzSR);

  TH1F* WalllSR = new TH1F("WalllSR", "l DV [cm]", 100, 0.0, 600.0);
  WallTree.Draw("DV_l>>WalllSR", SignalRegion);
  //WalllSR->Sumw2();
  WalllSR->Scale(SFWall);
  WalllSR->SetFillColor(kGreen);
  lStackSR->Add(WalllSR);

  TH1F* WallmSR = new TH1F("WallmSR", "m [GeV]", 50, 0.0, 200.0);
  WallTree.Draw("DV_m>>WallmSR", SignalRegion);
  //WallmSR->Sumw2();
  WallmSR->Scale(SFWall);
  WallmSR->SetFillColor(kGreen);
  mStackSR->Add(WallmSR);

  TH1F* WallmuPtSR = new TH1F("WallmuPtSR", "WallmuPtSR", 100, 0., 200.0);
  WallTree.Draw("DV_muPt1>>WallmuPtSR", SignalRegion);
  WallTree.Draw("DV_muPt2>>+WallmuPtSR", SignalRegion);
  //WallmuPtSR->Sumw2();
  WallmuPtSR->Scale(SFWall);
  WallmuPtSR->SetFillColor(kGreen);
  muPtStackSR->Add(WallmuPtSR);

  TH1F* WallmuD0SR = new TH1F("WallmuD0SR", "WallmuD0SR", 100, 0., 400.);
  WallTree.Draw("DV_muD01>>WallmuD0SR", SignalRegion);
  WallTree.Draw("DV_muD02>>+WallmuD0SR", SignalRegion);
  //WallmuD0SR->Sumw2();
  WallmuD0SR->Scale(SFWall);
  WallmuD0SR->SetFillColor(kGreen);
  muD0StackSR->Add(WallmuD0SR);

  TH1F* WallmuZ0SR = new TH1F("WallmuZ0SR", "WallmuZ0SR", 100, 0., 400.);
  WallTree.Draw("DV_muZ01>>WallmuZ0SR", SignalRegion);
  WallTree.Draw("DV_muZ02>>+WallmuZ0SR", SignalRegion);
  //WallmuZ0SR->Sumw2();
  WallmuZ0SR->Scale(SFWall);
  WallmuZ0SR->SetFillColor(kGreen);
  muZ0StackSR->Add(WallmuZ0SR);

  TH1F* WallmuEtaSR = new TH1F("WallmuEtaSR", "WallmuEtaSR", 50, -2.5, 2.5);
  WallTree.Draw("DV_muEta1>>WallmuEtaSR", SignalRegion);
  WallTree.Draw("DV_muEta2>>+WallmuEtaSR", SignalRegion);
  //WallmuEtaSR->Sumw2();
  WallmuEtaSR->Scale(SFWall);
  WallmuEtaSR->SetFillColor(kGreen);
  muEtaStackSR->Add(WallmuEtaSR);

  cout<<"W histos done"<<endl;

  // ZZ
  TH1F* ZZrCR = new TH1F("ZZrCR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  ZZrCR->SetFillColor(kYellow);
  ZZTree.Draw("DV_r>>ZZrCR", ControlRegion);
  //ZZrCR->Sumw2();
  ZZrCR->Scale(SFZZ);
  rStackCR->Add(ZZrCR);

  TH1F* ZZzCR = new TH1F("ZZzCR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  ZZTree.Draw("DV_z>>ZZzCR", ControlRegion);
  //ZZzCR->Sumw2();
  ZZzCR->Scale(SFZZ);
  ZZzCR->SetFillColor(kYellow);
  zStackCR->Add(ZZzCR);

  TH1F* ZZlCR = new TH1F("ZZlCR", "l DV [cm]", 100, 0.0, 600.0);
  ZZTree.Draw("DV_l>>ZZlCR", ControlRegion);
  //ZZlCR->Sumw2();
  ZZlCR->Scale(SFZZ);
  ZZlCR->SetFillColor(kYellow);
  lStackCR->Add(ZZlCR);

  TH1F* ZZmCR = new TH1F("ZZmCR", "m [GeV]", 100, 0.0, 200.0);
  ZZTree.Draw("DV_m>>ZZmCR", ControlRegion);
  //ZZmCR->Sumw2();
  ZZmCR->Scale(SFZZ);
  ZZmCR->SetFillColor(kYellow);
  mStackCR->Add(ZZmCR);

  TH1F* ZZmuPtCR = new TH1F("ZZmuPtCR", "ZZmuPtCR", 100, 0., 200.0);
  ZZTree.Draw("DV_muPt1>>ZZmuPtCR", ControlRegion);
  ZZTree.Draw("DV_muPt2>>+ZZmuPtCR", ControlRegion);
  //ZZmuPtCR->Sumw2();
  ZZmuPtCR->Scale(SFZZ);
  ZZmuPtCR->SetFillColor(kYellow);
  muPtStackCR->Add(ZZmuPtCR);

  TH1F* ZZmuD0CR = new TH1F("ZZmuD0CR", "ZZmuD0CR", 100, 0., 400.);
  ZZTree.Draw("DV_muD01>>ZZmuD0CR", ControlRegion);
  ZZTree.Draw("DV_muD02>>+ZZmuD0CR", ControlRegion);
  //ZZmuD0CR->Sumw2();
  ZZmuD0CR->Scale(SFZZ);
  ZZmuD0CR->SetFillColor(kYellow);
  muD0StackCR->Add(ZZmuD0CR);

  TH1F* ZZmuZ0CR = new TH1F("ZZmuZ0CR", "ZZmuZ0CR", 100, 0., 400.);
  ZZTree.Draw("DV_muZ01>>ZZmuZ0CR", ControlRegion);
  ZZTree.Draw("DV_muZ02>>+ZZmuZ0CR", ControlRegion);
  //ZZmuZ0CR->Sumw2();
  ZZmuZ0CR->Scale(SFZZ);
  ZZmuZ0CR->SetFillColor(kYellow);
  muZ0StackCR->Add(ZZmuZ0CR);

  TH1F* ZZmuEtaCR = new TH1F("ZZmuEtaCR", "ZZmuEtaCR", 50, -2.5, 2.5);
  ZZTree.Draw("DV_muEta1>>ZZmuEtaCR", ControlRegion);
  ZZTree.Draw("DV_muEta2>>+ZZmuEtaCR", ControlRegion);
  //ZZmuEtaCR->Sumw2();
  ZZmuEtaCR->Scale(SFZZ);
  ZZmuEtaCR->SetFillColor(kYellow);
  muEtaStackCR->Add(ZZmuEtaCR);

  TH1F* ZZrSR = new TH1F("ZZrSR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  ZZTree.Draw("DV_r>>ZZrSR", SignalRegion);
  //ZZrSR->Sumw2();
  ZZrSR->Scale(SFZZ);
  ZZrSR->SetFillColor(kYellow);
  rStackSR->Add(ZZrSR);

  TH1F* ZZzSR = new TH1F("ZZzSR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  ZZTree.Draw("DV_z>>ZZzSR", SignalRegion);
  //ZZzSR->Sumw2();
  ZZzSR->Scale(SFZZ);
  ZZzSR->SetFillColor(kYellow);
  zStackSR->Add(ZZzSR);

  TH1F* ZZlSR = new TH1F("ZZlSR", "l DV [cm]", 100, 0.0, 600.0);
  ZZTree.Draw("DV_l>>ZZlSR", SignalRegion);
  //ZZlSR->Sumw2();
  ZZlSR->Scale(SFZZ);
  ZZlSR->SetFillColor(kYellow);
  lStackSR->Add(ZZlSR);

  TH1F* ZZmSR = new TH1F("ZZmSR", "m [GeV]", 50, 0.0, 200.0);
  ZZTree.Draw("DV_m>>ZZmSR", SignalRegion);
  //ZZmSR->Sumw2();
  ZZmSR->Scale(SFZZ);
  ZZmSR->SetFillColor(kYellow);
  mStackSR->Add(ZZmSR);

  TH1F* ZZmuPtSR = new TH1F("ZZmuPtSR", "ZZmuPtSR", 100, 0., 200.0);
  ZZTree.Draw("DV_muPt1>>ZZmuPtSR", SignalRegion);
  ZZTree.Draw("DV_muPt2>>+ZZmuPtSR", SignalRegion);
  //ZZmuPtSR->Sumw2();
  ZZmuPtSR->Scale(SFZZ);
  ZZmuPtSR->SetFillColor(kYellow);
  muPtStackSR->Add(ZZmuPtSR);

  TH1F* ZZmuD0SR = new TH1F("ZZmuD0SR", "ZZmuD0SR", 100, 0., 400.);
  ZZTree.Draw("DV_muD01>>ZZmuD0SR", SignalRegion);
  ZZTree.Draw("DV_muD02>>+ZZmuD0SR", SignalRegion);
  //ZZmuD0SR->Sumw2();
  ZZmuD0SR->Scale(SFZZ);
  ZZmuD0SR->SetFillColor(kYellow);
  muD0StackSR->Add(ZZmuD0SR);

  TH1F* ZZmuZ0SR = new TH1F("ZZmuZ0SR", "ZZmuZ0SR", 100, 0., 400.);
  ZZTree.Draw("DV_muZ01>>ZZmuZ0SR", SignalRegion);
  ZZTree.Draw("DV_muZ02>>+ZZmuZ0SR", SignalRegion);
  //ZZmuZ0SR->Sumw2();
  ZZmuZ0SR->Scale(SFZZ);
  ZZmuZ0SR->SetFillColor(kYellow);
  muZ0StackSR->Add(ZZmuZ0SR);

  TH1F* ZZmuEtaSR = new TH1F("ZZmuEtaSR", "ZZmuEtaSR", 50, -2.5, 2.5);
  ZZTree.Draw("DV_muEta1>>ZZmuEtaSR", SignalRegion);
  ZZTree.Draw("DV_muEta2>>+ZZmuEtaSR", SignalRegion);
  //ZZmuEtaSR->Sumw2();
  ZZmuEtaSR->Scale(SFZZ);
  ZZmuEtaSR->SetFillColor(kYellow);
  muEtaStackSR->Add(ZZmuEtaSR);

  cout<<"ZZ histos done"<<endl;

  // ttbar
  TH1F* ttbarrCR = new TH1F("ttbarrCR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  ttbarrCR->SetFillColor(kBlue);
  ttbarTree.Draw("DV_r>>ttbarrCR", ControlRegion);
  //ttbarrCR->Sumw2();
  ttbarrCR->Scale(SFttbar);
  rStackCR->Add(ttbarrCR);

  TH1F* ttbarzCR = new TH1F("ttbarzCR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  ttbarTree.Draw("DV_z>>ttbarzCR", ControlRegion);
  //ttbarzCR->Sumw2();
  ttbarzCR->Scale(SFttbar);
  ttbarzCR->SetFillColor(kBlue);
  zStackCR->Add(ttbarzCR);

  TH1F* ttbarlCR = new TH1F("ttbarlCR", "l DV [cm]", 100, 0.0, 600.0);
  ttbarTree.Draw("DV_l>>ttbarlCR", ControlRegion);
  //ttbarlCR->Sumw2();
  ttbarlCR->Scale(SFttbar);
  ttbarlCR->SetFillColor(kBlue);
  lStackCR->Add(ttbarlCR);

  TH1F* ttbarmCR = new TH1F("ttbarmCR", "m [GeV]", 100, 0.0, 200.0);
  ttbarTree.Draw("DV_m>>ttbarmCR", ControlRegion);
  //ttbarmCR->Sumw2();
  ttbarmCR->Scale(SFttbar);
  ttbarmCR->SetFillColor(kBlue);
  mStackCR->Add(ttbarmCR);

  TH1F* ttbarmuPtCR = new TH1F("ttbarmuPtCR", "ttbarmuPtCR", 100, 0., 200.0);
  ttbarTree.Draw("DV_muPt1>>ttbarmuPtCR", ControlRegion);
  ttbarTree.Draw("DV_muPt2>>+ttbarmuPtCR", ControlRegion);
  //ttbarmuPtCR->Sumw2();
  ttbarmuPtCR->Scale(SFttbar);
  ttbarmuPtCR->SetFillColor(kBlue);
  muPtStackCR->Add(ttbarmuPtCR);

  TH1F* ttbarmuD0CR = new TH1F("ttbarmuD0CR", "ttbarmuD0CR", 100, 0., 400.);
  ttbarTree.Draw("DV_muD01>>ttbarmuD0CR", ControlRegion);
  ttbarTree.Draw("DV_muD02>>+ttbarmuD0CR", ControlRegion);
  //ttbarmuD0CR->Sumw2();
  ttbarmuD0CR->Scale(SFttbar);
  ttbarmuD0CR->SetFillColor(kBlue);
  muD0StackCR->Add(ttbarmuD0CR);

  TH1F* ttbarmuZ0CR = new TH1F("ttbarmuZ0CR", "ttbarmuZ0CR", 100, 0., 400.);
  ttbarTree.Draw("DV_muZ01>>ttbarmuZ0CR", ControlRegion);
  ttbarTree.Draw("DV_muZ02>>+ttbarmuZ0CR", ControlRegion);
  //ttbarmuZ0CR->Sumw2();
  ttbarmuZ0CR->Scale(SFttbar);
  ttbarmuZ0CR->SetFillColor(kBlue);
  muZ0StackCR->Add(ttbarmuZ0CR);

  TH1F* ttbarmuEtaCR = new TH1F("ttbarmuEtaCR", "ttbarmuEtaCR", 50, -2.5, 2.5);
  ttbarTree.Draw("DV_muEta1>>ttbarmuEtaCR", ControlRegion);
  ttbarTree.Draw("DV_muEta2>>+ttbarmuEtaCR", ControlRegion);
  //ttbarmuEtaCR->Sumw2();
  ttbarmuEtaCR->Scale(SFttbar);
  ttbarmuEtaCR->SetFillColor(kBlue);
  muEtaStackCR->Add(ttbarmuEtaCR);

  cout<<"Done with ttbar CR"<<endl;
  cout<<endl;

  TH1F* ttbarrSR = new TH1F("ttbarrSR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  ttbarTree.Draw("DV_r>>ttbarrSR", SignalRegion);
  //ttbarrSR->Sumw2();
  ttbarrSR->Scale(SFttbar);
  ttbarrSR->SetFillColor(kBlue);
  rStackSR->Add(ttbarrSR);

  TH1F* ttbarzSR = new TH1F("ttbarzSR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  ttbarTree.Draw("DV_z>>ttbarzSR", SignalRegion);
  //ttbarzSR->Sumw2();
  ttbarzSR->Scale(SFttbar);
  ttbarzSR->SetFillColor(kBlue);
  zStackSR->Add(ttbarzSR);

  TH1F* ttbarlSR = new TH1F("ttbarlSR", "l DV [cm]", 100, 0.0, 600.0);
  ttbarTree.Draw("DV_l>>ttbarlSR", SignalRegion);
  //ttbarlSR->Sumw2();
  ttbarlSR->Scale(SFttbar);
  ttbarlSR->SetFillColor(kBlue);
  lStackSR->Add(ttbarlSR);

  TH1F* ttbarmSR = new TH1F("ttbarmSR", "m [GeV]", 50, 0.0, 200.0);
  ttbarTree.Draw("DV_m>>ttbarmSR", SignalRegion);
  //ttbarmSR->Sumw2();
  ttbarmSR->Scale(SFttbar);
  ttbarmSR->SetFillColor(kBlue);
  mStackSR->Add(ttbarmSR);

  TH1F* ttbarmuPtSR = new TH1F("ttbarmuPtSR", "ttbarmuPtSR", 100, 0., 200.0);
  ttbarTree.Draw("DV_muPt1>>ttbarmuPtSR", SignalRegion);
  ttbarTree.Draw("DV_muPt2>>+ttbarmuPtSR", SignalRegion);
  //ttbarmuPtSR->Sumw2();
  ttbarmuPtSR->Scale(SFttbar);
  ttbarmuPtSR->SetFillColor(kBlue);
  muPtStackSR->Add(ttbarmuPtSR);

  TH1F* ttbarmuD0SR = new TH1F("ttbarmuD0SR", "ttbarmuD0SR", 100, 0., 400.);
  ttbarTree.Draw("DV_muD01>>ttbarmuD0SR", SignalRegion);
  ttbarTree.Draw("DV_muD02>>+ttbarmuD0SR", SignalRegion);
  //ttbarmuD0SR->Sumw2();
  ttbarmuD0SR->Scale(SFttbar);
  ttbarmuD0SR->SetFillColor(kBlue);
  muD0StackSR->Add(ttbarmuD0SR);

  TH1F* ttbarmuZ0SR = new TH1F("ttbarmuZ0SR", "ttbarmuZ0SR", 100, 0., 400.);
  ttbarTree.Draw("DV_muZ01>>ttbarmuZ0SR", SignalRegion);
  ttbarTree.Draw("DV_muZ02>>+ttbarmuZ0SR", SignalRegion);
  //ttbarmuZ0SR->Sumw2();
  ttbarmuZ0SR->Scale(SFttbar);
  ttbarmuZ0SR->SetFillColor(kBlue);
  muZ0StackSR->Add(ttbarmuZ0SR);

  TH1F* ttbarmuEtaSR = new TH1F("ttbarmuEtaSR", "ttbarmuEtaSR", 50, -2.5, 2.5);
  ttbarTree.Draw("DV_muEta1>>ttbarmuEtaSR", SignalRegion);
  ttbarTree.Draw("DV_muEta2>>+ttbarmuEtaSR", SignalRegion);
  //ttbarmuEtaSR->Sumw2();
  ttbarmuEtaSR->Scale(SFttbar);
  ttbarmuEtaSR->SetFillColor(kBlue);
  muEtaStackSR->Add(ttbarmuEtaSR);

  cout<<"ttbar histos done"<<endl;

  // Z+jets
  TH1F* ZmumurCR = new TH1F("ZmumurCR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  ZmumurCR->SetFillColor(kRed);
  ZmumuTree.Draw("DV_r>>ZmumurCR", ControlRegion);
  //ZmumurCR->Sumw2();
  ZmumurCR->Scale(SFZmumu);
  rStackCR->Add(ZmumurCR);

  TH1F* ZmumuzCR = new TH1F("ZmumuzCR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  ZmumuTree.Draw("DV_z>>ZmumuzCR", ControlRegion);
  //ZmumuzCR->Sumw2();
  ZmumuzCR->Scale(SFZmumu);
  ZmumuzCR->SetFillColor(kRed);
  zStackCR->Add(ZmumuzCR);

  TH1F* ZmumulCR = new TH1F("ZmumulCR", "l DV [cm]", 100, 0.0, 600.0);
  ZmumuTree.Draw("DV_l>>ZmumulCR", ControlRegion);
  //ZmumulCR->Sumw2();
  ZmumulCR->Scale(SFZmumu);
  ZmumulCR->SetFillColor(kRed);
  lStackCR->Add(ZmumulCR);

  TH1F* ZmumumCR = new TH1F("ZmumumCR", "m [GeV]", 100, 0.0, 200.0);
  ZmumuTree.Draw("DV_m>>ZmumumCR", ControlRegion);
  //ZmumumCR->Sumw2();
  ZmumumCR->Scale(SFZmumu);
  ZmumumCR->SetFillColor(kRed);
  mStackCR->Add(ZmumumCR);

  TH1F* ZmumumuPtCR = new TH1F("ZmumumuPtCR", "ZmumumuPtCR", 100, 0., 200.0);
  ZmumuTree.Draw("DV_muPt1>>ZmumumuPtCR", ControlRegion);
  ZmumuTree.Draw("DV_muPt2>>+ZmumumuPtCR", ControlRegion);
  //ZmumumuPtCR->Sumw2();
  ZmumumuPtCR->Scale(SFZmumu);
  ZmumumuPtCR->SetFillColor(kRed);
  muPtStackCR->Add(ZmumumuPtCR);

  TH1F* ZmumumuD0CR = new TH1F("ZmumumuD0CR", "ZmumumuD0CR", 100, 0., 400.);
  ZmumuTree.Draw("DV_muD01>>ZmumumuD0CR", ControlRegion);
  ZmumuTree.Draw("DV_muD02>>+ZmumumuD0CR", ControlRegion);
  //ZmumumuD0CR->Sumw2();
  ZmumumuD0CR->Scale(SFZmumu);
  ZmumumuD0CR->SetFillColor(kRed);
  muD0StackCR->Add(ZmumumuD0CR);

  TH1F* ZmumumuZ0CR = new TH1F("ZmumumuZ0CR", "ZmumumuZ0CR", 100, 0., 400.);
  ZmumuTree.Draw("DV_muZ01>>ZmumumuZ0CR", ControlRegion);
  ZmumuTree.Draw("DV_muZ02>>+ZmumumuZ0CR", ControlRegion);
  //ZmumumuZ0CR->Sumw2();
  ZmumumuZ0CR->Scale(SFZmumu);
  ZmumumuZ0CR->SetFillColor(kRed);
  muZ0StackCR->Add(ZmumumuZ0CR);

  TH1F* ZmumumuEtaCR = new TH1F("ZmumumuEtaCR", "ZmumumuEtaCR", 50, -2.5, 2.5);
  ZmumuTree.Draw("DV_muEta1>>ZmumumuEtaCR", ControlRegion);
  ZmumuTree.Draw("DV_muEta2>>+ZmumumuEtaCR", ControlRegion);
  //ZmumumuEtaCR->Sumw2();
  ZmumumuEtaCR->Scale(SFZmumu);
  ZmumumuEtaCR->SetFillColor(kRed);
  muEtaStackCR->Add(ZmumumuEtaCR);

  TH1F* ZmumurSR = new TH1F("ZmumurSR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  ZmumuTree.Draw("DV_r>>ZmumurSR", SignalRegion);
  //ZmumurSR->Sumw2();
  ZmumurSR->Scale(SFZmumu);
  ZmumurSR->SetFillColor(kRed);
  rStackSR->Add(ZmumurSR);

  TH1F* ZmumuzSR = new TH1F("ZmumuzSR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  ZmumuTree.Draw("DV_z>>ZmumuzSR", SignalRegion);
  //ZmumuzSR->Sumw2();
  ZmumuzSR->Scale(SFZmumu);
  ZmumuzSR->SetFillColor(kRed);
  zStackSR->Add(ZmumuzSR);

  TH1F* ZmumulSR = new TH1F("ZmumulSR", "l DV [cm]", 100, 0.0, 600.0);
  ZmumuTree.Draw("DV_l>>ZmumulSR", SignalRegion);
  //ZmumulSR->Sumw2();
  ZmumulSR->Scale(SFZmumu);
  ZmumulSR->SetFillColor(kRed);
  lStackSR->Add(ZmumulSR);

  TH1F* ZmumumSR = new TH1F("ZmumumSR", "m [GeV]", 50, 0.0, 200.0);
  ZmumuTree.Draw("DV_m>>ZmumumSR", SignalRegion);
  //ZmumumSR->Sumw2();
  ZmumumSR->Scale(SFZmumu);
  ZmumumSR->SetFillColor(kRed);
  mStackSR->Add(ZmumumSR);

  TH1F* ZmumumuPtSR = new TH1F("ZmumumuPtSR", "ZmumumuPtSR", 100, 0., 200.0);
  ZmumuTree.Draw("DV_muPt1>>ZmumumuPtSR", SignalRegion);
  ZmumuTree.Draw("DV_muPt2>>+ZmumumuPtSR", SignalRegion);
  //ZmumumuPtSR->Sumw2();
  ZmumumuPtSR->Scale(SFZmumu);
  ZmumumuPtSR->SetFillColor(kRed);
  muPtStackSR->Add(ZmumumuPtSR);

  TH1F* ZmumumuD0SR = new TH1F("ZmumumuD0SR", "ZmumumuD0SR", 100, 0., 400.);
  ZmumuTree.Draw("DV_muD01>>ZmumumuD0SR", SignalRegion);
  ZmumuTree.Draw("DV_muD02>>+ZmumumuD0SR", SignalRegion);
  //ZmumumuD0SR->Sumw2();
  ZmumumuD0SR->Scale(SFZmumu);
  ZmumumuD0SR->SetFillColor(kRed);
  muD0StackSR->Add(ZmumumuD0SR);

  TH1F* ZmumumuZ0SR = new TH1F("ZmumumuZ0SR", "ZmumumuZ0SR", 100, 0., 400.);
  ZmumuTree.Draw("DV_muZ01>>ZmumumuZ0SR", SignalRegion);
  ZmumuTree.Draw("DV_muZ02>>+ZmumumuZ0SR", SignalRegion);
  //ZmumumuZ0SR->Sumw2();
  ZmumumuZ0SR->Scale(SFZmumu);
  ZmumumuZ0SR->SetFillColor(kRed);
  muZ0StackSR->Add(ZmumumuZ0SR);

  TH1F* ZmumumuEtaSR = new TH1F("ZmumumuEtaSR", "ZmumumuEtaSR", 50, -2.5, 2.5);
  ZmumuTree.Draw("DV_muEta1>>ZmumumuEtaSR", SignalRegion);
  ZmumuTree.Draw("DV_muEta2>>+ZmumumuEtaSR", SignalRegion);
  //ZmumumuEtaSR->Sumw2();
  ZmumumuEtaSR->Scale(SFZmumu);
  ZmumumuEtaSR->SetFillColor(kRed);
  muEtaStackSR->Add(ZmumumuEtaSR);

  cout<<"Zmumu histos done"<<endl;
  cout<<endl;

  // Data
  TH1F* DatarCR = new TH1F("DatarCR", "r_{vtx} DV [cm]", 100, -400.0, 400.0);
  DatarCR->SetMarkerStyle(20);
  DataTree.Draw("DV_r>>DatarCR", ControlRegion);
  DatarCR->Sumw2();

  TH1F* DatazCR = new TH1F("DatazCR", "z_{vtx} DV [cm]", 100, -600.0, 600.0);
  DataTree.Draw("DV_z>>DatazCR", ControlRegion);
  DatazCR->Sumw2();

  TH1F* DatalCR = new TH1F("DatalCR", "l DV [cm]", 100, 0.0, 600.0);
  DataTree.Draw("DV_l>>DatalCR", ControlRegion);
  DatalCR->Sumw2();

  TH1F* DatamCR = new TH1F("DatamCR", "m [GeV]", 100, 0.0, 200.0);
  DataTree.Draw("DV_m>>DatamCR", ControlRegion);
  DatamCR->Sumw2();

  TH1F* DatamuPtCR = new TH1F("DatamuPtCR", "DatamuPtCR", 100, 0., 200.0);
  DataTree.Draw("DV_muPt1>>DatamuPtCR", ControlRegion);
  DataTree.Draw("DV_muPt2>>+DatamuPtCR", ControlRegion);
  DatamuPtCR->Sumw2();

  TH1F* DatamuD0CR = new TH1F("DatamuD0CR", "DatamuD0CR", 100, 0., 400.);
  DataTree.Draw("DV_muD01>>DatamuD0CR", ControlRegion);
  DataTree.Draw("DV_muD02>>+DatamuD0CR", ControlRegion);
  DatamuD0CR->Sumw2();

  TH1F* DatamuZ0CR = new TH1F("DatamuZ0CR", "DatamuZ0CR", 100, 0., 400.);
  DataTree.Draw("DV_muZ01>>DatamuZ0CR", ControlRegion);
  DataTree.Draw("DV_muZ02>>+DatamuZ0CR", ControlRegion);
  DatamuZ0CR->Sumw2();

  TH1F* DatamuEtaCR = new TH1F("DatamuEtaCR", "DatamuEtaCR", 50, -2.5, 2.5);
  DataTree.Draw("DV_muEta1>>DatamuEtaCR", ControlRegion);
  DataTree.Draw("DV_muEta2>>+DatamuEtaCR", ControlRegion);
  DatamuEtaCR->Sumw2();

  TH1F* DiffrCR = (TH1F*)DatarCR->Clone("DiffrCR");
  DiffrCR->Sumw2();
  DiffrCR->Add((TH1F*)rStackCR->GetStack()->Last(),-1.0);
  DiffrCR->SetXTitle("r_{vtx} [cm]");
  DiffrCR->SetYTitle("data-MC");

  TH1F* DiffzCR = (TH1F*)DatazCR->Clone("DiffzCR");
  DiffzCR->Sumw2();
  DiffzCR->Add((TH1F*)zStackCR->GetStack()->Last(),-1.0);
  DiffzCR->SetXTitle("z_{vtx} [cm]");
  DiffzCR->SetYTitle("data-MC");

  TH1F* DifflCR = (TH1F*)DatalCR->Clone("DifflCR");
  DifflCR->Sumw2();
  DifflCR->Add((TH1F*)lStackCR->GetStack()->Last(),-1.0);
  DifflCR->SetXTitle("L_{vtx} [cm]");
  DifflCR->SetYTitle("data-MC");

  TH1F* DiffmCR = (TH1F*)DatamCR->Clone("DiffmCR");
  DiffmCR->Sumw2();
  DiffmCR->Add((TH1F*)mStackCR->GetStack()->Last(),-1.0);
  DiffmCR->SetXTitle("m_{vtx} [GeV]");
  DiffmCR->SetYTitle("data-MC");

  TH1F* DiffmuPtCR = (TH1F*)DatamuPtCR->Clone("DiffmuPtCR");
  DiffmuPtCR->Sumw2();
  DiffmuPtCR->Add((TH1F*)muPtStackCR->GetStack()->Last(),-1.0);
  DiffmuPtCR->SetXTitle("#mu p_{T} [GeV]");
  DiffmuPtCR->SetYTitle("data-MC");

  TH1F* DiffmuEtaCR = (TH1F*)DatamuEtaCR->Clone("DiffmuEtaCR");
  DiffmuEtaCR->Sumw2();
  DiffmuEtaCR->Add((TH1F*)muEtaStackCR->GetStack()->Last(),-1.0);
  DiffmuEtaCR->SetXTitle("#mu #eta");
  DiffmuEtaCR->SetYTitle("data-MC");

  cout<<"Finished data plots"<<endl;
  cout<<endl;

  double NCRZ = ZmumumCR->Integral();
  double NCRttbar = ttbarmCR->Integral();
  double NCRW = WallmCR->Integral();
  double NCRZZ = ZZmCR->Integral();

  double NSRZ = ZmumumSR->Integral();
  double NSRttbar = ttbarmSR->Integral();
  double NSRW = WallmSR->Integral();
  double NSRZZ = ZZmSR->Integral();

  double NstatCRZ = ZmumumCR->GetEntries();
  double NstatCRttbar = ttbarmCR->GetEntries();
  double NstatCRW = WallmCR->GetEntries();
  double NstatCRZZ = ZZmCR->GetEntries();
  double NstatCRData = DatamCR->GetEntries();

  double NstatSRZ = ZmumumSR->GetEntries();
  double NstatSRttbar = ttbarmSR->GetEntries();
  double NstatSRW = WallmSR->GetEntries();
  double NstatSRZZ = ZZmSR->GetEntries();

  cout<<"CR Nstat_Zjets = "<<NstatCRZ<<endl;
  cout<<"CR Nstat_ttbar = "<<NstatCRttbar<<endl;
  cout<<"CR Nstat_Wjets = "<<NstatCRW<<endl;
  cout<<"CR Nstat_ZZ = "<<NstatCRZZ<<endl;
  cout<<"CR Nstat_Data = "<<NstatCRData<<endl;
  cout<<endl;
  cout<<"SR Nstat_Zjets = "<<NstatSRZ<<endl;
  cout<<"SR Nstat_ttbar = "<<NstatSRttbar<<endl;
  cout<<"SR Nstat_Wjets = "<<NstatSRW<<endl;
  cout<<"SR Nstat_ZZ = "<<NstatSRZZ<<endl;
  cout<<endl;

  cout<<"CR N_Zjets = "<<NCRZ<<" +/- "<<sqrt(NstatCRZ)*SFZmumu<<endl;
  cout<<"CR N_ttbar = "<<NCRttbar<<" +/- "<<sqrt(NstatCRttbar)*SFttbar<<endl;
  cout<<"CR N_Wjets = "<<NCRW<<" +/- "<<sqrt(NstatCRW)*SFWall<<endl;
  cout<<"CR N_ZZ = "<<NCRZZ<<" +/- "<<sqrt(NstatCRZZ)*SFZZ<<endl;
  cout<<endl;
  cout<<"SR N_Zjets = "<<NSRZ<<" +/- "<<sqrt(NstatSRZ)*SFZmumu<<endl;
  cout<<"SR N_ttbar = "<<NSRttbar<<" +/- "<<sqrt(NstatSRttbar)*SFttbar<<endl;
  cout<<"SR N_Wjets = "<<NSRW<<" +/- "<<sqrt(NstatSRW)*SFWall<<endl;
  cout<<"SR N_ZZ = "<<NSRZZ<<" +/- "<<sqrt(NstatSRZZ)*SFZZ<<endl;

  TCanvas* c1 = new TCanvas("c1", "c1", 1200, 800);

  c1->SetLogy(1);
  DatarCR->SetXTitle("r_{vtx} [cm]");
  DatarCR->SetYTitle("Number DV");
  DatarCR->Draw("EP");
  rStackCR->Draw("same");
  DatarCR->Draw("same");
  c1->SaveAs("rStackCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DatazCR->SetXTitle("z_{vtx} [cm]");
  DatazCR->SetYTitle("Number DV");
  DatazCR->Draw("EP");
  zStackCR->Draw("same");
  DatazCR->Draw("same");
  c1->SaveAs("zStackCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DatalCR->SetXTitle("L_{vtx} [cm]");
  DatalCR->SetYTitle("Number DV");
  DatalCR->Draw("EP");
  lStackCR->Draw("same");
  DatalCR->Draw("same");
  c1->SaveAs("lStackCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DiffrCR->Draw("EP");
  c1->SaveAs("DiffrCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DiffzCR->Draw("EP");
  c1->SaveAs("DiffzCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DifflCR->Draw("EP");
  c1->SaveAs("DifflCR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  DiffmCR->Draw("EP");
  c1->SaveAs("DiffmCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DiffmuPtCR->Draw("EP");
  c1->SaveAs("DiffmuPtCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DiffmuEtaCR->Draw("EP");
  c1->SaveAs("DiffmuEtaCR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  DatamCR->SetXTitle("m_{vtx} [GeV]");
  DatamCR->SetYTitle("Number DV");
  DatamCR->Draw("EP");
  mStackCR->Draw("same");
  DatamCR->Draw("same");
  c1->SaveAs("mStackCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DatamuPtCR->SetXTitle("#mu p_{T} [GeV]");
  DatamuPtCR->SetYTitle("Number DV");
  DatamuPtCR->Draw("EP");
  muPtStackCR->Draw("same");
  DatamuPtCR->Draw("same");
  c1->SaveAs("muPtStackCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DatamuD0CR->SetXTitle("#mu d_{0} [cm]");
  DatamuD0CR->SetYTitle("Number DV");
  DatamuD0CR->Draw("EP");
  muD0StackCR->Draw("same");
  DatamuD0CR->Draw("same");
  c1->SaveAs("muD0StackCR.pdf");
  c1->Clear();

  c1->SetLogy(1);
  DatamuZ0CR->SetXTitle("#mu z_{0} [cm]");
  DatamuZ0CR->SetYTitle("Number DV");
  DatamuZ0CR->Draw("EP");
  muZ0StackCR->Draw("same");
  DatamuZ0CR->Draw("same");
  c1->SaveAs("muZ0StackCR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  DatamuEtaCR->SetXTitle("#mu #eta");
  DatamuEtaCR->SetYTitle("Number DV");
  DatamuEtaCR->Draw("EP");
  muEtaStackCR->Draw("same");
  DatamuEtaCR->Draw("same");
  c1->SaveAs("muEtaStackCR.pdf");
  c1->Clear();

  // SR
  c1->SetLogy(0);
  rStackSR->Draw();
  c1->SaveAs("rStackSR.pdf");
  c1->Clear();

  c1->SetLogy(0);  
  zStackSR->Draw();
  c1->SaveAs("zStackSR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  lStackSR->Draw();
  c1->SaveAs("lStackSR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  mStackSR->Draw();
  c1->SaveAs("mStackSR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  muPtStackSR->Draw();
  c1->SaveAs("muPtStackSR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  muD0StackSR->Draw();
  c1->SaveAs("muD0StackSR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  muZ0StackSR->Draw();
  c1->SaveAs("muZ0StackSR.pdf");
  c1->Clear();

  c1->SetLogy(0);
  muEtaStackSR->Draw();
  c1->SaveAs("muEtaStackSR.pdf");
  c1->Clear();

  // Save all the histograms to an output Root file
  outputHistos->Write();
  outputHistos->Close();


}

