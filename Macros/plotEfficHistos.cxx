#include "TLatex.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2Poly.h"
#include "TCanvas.h"
#include "TString.h"

void plotEfficHistos(TString inputPath, TString fileName, TString sampleType) {

  //TFile DVHists("VLQ/hist-AOD.root");
  TFile DVHists(inputPath+fileName);

  TCanvas* c1 = new TCanvas("c1", "c1", 1200, 800);
  c1->SetRightMargin(0.15);

  TH1F* h_rPostVT;
  h_rPostVT = (TH1F*)DVHists.Get("h_rPostVT");
  TH1F* h_zPostVT;
  h_zPostVT = (TH1F*)DVHists.Get("h_zPostVT");
  TH1F* h_lPostVT;
  h_lPostVT = (TH1F*)DVHists.Get("h_lPostVT");
  TH1F* h_rT;
  h_rT = (TH1F*)DVHists.Get("h_rT");
  TH1F* h_zT;
  h_zT = (TH1F*)DVHists.Get("h_zT");
  TH1F* h_openAngT;
  h_openAngT = (TH1F*)DVHists.Get("h_openAngT");
  TH1F* h_openAngPostVT;
  h_openAngPostVT = (TH1F*)DVHists.Get("h_openAngVMatchedT");
  TH1F* h_mud0T;
  h_mud0T = (TH1F*)DVHists.Get("h_mud0T");
  TH1F* h_mud0C;
  h_mud0C = (TH1F*)DVHists.Get("h_mud0C");
  TH1F* h_mud0ZoomT;
  h_mud0ZoomT = (TH1F*)DVHists.Get("h_mud0ZoomT");
  TH1F* h_mud0RecoC;
  h_mud0RecoC = (TH1F*)DVHists.Get("h_mud0RecoC");
  TH1F* h_mud0SA;
  h_mud0SA = (TH1F*)DVHists.Get("h_mud0SA");
  TH1F* h_muPtT;
  h_muPtT = (TH1F*)DVHists.Get("h_muPtT");
  TH1F* h_muPtSA;
  h_muPtSA = (TH1F*)DVHists.Get("h_muPtSA");
  TH1F* h_mud0SAall;
  h_mud0SAall = (TH1F*)DVHists.Get("h_mud0SAall");
  TH1F* h_mud0ZoomSAall;
  h_mud0ZoomSAall = (TH1F*)DVHists.Get("h_mud0ZoomSAall");
  TH1F* h_mud0ZoomSA;
  h_mud0ZoomSA = (TH1F*)DVHists.Get("h_mud0ZoomSA");
  TH1F* h_muPtSAall;
  h_muPtSAall = (TH1F*)DVHists.Get("h_muPtSAall");
  TH1F* h_METpreTrig;
  h_METpreTrig = (TH1F*)DVHists.Get("h_METpreTrig");
  TH1F* h_METpostTrig;
  h_METpostTrig = (TH1F*)DVHists.Get("h_METpostTrig");

  TH1F* h_dR_Matched;
  h_dR_Matched = (TH1F*)DVHists.Get("h_dR_Matched");
  h_dR_Matched->SetYTitle("Events");
  h_dR_Matched->SetXTitle("#DeltaR(Comb,MS)");
  h_dR_Matched->SetStats(1);

  TH1F* h_effNumeratorShort;
  h_effNumeratorShort = (TH1F*)DVHists.Get("h_effNumeratorShort");
  //TH1F* h_effNumeratorMedium;
  //h_effNumeratorMedium = (TH1F*)DVHists.Get("h_effNumeratorMedium");
  TH1F* h_effNumeratorLong;
  h_effNumeratorLong = (TH1F*)DVHists.Get("h_effNumeratorLong");

  TH1F* h_effDenominatorShort;
  h_effDenominatorShort = (TH1F*)DVHists.Get("h_effDenominatorShort");
  //TH1F* h_effDenominatorMedium;
  //h_effDenominatorMedium = (TH1F*)DVHists.Get("h_effDenominatorMedium");
  TH1F* h_effDenominatorLong;
  h_effDenominatorLong = (TH1F*)DVHists.Get("h_effDenominatorLong");


  TH1F* h_muD0ResidMSOnly;
  h_muD0ResidMSOnly = (TH1F*)DVHists.Get("h_muD0ResidMSOnly");

  h_muD0ResidMSOnly->SetXTitle("MSOnly d_{0}^{}-d_{0}^{true} [cm]");
  h_muD0ResidMSOnly->SetYTitle("Events");

  TH2Poly* h_rVSzT;
  h_rVSzT = (TH2Poly*)DVHists.Get("h_rVSzT");
  TH2Poly* h_rVSzPostT;
  h_rVSzPostT = (TH2Poly*)DVHists.Get("h_rVSzPostT");

  h_rPostVT->Sumw2();
  h_zPostVT->Sumw2();
  h_lPostVT->Sumw2();
  h_rT->Sumw2();
  h_zT->Sumw2();
  h_openAngT->Sumw2();
  h_openAngPostVT->Sumw2();
  h_mud0T->Sumw2();
  h_mud0C->Sumw2();
  h_mud0ZoomT->Sumw2();
  h_mud0RecoC->Sumw2();
  h_mud0SA->Sumw2();
  h_muPtT->Sumw2();
  h_muPtSA->Sumw2();
  h_mud0SAall->Sumw2();
  h_mud0ZoomSAall->Sumw2();
  h_mud0ZoomSA->Sumw2();
  h_muPtSAall->Sumw2();
  h_METpreTrig->Sumw2();
  h_METpostTrig->Sumw2();

  TH1F* effMETDV = new TH1F(*h_METpreTrig);
  effMETDV->Divide(h_METpostTrig,h_METpreTrig,1.0,1.0,"B");
  effMETDV->SetXTitle("E_{T}^{miss} [GeV]");
  effMETDV->SetYTitle("MET Trigger Efficiency");

  TH1F* effrTrueDV = new TH1F(*h_rT);
  TH1F* effzTrueDV = new TH1F(*h_zT);
  effrTrueDV->Divide(h_rPostVT,h_rT,1.0,1.0,"B");
  effzTrueDV->Divide(h_zPostVT,h_zT,1.0,1.0,"B");
  effrTrueDV->SetXTitle("True r DV [cm]");
  effrTrueDV->SetYTitle("DV efficiency");

  effzTrueDV->SetXTitle("True z DV [cm]");
  effzTrueDV->SetYTitle("DV efficiency");
  
  TH1F* effd0TrueDV = new TH1F(*h_mud0T);
  effd0TrueDV->Divide(h_mud0SA,h_mud0T,1.0,1.0,"B");
  effd0TrueDV->SetXTitle("True d_{0} [cm]");
  effd0TrueDV->SetYTitle("MSOnly efficiency");

  TH1F* effPtTrueDV = new TH1F(*h_muPtT);
  effPtTrueDV->Divide(h_muPtSA,h_muPtT,1.0,1.0,"B");
  effPtTrueDV->SetXTitle("True p_{T} [GeV]");
  effPtTrueDV->SetYTitle("MSOnly efficiency");

  TH1F* effOpenAngDV = new TH1F(*h_openAngT);
  effOpenAngDV->Divide(h_openAngPostVT,h_openAngT,1.0,1.0,"B");
  effOpenAngDV->SetXTitle("True #theta_{#mu#mu}");
  effOpenAngDV->SetYTitle("DV efficiency");

  TH1F* effd0TrueAllDV = new TH1F(*h_mud0T);
  effd0TrueAllDV->Divide(h_mud0SAall,h_mud0T,1.0,1.0,"B");
  effd0TrueAllDV->SetXTitle("True d_{0} [cm]");
  effd0TrueAllDV->SetYTitle("MuSA efficiency");

  TH1F* effPtTrueAllDV = new TH1F(*h_muPtT);
  effPtTrueAllDV->Divide(h_muPtSAall,h_muPtT,1.0,1.0,"B");
  effPtTrueAllDV->SetXTitle("True p_{T} [GeV]");
  effPtTrueAllDV->SetYTitle("MuSA efficiency");

  TH1F* effd0CBTrueDV = new TH1F(*h_mud0ZoomT);
  effd0CBTrueDV->Divide(h_mud0C,h_mud0ZoomT,1.0,1.0,"B");
  effd0CBTrueDV->SetXTitle("True d_{0} [cm]");
  effd0CBTrueDV->SetYTitle("CombMu efficiency");

  TH1F* effd0ZoomSATrueDV = new TH1F(*h_mud0ZoomT);
  effd0ZoomSATrueDV->Divide(h_mud0ZoomSAall,h_mud0ZoomT,1.0,1.0,"B");
  effd0ZoomSATrueDV->SetXTitle("True d_{0} [cm]");
  effd0ZoomSATrueDV->SetYTitle("MuSA efficiency");

  TH1F* effd0ZoomMSOnlyTrueDV = new TH1F(*h_mud0ZoomT);
  effd0ZoomMSOnlyTrueDV->Divide(h_mud0ZoomSA,h_mud0ZoomT,1.0,1.0,"B");
  effd0ZoomMSOnlyTrueDV->SetXTitle("True d_{0} [cm]");
  effd0ZoomMSOnlyTrueDV->SetYTitle("MSOnly efficiency");

  TH1F* effLifetimesShort = new TH1F(*h_effNumeratorShort);
  effLifetimesShort->Divide(h_effNumeratorShort,h_effDenominatorShort,1.0,1.0);
  effLifetimesShort->SetXTitle("Proper Lifetime c#tau [cm]");
  effLifetimesShort->SetYTitle("Total Efficiency");

  /*TH1F* effLifetimesMedium = new TH1F(*h_effNumeratorMedium);
  effLifetimesMedium->Divide(h_effNumeratorMedium,h_effDenominatorMedium,1.0,1.0);
  effLifetimesMedium->SetXTitle("Proper Lifetime c#tau [cm]");
  effLifetimesMedium->SetYTitle("Total Efficiency");*/

  TH1F* effLifetimesLong = new TH1F(*h_effNumeratorLong);
  effLifetimesLong->Divide(h_effNumeratorLong,h_effDenominatorLong,1.0,1.0);
  effLifetimesLong->SetXTitle("Proper Lifetime c#tau [cm]");
  effLifetimesLong->SetYTitle("Total Efficiency");

  TH2Poly* effrVSzDV = (TH2Poly*)h_rVSzT->Clone("effrVSzDV");
  effrVSzDV->ClearBinContents();
  effrVSzDV->Divide(h_rVSzPostT,h_rVSzT);
  effrVSzDV->SetXTitle("true z_{DV} [cm]");
  effrVSzDV->SetYTitle("true r_{DV} [cm]");

  c1->Clear();
  effrTrueDV->SetMarkerStyle(20);
  effrTrueDV->SetMinimum(0.0);
  effrTrueDV->SetMaximum(1.0);
  effrTrueDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVrEff-"+sampleType+".pdf");

  c1->Clear();
  effzTrueDV->SetMarkerStyle(20);
  effzTrueDV->SetMinimum(0.0);
  effzTrueDV->SetMaximum(1.0);
  effzTrueDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVzEff-"+sampleType+".pdf");

  c1->Clear();
  effMETDV->SetMinimum(0.0);
  effMETDV->SetMaximum(1.0);
  effMETDV->SetMarkerStyle(20);
  effMETDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVMETEff-"+sampleType+".pdf");

  c1->Clear();
  effd0TrueDV->SetMarkerStyle(20);
  effd0TrueDV->SetMinimum(0.0);
  effd0TrueDV->SetMaximum(1.0);
  //effd0TrueAllDV->SetMarkerColor(2);
  effd0TrueDV->Draw("EP");
  //effd0TrueAllDV->Draw("EPS");
  c1->SaveAs(inputPath+"/DVd0Eff-"+sampleType+".pdf");

  c1->Clear();
  effd0CBTrueDV->SetMarkerStyle(20);
  effd0CBTrueDV->SetMinimum(0.0);
  effd0CBTrueDV->SetMaximum(1.0);
  effd0CBTrueDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVd0CBEff-"+sampleType+".pdf");

  c1->Clear();
  effd0TrueAllDV->SetMinimum(0.0);
  effd0TrueAllDV->SetMaximum(1.0);
  effd0TrueAllDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVd0EffNoMatch-"+sampleType+".pdf");

  c1->Clear();
  effd0ZoomSATrueDV->SetMarkerStyle(20);
  effd0ZoomSATrueDV->SetMinimum(0.0);
  effd0ZoomSATrueDV->SetMaximum(1.0);
  effd0ZoomSATrueDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVd0ZoomSAEff-"+sampleType+".pdf");

  c1->Clear();
  effd0ZoomMSOnlyTrueDV->SetMarkerStyle(20);
  effd0ZoomMSOnlyTrueDV->SetMinimum(0.0);
  effd0ZoomMSOnlyTrueDV->SetMaximum(1.0);
  effd0ZoomMSOnlyTrueDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVd0ZoomMSOnlyEff-"+sampleType+".pdf");

  c1->Clear();
  effPtTrueDV->SetMarkerStyle(20);
  effPtTrueAllDV->SetMarkerStyle(20);
  effPtTrueAllDV->SetMinimum(0.0);
  effPtTrueAllDV->SetMaximum(1.0);
  //effPtTrueAllDV->SetMarkerColor(2);
  effPtTrueDV->Draw("EP");
  //effPtTrueAllDV->Draw("EPS");
  c1->SaveAs(inputPath+"/DVPtEff-"+sampleType+".pdf");

  c1->Clear();
  effOpenAngDV->SetMarkerStyle(20);
  effOpenAngDV->SetMinimum(0.0);
  effOpenAngDV->SetMaximum(1.0);
  effOpenAngDV->Draw("EP");
  c1->SaveAs(inputPath+"/DVOpenAngEff-"+sampleType+".pdf");

  // Do some simple Gaussian fits to the residuals
  TF1* d0fitFunc = new TF1("d0fitFunc", "gaus", -6.0, 6.0);

  gStyle->SetOptFit(11);
  d0fitFunc->SetLineColor(2);
  d0fitFunc->SetParNames("A","Mean","sigma");

  h_muD0ResidMSOnly->Sumw2();
  h_muD0ResidMSOnly->Fit("d0fitFunc","R");

  c1->Clear();
  h_muD0ResidMSOnly->SetStats(1);
  h_muD0ResidMSOnly->SetMarkerStyle(20);
  h_muD0ResidMSOnly->Draw("EP");
  c1->SaveAs(inputPath+"/DVd0Resid-"+sampleType+".pdf");

  c1->Clear();
  effLifetimesShort->SetMinimum(0.0);
  effLifetimesShort->SetMaximum(1.0);
  effLifetimesShort->SetMarkerColor(2);
  effLifetimesShort->Draw("hist p");
  c1->SaveAs(inputPath+"/DVeffLifetimesShort-"+sampleType+".pdf");

  /*c1->Clear();
  effLifetimesMedium->SetMinimum(0.0);
  effLifetimesMedium->SetMaximum(1.0);
  effLifetimesMedium->Draw("hist p");
  c1->SaveAs(inputPath+"/DVeffLifetimesMedium-"+sampleType+".pdf");*/

  c1->Clear();
  effLifetimesLong->SetMinimum(0.001);
  effLifetimesLong->SetMaximum(1.0);
  c1->SetLogy();
  effLifetimesLong->SetMarkerColor(2);
  effLifetimesLong->Draw("hist p");
  c1->SaveAs(inputPath+"/DVeffLifetimesLong-"+sampleType+".pdf");

  c1->Clear();
  gStyle->SetNumberContours(80);
  gStyle->SetPalette(57);
  effrVSzDV->Draw("COLZ");
  c1->SaveAs(inputPath+"/DVeffDVrVSzPoly-"+sampleType+".pdf");

  c1->Clear();
  c1->SetLogy();
  gStyle->SetOptStat("ourme");
  h_dR_Matched->Draw();
  c1->SaveAs(inputPath+"/DVdeltaRAllMS-"+sampleType+".pdf");

}
