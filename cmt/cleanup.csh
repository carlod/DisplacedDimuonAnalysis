# echo "cleanup cmt_standalone v0 in /home/nrbern/testarea/AnalysisCode/DisplacedDimuonAnalysis/cmt"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtcmt_standalonetempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtcmt_standalonetempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=cmt_standalone -version=v0 -path=/home/nrbern/testarea/AnalysisCode/DisplacedDimuonAnalysis/cmt  -quiet -without_version_directory $* >${cmtcmt_standalonetempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=cmt_standalone -version=v0 -path=/home/nrbern/testarea/AnalysisCode/DisplacedDimuonAnalysis/cmt  -quiet -without_version_directory $* >${cmtcmt_standalonetempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtcmt_standalonetempfile}
  unset cmtcmt_standalonetempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtcmt_standalonetempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtcmt_standalonetempfile}
unset cmtcmt_standalonetempfile
exit $cmtcleanupstatus

