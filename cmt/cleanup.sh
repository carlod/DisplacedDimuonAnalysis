# echo "cleanup cmt_standalone v0 in /home/nrbern/testarea/AnalysisCode/DisplacedDimuonAnalysis/cmt"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtcmt_standalonetempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtcmt_standalonetempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=cmt_standalone -version=v0 -path=/home/nrbern/testarea/AnalysisCode/DisplacedDimuonAnalysis/cmt  -quiet -without_version_directory $* >${cmtcmt_standalonetempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=cmt_standalone -version=v0 -path=/home/nrbern/testarea/AnalysisCode/DisplacedDimuonAnalysis/cmt  -quiet -without_version_directory $* >${cmtcmt_standalonetempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtcmt_standalonetempfile}
  unset cmtcmt_standalonetempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtcmt_standalonetempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtcmt_standalonetempfile}
unset cmtcmt_standalonetempfile
return $cmtcleanupstatus

